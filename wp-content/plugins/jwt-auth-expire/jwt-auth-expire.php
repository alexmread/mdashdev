<?php

/**
 * Plugin Name: JWT Auth Expire
 * Plugin URI: http://oniria.com.br
 * Description: Simple plugin to change the jwt_auth_expire of Jwt Auth plugin
 * Version: 0.1
 * Author: Rodrigo Souza
 * Author URI: http://oniria.com.br
 */

if (!defined('WPINC')) {    die;	}
if ( ! defined( 'ABSPATH' ) ) die( 'restricted access' );

class JWTAuthExpire {
			
	public function __construct(){
	
		add_filter('jwt_auth_expire', array($this,'on_jwt_expire_token'),10,1);
	}

	function on_jwt_expire_token($exp){		
		$days = 182;
		$exp = time() + (86400 * $days);			
		return $exp;
	}
}

//======================================================
new JWTAuthExpire();

