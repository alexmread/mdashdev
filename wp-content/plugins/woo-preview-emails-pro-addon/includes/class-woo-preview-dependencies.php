<?php
/**
 * WC Dependency Checker
 *
 * Checks if WooCommerce is enabled
 */
class WOO_Preview_Load_Dependencies {

	private static $active_plugins;

	public static $woo_preview_item_id = '748';
	public static $woo_preview_store_url = 'https://www.codemanas.com';
	public static $woo_preview_option_page = 'admin.php?page=digthis-woocommerce-preview-emails&tab=license';

	public static function init() {

		self::$active_plugins = (array) get_option( 'active_plugins', array() );

		if ( is_multisite() )
			self::$active_plugins = array_merge( self::$active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );
	}

	public static function woocommerce_active_check() {

		if ( ! self::$active_plugins ) self::init();

		return in_array( 'woocommerce/woocommerce.php', self::$active_plugins ) || array_key_exists( 'woocommerce/woocommerce.php', self::$active_plugins );
	}

	public static function woocommerce_booking_active_check() {

		if ( ! self::$active_plugins ) self::init();

		return in_array( 'woocommerce-bookings/woocommerce-bookings.php', self::$active_plugins ) || array_key_exists( 'woocommerce-bookings/woocommerce-bookings.php', self::$active_plugins );
	}

	public static function woocommerce_subscriptions_active_check() {

		if ( ! self::$active_plugins ) self::init();

		return in_array( 'woocommerce-subscriptions/woocommerce-subscriptions.php', self::$active_plugins ) || array_key_exists( 'woocommerce-subscriptions/woocommerce-subscriptions.php', self::$active_plugins );
	}

	public static function woocommerce_membership_active_check() {

		if ( ! self::$active_plugins ) self::init();

		return in_array( 'woocommerce-memberships/woocommerce-memberships.php', self::$active_plugins ) || array_key_exists( 'woocommerce-memberships/woocommerce-memberships.php', self::$active_plugins );
	}

}