<?php
/**
 * @author Deepen.
 * @created_on 10/22/19
 *
 * Admin controller class
 */

if ( ! class_exists( 'WooPreviewEmailsProAdmin' ) ):

	class WooPreviewEmailsProAdmin {

		public function __construct() {
			add_filter( 'woo_preview_emails_unset_booking_emails', array( $this, 'show_booking_emails' ) );
			add_filter( 'woo_preview_emails_unset_subscription_emails', array( $this, 'show_subscription_emails' ) );
			add_filter( 'woo_preview_emails_unset_memebership_emails', array( $this, 'show_membership_emails' ) );

			//Implement Tabs
			add_action( 'woo_preview_emails_before_form', array( $this, 'render_tabs' ) );
			add_filter( 'woo_preview_emails_tabs', array( $this, 'tab_logic' ) );

			//Licensing
			add_action( 'admin_init', array( $this, 'activate_deactivation' ) );
			add_action( 'admin_notices', array( $this, 'notices' ) );
		}

		/**
		 * Show booking emails in the admin settings
		 *
		 * @param $emails
		 *
		 * @since 1.0.0
		 * @return bool
		 */
		function show_booking_emails( $emails ) {
			$bookings_active = WOO_Preview_Load_Dependencies::woocommerce_booking_active_check();
			if ( $bookings_active ) {
				$emails = false;
			}

			return $emails;
		}

		/**
		 * Show subscription emails in the admin settings
		 *
		 * @param $emails
		 *
		 * @since 1.0.0
		 * @return bool
		 */
		function show_subscription_emails( $emails ) {
			$subscriptions_active = WOO_Preview_Load_Dependencies::woocommerce_subscriptions_active_check();
			if ( $subscriptions_active ) {
				$emails = false;
			}

			return $emails;
		}

		/**
		 * Show membership emails in the admin settings
		 *
		 * @param $emails
		 *
		 * @since 1.0.0
		 * @return bool
		 */
		function show_membership_emails( $emails ) {
			$membership_active = WOO_Preview_Load_Dependencies::woocommerce_membership_active_check();
			if ( $membership_active ) {
				$emails = false;
			}

			return $emails;
		}

		/**
		 * Render the tabs
		 *
		 * @since 1.0.0
		 */
		function render_tabs() {
			$tab        = filter_input( INPUT_GET, 'tab', FILTER_SANITIZE_STRING );
			$active_tab = isset( $tab ) ? $tab : 'basic';
			?>
            <h2 class="nav-tab-wrapper">
                <a href="?page=digthis-woocommerce-preview-emails&tab=basic" class="nav-tab <?php echo ( 'basic' === $active_tab ) ? esc_attr( 'nav-tab-active' ) : ''; ?>">
					<?php esc_html_e( 'General Settings', 'woo-preview-emails' ); ?>
                </a>
                <a href="?page=digthis-woocommerce-preview-emails&tab=license" class="nav-tab <?php echo ( 'license' === $active_tab ) ? esc_attr( 'nav-tab-active' ) : ''; ?>">
					<?php esc_html_e( 'Licensing', 'woo-preview-emails' ); ?>
                </a>
            </h2>
			<?php
		}

		/**
		 * Process Tab Logic here
		 *
		 * @param $result
		 *
		 * @since 1.0.0
		 *
		 * @return bool
		 */
		function tab_logic( $result ) {
			$tab        = filter_input( INPUT_GET, 'tab', FILTER_SANITIZE_STRING );
			$active_tab = isset( $tab ) ? $tab : false;
			if ( $active_tab === "license" ) {
				$result = true;

				$license = get_option( '_woo_preview_emails_license_key' );
				$status  = get_option( '_woo_preview_emails_license_status' );

				require_once WOO_PREVIEW_EMAILS_PRO_DIR . '/views/license.php';
			}

			return $result;
		}

		function activate_deactivation() {
			// listen for our activate button to be clicked
			if ( isset( $_POST['license_activate'] ) ) {
				$this->activate_license();
			}

			// listen for our deactivate button to be clicked
			if ( isset( $_POST['deactive_the_license'] ) ) {
				$this->deactivate_license();
			}

			// listen for our deactivate button to be clicked
			if ( isset( $_GET['reset_keys'] ) ) {
				$this->reset_keys();
			}
		}

		/**
		 * Reset keys
		 */
		function reset_keys() {
			global $wp_version;

			$license    = trim( get_option( '_woo_preview_emails_license_key' ) );
			$api_params = array(
				'edd_action' => 'check_license',
				'license'    => $license,
				'item_name'  => urlencode( WOO_PREVIEW_EMAILS_PRO_ITEM_NAME ),
				'url'        => home_url()
			);

			// Call the custom API.
			$response = wp_remote_post( WOO_Preview_Load_Dependencies::$woo_preview_store_url, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );
			if ( is_wp_error( $response ) ) {
				return false;
			}

			$license_data = json_decode( wp_remote_retrieve_body( $response ) );
			if ( $license_data->license == 'valid' ) {
				update_option( '_woo_preview_emails_license_status', 'valid' );
			} else {
				update_option( '_woo_preview_emails_license_status', 'invalid' );
			}
		}

		/**
		 * Activate license implementation
		 *
		 * @since 1.0.0
		 */
		function activate_license() {
			// listen for our activate button to be clicked
			if ( isset( $_POST['license_activate'] ) ) {
				// run a quick security check
				if ( ! check_admin_referer( 'woo_preview_emails_licensing_nonce', 'woo_preview_emails_licensing_nonce' ) ) {
					return;
				}

				//Update License Key First
				$license_field = sanitize_text_field( filter_input( INPUT_POST, 'license_key' ) );
				update_option( '_woo_preview_emails_license_key', $license_field );

				// retrieve the license from the database
				$license = trim( get_option( '_woo_preview_emails_license_key' ) );

				// data to send in our API request
				$api_params = array(
					'edd_action' => 'activate_license',
					'license'    => $license,
					'item_id'    => WOO_Preview_Load_Dependencies::$woo_preview_item_id,
					'url'        => home_url(),
				);

				// Call the custom API.
				$response = wp_remote_post( WOO_Preview_Load_Dependencies::$woo_preview_store_url, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

				// make sure the response came back okay
				if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {
					if ( is_wp_error( $response ) ) {
						$message = $response->get_error_message();
					} else {
						$message = __( 'An error occurred, please try again.', 'woo-preview-emails-pro' );
					}
				} else {
					$license_data = json_decode( wp_remote_retrieve_body( $response ) );
					if ( false === $license_data->success ) {
						switch ( $license_data->error ) {
							case 'expired' :
								$message = sprintf( __( 'Your license key expired on %s.', 'woo-preview-emails-pro' ), date_i18n( get_option( 'date_format' ), strtotime( $license_data->expires, current_time( 'timestamp' ) ) ) );
								break;
							case 'disabled' :
							case 'revoked' :
								$message = __( 'Your license key has been disabled.', 'woo-preview-emails-pro' );
								break;
							case 'missing' :
								$message = __( 'Invalid license.', 'woo-preview-emails-pro' );
								break;
							case 'invalid' :
							case 'site_inactive' :
								$message = __( 'Your license is not active for this URL.', 'woo-preview-emails-pro' );
								break;
							case 'item_name_mismatch' :
								$message = sprintf( __( 'This appears to be an invalid license key for %s.', 'woo-preview-emails-pro' ), 'woo-preview-emails-pro' );
								break;
							case 'no_activations_left':
								$message = __( 'Your license key has reached its activation limit.', 'woo-preview-emails-pro' );
								break;
							default :
								$message = __( 'An error occurred, please try again.', 'woo-preview-emails-pro' );
								break;
						}
					}
				}

				// Check if anything passed on a message constituting a failure
				if ( ! empty( $message ) ) {
					$base_url = admin_url( WOO_Preview_Load_Dependencies::$woo_preview_option_page );
					$redirect = add_query_arg( array( 'sl_activation' => 'false', 'message' => urlencode( $message ) ), $base_url );
					wp_redirect( $redirect );
					exit();
				}

				// $license_data->license will be either "valid" or "invalid"
				update_option( '_woo_preview_emails_license_status', $license_data->license );
				wp_redirect( admin_url( WOO_Preview_Load_Dependencies::$woo_preview_option_page ) );
				exit();
			}
		}

		/**
		 * Deactivate license implementaiton
		 *
		 * @since 1.0.0
		 */
		function deactivate_license() {
			if ( isset( $_POST['deactive_the_license'] ) ) {

				// run a quick security check
				if ( ! check_admin_referer( 'woo_preview_emails_licensing_nonce', 'woo_preview_emails_licensing_nonce' ) ) {
					return;
				}

				// retrieve the license from the database
				$license = trim( get_option( '_woo_preview_emails_license_key' ) );

				// data to send in our API request
				$api_params = array(
					'edd_action' => 'deactivate_license',
					'license'    => $license,
					'item_id'    => WOO_Preview_Load_Dependencies::$woo_preview_item_id,
					'item_name'  => WOO_PREVIEW_EMAILS_PRO_ITEM_NAME,
					'url'        => home_url()
				);

				// Call the custom API.
				$response = wp_remote_post( WOO_Preview_Load_Dependencies::$woo_preview_store_url, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

				// make sure the response came back okay
				if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {
					if ( is_wp_error( $response ) ) {
						$message = $response->get_error_message();
					} else {
						$message = __( 'An error occurred, please try again.', 'woo-preview-emails-pro' );
					}
					$base_url = admin_url( WOO_Preview_Load_Dependencies::$woo_preview_option_page );
					$redirect = add_query_arg( array( 'sl_activation' => 'false', 'message' => urlencode( $message ) ), $base_url );
					wp_redirect( $redirect );
					exit();
				}

				// decode the license data
				$license_data = json_decode( wp_remote_retrieve_body( $response ) );
				if ( 200 === wp_remote_retrieve_response_code( $response ) && $license_data->success === false && $license_data->license === 'failed' ) {
					$message  = __( 'An error occurred, please try again.', 'woo-preview-emails-pro' );
					$base_url = admin_url( WOO_Preview_Load_Dependencies::$woo_preview_option_page );
					$redirect = add_query_arg( array( 'sl_activation' => 'false', 'message' => urlencode( $message ) ), $base_url );
					wp_redirect( $redirect );
					exit();
				}

				if ( $license_data->license == 'deactivated' ) {
					delete_option( '_woo_preview_emails_license_status' );
				}

				wp_redirect( admin_url( WOO_Preview_Load_Dependencies::$woo_preview_option_page ) );
				exit();
			}
		}

		/**
		 * Admin Notices
		 *
		 * @since 1.0.0
		 */
		function notices() {
			$status = @get_option( '_woo_preview_emails_license_status' );
			if ( empty( $status ) || $status === "invalid" ) {
				?>
                <div class="error">
                    <p><strong>Preview Emails for Woocommerce PRO</strong>: Invalid License Key</p>
                </div>
				<?php
			}

			if ( isset( $_GET['sl_activation'] ) && ! empty( $_GET['message'] ) ) {
				switch ( $_GET['sl_activation'] ) {
					case 'false':
						$message = urldecode( $_GET['message'] );
						?>
                        <div class="error">
                            <p><?php echo $message; ?></p>
                        </div>
						<?php
						break;
					case 'true':
					default:
						break;
				}
			}
		}
	}

	new WooPreviewEmailsProAdmin();

endif;
