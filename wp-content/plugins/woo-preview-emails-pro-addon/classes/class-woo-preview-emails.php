<?php
/**
 * @author     Deepen.
 * @created_on 10/22/19
 *
 * Main Class handler
 */

if ( ! class_exists( 'WooPreviewEmailsPro' ) ):

	class WooPreviewEmailsPro {

		/**
		 * Instance of this class.
		 *
		 * @var object
		 */
		protected static $instance = null;

		public static function get_instance() {
			// If the single instance hasn't been set, set it now.
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		public function __construct() {
			$this->load_dependencies();

			add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );

			$bookings_active = WOO_Preview_Load_Dependencies::woocommerce_booking_active_check();
			if ( $bookings_active ) {
				add_filter( 'woo_preview_additional_orderID', array( $this, 'wc_orders' ), 10, 4 );
			}

			add_action( 'woo_preview_additional_order_trigger', array( $this, 'trigger' ), 10, 2 );
		}

		/**
		 * @since 1.0.0
		 *
		 * Loading Dependencies
		 */
		function load_dependencies() {
			require WOO_PREVIEW_EMAILS_PRO_DIR . '/classes/class-woo-preview-admin-fields.php';
		}

		function admin_scripts( $hook ) {
			if ( $hook != 'woocommerce_page_digthis-woocommerce-preview-emails' ) {
				return;
			}

			wp_enqueue_script( 'woo-preview-email-pro', WOO_PREVIEW_EMAILS_PRO_DIR_URL . 'assets/js/woo-preview-emails-pro.js', array( 'jquery' ), '', true );
		}

		/**
		 * Make ready for booking form submission template display.
		 *
		 * @param $result
		 * @param $index
		 * @param $orderID
		 * @param $current_email
		 *
		 * @return array|bool
		 */
		function wc_orders( $result, $index, $orderID, $current_email ) {
			if ( $index === 'WC_Email_Booking_Reminder' || $index === "WC_Email_Booking_Confirmed" || $index === "WC_Email_Booking_Cancelled" || $index === "WC_Email_Admin_Booking_Cancelled" || $index === "WC_Email_New_Booking" ) {
				$booking_ids = WC_Booking_Data_Store::get_booking_ids_from_order_id( $orderID );
				if ( ! empty( $booking_ids ) ) {
					$result = array(
						'WC_Booking_Emails_Except_Notification',
						$booking_ids[0]
					);
				} else {
					$result = array( 'error' => sprintf( __( 'No Booking ID found with this Order ID. Choose which is a booking order ! <br><br>Go to orders page to check %s.', 'woo-preview-emails-pro' ), '<a target="_blank" href="' . admin_url( 'edit.php?post_type=shop_order' ) . '">Order ID</a>' ) );
				}
			} else if ( $index === 'WC_Email_Booking_Notification' ) {
				$booking_ids = WC_Booking_Data_Store::get_booking_ids_from_order_id( $orderID );
				if ( ! empty( $booking_ids ) ) {
					$result = array(
						'WC_Booking_Emails_Notification',
						$booking_ids[0],
						'Booking Notification',
						'This is a placeholder notification text.'
					);
				}
			} else if ( $index === 'WCS_Email_Expired_Subscription' || $index === 'WCS_Email_On_Hold_Subscription' ) {
				$subscription_valid = wcs_order_contains_subscription( $orderID );
				if ( ! $subscription_valid ) {
					$result = array( 'error' => sprintf( __( 'This is not a valid subscription order. Please select a valid subscription order ID. <br><br>Go to orders page to check %s.', 'woo-preview-emails-pro' ), '<a target="_blank" href="' . admin_url( 'edit.php?post_type=shop_order' ) . '">Order ID</a>' ) );
				}
			} else if ( $index === 'WC_Stripe_Email_Failed_Renewal_Authentication' ) {
				$subscription_valid = wcs_order_contains_subscription( $orderID );
				if ( ! $subscription_valid ) {
					$result = array( 'error' => sprintf( __( 'This is not a valid subscription order. Please select a valid subscription order ID. <br><br>Go to orders page to check %s.', 'woo-preview-emails-pro' ), '<a target="_blank" href="' . admin_url( 'edit.php?post_type=shop_order' ) . '">Order ID</a>' ) );
				} else {
					$result = array(
						'WC_Stripe_Email_Failed_Renewal_Authentication',
						wc_get_order( $orderID )
					);
				}
			} else if ( $index === 'WC_Stripe_Email_Failed_Preorder_Authentication' ) {
				$result = array( 'error' => __( 'WC_Stripe_Email_Failed_Preorder_Authentication email template is not viewable. Please select another template to view.', 'woo-preview-emails-pro' ) );
			} else if ( $index === 'WC_Stripe_Email_Failed_Authentication_Retry' ) {
				$result = array( 'error' => __( 'WC_Stripe_Email_Failed_Authentication_Retry email template is not viewable. Please select another template to view.', 'woo-preview-emails-pro' ) );
			}

			return $result;
		}

		/**
		 * Hook action in @woo_preview_additional_orderID
		 *
		 * @param $current_email
		 * @param $data
		 */
		function trigger( $current_email, $data ) {
			if ( isset( $data['error'] ) ) {
				$msg = $data['error'];
				$msg .= '<p><a href="' . admin_url( 'admin.php?page=digthis-woocommerce-preview-emails' ) . '">Take me back to choosing emails</a></p>';
				wp_die( $msg );
			}

			if ( in_array( 'WC_Booking_Emails_Except_Notification', $data ) ) {
				$current_email->trigger( $data[1] );
			}

			if ( in_array( 'WC_Booking_Emails_Notification', $data ) ) {
				$current_email->heading              = "Booking Notification";
				$current_email->notification_message = "This is a placeholder notification text.";
				$current_email->trigger( $data[1], $current_email->heading, $current_email->notification_message, false );
			}

			if ( in_array( 'WC_Stripe_Email_Failed_Renewal_Authentication', $data ) ) {
				$current_email->trigger( $data[1] );
			}
		}

	}

	add_action( 'plugins_loaded', array( 'WooPreviewEmailsPro', 'get_instance' ), 9999 );

endif;