### Preview Emails for WooCommerce Addon

PRO Version of Preview E-mails for WooCommerce is an Extension for WooCommerce that allows you to Preview Email Templates.

This plugin allows you to view different WooCommerce email templates directly from your admin dashboard. Find the option inside WooCommerce > Preview Emails to start.

**Features Supported:**

* Support for WooCommerce Booking Email templates
* Support for WooCommerce Subscription Email Templates
* Other WooCommerce addon emails as well.

**Below are few instructions to get started:**

* Just install the plugin.
* Find template view option in WooCommerce > Preview emails
* Choose the templates and an Order
* A preview of the selected E-mail will be shown