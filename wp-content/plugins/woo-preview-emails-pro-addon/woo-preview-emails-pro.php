<?php
/*
Plugin Name: Preview E-mails for WooCommerce PRO Add-On
Description: PRO Add-On for Preview E-mails for WooCommerce - allows to preview booking e-mails. Requires Free Version.
Plugin URI: https://www.codemanas.com
Author: Digamber Pradhan
Author URI: https://digamberpradhan.com/
Version: 1.0.0
WC requires at least: 3.0.0
WC tested up to: 3.6.2
Text Domain: woo-preview-emails-pro
Domain Path: /languages
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

define( 'WOO_PREVIEW_EMAILS_PRO_ITEM_NAME', 'Preview E-mails for WooCommerce PRO' ); //EDD Defined
define( 'WOO_PREVIEW_EMAILS_PRO_VERSION', '1.0.0' );

if ( ! defined( 'WOO_PREVIEW_EMAILS_PRO_DIR' ) ) {
	define( 'WOO_PREVIEW_EMAILS_PRO_DIR', dirname( __FILE__ ) );
}

if ( ! defined( 'WOO_PREVIEW_EMAILS_PRO_DIR_URL' ) ) {
	define( 'WOO_PREVIEW_EMAILS_PRO_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'WOO_PREVIEW_EMAILS_PRO_FILE' ) ) {
	define( 'WOO_PREVIEW_EMAILS_PRO_FILE', __FILE__ );
}

if ( in_array( 'woo-preview-emails/woocommerce-preview-emails.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	if ( ! function_exists( 'is_woocommerce_active' ) ) {
		require_once( 'includes/woo-functions.php' );
	}

	if ( is_woocommerce_active() ) {
		require_once( 'includes/class-woo-preview-dependencies.php' );
		require_once( 'classes/class-woo-preview-emails.php' );
	}

	if ( ! class_exists( 'WooPreviewEmailsPro_Updater' ) ) {
		require_once WOO_PREVIEW_EMAILS_PRO_DIR . '/classes/class-woo-preview-updater.php';
	}

	// setup the updater
	function woopreview_emails_addon_pro_updater() {
		$license_key = trim( get_option( '_woo_preview_emails_license_key' ) );
		new WooPreviewEmailsPro_Updater( WOO_Preview_Load_Dependencies::$woo_preview_store_url, __FILE__, array(
			'version' => WOO_PREVIEW_EMAILS_PRO_VERSION,
			'license' => $license_key,
			'author'  => 'Deepen Bajracharya',
			'beta'    => false,
		) );
	}

	add_action( 'admin_init', 'woopreview_emails_addon_pro_updater', 0 );
}

function woo_preview_emails_pro_load_text_domain() {
	load_plugin_textdomain( 'woo-preview-emails-pro', false, plugin_basename( dirname( __FILE__ ) ) . '/languages/' );
}

add_action( 'plugins_loaded', 'woo_preview_emails_pro_load_text_domain' );

//Check if FREE version is active if not then show admin notice message
if ( ! in_array( 'woo-preview-emails/woocommerce-preview-emails.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	add_action( 'admin_notices', 'deepu_wooPreviewEmailsPro_noticess' );
	function deepu_wooPreviewEmailsPro_noticess() {
		?>
        <div class="error">
            <p>Free version of <strong>Preview E-mails for WooCommerce</strong> is required for PRO version to work. Please Activate or Download from <a href="https://wordpress.org/plugins/woo-preview-emails/">WordPress repository</a> or search <strong>"Preview E-mails for WooCommerce"</strong> in <a href="<?php echo admin_url( 'plugin-install.php' ) ?>">plugin page</a>.</p>
        </div>
		<?php
	}
}