<div class="wrap">
    <form method="POST" action="">
        <table class="form-table">
            <tbody>
            <tr valign="top">
                <th scope="row" valign="top">
					<?php _e( 'License Key', 'woo-preview-emails-pro' ); ?>
                </th>
                <td>
                    <input id="license_key" name="license_key" type="password" class="regular-text" value="<?php esc_attr_e( $license ); ?>"/>
                    <p class="description" for="license_key"><?php _e( 'Enter your license key', 'woo-preview-emails-pro' ); ?></p>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" valign="top">
					<?php _e( 'License', 'woo-preview-emails-pro' ); ?>
                </th>
                <td>
					<?php wp_nonce_field( 'woo_preview_emails_licensing_nonce', 'woo_preview_emails_licensing_nonce' ); ?>
					<?php if ( $status !== false && $status == 'valid' ) { ?>
                        <span style="background: green;display: block;padding: 10px;margin-bottom: 10px;width: 117px;color: #fff;text-align: center;text-transform: uppercase;font-weight: 800;"><?php _e( 'Activated', 'woo-preview-emails-pro' ); ?></span>
                        <input type="submit" class="button button-primary"  name="deactive_the_license" value="<?php _e( 'Deactivate License', 'woo-preview-emails-pro' ); ?>"/>
					<?php } else { ?>
                        <input type="submit" class="button button-primary" name="license_activate" value="<?php _e( 'Save and Activate License', 'woo-preview-emails-pro' ); ?>"/>
					<?php } ?>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>