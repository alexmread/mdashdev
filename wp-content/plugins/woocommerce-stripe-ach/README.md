=== WooCommerce ACH Stripe Gateway ===
Tags: WooCommerce, ACH, Stripe, Payment, Woocommerce Gateway, Gateway, ach, woo, wordpress, wordpress plugin
Requires at least: 4.7
Tested up to: 5.2.2
WooCommerce required at least: 2.5
WooCommerce tested up to: 3.6.5
PHP version: 5.6+
Version: 1.5.0
Author: Ruslan Askarov & Ildar Akhmetov
Plugin URI: http://festplugins.com/woocommerce-stripe-ach-gateway/
License: Standard Codecanyon Extended License
License URI: https://codecanyon.net/licenses/terms/extended 

== Description ==
WooCommerce ACH Stripe Gateway accepts an ACH payments via Stripe. Now you can receive payments from bank accounts.

#TRY IT YOURSELF

WooCommerce ACH Stripe Live Demo - http://festplugins.com/woocommerce-stripe-ach-gateway/

#Fast and Friendly Support

We will go the extra mile to make our customers happy. 
Do you have an issue wirth the installation? We can install this plugin on your server for you for free. 
Need more functionality? We would love to hear your ideas
Have general questions? We usually reply within 24 hours.

== Installation ==
After you have purchased the plugin, you will recieve plugin file in .zip package.

1) To install the plugin, go to Admin Panel > Plugins.
2) Click the “Add New” button > then “Upload Plugin” button.
3) Select .zip package & click “Insall Now”. After that activate the plugin and start to use it.

Remember that you should have the WooCommerce plugin working on your site!