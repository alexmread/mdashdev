<?php
/*
 * Plugin Name: WooCommerce ACH Stripe Gateway
 * Plugin URI: http://festplugins.com/woocommerce-stripe-ach-gateway/
 * Description: New Woocommerce gateway to accept ACH payments from your website.
 * Version: 1.6.1
 * Author: FEST Agency
 * Author URI: http://festplugins.com/
 * Developer: Ruslan Askarov && Ildar Akhmetov
 * Developer URI: http://festplugins.com/
 * Text Domain: woocommerce-gateway-ach-stripe
 *
 * Requires at least: 5.0
 * Tested up to: 5.4.2
 * WC requires at least: 2.5
 * WC tested up to: 4.0.1
 *
 * Copyright 2020  FEST Agency (email : info@festagency.com)
 */

if (!defined('ABSPATH')) {
	exit;
}

define('WC_ACH_STRIPE_ROOT', __FILE__);
define('WC_ACH_STRIPE_ROOT_URL', plugins_url('/', __FILE__));
define( 'WC_ACH_STRIPE_PATH', plugin_dir_path( __FILE__ ) );

/* Rate section */

function wc_ach_plugin_rate_link($links)
{
	$cc_link = 'https://codecanyon.net/item/woocommerce-stripe-ach-gateway/reviews/19743197';
	ob_start();
	?>
	<a href="<?= $cc_link ?>" target="_blank">
		<?= __('Rate this plugin', 'woocommerce-gateway-ach-stripe') ?> ☆☆☆☆☆
	</a>
	<?php
	$rate_link = ob_get_clean();
	array_unshift($links, $rate_link);
	return $links;
}

$plugin_file = plugin_basename(__FILE__);
add_filter("plugin_action_links_$plugin_file", 'wc_ach_plugin_rate_link');
/* End Rate section */


// Include the main WooCommerce class.
if ( ! class_exists( 'WC_ACH_Stripe', false ) ) {
	include_once dirname( WC_ACH_STRIPE_ROOT ) . '/inc/class-wc-ach-stripe.php';
}

if ( WC_ACH_Stripe::is_woocommerce_active() ) {
	$GLOBALS['wc_ach_stripe'] = WC_ACH_Stripe::get_instance();
} else {
	add_action( 'admin_notices', array( 'WC_ACH_Stripe', 'woocommerce_inactive_notice' ) );
}