<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WC_Gateway_Stripe class.
 *
 * @extends WC_Payment_Gateway
 */
class WC_Gateway_Plaid extends WC_Payment_Gateway {

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->id                   = 'plaid';
		$this->method_title         = __( 'Plaid', 'woocommerce-gateway-ach-stripe' );
		$this->method_description   = __( '', 'woocommerce-gateway-ach-stripe' );
		$this->has_fields           = true;

		// Load the form fields
		$this->init_form_fields();

		// Load the settings.
		$this->init_settings();

		// Get setting values.
		$this->company                = $this->get_option( 'company' );
		$this->title                  = $this->get_option( 'title' );
		$this->description            = $this->get_option( 'description' );
		$this->enabled                = $this->get_option( 'enabled' );
		$this->mode               	  = $this->get_option( 'mode' );
		$this->plaid_public_key       = $this->get_option( 'plaid_public_key' );
		$this->plaid_client_id        = $this->get_option( 'plaid_client_id' );
		$this->plaid_secret        	  = $this->get_option( 'plaid_secret' );
		$this->pay_for_order_filter   = 'yes' === $this->get_option( 'pay_for_order_filter' );
		$this->logging                = 'yes' === $this->get_option( 'logging' );

		$this->stripe_options = get_option( 'woocommerce_ach_stripe_settings' );
		$this->stripe_secret_key      = $this->stripe_options['testmode'] === 'yes' ? $this->stripe_options['test_secret_key'] : $this->stripe_options['secret_key'];
		$this->stripe_public_key      = $this->stripe_options['testmode'] === 'yes' ? $this->stripe_options['test_publishable_key'] : $this->stripe_options['publishable_key'];
        $this->order_button_text = __( 'Place order', 'woocommerce-gateway-ach-stripe' );
		$this->supports = array(
			'subscriptions',
			'products',
			'refunds',
			'subscription_cancellation',
			'subscription_reactivation',
			'subscription_suspension',
			'subscription_amount_changes',
			'subscription_payment_method_change', // Subs 1.n compatibility.
			'subscription_payment_method_change_customer',
			'subscription_payment_method_change_admin',
			'subscription_date_changes',
			'multiple_subscriptions',
		);

		if ( $this->mode == "sandbox" ) {
			$this->description .= ' ' . sprintf( __( 'TEST MODE ENABLED. </br>Search for <strong>Tartan Bank</strong></br><strong>User</strong> user_good</br><strong>Password</strong> pass_good</br><a href="%s">Test Banks List</a>', 'woocommerce-gateway-ach-stripe' ), 'https://plaid.com/docs/api/#sandbox-institutions' );
			$this->description  = trim( $this->description );
		}

		WC_ACH_Stripe_API::set_plaid_data( $this->plaid_secret, $this->plaid_client_id );
		//WC_ACH_Stripe_API::set_api_init( WC_ACH_Stripe_API::get_api_init() );

		// Hooks
		add_action( 'wp_enqueue_scripts', array( $this, 'payment_scripts' ), 9001 );
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );

	}


	/**
	 * Initialise Gateway Settings Form Fields
	 */
	public function init_form_fields() {
        $this->form_fields = array(
            'enabled' => array(
                'title'       => __( 'Enable/Disable', 'woocommerce-gateway-ach-stripe' ),
                'label'       => __( 'Enable Plaid', 'woocommerce-gateway-ach-stripe' ),
                'type'        => 'checkbox',
                'description' => '',
                'default'     => 'no',
            ),
            'company' => array(
                'title'       => __( 'Company Name', 'woocommerce-gateway-ach-stripe' ),
                'type'        => 'text',
                'description' => __( 'To let customers know in checkout process.', 'woocommerce-gateway-ach-stripe' ),
                'default'     => __( 'Biff Co', 'woocommerce-gateway-ach-stripe' ),
                'desc_tip'    => true,
            ),
            'title' => array(
                'title'       => __( 'Title', 'woocommerce-gateway-ach-stripe' ),
                'type'        => 'text',
                'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce-gateway-ach-stripe' ),
                'default'     => __( 'Online Banking', 'woocommerce-gateway-ach-stripe' ),
                'desc_tip'    => true,
            ),
            'description' => array(
                'title'       => __( 'Description', 'woocommerce-gateway-ach-stripe' ),
                'type'        => 'text',
                'description' => __( 'This controls the description which the user sees during checkout.', 'woocommerce-gateway-ach-stripe' ),
                'default'     => __( 'Pay directly from your bank account with your Online Banking Account.', 'woocommerce-gateway-ach-stripe' ),
                'desc_tip'    => true,
            ),
            'mode' => array(
                'title'       => __( 'Mode', 'woocommerce-gateway-ach-stripe' ),
                'label'       => __( 'Choose the payment mode', 'woocommerce-gateway-ach-stripe' ),
                'type'        => 'select',
                'description' => __( 'Read more <a href="https://plaid.com/docs/quickstart/#platform-overview">here<a/>', 'woocommerce-gateway-ach-stripe' ),
				'default'     => 'sandbox',
				'options'	  => array(
					'sandbox' 	=> 'Sandbox',
					'development'	=> 'Development',
					'production'=> 'Production'
				),
                'desc_tip'    => false,
            ),
            'plaid_client_id' => array(
                'title'       => __( 'Plaid Client ID', 'woocommerce-gateway-ach-stripe' ),
                'type'        => 'text',
                'description' => __( 'Get your client ID from your Plaid dashboard.', 'woocommerce-gateway-ach-stripe' ),
                'default'     => '',
                'desc_tip'    => true,
            ),
            'plaid_public_key' => array(
                'title'       => __( 'Plaid Public Key', 'woocommerce-gateway-ach-stripe' ),
                'type'        => 'text',
                'description' => __( '<b>Optional</b> <br>If you have a public key in your Plaid dashboard, you can use it.', 'woocommerce-gateway-ach-stripe' ),
                'default'     => '',
                'desc_tip'    => true,
            ),
            'plaid_secret' => array(
                'title'       => __( 'Plaid Secret', 'woocommerce-gateway-ach-stripe' ),
                'type'        => 'text',
                'description' => __( 'Get your secret from your Plaid dashboard.', 'woocommerce-gateway-ach-stripe' ),
                'default'     => '',
                'desc_tip'    => true,
            ),
			'pay_for_order_filter' => array(
				'title'       => __( '`Pay for Order` Filter', 'woocommerce-gateway-ach-stripe' ),
				'label'       => __( 'Let the plugin filter the `Pay for Order` page gateways', 'woocommerce-gateway-ach-stripe' ),
				'type'        => 'checkbox',
				'description' => __( 'If enabled, the plugin will filter gateways that the customer can pay for the order with - if the order was created with the Plaid than it will be the only gateway the customer can use.<br>
				In other cases, the customer will not see this gateway.<br>
				<strong>Disable this option if you don\'t see the Plaid gateway where you expect to see it!</strong>', 'woocommerce-gateway-ach-stripe' ),
				'default'     => 'yes',
				'desc_tip'    => false,
			),
			'logging' => array(
				'title'       => __( 'Logging', 'woocommerce-gateway-ach-stripe' ),
				'label'       => __( 'Log debug messages', 'woocommerce-gateway-ach-stripe' ),
				'type'        => 'checkbox',
				'description' => __( 'Save debug messages to the WooCommerce System Status log.', 'woocommerce-gateway-ach-stripe' ),
				'default'     => 'no',
				'desc_tip'    => true,
			),
        );
	}

	/**
	 * Payment form on checkout page
	 */
	public function payment_fields() {
		// if the website is not HTTPS then prevent showing fields or show message in test mode
		if( !isset( $_SERVER["HTTPS"] ) ) {
			if( $this->mode == "sandbox" ) {
				echo '<p class="woocommerce-message">You should enable HTTPS on your website to serve live payments!</p>';
			} else {
				echo '<p class="woocommerce-error">HTTPS is not enabled. To serve payment, please enable HTTPS on your website.</p>';
				return;
			}
		}
		?>
		<fieldset id="plaid-payment-form" class="plaid-payment-fields"
			data-mode="<?= $this->get_option( 'mode' ); ?>"
			data-public="<?= $this->get_option( 'plaid_public_key' ); ?>"
			data-company="<?= $this->company; ?>"
		>
			<?php
				if ( $this->description ) {
					echo wpautop( wp_kses_post( $this->description ) );
				}
				echo '<div id="plaid-place-order-img">
				<img src="' . WC_HTTPS::force_https_url( WC_ACH_STRIPE_ROOT_URL . 'assets/img/plaid-banks.png' ) . '" style="float:none; max-height:none; cursor: pointer; max-width: 300px; width: 100%;"
				alt="Plaid" title="Place order" width="100%" />
				</div>';
			?>
		</fieldset>
		<?php
	}

	/**
	 * Load and handle the scripts.
	 *
	 * @return void
	 */
	public function payment_scripts() {
		global $wp_scripts;
		$gateways = WC()->payment_gateways->get_available_payment_gateways();
		$ach_stripe_script_name = "stripe";
		wp_enqueue_script( 'plaid', 'https://cdn.plaid.com/link/v2/stable/link-initialize.js', '', null, true );

		if( !wp_script_is( "stripe", "enqueued" ) || stripos( $wp_scripts->registered["stripe"]->src, "v3" ) !== false ) {
			$ach_stripe_script_name = "stripev2";
			wp_enqueue_script( $ach_stripe_script_name, 'https://js.stripe.com/v2/', '', '1.0', true );
		}

		if( !isset( $gateways["ach_stripe"] ) ) {
			wp_enqueue_script( 'dialog-polyfill', plugins_url( 'assets/js/dialog-polyfill.js', WC_ACH_STRIPE_ROOT ));
			wp_enqueue_script( 'woocommerce_ach_stripe', plugins_url( 'assets/js/ach-stripe.js', WC_ACH_STRIPE_ROOT ), array( 'jquery-payment', $ach_stripe_script_name, 'plaid', 'dialog-polyfill' ), '1.5.3', true );
		}

		//if( isset( $gateways["stripe"] ) )
			//$this->stripe_public_key = "no";

		$stripe_params = array(
			'key'                  => $this->stripe_public_key,
			'i18n_terms'           => __( 'Please accept the terms and conditions first', 'woocommerce-gateway-ach-stripe' ),
			'i18n_required_fields' => __( 'Please fill in required checkout fields first', 'woocommerce-gateway-ach-stripe' ),
		);

		wp_localize_script( 'woocommerce_ach_stripe', 'wc_ach_stripe_params', $stripe_params );
	}

	/**
	 * Get_icon function.
	 *
	 * @return string
	 */
	public function get_icon() {
		$icon  = '<img src="' . WC_HTTPS::force_https_url( WC_ACH_STRIPE_ROOT_URL . 'assets/img/plaid.png' ) . '" alt="Plaid" width="57" />';

		return apply_filters( 'woocommerce_gateway_icon', $icon, $this->id );
	}

	/**
	 * Check if the gateway is available for use.
	 * @override
	 *
	 * @return bool
	 */
	public function is_available() {
		global $wp_query;

		$is_available = parent::is_available();

		if( !isset( $wp_query->query["order-pay"] ) )
			return $is_available;

		$payment_method = get_post_meta( $wp_query->query["order-pay"], "_payment_method", true );
		if( $this->pay_for_order_filter && $payment_method != $this->id ) {
			return false;
		}

		return $is_available;
	}

	/**
	 * Process the payment
	 */
	public function process_payment( $order_id, $retry = true, $force_customer = false, $amount = 0 ) {
		if( NULL === $_POST['plaidToken'] || empty($_POST['plaidToken']) ) {
			wc_add_notice( "Plaid isn't loaded yet. Please, try again.", 'error' );
			return array( 'result' => 'fail' );
		}
		try {
			$order  = wc_get_order( $order_id );

			if( $amount > 0 ) {

				WC_ACH_Stripe::log( "Info: Begin processing payment for subscription $order_id for the amount of {$amount}" );

				// Make the request
				try {
					WC_ACH_Stripe_API::make_charge_for_renewal( $order, $amount );
				} catch( Exception $e ) {
					WC_ACH_Stripe::log( "The payment will not be processed further. Sending emails.." );
				} finally {

					/*$mailer = WC()->mailer();
					$mails = $mailer->get_emails();
					if ( ! empty( $mails ) ) {
						foreach ( $mails as $mail ) {
							if ( $mail->id == 'new_order' || $mail->id == 'customer_processing_order' || $mail->id == "customer_invoice" ){
								$mail->trigger( $order_id );
							}
						}
					}*/

				}
			}

			// Handle payment
			else if ( $order->get_total() > 0 ) {

				WC_ACH_Stripe::log( "Info: Begin processing payment for order $order_id for the amount of {$order->get_total()}" );

				// Make the request
				WC_ACH_Stripe_API::make_charge_if_necessary( $order );

				/*$mailer = WC()->mailer();
				$mails = $mailer->get_emails();
				if ( ! empty( $mails ) ) {
					foreach ( $mails as $mail ) {
						if ( $mail->id == 'new_order' || $mail->id == 'customer_processing_order' || $mail->id == "customer_invoice" ){
							$mail->trigger( $order_id );
						}
					}
				}*/
			} else {
				$order->payment_complete();
			}

			// Remove cart
			if( WC()->cart ) {
				WC()->cart->empty_cart();
			}

			// Return thank you page redirect
			return array(
				'result'   => 'success',
				'redirect' => $this->get_return_url( $order )
			);

		} catch ( Exception $e ) {
			if( function_exists( "wc_add_notice" ) ) {
				wc_add_notice( $e->getMessage(), 'error' );
			}
			if( !is_null( WC()->session ) ) {
				WC()->session->set( 'refresh_totals', true );
			}
			WC_ACH_Stripe::log( sprintf( __( 'Error: %s', 'woocommerce-gateway-ach-stripe' ), $e->getMessage() ) );
			return;
		}
	}

	/**
	 * Processing a refund
	 *
	 * @param [int] $order_id
	 * @param [float] $amount - in USD
	 * @param [string] $reason
	 * @return void
	 */
	public function process_refund( $order_id, $amount = null, $reason = null ) {
		// Do your refund here. Refund $amount for the order with ID $order_id
		WC_ACH_Stripe_API::do_refund( $order_id, $amount, $reason );

		return true;
	}
}