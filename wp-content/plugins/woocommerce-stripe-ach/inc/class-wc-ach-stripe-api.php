<?php

if (! defined('ABSPATH')) {
    exit;
}

/**
 * WC_Stripe_API class.
 *
 * Communicates with Stripe API.
 */
class WC_ACH_Stripe_API
{

    /**
     * Stripe API Endpoint
     */
    const ENDPOINT = 'https://api.stripe.com/v1/';

    /**
     * Secret key.
     *
     * @var string
     */
    private static $secret_key = '';

    /**
     * Stripe API version
     *
     * @var string
     */
    private static $api_ver = "";

    /**
     * Secret key.
     *
     * @var string
     */
    private static $plaid_secret_key = '';

    
    private static $plaid_client_id = '';

    /**
     * Verification microdeposit;
     *
     * @var [array]
     */
    private static $microdeposit;

    private static $plaid_bank_token = "";

    public static $body_to_send = "";

    public static $table_name = 'stripe_ach_data';

    /**
     * Set a plaid client ID.
     *
     * @param [string] $client_id
     * @return void
     */
    public static function set_plaid_data($secret, $client_id)
    {
        self::$plaid_secret_key = $secret;
        self::$plaid_client_id = $client_id;
    }

    /**
     * Set a secret key and API version
     *
     * @param [string] $secret_key
     * @return void
     */
    public static function set_api_init($params)
    {
        self::$secret_key = $params["secret_key"];
        self::$api_ver = $params["api_ver"];

        try {
            \Stripe\Stripe::setApiKey(self::$secret_key);
            \Stripe\Stripe::setApiVersion(self::$api_ver);
            \Stripe\Stripe::setAppInfo(
                "WooCommerce ACH Stripe Gateway",
                "1.5.0",
                "http://festplugins.com/woocommerce-stripe-ach-gateway/",
                "pp_partner_EFfQJPq2bIcSKL"
            );
        } catch (Exception $e) {
            self::log($e->getMessage());
            wp_send_json(array(
                "verified" => "fail",
                "msg" => __("Payment failed due to server-side error, please contact the website admin.")
            ));
        }
    }

    /**
     * Get a secret key.
     *
     * @return void
     */
    public static function get_api_init()
    {
        $options = get_option('woocommerce_ach_stripe_settings');

        $secret_key = "";
        $api_ver = "";

        if (isset($options['testmode'], $options['secret_key'], $options['test_secret_key'])) {
            $secret_key = 'yes' === $options['testmode'] ? $options['test_secret_key'] : $options['secret_key'];
        }
        if (isset($options["api_ver"])) {
            $api_ver = $options["api_ver"];
        }
        return array(
            "secret_key" => $secret_key,
            "api_ver" => $api_ver
        );
    }

    /**
     * Set a verification microdeposit.
     *
     * @param [type] $testmode
     * @return void
     */
    public static function set_microdeposit($testmode)
    {
        self::log("Set the micro with testmode =" . $testmode);
        if ($testmode) {
            self::$microdeposit = array( 32, 45 );
        } elseif (isset($_POST["microdep"]) && !$testmode) {
            $dep = wc_clean($_POST["microdep"]);
            $dep_arr = explode(" ", $dep);
            foreach ($dep_arr as &$val) {
                $val = intval($val);
            }

            self::$microdeposit = $dep_arr;
        }
    }

    /**
     * Set a verifitication microdeposit through a dialog on a front.
     *
     * @return void
     */
    public static function set_live_microdeposit($testmode)
    {
        $dep = wc_clean($_POST["microdep"]);
        $dep_arr = explode(" ", $dep);
        foreach ($dep_arr as &$val) {
            $val = intval($val);
        }

        self::$microdeposit = $dep_arr;
    }

    /**
     * Start a requests to Stripe to make a charge.
     *
     * @param [WC_Order] $order
     * @return void
     */
    public static function make_charge_if_necessary($order)
    {
        if ($order) {
            if (isset($_POST["ach_stripe_token"])) {
                self::log("Charge: look if we need a charge with token " . $_POST["ach_stripe_token"]);
                $type = 'ACH STRIPE';
            } else {
                $type = 'PLAID';
            }

            self::log("Setting the secret key...");
            self::set_api_init(self::get_api_init());
            self::log("Secret key is set");

            self::log("Charge: begin processing for $type.");

            $customer = self::get_ach_stripe_customer($order);

            if (isset($_POST["ach_stripe_token"])) {
                $customer = self::verify_bank_account($customer);
            }

            if ($customer !== false) {
                self::do_charge($customer, $order);
            } else {
                self::log("Verification was failed!");
                throw new Exception("Verification microdeposits you entered are not equal to those sent!");
            }
        }
    }

    /**
     * Start a requests to Stripe to make a charge.
     *
     * @param [WC_Order] $order
     * @return void
     */
    public static function make_charge_for_renewal($order, $amount)
    {
        if ( $order ) {
            self::log("Charge: begin processing for renewal.");

            self::log("Setting the secret key...");
            self::set_api_init(self::get_api_init());
            self::log("Secret key is set");

            $ach_customer_id = get_post_meta($order->get_id(), "_ach_stripe_customer_id", true);
            $ach_account_id = get_post_meta($order->get_id(), "_ach_stripe_account_id", true);

            if (empty($ach_customer_id) || false === $ach_customer_id) {
                self::log("There is no customer attached to this order .");
                $order->add_order_note("ACH Stripe: cannot process renewal because there is no customer attached to this order.");
                return;
            }

            if (empty($ach_account_id) || false === $ach_account_id) {
                self::log("The customer has no bank account, charge failed.");
                $order->add_order_note("ACH Stripe: cannot process renewal because the customer has no attached bank account in the Stripe.");
                return;
            }

            self::log("Got customer {$ach_customer_id} with bank {$ach_account_id}");

            try {
                $customer = \Stripe\Customer::retrieve($ach_customer_id);
                self::log("Got customer " . json_encode($customer));
            } catch (Exception $e) {
                self::log("Error while getting a customer in Stripe with ID {$ach_customer_id}: " . $e->getMessage());
                return;
            }

            $bank = array();

            foreach ($customer->sources->data as $bank_acc) {
                if ($bank_acc->id == $ach_account_id) {
                    $bank = $bank_acc;
                }
            }

            if (empty($bank)) {
                self::log("There is no bank attached to this order.");
                $order->add_order_note("ACH Stripe: cannot process renewal because the customer has no attached bank account in the Stripe.");
                return;
            }

            $_POST["wc-ach-stripe-currency"] = "USD";
            $_POST["wc-ach-stripe-routing-number"] = $bank->routing_number;
            $_POST["wc-ach-stripe-account-number"] = $bank->last4;
            $_POST["ach_stripe_token"] = "fake_token_for_renewal";

            self::do_charge($customer, $order);
        }
    }

    /**
     * Get Stripe Customer from the Stripe server or create it.
     *
     * @param [WP_User] $user
     * @return void
     */
    public static function get_ach_stripe_customer( $order )
    {
        $token = isset($_POST["ach_stripe_token"]) ? wc_clean($_POST["ach_stripe_token"]) : false;
        $first_name = isset($_POST["billing_first_name"]) ? wc_clean($_POST["billing_first_name"]) : $order->get_billing_first_name();
        $last_name = isset($_POST["billing_last_name"]) ? wc_clean($_POST["billing_last_name"]) : $order->get_billing_last_name();
        $email = $order->get_billing_email();
        $user_id = $order->get_user_id();
        $plaid_token = isset($_POST["plaidToken"]) ? wc_clean($_POST["plaidToken"]) : '';
        $plaid_options = get_option('woocommerce_plaid_settings');
        $ach_options = get_option('woocommerce_ach_stripe_settings');
        $plaid_customer_id_title = "_ach_plaid_{$plaid_options["mode"]}_customer_id";
        $payment_method = !$token ? $plaid_customer_id_title : '_ach_stripe_customer_id';


        $result_find = self::find_customer($email, $payment_method);
        $find_customer = $result_find['stripe_customer'];

        self::log('Customer has Stripe customer ID: ' . $find_customer->id);
        if (isset($_POST["wc-ach-stripe-account-number"])) {
            $last4 = substr(trim(wc_clean($_POST["wc-ach-stripe-account-number"])), -4);
        }
        if (isset($_POST["wc-ach-stripe-routing-number"])) {
            $routing = trim(wc_clean($_POST["wc-ach-stripe-routing-number"]));
        }

        $current_stripe_customer = "";

        if ($token === false) {
            $plaid_request = true;
            $bank_data = self::curl_to_plaid($plaid_options['mode']);
            self::log("Bank data: " . json_encode($bank_data));
            if (!empty($bank_data)) {
                $last4 = $bank_data->last4;
                $routing = $bank_data->routing_number;
            }
            $token = self::$plaid_bank_token;
        }
        self::log("Charge: search for a saved customer with token {$token}");
        
        if (!is_null($find_customer)) {
            foreach ($find_customer->sources->data as $bank) {
                if ($last4 == $bank->last4 && $routing == $bank->routing_number) {
                    $current_stripe_customer = $find_customer;
                }
            }
            if (empty($current_stripe_customer)) {
                // customer exists but it has no bank account
                self::log("Charge: create bank account with token {$token}");
                $new_bank = \Stripe\Customer::createSource($find_customer->id, array(
                    'source' => $token
                    ));
                $current_stripe_customer = $find_customer;
            }
        }


        if (empty($current_stripe_customer)) {
            // current stripe customer might be empty if the customer
            // purchase something for the first time
            // or he pays via Plaid
            if (empty($plaid_token)) {
                // the first Stripe purchase
                self::log("Charge: the first Stripe purchase, create customer with token {$token}");
                $customer = \Stripe\Customer::create(
                    array(
                    "source" => $token,
                    "description" => "{$first_name} {$last_name}",
                    "email" => $email
                    )
                );
            } elseif (isset($user_id) && empty(get_user_meta($user_id, $plaid_customer_id_title, true))) {

                // the first Plaid purchase
                self::log("Charge: the first Plaid purchase, create customer with token {$token}");
                $customer = \Stripe\Customer::create(
                    array(
                    "source" => $token,
                    "description" => "{$first_name} {$last_name}",
                    "email" => $email
                    )
                );
                update_user_meta($user_id, $plaid_customer_id_title, $customer->id);
            } else {
                // not the first Plaid purchase, get the customer
                
                $current_stripe_customer = get_user_meta($user_id, $plaid_customer_id_title, true);
                $customer = \Stripe\Customer::retrieve($current_stripe_customer);
                self::log("Charge: not the first Plaid purchase, got customer: " . json_encode($customer));
                // customer was removed
                if (isset($customer->deleted) && $customer->deleted == true) {
                    $customer = \Stripe\Customer::create(
                        array(
                        "source" => $token,
                        "description" => "{$first_name} {$last_name}",
                        "email" => $email
                        )
                    );
                    update_user_meta($user_id, $plaid_customer_id_title, $customer->id);
                }
            }
            self::log("Charge: can't find a saved user. Created with id {$customer->id}");
        } else {
            $customer = $current_stripe_customer;
        }
        self::log("Charge: result get_ach_stripe_customer: " . json_encode($customer));
        return $customer;
    }

    /**
     * Request by curl to PLAID
     *
     * @param string $mode
     * @return void
     */
    public static function curl_to_plaid($mode)
    {
        $plaid_token = wc_clean($_POST["plaidToken"]);
        $acc_id = wc_clean($_POST["plaidAccountID"]);
        $environment = $mode;
        
        self::log("Charge: will send to https://{$environment}.plaid.com/item/public_token/exchange the following " . json_encode(array(
            'client_id' => self::$plaid_client_id,
            'secret' => self::$plaid_secret_key,
            'public_token' => $plaid_token,
        )));


        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => "https://{$environment}.plaid.com/item/public_token/exchange",
            CURLOPT_POST => true,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
            CURLOPT_POSTFIELDS => json_encode(array(
                'client_id' => self::$plaid_client_id,
                'secret' => self::$plaid_secret_key,
                'public_token' => $plaid_token,
            )),
        ));
        $response = curl_exec($ch);
        curl_close($ch);
        self::log("Charge: response from {$environment} exchange token environment: {$response}");

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => "https://{$environment}.plaid.com/processor/stripe/bank_account_token/create",
            CURLOPT_POST => true,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
            CURLOPT_POSTFIELDS => json_encode(array(
                'client_id' => self::$plaid_client_id,
                'secret' => self::$plaid_secret_key,
                'access_token' => json_decode($response)->access_token,
                'account_id' => $acc_id,
            )),
        ));
        $response_stripe = curl_exec($ch);
        curl_close($ch);
        self::log("Charge: response from {$environment} bank token environment: {$response_stripe}");

        self::$plaid_bank_token = json_decode($response_stripe)->stripe_bank_account_token;

        try {
            self::log("Trying to get the bank data for the token " . self::$plaid_bank_token);
            $bank_data = \Stripe\Token::retrieve(self::$plaid_bank_token)->bank_account;
            self:: log("Got bank data for Plaid: " . json_encode($bank_data));
        } catch (Exception $e) {
            self::log("Error while getting the bank data for Plaid: {$e->getMessage()}");
        }
        return $bank_data;
    }

    /**
     * Stripe Bank account verification and deletion of extra.
     *
     * @param [object] $customer
     * @return [object] $customer
     */
    public static function verify_bank_account(&$customer)
    {
        $last4_from_form = substr(trim(wc_clean($_POST["wc-ach-stripe-account-number"])), -4);
        $routing_number = trim(wc_clean($_POST["wc-ach-stripe-routing-number"]));
        $token = isset($_POST["ach_stripe_token"]) ? wc_clean($_POST["ach_stripe_token"]) : false;
        $microdep = self::$microdeposit;
        $response = false;
        $FLAG = true;

        foreach ($microdep as &$mdp) {
            $mdp = intval($mdp);
        }

        self::log("Charge: received from FORM last4: {$last4_from_form}, routing number: {$routing_number}");

        for ($i = 0; $i < count($customer->sources->data); $i++) {
            $bank = &$customer->sources->data[$i];

            self::log( "Charge: check bank verification last4: {$bank->last4} == {$last4_from_form}, routing number: {$bank->routing_number} == {$routing_number},with status: {$bank->status}");
            if ($bank->last4 == $last4_from_form && $bank->routing_number == $routing_number) {
                if ($bank->status == "new") {
                    self::log("Charge: verificate bank with last4: {$bank->last4}, routing number: {$bank->routing_number}");
                    $bank_account = $customer->sources->retrieve($bank->id);
                    $bank_account->verify(array( 'amounts' => $microdep ));
                    $response = $customer;
                    $FLAG = false;
                } elseif ($bank->status == "verification_failed") {
                    $response = false;
                }
            }
        }
        if( $FLAG && $response ) {
            // stored customer doesn't have such bank account, so add new one
            $customer->sources->create( array( "source" => $token ) );
            self::log("Stored customer doesn't have such bank account {$routing_number} {$last4_from_form}, so add new one");
        }

        return $response;
    }

    /**
     * Create a charge when a customer received and a bank account verified.
     *
     * @param [object] $customer
     * @param [WC_Order] $order
     * @return void
     */
    public static function do_charge($customer, $order)
    {
        $currency = isset($_POST["wc-ach-stripe-currency"]) ? strtolower(wc_clean($_POST["wc-ach-stripe-currency"])) : "USD";
        $routing = isset($_POST["wc-ach-stripe-routing-number"]) ? wc_clean($_POST["wc-ach-stripe-routing-number"]) : false;
        $last4 = isset($_POST["wc-ach-stripe-account-number"]) ? substr($_POST["wc-ach-stripe-account-number"], -4) : false;
        $token = isset($_POST["ach_stripe_token"]) ? wc_clean($_POST["ach_stripe_token"]) : false;
        $first_name = isset($_POST["billing_first_name"]) ? wc_clean($_POST["billing_first_name"]) : $order->get_billing_first_name();
        $last_name = isset($_POST["billing_last_name"]) ? wc_clean($_POST["billing_last_name"]) : $order->get_billing_last_name();
        $email = $order->get_billing_email();

        $db_options = get_option("woocommerce_ach_stripe_settings");

        if (get_current_user_id() && $db_options["saved_banks"] === "yes" && isset($_POST["wc-ach_stripe-new-payment-method"]) && $_POST["wc-ach_stripe-new-payment-method"]) {
            $_POST["routing_number"] = $routing;
            $_POST["account_number"] = $last4;
            $_POST["country"] = wc_clean($_POST["wc-ach-stripe-country"]);
            $_POST["currency"] = $currency;
            $_POST["account_holder_name"] = wc_clean($_POST["wc-ach-stripe-account-holder-name"]);
            $_POST["account_holder_type"] = wc_clean($_POST["wc-ach-stripe-account-holder-type"]);
            WC_Gateway_ACH_Stripe::ach_add_payment_method(true);
        }

        if (!$token) {
            // plaid used
            // get bank data by the token
            try {
                self::log("Trying to get the bank data for the token " . self::$plaid_bank_token);
                $bank_data = \Stripe\Token::retrieve(self::$plaid_bank_token)->bank_account;
                $routing = $bank_data->routing_number;
                $last4 = $bank_data->last4;
            } catch (Exception $e) {
                self::log("Error while getting the bank data for Plaid: {$e->getMessage()}. Then create the new one");
                // empty response, create a bank account
                $customer = \Stripe\Customer::create(
                    array(
                    "description" => "{$first_name} {$last_name}",
                    "email" => $email
                    )
                );
                $bank_data = \Stripe\Customer::createSource(
                    $customer->id,
                    ['source' => self::$plaid_bank_token]
                );
                $customer = \Stripe\Customer::retrieve($customer->id);
            }
        }

        self::log("Charge: currency is $currency");
        self::log("do_charge: " . json_encode($customer));
        if (is_null($currency) || empty($currency)) {
            $currency = "usd";
        }

        $bank = "";
        for ($i = 0; $i < count($customer->sources->data); $i++) {
            self::log("Checking Customer sources: " . $customer->sources->data[$i]->routing_number . ", " . $customer->sources->data[$i]->last4);
            if ($customer->sources->data[$i]->routing_number == $routing && $customer->sources->data[$i]->last4 == $last4) {
                $bank = $customer->sources->data[$i]->id;
            }
        }

        $user_id = $order->get_customer_id();

        $amount = $order->get_total();

        if (empty($bank)) {
            if (isset($bank_data) && !empty($bank_data) && !is_null($bank_data)) {
                $bank = $bank_data->id;
            } else {
                self::log("Customer has no bank account matching the routing $routing and last4 $last4.");
                //throw new Exception( "Customer has no bank account matching the routing $routing and last4 $last4" );
                wp_send_json(array(
                    "verified" => "fail",
                    "msg" => __("You have no bank account matching the routing {$routing} and last4 {$last4}. You should check your bank account or contact Stripe support.")
                ));
            }
        }
        $charge_data = array(
            "amount" => $amount * 100,
            "description" => get_bloginfo("name") . " - Order {$order->get_order_number()}",
        );
        self::log("Charge: make a charge for " . json_encode($bank));

        /**
         * wc_stripe_generate_create_intent_request - Filter hook for change charge data
         */
        $charge_data = apply_filters('wc_stripe_generate_create_intent_request', $charge_data);
        $charge_data["currency"] = $currency;
        $charge_data["customer"] = $customer->id;
        $charge_data["source"] = $bank;

        $charge_response = \Stripe\Charge::create($charge_data);
        $order->update_status("Pending payment", 'Waiting for receiving the money to Stripe.', true);
        $order->add_order_note("ACH Stripe charge: waiting for receiving the money to Stripe for Charge ID " . json_encode($charge_response->id));
        //$order->add_order_note( "ACH Stripe charge complete: Charge ID " . json_encode( $charge_response ) );

        update_post_meta($order->get_id(), '_ach_stripe_charge_id', $charge_response->id, true);
        update_post_meta($order->get_id(), '_ach_stripe_transaction_id', $charge_response->balance_transaction, true);
        update_post_meta($order->get_id(), "_created_via", "ACH Stripe");
        update_post_meta($order->get_id(), '_ach_stripe_customer_id', $customer->id);
        update_post_meta($order->get_id(), '_ach_stripe_account_id', $bank);

        /*$verified_banks = get_user_meta( get_current_user_id(), "_ach_stripe_verified", true );
        array_push( $verified_banks, $charge_response->source->last4 );
        $verified_banks = array_unique( $verified_banks );

        update_user_meta( get_current_user_id(), '_ach_stripe_verified_', $verified_banks, true );*/

        self::log("Charge: charge $charge_response");
        if (WC()->session) {
            WC()->session->set('ach_charge', $charge_response);
        }
    }

    /**
     * Make a charge with saved payment method token
     * If there is no saved customer then throw exception
     *
     * @return void
     */
    public static function payment_by_saved_token()
    {
        global $wpdb;
        self::set_api_init(self::get_api_init());

        $payment_token_id = wc_clean($_POST["token_id"]);

        self::log("Begin payment with saved payment token {$payment_token_id}");

        $last4 = $wpdb->get_var("SELECT meta_value FROM {$wpdb->prefix}woocommerce_payment_tokenmeta WHERE payment_token_id = {$payment_token_id} AND meta_key = 'last4'");
        $routing = $wpdb->get_var("SELECT meta_value FROM {$wpdb->prefix}woocommerce_payment_tokenmeta WHERE payment_token_id = {$payment_token_id} AND meta_key = 'routing'");
        $email = wc_clean($_POST["billing_email"]);

        $customer_data = array();
        $bank_data = array();

        self::log("Looking for customer with last4 = {$last4}...");

        
        $result_find = self::find_customer($email, '_ach_stripe_customer_id');
        $find_customer = $result_find['stripe_customer'];



        foreach ($find_customer->sources->data as $bank) {
            if ($last4 == $bank->last4) {
                $bank_data = $bank;
                $customer_data = $find_customer;
            }
        }
        // !!!! here we won't create a new source for customer since it should be laready in the stripe

        if (empty($customer_data) || !$customer_data || is_null($customer_data)) {
            self::log("Not found such a customer.");
            wp_send_json(array(
                "verified" => "fail",
                "msg" => __("The customer account you have in the store does not exist in Stripe. Please, fill the 'New payment method' form.")
            ));
        }

        if (empty($bank_data) || !$bank_data || is_null($bank_data)) {
            self::log("Not found such a bank data but got customer with id {$customer_data->id}.");
            wp_send_json(array(
                "verified" => "fail",
                "msg" => __("The bank account you have in the store does not exist in Stripe. Please, choose another on or fill the 'New payment method' form.")
            ));
        }

        try {
            // we can't use saved accounts on Pay For Order page while the user IS NOT VERIFIED
            // because we need the private data (account_number) to make any opeartions
            // That's why we should check it to make further actions
            $_POST["account_number"] = wc_clean($_POST["account_number"]);

            if (is_null($_POST["account_number"]) || empty($_POST["account_number"])) {
                self::log("Cannot process a payment via SAVED METHOD because of wrong account number: " . $_POST["account_number"]);
                wp_send_json(array(
                    "verified" => "fail",
                    "msg" => __("Please, confirm your account number.")
                ));
            }

            $_POST["ach_stripe_token"] = \Stripe\Token::create(array(
                "bank_account" => array(
                    "country" => "US",
                    "currency" => "usd",
                    "account_holder_name" => $bank_data->account_holder_name,
                    "account_holder_type" => $bank_data->account_holder_type,
                    "routing_number" => $bank_data->routing_number,
                    "account_number" => $_POST["account_number"]
                )
            ))->id;

            self::check_bank_verification($bank_data);
        } catch (Exception $e) {
            self::log("Cannot process a payment: " . $e->getMessage());
            wp_send_json(array(
                "verified" => "fail",
                "msg" => $e->getMessage()
            ));
        }
    }

    /**
     * Check bank verification via ajax.
     *
     * @return void
     */
    public static function check_bank_verification($bank_for_saved_payment = false)
    {
        global $woocommerce, $wp;
        self::log("============ START LOG =============");

        self::log("Start checking bank verification...");

        $last4 = substr(trim(wc_clean($_POST["account_number"])), -4);
        $routing = trim(wc_clean($bank_for_saved_payment ? $bank_for_saved_payment["routing_number"] : $_POST["routing_number"]));
        $deferred_token = wc_clean($_POST["ach_stripe_token"]);
        $email = isset($_POST["billing_email"]) ? wc_clean($_POST["billing_email"]) : '';

        $current_stripe_customer = "";
        self::log("Setting the secret key...");
        self::set_api_init(self::get_api_init());
        self::log("Secret key is set");


        if(isset($_POST['order_key']) && $_POST['order_key']) {
            $order_id = wc_get_order_id_by_order_key($_POST['order_key']);
            $order = wc_get_order( $order_id );
            $email = $order->get_billing_email();
        }

        $result_find = self::find_customer($email, '_ach_stripe_customer_id');
        $find_customer = $result_find['stripe_customer'];
        $user_id = $result_find['user_id'];

        self::log('Find customer: ' . json_encode($find_customer));
        $db_options = get_option("woocommerce_ach_stripe_settings");
        $options = array_merge(WC_ACH_Stripe::$ajax_response_standard_values, $db_options);

        if(!is_null($find_customer)) {
            foreach ($find_customer->sources->data as $bank) {
                if ($last4 == $bank->last4 && $routing == $bank->routing_number) {
                    $current_stripe_customer = $find_customer;
                    $current_stripe_customer->bank = $bank;
                    if ($bank->status == "verified") {
                        wp_send_json(array(
                            "bank" => $bank_for_saved_payment,
                            "verified" => true,
                            "ach_stripe_token" => wc_clean($_POST["ach_stripe_token"])
                        ));
                    }
                } 
            }
            if (empty($current_stripe_customer)) {
                // we've found a customer but it doesn't contain required sources
                // add a new source
                self::log("Let's create a new source {$deferred_token} for a customer {$find_customer->id}");
                $new_bank = \Stripe\Customer::createSource($find_customer->id, array(
                    'source' => $deferred_token
                ));
    
                $current_stripe_customer = $find_customer;
                $current_stripe_customer->bank = $new_bank;
            }
        }

        

        self::log("Checking if the customer object empty or not");

        # else we make a verification payment but first check if these payments was before
        if (!empty($current_stripe_customer->id)) {
            self::log("Not empty Stripe customer, WC version $woocommerce->version");

            # HERE TO SEND AN EMAIL TO CUSTOMER
            if ($_POST["order_key"] != "0" && isset($_POST["order_key"])) {
                self::log("Will get an order with key = {$_POST["order_key"]}");
                $ord = new \WC_Order(wc_get_order_id_by_order_key($_POST["order_key"]));
            } else {
                self::log("There is no order key.");
                wc_maybe_define_constant('WOOCOMMERCE_CHECKOUT', true);
                wc_set_time_limit(0);

                do_action('woocommerce_before_checkout_process');
                self::log("Before checkout process action done.");

                if( WC()->cart->is_empty() ) {
                    wp_send_json(array(
						"verified" => "fail",
						"msg" => sprintf( __( 'Sorry, your session has expired. Please report an error to the administration of the site. <a href="%s" class="wc-backward">Return to shop</a>', 'woocommerce' ), esc_url( wc_get_page_permalink( 'shop' ) ) )
					));
                }
                self::log("Cart is not empty.");

                do_action('woocommerce_checkout_process');
                self::log("Checkout process action done.");

                $errors      = new WP_Error();
                $posted_data = WC()->checkout()->get_posted_data();
                self::log("Posted data is " . json_encode($posted_data));

                self::update_session($posted_data);

                foreach ($errors->get_error_messages() as $message) {
                    wc_add_notice($message, 'error');
                }

                if (empty($posted_data['woocommerce_checkout_update_totals']) && 0 === wc_notice_count('error')) {
                    try {
                        self::process_customer($posted_data);
                    } catch (Exception $e) {
                        wp_send_json(array(
                            "verified" => "fail",
                            "msg" => $e->getMessage()
                        ));
                    }
                    //self::log( "WC Customer processed" );

                    $ord = self::create_custom_order_to_pay_later($user_id);
                    $order_id = $ord->get_id();
                    self::log("Custom order {$order_id} created.");

                    if (is_wp_error($order_id)) {
                        throw new Exception($order_id->get_error_message());
                    }

                    do_action('woocommerce_checkout_order_processed', $order_id, $posted_data, $ord);
                    self::log("WC Checkout Order processed action done.");
                }
            }

            if ( get_post_meta($ord->get_id(), "_ach_is_emailed", true) !== "yes" ) {
                // emailing anyway
                $email_content = self::email_template($ord);
                self::log("The email content is {$email_content}");

                $headers[] = 'From: ' . get_option("woocommerce_email_from_name") . ' <' . get_option("woocommerce_email_from_address") . '>';
                $headers[] = 'content-type: text/html';
                $headers[] = 'Reply-to: ' . $ord->get_billing_first_name() . ' ' . $ord->get_billing_last_name() . ' <' . $ord->get_billing_email() . ">";

                if (is_null($email) || empty($email) || !isset($email)) {
                    $email = $ord->get_billing_email();
                }

                if (!is_null($email) && !empty($email) && isset($email)) {
                    $order_number = $ord->get_order_number();
                    $mail_header = "Invoice for order #{$order_number} - Verify your bank account";

                    ob_start();
                    $ismail = wp_mail($email, $mail_header, $email_content, $headers);
                    ob_clean();
                    self::log("The email sending status is " . strval($ismail));
                } else {
                    self::log("The email is not defined");
                }
                update_post_meta($ord->get_id(), "_ach_is_emailed", "yes");
            }

            if($ord->get_customer_id() === 0) {
                $ord->set_customer_id($user_id);
            }

            self::log("Going to empty the cart....");
            $woocommerce->cart->empty_cart();

            self::log("Received data " . json_encode($_POST));

            if (version_compare($woocommerce->version, "3.0", ">=")) {
                self::log("WC > 3.0");
                self::log(json_encode($options));

                $redirection_link = get_permalink(wc_get_page_id('shop'));
                if (isset($options["after_checkout_redirection"]) && !empty($options["after_checkout_redirection"])) {
                    // redirection link should be in the right format
                    $redirection_link = $options["after_checkout_redirection"];
                    if ($redirection_link[0] != "/") {
                        $redirection_link = "/" . $redirection_link;
                    }
                }
                if (isset($options["redirect_to_confirmation"]) && !empty($options["redirect_to_confirmation"]) && $options["redirect_to_confirmation"] === "yes") {
                    $redirection_link = $ord->get_checkout_order_received_url();
                }
                self::log($redirection_link);
            } else {
                self::log("WC < 3.0");
                self::log(json_encode($options));

                $redirection_link = get_permalink(woocommerce_get_page_id('shop'));
                if (isset($options["after_checkout_redirection"]) && !empty($options["after_checkout_redirection"])) {
                    // redirection link should be in the right format
                    $redirection_link = $options["after_checkout_redirection"];
                    if ($redirection_link[0] != "/") {
                        $redirection_link = "/" . $redirection_link;
                    }
                }
                if (isset($options["redirect_to_confirmation"]) && !empty($options["redirect_to_confirmation"]) && $options["redirect_to_confirmation"] === "yes") {
                    $redirection_link = $ord->get_checkout_order_received_url();
                }
                self::log($redirection_link);
            }

            update_post_meta($ord->get_id(), '_ach_stripe_customer_id', $current_stripe_customer->id);
            update_post_meta($ord->get_id(), '_ach_stripe_account_id', $current_stripe_customer->bank->id);

            if (function_exists("wcs_get_subscriptions_for_order")) {
                self::log("Going to get subscriptions for the order...");
                $order_subscriptions = wcs_get_subscriptions_for_order($ord->get_id());

                foreach ($order_subscriptions as $sub_id => $sub) {
                    update_post_meta($ord->get_id(), '_ach_stripe_customer_id', $current_stripe_customer->id);
                    update_post_meta($sub_id, '_ach_stripe_account_id', $current_stripe_customer->bank->id);
                }
            }

            // save the bank account if necessary
            self::log("Check if we need to save method: " . json_encode(array( $db_options["saved_banks"], strval(!$bank_for_saved_payment), strval(checked($_POST["wc-ach_stripe-new-payment-method"])) )));
            if (get_current_user_id() && $db_options["saved_banks"] === "yes" && !$bank_for_saved_payment && $_POST["save_account"] == 1) {
                WC_Gateway_ACH_Stripe::ach_add_payment_method(true);
            }

            self::log("Sending the response: " . json_encode(array(
                "bank" => $bank_for_saved_payment,
                "verified" => "waiting",
                "redirection_after_checkout" => $redirection_link,
                "popup_header" => $options["popup_header"],
                "popup_text" => $options["popup_text"],
                "popup_text_waiting" => $options["popup_text_waiting"],
                "ach_stripe_token" => wc_clean($_POST["ach_stripe_token"])
            )));

            wp_send_json(array(
                "bank" => $bank_for_saved_payment,
                "verified" => "waiting",
                "redirection_after_checkout" => $redirection_link,
                "popup_header" => $options["popup_header"],
                "popup_text" => $options["popup_text"],
                "popup_text_waiting" => $options["popup_text_waiting"],
                "ach_stripe_token" => wc_clean($_POST["ach_stripe_token"])
            ));
            self::log("Response error!");
        } else {
            self::log("Empty Stripe customer has no bank account, WC version $woocommerce->version");

            # very first purchase of current user
            # create a Stripe customer
            $first_name = isset($_POST["billing_first_name"]) ? wc_clean($_POST["billing_first_name"]) : '';
            $last_name = isset($_POST["billing_last_name"]) ? wc_clean($_POST["billing_last_name"]) : '';
            self::log("Customer name is {$first_name} {$last_name}");

            try {
                $new_customer = \Stripe\Customer::create(
                    array(
                    "source" => $deferred_token,
                    "description" => "{$first_name} {$last_name}",
                    "email" => $email
                    )
                );
            } catch (Exception $e) {
                self::log($e->getMessage());
                wp_send_json(array(
                    "verified" => "fail",
                    "msg" => $e->getMessage()
                ));
                exit;
            }
            self::log("New customer created!");

            /*if( is_null( $new_customer->currency ) || empty( $new_customer->currency ) ) {
                $new_customer->sources->currency = "usd";
                $new_customer->save();
            }
            self::log( "New customer currency set!" );	*/

            if ($_POST["order_key"] != "0" && isset($_POST["order_key"])) {
                self::log("Will get an order with key = {$_POST["order_key"]}");

                $ord = new \WC_Order(wc_get_order_id_by_order_key($_POST["order_key"]));
            } else {
                self::log("There is no order key.");

                wc_maybe_define_constant('WOOCOMMERCE_CHECKOUT', true);
                wc_set_time_limit(0);

                do_action('woocommerce_before_checkout_process');
                self::log("Before checkout process action done.");

                if (WC()->cart->is_empty()) {
                    wp_send_json(array(
						"verified" => "fail",
						"msg" => sprintf( __( 'Sorry, your session has expired. Please report an error to the administration of the site. <a href="%s" class="wc-backward">Return to shop</a>', 'woocommerce' ), esc_url( wc_get_page_permalink( 'shop' ) ) )
					));
                }
                self::log("Cart is not empty.");

                do_action('woocommerce_checkout_process');
                self::log("Checkout process action done.");

                $errors      = new WP_Error();
                $posted_data = WC()->checkout()->get_posted_data();
                self::log("Posted data is " . json_encode($posted_data));

                self::update_session($posted_data);

                foreach ($errors->get_error_messages() as $message) {
                    wc_add_notice($message, 'error');
                }

                if (empty($posted_data['woocommerce_checkout_update_totals']) && 0 === wc_notice_count('error')) {
                    try {
                        self::process_customer($posted_data);
                    } catch (Exception $e) {
                        wp_send_json(array(
                            "verified" => "fail",
                            "msg" => $e->getMessage()
                        ));
                    }
                    //self::log( "WC Customer processed" );

                    $ord = self::create_custom_order_to_pay_later($user_id);
                    $order_id = $ord->get_id();
                    self::log("Custom order {$order_id} created.");

                    if (is_wp_error($order_id)) {
                        throw new Exception($order_id->get_error_message());
                    }

                    do_action('woocommerce_checkout_order_processed', $order_id, $posted_data, $ord);
                    self::log("WC Checkout Order processed action done.");
                }
            }


            if (get_post_meta($ord->get_id(), "_ach_is_emailed", true) !== "yes") {
                // emailing anyway
                $email_content = self::email_template($ord);
                self::log("The email content is {$email_content}");

                $headers[] = 'From: ' . get_option("woocommerce_email_from_name") . ' <' . get_option("woocommerce_email_from_address") . '>';
                $headers[] = 'content-type: text/html';
                $headers[] = 'Reply-to: ' . $ord->get_billing_first_name() . ' ' . $ord->get_billing_last_name() . ' <' . $ord->get_billing_email() . ">";

                if (is_null($email) || empty($email) || !isset($email)) {
                    $email = $ord->get_billing_email();
                }

                if (!is_null($email) && !empty($email) && isset($email)) {
                    $order_number = $ord->get_order_number();
                    $mail_header = "Invoice for order #{$order_number} - Verify your bank account";

                    ob_start();
                    $ismail = wp_mail($email, $mail_header, $email_content, $headers);
                    ob_clean();
                    self::log("The email sending status is " . strval($ismail));
                } else {
                    self::log("The email is not defined");
                }
                update_post_meta($ord->get_id(), "_ach_is_emailed", "yes");
            }

            if($ord->get_customer_id() === 0) {
                $ord->set_customer_id($user_id);
            }

            self::log("Going to empty the cart....");
            $woocommerce->cart->empty_cart();

            if (version_compare($woocommerce->version, "3.0", ">=")) {
                $redirection_link = get_permalink(wc_get_page_id('shop'));
                if (isset($options["after_checkout_redirection"]) && !empty($options["after_checkout_redirection"])) {
                    // redirection link should be in the right format
                    $redirection_link = $options["after_checkout_redirection"];
                    if ($redirection_link[0] != "/") {
                        $redirection_link = "/" . $redirection_link;
                    }
                }
                if (isset($options["redirect_to_confirmation"]) && !empty($options["redirect_to_confirmation"]) && $options["redirect_to_confirmation"] === "yes") {
                    $redirection_link = $ord->get_checkout_order_received_url();
                }
            } else {
                $redirection_link = get_permalink(woocommerce_get_page_id('shop'));
                if (isset($options["after_checkout_redirection"]) && !empty($options["after_checkout_redirection"])) {
                    // redirection link should be in the right format
                    $redirection_link = $options["after_checkout_redirection"];
                    if ($redirection_link[0] != "/") {
                        $redirection_link = "/" . $redirection_link;
                    }
                }
                if (isset($options["redirect_to_confirmation"]) && !empty($options["redirect_to_confirmation"]) && $options["redirect_to_confirmation"] === "yes") {
                    $redirection_link = $ord->get_checkout_order_received_url();
                }
            }

            update_post_meta($ord->get_id(), '_ach_stripe_customer_id', $new_customer->id);
            update_post_meta($ord->get_id(), '_ach_stripe_account_id', $new_customer->sources->data[0]->id);

            if (function_exists("wcs_get_subscriptions_for_order")) {
                self::log("Going to get subscriptions for the order...");
                $order_subscriptions = wcs_get_subscriptions_for_order($ord->get_id());

                foreach ($order_subscriptions as $sub_id => $sub) {
                    update_post_meta($sub_id, '_ach_stripe_customer_id', $current_stripe_customer->id);
                    update_post_meta($sub_id, '_ach_stripe_account_id', $current_stripe_customer->bank->id);
                }
            }

            // save the bank account if necessary
            self::log("Check if we need to save method: " . json_encode(array( $db_options["saved_banks"], strval(!$bank_for_saved_payment), strval(checked($_POST["wc-ach_stripe-new-payment-method"])) )));
            if (get_current_user_id() && $db_options["saved_banks"] === "yes" && !$bank_for_saved_payment && $_POST["save_account"] == 1) {
                WC_Gateway_ACH_Stripe::ach_add_payment_method(true);
            }


            self::log("Sending the response: " . json_encode(array(
                "bank" => $bank_for_saved_payment,
                "verified" => false,
                "redirection_after_checkout" => $redirection_link,
                "popup_header" => $options["popup_header"],
                "popup_text" => $options["popup_text"],
                "popup_text_waiting" => $options["popup_text_waiting"],
                "ach_stripe_token" => wc_clean($_POST["ach_stripe_token"])
            )));

            wp_send_json(array(
                "bank" => $bank_for_saved_payment,
                "verified" => false,
                "redirection_after_checkout" => $redirection_link,
                "popup_header" => $options["popup_header"],
                "popup_text" => $options["popup_text"],
                "popup_text_waiting" => $options["popup_text_waiting"],
                "ach_stripe_token" => wc_clean($_POST["ach_stripe_token"])
            ));
        }
    }

    /**
     * Get the order's transactions status
     *
     * @param int $order_id
     * @return string|boolean
     */
    public static function get_order_ach_status($order_id)
    {
        global $wpdb;
        $charge_status = get_post_meta($order_id, "_ach_stripe_charge_status", true);
        return $charge_status;
    }

    /**
     * Check if the order has a charge status - ie it is pending or completed
     *
     * @param int $order_id
     * @return boolean
     */
    public static function is_order_paid($order_id)
    {
        $status = self::get_order_ach_status($order_id);
        return $status !== false && !empty($status);
    }

    /**
     * Set order as paid for while we are waiting for charge to be completed.
     * Preventing a repeated payment
     *
     * @param array $charge_data
     * @return void
     */
    public static function set_order_as_paid($charge_data, $status) {
        global $wpdb;
        $meta_value = $charge_data['data']['object']['id'];
        $order_id = $wpdb->get_results(
            "SELECT post_id
			FROM {$wpdb->prefix}postmeta
			WHERE
				meta_key = '_ach_stripe_charge_id'
				AND
				meta_value = '{$meta_value}'"
        )[0]->post_id;

        update_post_meta($order_id, "_ach_stripe_charge_status", $status);
        $amount = intval($charge_data['data']['object']['amount']) / 100;
        $fee = $amount > 625 ? 5 : number_format($amount * 0.008, 2, '.', ' ');
        $net = number_format($amount - $fee, 2, '.', ' ');

        update_post_meta($order_id, '_ach_fees', $fee);
        update_post_meta($order_id, '_ach_net', $net);
    }

    /**
     * When payment is successfull complete the delayed order
     *
     * @param [type] $charge_data
     * @return void
     */
    public static function order_completed_from_stripe($charge_data, $trustworthy_customers)
    {
        global $wpdb;
        $meta_value = $charge_data['data']['object']['id'];
        $order_id = $wpdb->get_results(
            "SELECT post_id
			FROM {$wpdb->prefix}postmeta
			WHERE
				meta_key = '_ach_stripe_charge_id'
				AND
				meta_value = '{$meta_value}'"
        )[0]->post_id;
        if (isset($order_id) && !empty($order_id)) {
            self::log("Handling Stripe request, order ID: $order_id");
            $order = wc_get_order($order_id);

            // check if completing not fr all users
            if (!empty($trustworthy_customers) && $charge_data["type"] != "charge.succeeded") {
                $should_be_completed = false;
                foreach ($trustworthy_customers as $t_customer) {
                    if (strpos($t_customer, "@") !== false) {
                        // email
                        if ($order->get_billing_email() == $t_customer) {
                            $should_be_completed = true;
                        }
                    } else {
                        // id
                        if ($order->get_user_id() == $t_customer) {
                            $should_be_completed = true;
                        }
                    }
                }

                if (!$should_be_completed) {
                    self::log("This order will not be completed immediately cause its billing email or ID is not as same as you set!");
                    return;
                }
            }

            self::log($order);
            $order->payment_complete($meta_value);
            $changed = $order->add_order_note("Charge {$meta_value} succeeded.");
            self::log("Changed? " . json_encode($changed));
        } else {
            self::log("Can't find an order with payment $meta_value");
        }
    }

    public static function clear_recipients_to_send_no_emails($recipient, $object)
    {
        return array();
    }

    /**
     * The customer bank account might be not verified and he is not able to make
     * charges. So, we can't submit the payment form to automatically
     * create an order. We have create order by ourselves.
     *
     * Because of it, this function NOT provide charges to Stripe. Only order.
     *
     * @return void
     */
    public static function create_custom_order_to_pay_later($user_id)
    {
        global $woocommerce;

        add_filter("woocommerce_email_recipient_new_order", array( self, "clear_recipients_to_send_no_emails" ), 10, 2);
        add_filter("woocommerce_email_recipient_customer_processing_order", array( self, "clear_recipients_to_send_no_emails" ), 10, 2);
        add_filter("woocommerce_email_recipient_customer_invoice", array( self, "clear_recipients_to_send_no_emails" ), 10, 10);

        $address = array(
            'first_name' =>  wc_clean($_POST["billing_first_name"]),
            'last_name'  =>  wc_clean($_POST["billing_last_name"]),
            'company'    =>  wc_clean($_POST["billing_company"]),
            'email'      =>  wc_clean($_POST["billing_email"]),
            'phone'      =>  wc_clean($_POST["billing_phone"]),
            'address_1'  =>  wc_clean($_POST["billing_address_1"]),
            'address_2'  =>  wc_clean($_POST["billing_address_2"]),
            'city'       =>  wc_clean($_POST["billing_city"]),
            'state'      =>  wc_clean($_POST["billing_state"]),
            'postcode'   =>  wc_clean($_POST["billing_postcode"]),
            'country'    =>  wc_clean($_POST["billing_country"])
        );

        $cart_hash = md5(json_encode(wc_clean(WC()->cart->get_cart_for_session())) . WC()->cart->total);
        $posted_data = WC()->checkout()->get_posted_data();
        $available_gateways = WC()->payment_gateways->get_available_payment_gateways();

        // Now we create the order
        $order = wc_create_order();

        $order->set_billing_first_name($address["first_name"]);
        $order->set_billing_last_name($address["last_name"]);
        $order->set_billing_company($address["company"]);
        $order->set_billing_address_1($address["address_1"]);
        $order->set_billing_address_2($address["address_2"]);
        $order->set_billing_city($address["city"]);
        $order->set_billing_state($address["state"]);
        $order->set_billing_postcode($address["postcode"]);
        $order->set_billing_country($address["country"]);
        $order->set_billing_email($address["email"]);
        $order->set_billing_phone($address["phone"]);

        $order->set_customer_id($user_id);

        if (isset($_POST["shipping_first_name"]) && !empty($_POST["shipping_first_name"])) {
            $shipping_address = array(
                'first_name' =>  wc_clean($_POST["shipping_first_name"]),
                'last_name'  =>  wc_clean($_POST["shipping_last_name"]),
                'company'    =>  wc_clean($_POST["shipping_company"]),
                'email'      =>  wc_clean($_POST["shipping_email"]),
                'phone'      =>  wc_clean($_POST["shipping_phone"]),
                'address_1'  =>  wc_clean($_POST["shipping_address_1"]),
                'address_2'  =>  wc_clean($_POST["shipping_address_2"]),
                'city'       =>  wc_clean($_POST["shipping_city"]),
                'state'      =>  wc_clean($_POST["shipping_state"]),
                'postcode'   =>  wc_clean($_POST["shipping_postcode"]),
                'country'    =>  wc_clean($_POST["shipping_country"])
            );

            $order->set_address($shipping_address, 'shipping');
        } else {
            $order->set_address($address, 'shipping');
        }

        $order->set_cart_hash($cart_hash);
        $order->set_customer_id(apply_filters('woocommerce_checkout_customer_id', $user_id));
        $order->set_payment_method(isset($available_gateways[ $posted_data['payment_method'] ]) ? $available_gateways[ $posted_data['payment_method'] ]  : $posted_data['payment_method']);

        $order->set_prices_include_tax('yes' === get_option('woocommerce_prices_include_tax'));

        $order->set_customer_note(isset($data['order_comments']) ? $data['order_comments'] : '');

        $order->set_shipping_total(WC()->cart->get_shipping_total());
        $order->set_discount_total(WC()->cart->get_discount_total());
        $order->set_discount_tax(WC()->cart->get_discount_tax());
        $order->set_cart_tax(WC()->cart->get_cart_contents_tax() + WC()->cart->get_fee_tax());
        $order->set_shipping_tax(WC()->cart->get_shipping_tax());

        WC()->checkout()->create_order_line_items($order, WC()->cart);
        WC()->checkout()->create_order_fee_lines($order, WC()->cart);
        WC()->checkout()->create_order_shipping_lines($order, WC()->session->get('chosen_shipping_methods'), WC()->shipping->get_packages());
        WC()->checkout()->create_order_tax_lines($order, WC()->cart);
        WC()->checkout()->create_order_coupon_lines($order, WC()->cart);

        $order->calculate_totals();

        /**
         * Action hook to adjust order before save.
         * @since 3.0.0
         */
        do_action('woocommerce_checkout_create_order', $order, $posted_data);
        do_action('woocommerce_checkout_update_order_meta', $order->get_id(), array());

        // set order meta
        $meta = array_intersect_key($_POST, array_flip(preg_grep('/^meta_/', array_keys($_POST))));
        if (!empty($meta)) {
            foreach ($meta as $meta_key => $meta_value) {
                update_post_meta($order->get_id(), $meta_key, $meta_value);
            }
        }

        $order->update_status("Pending payment", 'Waiting for bank account verification.', true);
        $order->add_order_note("ACH Stripe: Waiting for bank account verification.");

        $trustworthy_customers_string = isset(get_option("woocommerce_ach_stripe_settings")["trustworthy_customers"]) ? get_option("woocommerce_ach_stripe_settings")["trustworthy_customers"] : "";
        $t_customers = self::trustworthy_customers_to_array($trustworthy_customers_string);

        // check if completing not fr all users
        if (!empty($t_customers)) {
            $should_be_completed = false;
            self::log("Trust customers are " . json_encode($t_customers));
            foreach ($t_customers as $t_customer) {
                if (strpos($t_customer, "@") !== false) {
                    // email
                    if ($order->get_billing_email() === $t_customer) {
                        $should_be_completed = true;
                    }
                } elseif ($order->get_user_id() != 0) {
                    // id
                    if ($order->get_user_id() === intval($t_customer)) {
                        $should_be_completed = true;
                    }
                }
            }

            if ($should_be_completed) {
                $order->update_status("processing", 'Order is processing due to trustworthy customer. But waiting for bank account verification.', true);
                $order->add_order_note("ACH Stripe: Trustworthy customer - Waiting for bank account verification.");
            }
        }

        $no_account_to_pay = isset(get_option("woocommerce_ach_stripe_settings")["no_account_to_pay"]) ? get_option("woocommerce_ach_stripe_settings")["no_account_to_pay"] === "yes" : "no";

        self::log("Check if we need money... " . $no_account_to_pay);
        if ($no_account_to_pay) {
            $order->update_status("processing", __("Changed to Processing without any guarantee of money due to enabled option in the ACH plugin.", 'woocommerce-gateway-ach-stripe'));
        }

        if (isset($_POST["order_comments"])) {
            $order->add_order_note(wc_clean($_POST["order_comments"]));
        }
        $order->set_created_via("ACH Stripe");

        if ($order->get_status() !== "processing" && $order->get_status() !== "completed") {
            update_post_meta($order->get_order_number(), "_created_via", "ACH Stripe");
        }

        self::log("Begin checking the cart for subscriptions...");
        foreach ($woocommerce->cart->get_cart() as $cart_item) {
            $product = wc_get_product($cart_item["product_id"]);
            self::log("Checking the product type for subscription... " . json_encode($product->is_type("subscription")));
            if (version_compare($woocommerce->version, "3.0", ">=")) {
                if (!$product->is_type("subscription")) {
                    continue;
                } else {
                    self::log("Yes!");
                }
            } elseif (!$product->is_type("subscription")) {
                continue;
            } else {
                self::log("Yes!");
            }

            /*$sub = wcs_create_subscription( array(
                'order_id' => $order->id,
                'billing_period' => $period,
                'billing_interval' => $interval,
                'start_date' => $woocommerce->cart->start_date,
                'customer_id' => $order->get_customer_id(),
                'customer_note' => wc_clean( $_POST["order_comments"] ),
            ) );
            self::log( "Subscription created: " . json_encode( $sub ) );

            self::log( "Adding the product with ID {$product->ID}" );
            $sub->add_product( $product, $order->get_item_meta( $product->ID, "_qty", true ) );
            self::log( "Setting billing address" );
            $sub->set_address( $address, 'billing' );
            self::log( "Setting shipping address" );
            $sub->set_address( $address, 'shipping' );

            self::log( "Calcuating totals..." );
            $totals = $sub->calculate_totals();
            self::log( "Totals = $totals" );*/

            if (class_exists("WC_Subscriptions_Manager")) {
                WC_Subscriptions_Manager::create_pending_subscription_for_order($order->get_id(), $product->get_id());

                self::log("Pending subscription created.");
            } else {
                self::log("Subscription cannot be created since the WC_Subscription_Manager class doesn't exist");
            }


            /*if( $order->get_status() == "processing" || $order->get_status() == "completed" ) {
                WC_Subscriptions_Manager::activate_subscriptions_for_order( $order );
                self::log( "Subscription activated!" );
            }*/
        }

        self::log("Order is {$order}");

        return $order;
    }

    /**
     * Make a refund in Stripe system
     *
     * @param [int] $order_id - order id
     * @param [float] $amount - amount in USD
     * @param [string] $reason - string of reason
     * @return void
     */
    public static function do_refund($order_id, $amount = null, $reason)
    {
        $refund_arr = array(
            'charge' => get_post_meta($order_id, '_ach_stripe_charge_id', true),
            "reason" => "requested_by_customer"
        );

        if (!is_null($amount)) {
            $refund_arr['amount'] = floatval($amount) * 100;
        }

        if (empty($reason)) {
            $reason = null;
        } else {
            $refund_arr['metadata'] = array( "reason_from_shop" => $reason );
        }

        $refund_result = \Stripe\Refund::create($refund_arr);
    }

    /**
     * Logs
     *
     * @param string $message
     */
    public static function log($message)
    {
        $options = get_option('woocommerce_ach_stripe_settings');

        if ('yes' === $options['logging']) {
            WC_ACH_Stripe::log($message);
        }
    }

    /**
     * Get email template
     *
     * @param object $ord - Order object
     * @return void
     */
    public static function email_template($ord)
    {
        $template = get_option("woocommerce_ach_stripe_settings")["email_template"];

        if (!$template) {
            $template = self::default_email_template();
        }

        self::$body_to_send = $template;
        self::filter_body_for_images();

        $order_pay_url = $ord->get_checkout_payment_url() . "&show_microdeps=true";
        self::$body_to_send = preg_replace("/\[order-url\]/", $order_pay_url, self::$body_to_send);

        return self::$body_to_send;
    }

    /**
     * Get default email template
     *
     * @return void
     */
    public static function default_email_template()
    {
        $standard_image_id = get_option("woocommerce_ach_standard_email_image_id");
        if ($standard_image_id !== false) {
            $standard_image = get_post($standard_image_id)->guid;
        } else {
            $standard_image = "";
        }
        ob_start(); ?>
		<p>Hi!</p>
		<p>Thank you for your order. We need to verify your bank account information.</p>
		<p>You will receive <strong>two micro withdrawals in 2-3 business days from 1 – 99 cents.</strong></p>
		<p>When you see these micro deposits <a href="[order-url]">follow this link</a>, enter your bank information and micro deposits like following:</p>
		<p style="text-align:center;"><img width="550" style="display:inline;" src="<?php echo $standard_image; ?>" /></p>
		<p>You will need to perform this process only once and then you will be able to purchase anything from our store without verification.</p><br>
		<p style="text-align:right;"><em>Thank you for being with us!</em></p>
		<?php
        $template = ob_get_clean();
        return $template;
    }

    /**
     * Insert image to body email
     *
     * @return void
     */
    public static function filter_body_for_images()
    {
        global $phpmailer;

        $body = stripslashes(wp_kses_stripslashes(get_option("woocommerce_ach_stripe_settings")["email_template"]));

        $sources = array();
        preg_match_all("/\ssrc=\"([^\"]+)\"/s", $body, $sources);

        $index = 0;

        foreach ($sources[1] as $source) {
            if (empty($source)) {
                continue;
            }

            $url = $source; //phpmailer will load this file
            $baseurl = wp_upload_dir()["baseurl"];

            $time_and_file = array();
            $time_and_file[0] = str_replace($baseurl, "", $url); // time folder and file like 2018/05/abc.png or without time if there is no such
            $time_and_file[1] = substr($time_and_file[0], strripos($time_and_file[0], "/") + 1); // only filename. Remember that the 0 element might be a filename only too

            $folder = substr($time_and_file[0], 0, strripos($time_and_file[0], "/")); // only time here or '/'
            $file = str_replace("\\", "/", wp_upload_dir()["basedir"] . "{$folder}/{$time_and_file[1]}");


            $uid = 'src' . $index; //will map it to this UID
            $name = $uid . substr($file, -4); //this will be the file name for the attachment
            $body = str_replace($url, "cid:{$uid}", $body);

            self::$body_to_send = $body;

            $index ++;
        }

        add_action('phpmailer_init', function ($phpmailer) {
            global $phpmailer;
            $phpmailer->SMTPDebug = 2;

            $body = wp_kses_stripslashes(get_option("woocommerce_ach_stripe_settings")["email_template"]);

            $sources = array();
            preg_match_all("/\ssrc=\"([^\"]+)\"/s", $body, $sources);

            $index = 0;

            foreach ($sources[1] as $source) {
                if (empty($source)) {
                    continue;
                }

                self::log("==== Mail Attachment Log ====");

                $url = $source; //phpmailer will load this file
                self::log("source: $source");
                self::log("url: $url");
                $baseurl = wp_upload_dir()["baseurl"];
                self::log("baseurl: $baseurl");

                $time_and_file = array();
                $time_and_file[0] = str_replace($baseurl, "", $url); // time folder and file like 2018/05/abc.png or without time if there is no such
                self::log("time_and_file 0: $time_and_file[0]");
                $time_and_file[1] = substr($time_and_file[0], strripos($time_and_file[0], "/") + 1); // only filename. Remember that the 0 element might be a filename only too
                self::log("time_and_file 1: $time_and_file[1]");

                $folder = substr($time_and_file[0], 0, strripos($time_and_file[0], "/")); // only time here or '/'
                self::log("folder: $folder");
                $file = str_replace("\\", "/", wp_upload_dir()["basedir"] . "{$folder}/{$time_and_file[1]}");
                self::log("file: $file");

                $uid = 'src' . $index; //will map it to this UID
                self::log("uid: $uid");
                $name = $uid . substr($file, -4); //this will be the file name for the attachment
                self::log("name: $name");

                self::log("==== END OF Mail Attachment Log ====");

                $phpmailer->AddEmbeddedImage($file, $uid, $name, "base64", "image/png");

                $index ++;
            }
        });
    }

    /**
     * Converting a string to an array
     *
     * @param string $string
     * @return void
     */
    public static function trustworthy_customers_to_array($string = "")
    {
        if (empty($string)) {
            return array();
        }

        $string = str_replace(" ", "", $string);
        $arr = explode(",", $string);

        return $arr;
    }


    /**
     * Update customer and session data from the posted checkout data.
     *
     * @since  3.0.0
     * @param  array $data
     */
    public static function update_session($data)
    {
        // Update both shipping and billing to the passed billing address first if set.
        $address_fields = array(
            'address_1',
            'address_2',
            'city',
            'postcode',
            'state',
            'country',
        );

        array_walk($address_fields, function ($field, $key, $data) {
            if (isset($data[ "billing_{$field}" ])) {
                WC()->customer->{"set_billing_{$field}"}($data[ "billing_{$field}" ]);
                WC()->customer->{"set_shipping_{$field}"}($data[ "billing_{$field}" ]);
            }

            if (isset($data[ "shipping_{$field}" ])) {
                WC()->customer->{"set_shipping_{$field}"}($data[ "shipping_{$field}" ]);
            }
        }, $data);
        WC()->customer->save();
        self::log("Session customer updated");

        // Update customer shipping and payment method to posted method
        $chosen_shipping_methods = WC()->session->get('chosen_shipping_methods');

        if (is_array($data['shipping_method'])) {
            foreach ($data['shipping_method'] as $i => $value) {
                $chosen_shipping_methods[ $i ] = $value;
            }
        }

        WC()->session->set('chosen_shipping_methods', $chosen_shipping_methods);
        WC()->session->set('chosen_payment_method', $data['payment_method']);
        WC()->cart->calculate_totals();
        self::log("Session updated");
    }



    /**
     * Assigning data from additional form fields.
     * @param  array $data
     * 
     */
    public static function process_customer($data)
    {
        $customer_id = apply_filters('woocommerce_checkout_customer_id', get_current_user_id());
        self::log(sprintf("Is customer will be created - %s, %s, %s", strval(is_user_logged_in()), strval(WC()->checkout()->is_registration_required()), strval(empty($data['createaccount']))));

        // On multisite, ensure user exists on current site, if not add them before allowing login.
        if ($customer_id && is_multisite() && is_user_logged_in() && !is_user_member_of_blog()) {
            add_user_to_blog(get_current_blog_id(), $customer_id, 'customer');
        }

        // Add customer info from other fields.
        if ($customer_id) {
            $customer = new WC_Customer($customer_id);

            if (! empty($data['billing_first_name'])) {
                $customer->set_first_name($data['billing_first_name']);
            }

            if (! empty($data['billing_last_name'])) {
                $customer->set_last_name($data['billing_last_name']);
            }

            // If the display name is an email, update to the user's full name.
            if (is_email($customer->get_display_name())) {
                $customer->set_display_name($data['billing_first_name'] . ' ' . $data['billing_last_name']);
            }

            foreach ($data as $key => $value) {
                // Use setters where available.
                if (is_callable(array( $customer, "set_{$key}" ))) {
                    $customer->{"set_{$key}"}($value);

                // Store custom fields prefixed with wither shipping_ or billing_.
                } elseif (0 === stripos($key, 'billing_') || 0 === stripos($key, 'shipping_')) {
                    $customer->update_meta_data($key, $value);
                }
            }

            /**
             * Action hook to adjust customer before save.
             * @since 3.0.0
             */
            do_action('woocommerce_checkout_update_customer', $customer, $data);

            $customer->save();
            self::log("WC customer saved.");
        }

        do_action('woocommerce_checkout_update_user_meta', $customer_id, $data);
    }

    /**
     * Validate checkout fields for unverified user because we can't use native WC validation in this case
     *
     * @return void
     */
    public static function validate_front_fields_for_not_verified_yet()
    {
        $errors = array();
        foreach ($_POST as $field => $value) {
            if (empty($value)) {
                $name = str_replace('_', ' ', $field);
                // Remove meta for custom checkout fields plugin
                $name = str_replace('meta ', ' ', $name);
                $name = ucwords($name);
                wc_add_notice(sprintf('<strong>%s</strong> is a required field.', $name), 'error');
            }
        }
        ob_start();
        woocommerce_output_all_notices();
        $msg = ob_get_clean();
        wp_send_json($msg);
    }

    /**
     * Find or create customer on site by email
     *
     * @param string $email
     * @return void
     * @throws Exception
     */
    public static function find_customer($email, $method) {
        $user_id = get_current_user_id();
        if ( $user_id ) {
            self::log('(1)Site user has ID: ' . $user_id);
            $stripe_customer_id = get_user_meta($user_id, $method, true);
            if( $stripe_customer_id ) {
                self::log('(2)Site user has Stripe customer ID: ' . $stripe_customer_id);
                return [
                    'stripe_customer' => self::get_stripe_customer($stripe_customer_id, $user_id, $method),
                    'user_id' => $user_id
                ];
            }
            $stripe_customer_id = self::create_stripe_customer($email, $user_id, $method);
            self::log('(3)Site user has Stripe customer ID: ' . $stripe_customer_id);
        } else {
            $user = get_user_by('email', $email);
            if(!$user) {
                $username = '';
                $password = '';
                if(isset($_POST['createaccount'])) 
                    $username = wc_clean($_POST['account_username']);
                else 
                    $username = strstr($email, '@', true) . '-' . wp_generate_password( 3, false );
                
                if(isset($_POST['createaccount']))  
                    $password = wc_clean($_POST['account_password']);
                else
                    $password = wp_generate_password(10, false);
                
                $username = apply_filters('ach_stripe_registration_username', $username);
                $user_id = wp_create_user($username, $password, $email);
                if (is_wp_error($user_id)) {
                    throw new Exception($user_id->get_error_message());
                }
                    
                $user = new WP_User( $user_id );
                $user->set_role( 'customer' );

                self::send_email_about_registration( $email, $username, $password );
                if(isset($_POST['createaccount']) && $_POST['createaccount']) {
                    wp_set_current_user($user_id);
                    wc_set_customer_auth_cookie($user_id);

                    // As we are now logged in, checkout will need to refresh to show logged in data
                    WC()->session->set('reload_checkout', true);

                    // Also, recalculate cart totals to reveal any role-based discounts that were unavailable before registering
                    WC()->cart->calculate_totals();
                }
                self::log('(4)Site user has ID: ' . $user_id);
                $stripe_customer_id = self::create_stripe_customer($email, $user_id, $method);
                self::log('(5)Site user has Stripe customer ID: ' . $stripe_customer_id);
            } else {
                $user_id = $user->ID;
                $stripe_customer_id = get_user_meta($user_id, $method, true);
                self::log('(6)Site user has ID: ' . $user_id);
                
                if( $stripe_customer_id ) {
                    self::log('(7)Site user has Stripe customer ID: ' . $stripe_customer_id);
                    return [
                        'stripe_customer' => self::get_stripe_customer($stripe_customer_id, $user_id, $method),
                        'user_id' => $user_id
                    ];
                }
                
                $stripe_customer_id = self::create_stripe_customer($email, $user_id, $method);
                self::log('(8)Site user has Stripe customer ID: ' . $stripe_customer_id);
            }
        }
        return [
            'stripe_customer' => self::get_stripe_customer($stripe_customer_id, $user_id, $method),
            'user_id' => $user_id
        ];
    }

    /**
     * Create Stripe customer for site user
     *
     * @param string $email
     * @param int $user_id
     * @return void
     */
    public static function create_stripe_customer($email, $user_id, $method) {
        $first_name = wc_clean($_POST["billing_first_name"]);
        $last_name = wc_clean($_POST["billing_last_name"]);
        self::log("Customer name is {$first_name} {$last_name}");

        try {
            $new_stripe_customer = \Stripe\Customer::create([
                "description" => "{$first_name} {$last_name}",
                "email" => $email
            ]);
            update_user_meta( $user_id, $method, $new_stripe_customer->id );
        } catch (Exception $e) {
            self::log($e->getMessage());
            wp_send_json([
                "verified" => "fail",
                "msg" => $e->getMessage()
            ]);
            exit;
        }
        self::log("New customer created!");
        return $new_stripe_customer->id;
    }

    /**
     * Get Stripe customer for site user by ID Stripe customer and ID user
     *
     * @param string $customer_stripe_id
     * @param int $user_id
     * @return void
     */
    public static function get_stripe_customer( $customer_stripe_id, $user_id, $method ) {
        $stripe_customer = null;
        if (!empty($customer_stripe_id)) {
            try {
                // first let's find previous Stripe orders of this customer
                
                $stripe_customer = \Stripe\Customer::retrieve($customer_stripe_id);
                if (isset($stripe_customer->deleted) && $stripe_customer->deleted) {
                    self::log('The user account in Stripe has been deleted.');
                    if (!empty($user_id)) {
                        $stripe_customer = self::create_stripe_customer($email, $user_id, $method);
                        self::log('Added a new user account in Stripe.');
                        $stripe_customer = \Stripe\Customer::retrieve($stripe_customer);
                    }
                }
            } catch (Exception $e) {
                self::log($e->getMessage());
                wp_send_json(array(
                    "verified" => "fail",
                    "msg" => __("Payment failed due to server-side error, please contact the website admin.")
                ));
            }
        }
        if($stripe_customer === null) {
            self::log('Don\'t find a bank account in Stripe'); 
        }
        return $stripe_customer;
    }

    public static function send_email_about_registration( $email, $user_name, $random_password ) {
        $blogname = get_option("woocommerce_email_from_name");
        $subject = 'Welcome to ' . $blogname;
        $headers = "From: $blogname <" . get_option("woocommerce_email_from_address") . "> \n\n";

        $mailer = WC()->mailer();
        
        $redirect_url = get_permalink( wc_get_page_id( 'myaccount' ) );
        $message =  "Your username is <b>$user_name</b>. \r\n" .
                    "Your password is <b>$random_password</b>. \r\n" .
                    "You can access your account area to view orders, change your password, and more at: $redirect_url" ; 
        

        $content = self::get_custom_email_html( $user_name, $subject, $mailer, $message );
        $headers .= "Content-Type: text/html\r\n";
            
        $mailer->send( $email, $subject, $content, $headers );
    }

    public static function get_custom_email_html( $login, $heading = true, $mailer , $message = '') {
	     
        $template = 'email/custome_registration.php';
     
        return wc_get_template_html( $template, array(
            'login'         => $login,
            'email_heading' => $heading,
            'sent_to_admin' => false,
            'plain_text'    => false,
            'email'         => $mailer,
            'message'       => $message
        ), '', WC_ACH_STRIPE_PATH  );
    }

    public function get_link_token() {
        $mode = isset( $_POST['mode'] ) ?  $_POST['mode'] : 'sandbox';
        if( isset( $_POST['user_id'] ) && $_POST['user_id'] != 'false' ) {
            $user_id = $_POST['user_id'];
            $new_user = false;
        } else {
            if( is_user_logged_in( ) ) {
                $user_id = get_current_user_id( );
                $new_user = false;
            } else {
                $user_id = rand( 0, 100 ) . time( );
                $new_user = $user_id;
            }
        }
        $plaid_options = get_option( 'woocommerce_plaid_settings' );
		$data = array(
			"client_id" => $plaid_options['plaid_client_id'],
			"secret" => $plaid_options['plaid_secret'],
			"client_name" => $plaid_options['company'],
			"user" => array(
				"client_user_id" =>  $user_id
			),
			"products" => ["transactions"],
			"country_codes" => ["US"],
			"language" => "en",
		);

		$data = json_encode( $data );

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://$mode.plaid.com/link/token/create",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $data,
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json"
			),
		));

		$response = curl_exec( $curl );
        $response = json_decode( $response );
        curl_close($curl);
		wp_send_json( array( "link_token" => $response->link_token, 'new_user' => $new_user ) );
	}
}