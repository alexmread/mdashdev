<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WC_Gateway_Stripe class.
 *
 * @extends WC_Payment_Gateway
 */
class WC_Gateway_ACH_Stripe extends WC_Payment_Gateway {

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->id                   = 'ach_stripe';
		$this->method_title         = __( 'ACH Stripe', 'woocommerce-gateway-ach-stripe' );
		$this->method_description   = __( 'It is highly recommended to email your customers while ordering and leave a contact information of your shop in accordance with <a href="https://support.stripe.com/questions/accepting-ach-payments-with-stripe#ach-authorization" target="_blank">ACH Authorization</a>', 'woocommerce-gateway-ach-stripe' );
		$this->has_fields           = true;
		$this->email_template 		= !empty( $this->get_option( 'email_template' ) ) && !is_null( $this->get_option( 'email_template' ) ) ? $this->get_option( 'email_template' ) : WC_ACH_Stripe_API::default_email_template();

		// Load the form fields
		$this->init_form_fields();

		// Load the settings.
		$this->init_settings();

		// Get setting values.
		$this->title                  = $this->get_option( 'title' );
		$this->description            = $this->get_option( 'description' );
		$this->company_name           = $this->get_option( 'company_name' );
		$this->enabled                = $this->get_option( 'enabled' );
		$this->testmode               = 'yes' === $this->get_option( 'testmode' );
		$this->secret_key             = $this->testmode ? $this->get_option( 'test_secret_key' ) : $this->get_option( 'secret_key' );
		$this->publishable_key        = $this->testmode ? $this->get_option( 'test_publishable_key' ) : $this->get_option( 'publishable_key' );
		$this->process_on_pending     = $this->get_option( 'process_on_pending' );
		$this->saved_banks            = 'yes' === $this->get_option( 'saved_banks' );
		$this->pay_for_order_filter   = 'yes' === $this->get_option( 'pay_for_order_filter' );
		$this->logging                = 'yes' === $this->get_option( 'logging' );
		$this->no_account_to_pay      = 'yes' === $this->get_option( 'no_account_to_pay' );
        $this->order_button_text = isset( $_GET['pay_for_order'] ) && isset( $_GET['key'] ) && isset( $_GET["show_microdeps"] ) ? __( 'Continue', 'woocommerce-gateway-ach-stripe' ) : __( 'Place order', 'woocommerce-gateway-ach-stripe' );
		$this->redirect_to_confirmation = 'yes' === $this->get_option( 'redirect_to_confirmation' );
		$this->supports = array(
			'subscriptions',
			'products',
			'refunds',
			'subscription_cancellation',
			'subscription_reactivation',
			'subscription_suspension',
			'subscription_amount_changes',
			'subscription_payment_method_change', // Subs 1.n compatibility.
			'subscription_payment_method_change_customer',
			'subscription_payment_method_change_admin',
			'subscription_date_changes',
			'multiple_subscriptions',
			'tokenization',
			'add_payment_method',
		);

		if ( $this->testmode ) {
			$this->description .= ' ' . sprintf( __( 'TEST MODE ENABLED.</br><strong>Routing Number</strong> 110000000</br><strong>Acc Number</strong> 000123456789</br> and any name</br> For more info check "<a href="%s">Testing ACH Stripe</a>".', 'woocommerce-gateway-ach-stripe' ), 'https://stripe.com/docs/ach#testing-ach' );
			$this->description  = trim( $this->description );
		}

		//WC_ACH_Stripe_API::set_api_init( WC_ACH_Stripe_API::get_api_init() );
		//WC_ACH_Stripe_API::set_microdeposit( $this->testmode );

		// Hooks
		add_action( 'wp_enqueue_scripts', array( $this, 'payment_scripts' ), 9000 );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
		add_filter( 'woocommerce_payment_methods_list_item', array( $this, "get_account_saved_payment_methods_list_item" ), 10, 2 );
		add_filter( "woocommerce_payment_gateway_get_saved_payment_method_option_html", array( $this, "confirm_account_input" ), 10, 3 );
	}


	/**
	 * Initialise Gateway Settings Form Fields
	 */
	public function init_form_fields() {
		$default_redirection = get_permalink( wc_get_page_id( 'shop' ) );

        $this->form_fields = array(
            'enabled' => array(
                'title'       => __( 'Enable/Disable', 'woocommerce-gateway-ach-stripe' ),
                'label'       => __( 'Enable ACH Stripe', 'woocommerce-gateway-ach-stripe' ),
                'type'        => 'checkbox',
                'description' => '',
                'default'     => 'no',
            ),
            'title' => array(
                'title'       => __( 'Title', 'woocommerce-gateway-ach-stripe' ),
                'type'        => 'text',
                'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce-gateway-ach-stripe' ),
                'default'     => __( 'Bank Account', 'woocommerce-gateway-ach-stripe' ),
                'desc_tip'    => true,
            ),
            'description' => array(
                'title'       => __( 'Description', 'woocommerce-gateway-ach-stripe' ),
                'type'        => 'text',
                'description' => __( 'This controls the description which the user sees during checkout.', 'woocommerce-gateway-ach-stripe' ),
                'default'     => __( 'Pay directly from your bank account via Stripe.', 'woocommerce-gateway-ach-stripe' ),
                'desc_tip'    => true,
            ),
            'company_name' => array(
                'title'       => __( 'Company Name', 'woocommerce-gateway-ach-stripe' ),
                'type'        => 'text',
                'description' => __( 'For customers to know what company they authorize for a payment.', 'woocommerce-gateway-ach-stripe' ),
                'default'     => __( get_bloginfo( "name" ), 'woocommerce-gateway-ach-stripe' ),
                'desc_tip'    => true,
            ),
            'process_on_pending' => array(
                'title'       => __( 'Process orders immediately', 'woocommerce-gateway-ach-stripe' ),
				'label'       => __( 'Don\'t wait for Stripe charge beign completed', 'woocommerce-gateway-ach-stripe' ),
                'type'        => 'checkbox',
                'description' =>  __( 'only for these customers (user IDs or emails separated by a comma):', 'woocommerce-gateway-ach-stripe' ),
                'default'     => 'no',
            ),
            'trustworthy_customers' => array(
                'title'       => "",
				'label'       => __( 'Only for these customers', 'woocommerce-gateway-ach-stripe' ),
                'type'        => 'text',
                'description' => __( '<strong>IT IS HIGHLY RECOMMENDED TO READ DOCS!</strong><br><br>When the <strong>verified customer</strong> makes an order in your shop a charge is automatically created in your Stripe dashboard.<br>
				This charge has "Pending" status and it means that the money will be on you account approximately in 7 days.<br>
				By default, the plugin change an orders\' statuses after you received the money, i.e. in 7 days after purchase.<br>
				Check this option to process orders right after they were made to make your customer doesn\'t wait for 7 days.<br>
				But remember that there is no money for this order right now.', 'woocommerce-gateway-ach-stripe' ),
				'placeholder' => __( "IDs or emails separated by a comma...", 'woocommerce-gateway-ach-stripe' ),
			),

			// Stripe settings

			'stripe_section' => array(
				'title'       => __( 'Stripe API Settings', 'woocommerce-gateway-ach-stripe' ),
				'type'        => 'title',
				'class' 	  => 'ach-subtitle',
			),

            'testmode' => array(
                'title'       => __( 'Test mode', 'woocommerce-gateway-ach-stripe' ),
                'label'       => __( 'Enable Test Mode', 'woocommerce-gateway-ach-stripe' ),
                'type'        => 'checkbox',
                'description' => __( '<strong>NOTE.</strong></br>If you are using the Stripe plugin together with this one, please keep them in the same mode!', 'woocommerce-gateway-ach-stripe' ),
                'default'     => 'yes',
                'desc_tip'    => false,
            ),
            'test_publishable_key' => array(
                'title'       => __( 'Stripe Test Publishable Key', 'woocommerce-gateway-ach-stripe' ),
                'type'        => 'text',
                'description' => __( 'Get your API keys from your stripe account.', 'woocommerce-gateway-ach-stripe' ),
                'default'     => '',
                'desc_tip'    => true,
            ),
            'test_secret_key' => array(
                'title'       => __( 'Stripe Test Secret Key', 'woocommerce-gateway-ach-stripe' ),
                'type'        => 'password',
                'description' => __( 'Get your API keys from your stripe account.', 'woocommerce-gateway-ach-stripe' ),
                'default'     => '',
                'desc_tip'    => true,
            ),
			'publishable_key' => array(
				'title'       => __( 'Stripe Live Publishable Key', 'woocommerce-gateway-ach-stripe' ),
				'type'        => 'text',
				'description' => __( 'Get your API keys from your stripe account.', 'woocommerce-gateway-ach-stripe' ),
				'default'     => '',
				'desc_tip'    => true,
			),
			'secret_key' => array(
				'title'       => __( 'Stripe Live Secret Key', 'woocommerce-gateway-ach-stripe' ),
				'type'        => 'password',
				'description' => __( 'Get your API keys from your stripe account.', 'woocommerce-gateway-ach-stripe' ),
				'default'     => '',
				'desc_tip'    => true,
			),
			'api_ver' => array(
				'title'       => __( 'Stripe API Version', 'woocommerce-gateway-ach-stripe' ),
				'type'        => 'text',
				'description' => __( 'Please, use the API version that is less than 18 months old in the plugin and <a href="https://dashboard.stripe.com" target="_blank">your Dashboard</a>. You can find it <a href="https://stripe.com/docs/api/versioning" target="_blank">here</a>.', 'woocommerce-gateway-ach-stripe' ),
				'default'     => '2018-11-08',
				'desc_tip'    => false,
			),

			'after_checkout_redirection' => array(
				'title'       => __( 'Redirection after checkout', 'woocommerce-gateway-ach-stripe' ),
				'type'        => 'text',
				'description' => __( "The redirection address where the customer will be placed if he needs his bank account to be verified. <code><span>{$default_redirection}</span></code> by default.", 'woocommerce-gateway-ach-stripe' ),
				'default'     => WC_ACH_Stripe::$ajax_response_standard_values["after_checkout_redirection"],
				'placeholder' => "The format is: /my-page/my-subpage/",
				'desc_tip'    => false,
			),
			'redirect_to_confirmation' => array(
                'title'       => __( 'Redirect to Order Confirmation page', 'woocommerce-gateway-ach-stripe' ),
                'label'       => __( 'Redirect to Order Confirmation page', 'woocommerce-gateway-ach-stripe' ),
                'type'        => 'checkbox',
                'description' => __( 'Redirect customer to order confirmation page even if he doesn\'t have a verified bank account.', 'woocommerce-gateway-ach-stripe' ),
                'default'     => 'no',
                'desc_tip'    => false,
            ),

			// Verification Popup

			'popup_section' => array(
				'title'       => __( 'Verification Popup', 'woocommerce-gateway-ach-stripe' ),
				'type'        => 'title',
				'class' 	  => 'ach-subtitle',
			),

			'popup_header' => array(
				'title'       => __( 'Verification popup header', 'woocommerce-gateway-ach-stripe' ),
				'type'        => 'text',
				'description' => __( "The header of the popup appeared when customer needs its bank account to be verified.", "woocommerce-gateway-ach-stripe" ),
				'default'     => WC_ACH_Stripe::$ajax_response_standard_values["popup_header"],
				'placeholder' => "Enter the header...",
				'desc_tip'    => false,
			),
			'popup_text' => array(
				'title'       => __( 'Verification popup text', 'woocommerce-gateway-ach-stripe' ),
				'type'        => 'text',
				'description' => __( "The text of the popup appeared only in the first time.", "woocommerce-gateway-ach-stripe" ),
				'default'     => WC_ACH_Stripe::$ajax_response_standard_values["popup_text"],
				'placeholder' => "Enter the text...",
				'desc_tip'    => false,
			),
			'popup_text_waiting' => array(
				'title'       => __( 'Verification popup text for waiting', 'woocommerce-gateway-ach-stripe' ),
				'type'        => 'text',
				'description' => __( "The text of the popup user sees in later times. It says that the current purchase requires the verification too and he should wait for the microdeposits.", "woocommerce-gateway-ach-stripe" ),
				'default'     => WC_ACH_Stripe::$ajax_response_standard_values["popup_text_waiting"],
				'placeholder' => "Enter the text...",
				'desc_tip'    => false,
			),

			// Other

			'other_section' => array(
				'title'       => __( 'Other Useful Settings', 'woocommerce-gateway-ach-stripe' ),
				'type'        => 'title',
				'class' 	  => 'ach-subtitle',
			),

			'saved_banks' => array(
				'title'       => __( 'Saved Bank Accounts', 'woocommerce-gateway-ach-stripe' ),
				'label'       => __( 'Enable payments via Saved Bank Accounts', 'woocommerce-gateway-ach-stripe' ),
				'type'        => 'checkbox',
				'description' => __( 'If enabled, users will be able to pay with a saved bank account during checkout. Bank account data is saved on Stripe servers, not on your store.', 'woocommerce-gateway-ach-stripe' ),
				'default'     => 'no',
				'desc_tip'    => true,
			),
			'pay_for_order_filter' => array(
				'title'       => __( '`Pay for Order` Filter', 'woocommerce-gateway-ach-stripe' ),
				'label'       => __( 'Let the plugin filter the `Pay for Order` page gateways', 'woocommerce-gateway-ach-stripe' ),
				'type'        => 'checkbox',
				'description' => __( 'If enabled, the plugin will filter gateways that the customer can pay for the order with - if the order was created with the ACH than it will be the only gateway the customer can use.<br>
				In other cases, the customer will not see this gateway.<br>
				<strong>Disable this option if you don\'t see the ACH gateway where you expect to see it!</strong>', 'woocommerce-gateway-ach-stripe' ),
				'default'     => 'yes',
				'desc_tip'    => false,
			),
			'logging' => array(
				'title'       => __( 'Logging', 'woocommerce-gateway-ach-stripe' ),
				'label'       => __( 'Log debug messages', 'woocommerce-gateway-ach-stripe' ),
				'type'        => 'checkbox',
				'description' => __( 'Save debug messages to the WooCommerce System Status log.', 'woocommerce-gateway-ach-stripe' ),
				'default'     => 'no',
				'desc_tip'    => true,
			),
			'email_template' => array(
				'title'       => __( 'Email template', 'woocommerce-gateway-ach-stripe' ),
				'type'        => 'textarea',
				'description' => __( 'Verification email template. You can write your own verification instructions. Use [order-url] shortcode to put the payment url necessarily! For ex.: <code><span><</span>a href="[order-url]">Pay now</a<span>></span></code>', 'woocommerce-gateway-ach-stripe' ),
				'default'     => WC_ACH_Stripe_API::default_email_template(),
				'desc_tip'    => false,
			),
			'no_account_to_pay' => array(
				'title'       => __( 'Pay without a Bank Account', 'woocommerce-gateway-ach-stripe' ),
				'label'       => __( 'READ THE DESCRIPTION VERY ATTENTIVELY!', 'woocommerce-gateway-ach-stripe' ),
				'type'        => 'checkbox',
				'description' => __( '<strong style="color:#dc3545;">WE DON\'T RECOMMEND TO ENABLE THIS OPTION! YOU DO THIS AT YOUR OWN RISK!</strong><br>
				When a customer makes a purchase at your website in the first time he doesn\'t have a bank account in your Stripe Dashboard.<br>
				We create this bank account and make a request to verify it. But it can\'t be done immediately. And so the customer just has an order that is on-hold.</br>
				It means that you have to wait until the customer has not verified his account in 5-7 days.<br>
				When the verification happens the charge will be created in the Stripe and will be completed in 1-2 days. And after this the will begin to process an order.<br>
				Well, enable this option if want to process orders immediately.<br>
				But remember <strong>YOU WILL PROCESS ORDERS FACTICALY WITHOUT A BANK ACCOUNT AND ANY GUARANTEE OF MONEY!</strong>
				', 'woocommerce-gateway-ach-stripe' ),
				'default'     => 'no',
				'desc_tip'    => false,
			),
        );
	}

	/**
	 * @override WC_Settings_API
	 * Generate Textarea HTML.
	 *
	 * @param  mixed $key
	 * @param  mixed $data
	 * @since  1.0.0
	 * @return string
	 */
	public function generate_textarea_html( $key, $data ) {
		$field_key = $this->get_field_key( $key );
		$defaults  = array(
			'title'             => '',
			'disabled'          => false,
			'class'             => '',
			'css'               => '',
			'placeholder'       => '',
			'type'              => 'text',
			'desc_tip'          => false,
			'description'       => '',
			'custom_attributes' => array(),
		);

		$data = wp_parse_args( $data, $defaults );

		ob_start();
		?>
		<tr valign="top">
			<th scope="row" class="titledesc">
				<?php echo $this->get_tooltip_html( $data ); ?>
				<label for="<?php echo esc_attr( $field_key ); ?>"><?php echo wp_kses_post( $data['title'] ); ?></label>
			</th>
			<td class="forminp">
				<fieldset>
					<legend class="screen-reader-text"><span><?php echo wp_kses_post( $data['title'] ); ?></span></legend>
					<?php if( $key != "email_template" ) : ?>
						<textarea rows="3" cols="20" class="input-text wide-input <?php echo esc_attr( $data['class'] ); ?>" type="<?php echo esc_attr( $data['type'] ); ?>" name="<?php echo esc_attr( $field_key ); ?>" id="<?php echo esc_attr( $field_key ); ?>" style="<?php echo esc_attr( $data['css'] ); ?>" placeholder="<?php echo esc_attr( $data['placeholder'] ); ?>" <?php disabled( $data['disabled'], true ); ?> <?php echo $this->get_custom_attribute_html( $data ); ?>><?php echo esc_textarea( $this->get_option( $key ) ); ?></textarea>
					<?php else : ?>
						<?php wp_editor( $this->email_template, "ach_email_template", array(
							'textarea_name' => esc_attr( $field_key ),
							'textarea_id' => esc_attr( $field_key )
							) ); ?>
					<?php endif; ?>
					<?php echo $this->get_description_html( $data ); ?>
				</fieldset>
			</td>
		</tr>
		<?php

		return ob_get_clean();
	}

	/**
	 * Payment form on checkout page
	 */
	public function payment_fields() {
		// if the website is not HTTPS then prevent showing fields or show message in test mode
		if( !isset( $_SERVER["HTTPS"] ) ) {
			if( $this->testmode ) {
				echo '<p class="woocommerce-message">You should enable HTTPS on your website to serve live payments!</p>';
			} else {
				echo '<p class="woocommerce-error">HTTPS is not enabled. To serve payment, please enable HTTPS on your website.</p>';
				return;
			}
		}

		$display_tokenization = $this->supports( 'tokenization' ) && is_checkout() && $this->saved_banks;

		if ( $display_tokenization ) {
			$this->tokenization_script();
			$this->saved_payment_methods();
		}
		?>
		<fieldset id="ach-payment-form" class="ach-stripe-payment-fields" data-verified="true">
			<?php
			if ( $this->description ) {
				echo wpautop( wp_kses_post( $this->description ) );
			}

			?>
			<p class="form-row form-row-wide">
				<label for="ach-country">
					Country
					<select id="ach-country" name="wc-ach-stripe-country">
						<option value="US" selected>USA</option>
					</select>
				</label>
				<label for="ach-currency">
					Currency
					<select id="ach-currency" name="wc-ach-stripe-currency">
						<option value="USD" selected>USD</option>
					</select>
				</label>
				<label for="ach-routing-number">
					Bank Routing Number
					<input type="text" id="ach-routing-number" name="wc-ach-stripe-routing-number" />
				</label>
				<label for="ach-account-number">
					Bank Account Number
					<input type="text" id="ach-account-number" name="wc-ach-stripe-account-number" />
				</label>
				<label for="ach-account-holder-name">
					Holder Name
					<input type="text" id="ach-account-holder-name" name="wc-ach-stripe-account-holder-name" />
				</label>
				<label for="ach-account-holder-type">
					Holder Type
					<select id="ach-account-holder-type" name="wc-ach-stripe-account-holder-type">
						<option value="individual" selected>Individual</option>
						<option value="company">Company</option>
					</select>
				</label>
				<br>
				<label for="ach-auth">
					<input type="checkbox" id="ach-auth" name="wc-ach-stripe-auth" />
					I authorize <?php echo $this->company_name; ?> to electronically debit my account and, if necessary, electronically credit my account to correct erroneous debits.
				</label>
			</p>
			<?php

			if ( $display_tokenization && ! is_add_payment_method_page() && ! isset( $_GET['change_payment_method'] ) ) {
				$this->save_payment_method_checkbox();
			}
			?>
	</fieldset>
	<?php
		// powered by stripe
		echo '<img src="' . plugin_dir_url( __DIR__ ) . "assets/img/powered_by_stripe.png" . '" style="display: block;float: none;margin: 0 auto;">';
	}

	/**
	 * Display and input field to enter the Account Number for saved payment methods
	 * Because we can't store it and it is need to create a Stripe token
	 *
	 * @param [type] $html
	 * @param [type] $token
	 * @param [type] $gateway
	 * @return void
	 */
	public function confirm_account_input( $html, $token, $gateway ) {
		if( $gateway->id !== "ach_stripe" ) {
			return $html;
		}

		$label = __( "We are not storing your account data due to security reasons. Please, confirm your <strong>Account Number</strong>" );
		$html .= sprintf( '<label class="wc-ach-saved-account-notice" for="ach-account-number-%1$s">%2$s<input type="text" id="ach-account-number-%1$s" name="wc-ach-stripe-account-number-%1$s"></label>', $token->get_id(), $label );

		return $html;
	}

	/**
	 * Check if the gateway is available for use.
	 * @override
	 *
	 * @return bool
	 */
	public function is_available() {
		global $wp_query;

		$is_available = parent::is_available();

		if( !isset( $wp_query->query["order-pay"] ) )
			return $is_available;

		$payment_method = get_post_meta( $wp_query->query["order-pay"], "_payment_method", true );
		if( $this->pay_for_order_filter && $payment_method != $this->id ) {
			return false;
		}

		return $is_available;
	}

	/**
	 * Add payment method via account screen.
	 * We don't store the token locally, but to the Stripe API.
	 *
	 * $return means that we are calling this function NOT from AJAX
	 */
	public static function ach_add_payment_method( $return = false ) {

		$settings = get_option( "woocommerce_ach_stripe_settings" );
		$testmode = "yes" === $settings["testmode"];

		WC_ACH_Stripe::log( "Begin adding a payment method..." );

		$routing = $_POST["routing_number"];
		$account = $_POST["account_number"];
		$country = $_POST["country"];
		$currency = $_POST["currency"];
		$holder = $_POST["account_holder_name"];
		$holder_type = $_POST["account_holder_type"];
		$token = $_POST["ach_stripe_token"];
		$email = $_POST["billing_email"];

		$last4 = substr( wc_clean( $account ), -4, 4 );

		//check if there is alerady saved account
		// later you can add the Routing Number check along with last4
		WC_ACH_Stripe::log( "Check if this method was added before..." );
		$tokens = WC_Payment_Tokens::get_customer_tokens( get_current_user_id(), 'ach_stripe' );
		foreach( $tokens as $t ) {
			if( $t->get_last4() === $last4 ) {
				WC_ACH_Stripe::log( "It was added. Exitting." );
				if( $return ) return;

				wp_send_json( array(
					'result'   => 'success',
					'redirect' => wc_get_endpoint_url( 'my-account/payment-methods' ),
				) );
			}
		}

		// check if there is a Stripe customer attached to WP user
		$user = get_userdata( get_current_user_id() );
		$user_id = $user->ID;
		$mode = $testmode ? "_test" : "";

		$attached_customer_id = get_user_meta( $user_id, "_ach_stripe_customer_id", true );

		// then check if there is a customer in Stripe
		if( !isset( $attached_customer_id ) || empty( $attached_customer_id ) || !$attached_customer_id ) {
			WC_ACH_Stripe::log( "Looking for a customer in Stripe..." );
			$find_customer = WC_ACH_Stripe_API::get_stripe_customer($attached_customer_id, $user_id, '_ach_stripe_customer_id');
			foreach( $find_customer->sources->data as $bank ) {
				if( $last4 == $bank->last4 ) {
					$attached_customer_id = $find_customer->id;
					update_user_meta( $user_id, "_ach_stripe_customer_id", $attached_customer_id );
					WC_ACH_Stripe::log( "Got a customer {$attached_customer_id}" );
				}
			}

			if (!is_null($find_customer) && !$attached_customer_id) {
				// we've found a customer but it doesn't contain required sources
				// add a new source
				$new_bank = \Stripe\Customer::createSource($find_customer->id, array(
					'source' => $token
				));

				$attached_customer_id = $find_customer->id;
				update_user_meta( $user_id, "_ach_stripe_customer_id", $attached_customer_id );
				WC_ACH_Stripe::log( "Got a customer {$attached_customer_id} with a created source" );
			}
		}

		if( !isset( $attached_customer_id ) || empty( $attached_customer_id ) || !$attached_customer_id ) {
			// if no then create it
			try {
				WC_ACH_Stripe::log( "Creating customer in Stripe..." );
				$customer = \Stripe\Customer::create( array(
					"source" => $token,
					"description" => $holder,
					"email" => $user->get( "user_email" )
					)
				);
				$attached_customer_id = $customer->id;
				WC_ACH_Stripe::log( "Created and saved {$attached_customer_id}!" );
				update_user_meta( $user_id, "_ach_stripe_customer_id", $attached_customer_id );
			} catch( Exception $e ) {
				WC_ACH_Stripe::log( "Creation failed due to: " . $e->getMessage() );

				if( $return ) {
					return;
				}

				wp_send_json( array(
					'result'   => 'failure',
					'redirect' => false,
					"msg" => $e->getMessage()
				) );
			}
		}
		update_user_meta( $user_id, "_ach_stripe_saved_last4", $last4 );
		update_user_meta( $user_id, "_ach_stripe_saved_routing", $routing );
		$result = false;

		// Add token to WooCommerce
		if ( $user_id > 0 && class_exists( 'WC_Payment_Token_ACH' ) ) {
			WC_ACH_Stripe::log( "Saving the ACH payment method for user {$user_id}..." );
			$token = new WC_Payment_Token_ACH();
			$token->set_token( $user_id );
			$token->set_gateway_id( "ach_stripe" );
			$token->set_last4( $last4 );
			$token->set_routing( $routing );
			$token->set_user_id( $user_id );
			$result = $token->save();
			WC_ACH_Stripe::log( "Saved. " . $result );
		}

		if( $return ) {
			return;
		}

		if( !$result ) {
			wp_send_json( array(
				'result'   => 'failure',
				'redirect' => false,
				"msg" => "Unexpected error while saving the payment method!"
			) );
		}

		wp_send_json( array(
			'result'   => 'success',
			'redirect' => wc_get_endpoint_url( 'my-account/payment-methods' ),
		) );
	}

	/**
	 * Controls the output for ACH on the my account page.
	 *
	 * @since 2.6
	 * @param  array             $item         Individual list item from woocommerce_saved_payment_methods_list
	 * @param  WC_Payment_Token $payment_token The payment token associated with this method entry
	 * @return array                           Filtered item
	 */
	public function get_account_saved_payment_methods_list_item( $item, $payment_token ) {
		if ( "ACH" !== $payment_token->get_type() ) {
			return $item;
		}
		$last4 = $payment_token->get_last4();
		$item['method']['brand'] = esc_html__( sprintf( 'Bank account ending in %s', $last4 ), 'woocommerce-gateway-ach-stripe' );

		return $item;
	}

	/**
	 * Enqueue scripts into admin panel
	 *
	 * @return void
	 */
	public function admin_scripts() {
		wp_enqueue_style( 'ach-stripe-admin-css', plugins_url( 'assets/css/admin.css', WC_ACH_STRIPE_ROOT ) );
		wp_enqueue_script( 'ach-stripe-admin', plugins_url( 'assets/js/admin.js', WC_ACH_STRIPE_ROOT ), array( 'jquery' ), '0.0', true );
		wp_localize_script( "ach-stripe-admin", "ext_data", array(
			"endpoint" => home_url( "/wp-json/ach/v1/charge" )
		) );
	}

	/**
	 * Load and handle the scripts.
	 *
	 * @return void
	 */
	public function payment_scripts() {
		global $wp_scripts;
		$gateways = WC()->payment_gateways->get_available_payment_gateways();
		$ach_stripe_script_name = "stripe";

		if( !wp_script_is( "stripe", "enqueued" ) || stripos( $wp_scripts->registered["stripe"]->src, "v3" ) !== false ) {
			$ach_stripe_script_name = "stripev2";
			wp_enqueue_script( $ach_stripe_script_name, 'https://js.stripe.com/v2/', '', '1.0', true );
		}

		wp_enqueue_style( 'jquery-migrate', '//code.jquery.com/jquery-migrate-3.0.0.min.js' );
		wp_enqueue_style( 'jquery-ui-theme', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css' );
		wp_enqueue_style( 'wc_ach_stripe_front', plugins_url( 'assets/css/front.css', WC_ACH_STRIPE_ROOT ) );
		wp_enqueue_script( 'dialog-polyfill', plugins_url( 'assets/js/dialog-polyfill.js', WC_ACH_STRIPE_ROOT ));
		wp_enqueue_script( 'woocommerce_ach_stripe', plugins_url( 'assets/js/ach-stripe.js', WC_ACH_STRIPE_ROOT ), array( 'jquery-payment', $ach_stripe_script_name, 'plaid', 'dialog-polyfill' ), '1.5.3', true );
		wp_enqueue_script( "jquery-ui-core", array( 'jquery', 'jquery-migrate' ) );
		wp_enqueue_script( "jquery-ui-dialog", array( 'jquery-ui-core' ) );

		//if( isset( $gateways["stripe"] ) )
			//$this->publishable_key = "no";

		$stripe_params = array(
			'key'                  => $this->publishable_key,
			'i18n_terms'           => __( 'Please accept the terms and conditions first', 'woocommerce-gateway-ach-stripe' ),
			'i18n_required_fields' => __( 'Please fill in required checkout fields first', 'woocommerce-gateway-ach-stripe' ),
		);

		wp_localize_script( 'woocommerce_ach_stripe', 'wc_ach_stripe_params', $stripe_params );
		wp_localize_script( 'woocommerce_ach_stripe', 'wc_ach_stripe_ajax',
            array(
				'ajaxurl' => admin_url( 'admin-ajax.php' ),
				"mode" => $this->testmode ? "test" : "live"
			)
		);
	}

	/**
	 * Get_icon function.
	 *
	 * @return string
	 */
	public function get_icon() {
		$icon  = '<img src="' . WC_HTTPS::force_https_url( WC_ACH_STRIPE_ROOT_URL . 'assets/img/ach.png' ) . '" alt="ACH" width="57" />';

		return apply_filters( 'woocommerce_gateway_icon', $icon, $this->id );
	}

	/**
	 * Process the payment
	 */
	public function process_payment( $order_id, $retry = true, $force_customer = false, $amount = 0 ) {
		try {
			$order  = wc_get_order( $order_id );

			if( $amount > 0 ) {

				WC_ACH_Stripe::log( "Info: Begin processing payment for subscription $order_id for the amount of {$amount}" );

				// Make the request
				WC_ACH_Stripe_API::set_microdeposit( $this->testmode );
				try {
					WC_ACH_Stripe_API::make_charge_for_renewal( $order, $amount );
				} catch( Exception $e ) {
					WC_ACH_Stripe::log( "The payment will not be processed further. Sending emails.." );
				}
			}

			// Handle payment
			else if ( $order->get_total() > 0 ) {

				WC_ACH_Stripe::log( "Info: Begin processing payment for order $order_id for the amount of {$order->get_total()}" );

				// Make the request
				WC_ACH_Stripe_API::set_microdeposit( $this->testmode );
				WC_ACH_Stripe_API::make_charge_if_necessary( $order );
			} else {
				$order->payment_complete();
			}

			if ( is_callable( array( $order, 'save' ) ) ) {
				$order->save();
			}

			// Remove cart
			if( WC()->cart ) {
				WC()->cart->empty_cart();
			}

			WC()->mailer()->get_emails()['WC_Email_Customer_Processing_Order']->trigger( $order->get_id(), $order );

			// Return thank you page redirect
			return array(
				'result'   => 'success',
				'redirect' => $this->get_return_url( $order )
			);

		} catch ( Exception $e ) {
			if( function_exists( "wc_add_notice" ) ) {
				wc_add_notice( $e->getMessage(), 'error' );
			}
			if( !is_null( WC()->session ) ) {
				WC()->session->set( 'refresh_totals', true );
			}
			WC_ACH_Stripe::log( sprintf( __( 'Error: %s', 'woocommerce-gateway-ach-stripe' ), $e->getMessage() ) );
			return;
		}
	}

	/**
	 * Processing a refund
	 *
	 * @param [int] $order_id
	 * @param [float] $amount - in USD
	 * @param [string] $reason
	 * @return void
	 */
	public function process_refund( $order_id, $amount = null, $reason = null ) {
		// Do your refund here. Refund $amount for the order with ID $order_id
		WC_ACH_Stripe_API::do_refund( $order_id, $amount, $reason );

		return true;
	}
}