<?php
if (! defined('ABSPATH')) {
    exit;
}

/**
 * Class WooCommerce ACH Stripe Gateway
 *
 * @class WC_ACH_Stripe
 * @package WooCommerce ACH Stripe Gateway
 */
if (!class_exists('WC_ACH_Stripe')) :

    class WC_ACH_Stripe {

        /**
         * Singleton realization.
         *
         * @var [type]
         */
        private static $instance;

        /**
         * Log singleton.
         *
         * @var [type]
         */
        private static $log;

        /**
         * Flag to indicate whether or not we need to load code for / support subscriptions.
         *
         * @var bool
         */
        private $subscription_support_enabled = false;

        /**
         * Default values for ajax requests.
         *
         * @var array
         */
        public static $ajax_response_standard_values;

        /**
         * Returns a singleton instance.
         *
         * @return [wC_ACH_Stripe] instance.
         */
        public static function get_instance()
        {
            if (null === self::$instance) {
                self::$instance = new self();
            }
            return self::$instance;
        }

        /**
         * Override the clone method to prevent a cloning of a singleton.
         *
         * @return void
         */
        private function __clone()
        {
        }

        /**
         * Override the wakeup to prevent serializng.
         */
        private function __wakeup()
        {
        }

        /**
         * Protected construct to prevent an outside creation.
         */
        protected function __construct()
        {
            self::$ajax_response_standard_values = array(
                "popup_header" => __("Thank you for your purchase!", 'woocommerce-gateway-ach-stripe'),
                "popup_text" => __("Your bank account is not verified yet. You will receive an email with further instructions.", 'woocommerce-gateway-ach-stripe'),
                "popup_text_waiting" => __("Your bank account is not verified yet but you will receive verification microdeposits in a couple of days. You will receive an email with further instructions.", 'woocommerce-gateway-ach-stripe'),
                "after_checkout_redirection" => "/shop/",
            );
            add_action('plugins_loaded', array($this, 'init'));
        }

        /**
         * Init the plugin after plugins_loaded so environment variables are set.
         */
        public function init()
        {
            include_once(dirname(dirname(__FILE__)) . '/vendor/autoload.php');
            include_once(dirname(dirname(__FILE__)) . '/inc/class-wc-ach-stripe-api.php');
            include_once(dirname(dirname(__FILE__)) . '/inc/class-wc-ach-rest-controller.php');
            // Init the gateway itself
            $this->init_gateways();
            add_action('admin_notices', array($this, 'admin_notices'));
            add_action("woocommerce_order_status_processing", array("WC_ACH_Stripe", "create_subscription_if_status_changed"), 10, 1);

            add_action("before_woocommerce_pay", array($this, "pay_now_instructions"), 10, 0);
            add_filter("woocommerce_get_checkout_payment_url", array($this, "filter_customer_payment_page"), 10, 2);


            add_action('wp_ajax_check_already_bank_verification', array("WC_ACH_Stripe_API", 'check_bank_verification'));
            add_action('wp_ajax_nopriv_check_already_bank_verification', array("WC_ACH_Stripe_API", 'check_bank_verification'));

            add_action('wp_ajax_get_link_token', array("WC_ACH_Stripe_API", 'get_link_token'));
            add_action('wp_ajax_nopriv_get_link_token', array("WC_ACH_Stripe_API", 'get_link_token'));

            add_action('wp_ajax_ach_payment_by_saved_token', array("WC_ACH_Stripe_API", 'payment_by_saved_token'));
            add_action('wp_ajax_nopriv_ach_payment_by_saved_token', array("WC_ACH_Stripe_API", 'payment_by_saved_token'));

            add_action("wp_ajax_add_payment_method_ach", array("WC_Gateway_ACH_Stripe", "ach_add_payment_method"));
            add_action("wp_ajax_nopriv_add_payment_method_ach", array("WC_Gateway_ACH_Stripe", "ach_add_payment_method"));

            add_action("wp_ajax_validate_front_fields_for_not_verified_yet", array("WC_ACH_Stripe_API", "validate_front_fields_for_not_verified_yet"));
            add_action("wp_ajax_nopriv_validate_front_fields_for_not_verified_yet", array("WC_ACH_Stripe_API", "validate_front_fields_for_not_verified_yet"));

            add_filter('tiny_mce_before_init', array($this, 'tiny_mce_not_remove_styles'), 10, 2);

            add_action("woocommerce_admin_order_totals_after_total", array($this, "show_ach_fees"));

            if (isset($_GET['pay_for_order']) && isset($_GET['key']) && isset($_GET["show_microdeps"])) {
                remove_all_actions('template_redirect',  100);
            }

            add_action("wp_loaded", array("WC_ACH_Stripe", "create_standard_email_image"));
        }
        
        /**
         * Creating image for standart email
         *
         * @return void
         */
        public static function create_standard_email_image()
        {
            if (get_option("woocommerce_ach_standard_email_image_id") !== false) {
                return;
            }
            $cont = file_get_contents(plugin_dir_path(__FILE__) . "/assets/img/email.png");
            $new_file_name = 'ach_email.png';
            $upload = wp_upload_bits($new_file_name, null, $cont);
            // $filename should be the path to a file in the upload directory.
            $filename = $upload["file"];

            // Check the type of file. We'll use this as the 'post_mime_type'.
            $filetype = wp_check_filetype(basename($filename), null);

            // Get the path to the upload directory.
            $wp_upload_dir = wp_upload_dir();

            // Prepare an array of post data for the attachment.
            $attachment = array(
                'guid'           => $wp_upload_dir['url'] . '/' . basename($filename),
                'post_mime_type' => $filetype['type'],
                'post_title'     => preg_replace('/\.[^.]+$/', '', basename($filename)),
                'post_content'   => '',
                'post_status'    => 'inherit'
            );

            // Insert the attachment.
            $attach_id = wp_insert_attachment($attachment, $filename);

            // Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
            require_once(ABSPATH . 'wp-admin/includes/image.php');

            // Generate the metadata for the attachment, and update the database record.
            $attach_data = wp_generate_attachment_metadata($attach_id, $filename);
            wp_update_attachment_metadata($attach_id, $attach_data);

            update_option("woocommerce_ach_standard_email_image_id", $attach_id);
        }

        public function pay_now_instructions()
        {
            global $wp_query;
            if (isset($wp_query->query["order-pay"])) :
                $order = wc_get_order($wp_query->query_vars["order-pay"]);
                $message = "To enter micro withdrawals, please enter your bank data again!";
                if (WC_ACH_Stripe_API::is_order_paid($wp_query->query_vars["order-pay"])) {
                    $message = "You cannot pay for one order twice.";
                    if (WC_ACH_Stripe_API::get_order_ach_status($wp_query->query_vars["order-pay"]) == "pending") {
                        $message .= " We are currently processing your charge in the Stripe.";
                    } else if (WC_ACH_Stripe_API::get_order_ach_status($wp_query->query_vars["order-pay"]) == "succeeded") {
                        $message .= " You charge is already completed in the Stripe.";
                    }
                }

                if ($order->get_payment_method() == "ach_stripe") : ?>
                    <div class="woocommerce-message">
                        <strong><?php echo $message; ?></strong>
                    </div>
                <?php
                endif;
            endif;
        }

        /**
         * Initialize the gateway. Called very early - in the context of the plugins_loaded action
         *
         */
        public function init_gateways()
        {
            if (class_exists('WC_Subscriptions_Order') && function_exists('wcs_create_renewal_order')) {
                $this->subscription_support_enabled = true;
            }
            include_once(dirname(dirname(__FILE__)) . '/inc/class-wc-payment-token-ach.php');
            include_once(dirname(dirname(__FILE__)) . '/inc/class-wc-gateway-ach-stripe.php');
            include_once(dirname(dirname(__FILE__)) . '/inc/class-wc-gateway-plaid.php');
            include_once(dirname(dirname(__FILE__)) . '/inc/class-wc-gateway-ach-stripe-addons.php');
            include_once(dirname(dirname(__FILE__)) . '/inc/class-wc-gateway-plaid-addons.php');
            add_filter('woocommerce_payment_gateways', array($this, 'add_gateways'));
            add_filter("woocommerce_available_payment_gateways", array($this, "remove_all_gateways_on_invoice"), 1);
        }

        /**
         * Add the gateways to WooCommerce
         */
        public function add_gateways($methods)
        {
            if ($this->subscription_support_enabled) {
                $methods[] = 'WC_Gateway_ACH_Stripe_Addons';
                $methods[] = 'WC_Gateway_Plaid_Addons';
            } else {
                $methods[] = 'WC_Gateway_ACH_Stripe';
                $methods[] = 'WC_Gateway_Plaid';
            }
            return $methods;
        }

        public function remove_all_gateways_on_invoice($gateways)
        {
            global $woocommerce, $wp_query;

            if (!isset($wp_query->query["order-pay"]))
                return $gateways;

            $filter_ach = isset($gateways["ach_stripe"]) && $gateways["ach_stripe"]->pay_for_order_filter;
            $filter_plaid = isset($gateways["plaid"]) && $gateways["plaid"]->pay_for_order_filter;

            $payment_method = get_post_meta($wp_query->query["order-pay"], "_payment_method", true);

            foreach ($gateways as $gateway_name => $gateway) {
                if ($filter_ach && $payment_method == "ach_stripe") {
                    if ($gateway_name != "ach_stripe")
                        unset($gateways[$gateway_name]);
                } else if ($filter_plaid && $payment_method == "plaid") {
                    if ($gateway_name != "plaid")
                        unset($gateways[$gateway_name]);
                }
            }

            // remove ACH gateways if the order is already paid
            if (WC_ACH_Stripe_API::is_order_paid($wp_query->query["order-pay"])) {
                unset($gateways["ach_stripe"]);
                unset($gateways["plaid"]);
            }

            return $gateways;
        }

        /**
         * If bank account is not verified, status will be 'pending payment'.
         * This means no subscription till payment not made.
         * When money are received the status will be changed and the subscription will be created automatically.
         *
         * @param [type] $order_id
         * @return void
         */
        public static function create_subscription_if_status_changed($order_id)
        {
            global $woocommerce;
            $order = wc_get_order($order_id);
            self::log("Creating subscription after status changed - created via " . get_post_meta($order_id, "_created_via", true));
            if (!empty(get_post_meta($order_id, "_created_via", true)))
                $order->set_created_via(get_post_meta($order_id, "_created_via", true));
        }

        /**
         * Print a notices in admin panel.
         *
         * @return void
         */
        public function admin_notices()
        {
            // Show message if enabled and FORCE SSL is disabled and WordpressHTTPS plugin is not detected.
            if ((function_exists('wc_site_is_https') && !wc_site_is_https()) && ('no' === get_option('woocommerce_force_ssl_checkout') && !class_exists('WordPressHTTPS'))) {
                echo '<div class="error"><p>' . sprintf(__('ACH Stripe is enabled, but the <a href="%s">force SSL option</a> is disabled; your checkout may not be secure!', 'woocommerce-gateway-ach-stripe'), admin_url('admin.php?page=wc-settings&tab=checkout')) . '</p></div>';
            }
        }

        /**
         * Check if woocommerce is active.
         *
         * @return void
         */
        public static function is_woocommerce_active()
        {
            $active_plugins = (array) get_option('active_plugins', array());

            if (is_multisite()) {
                $active_plugins = array_merge($active_plugins, get_site_option('active_sitewide_plugins', array()));
            }

            return in_array('woocommerce/woocommerce.php', $active_plugins) || array_key_exists('woocommerce/woocommerce.php', $active_plugins);
        }


        /**
         * WooCommerce inactive notice.
         *
         * @return string
         */
        public static function woocommerce_inactive_notice()
        {
            if (current_user_can('activate_plugins' && is_admin())) {
                echo '<div id="message" class="error"><p>';
                printf(__('%1$sWooCommerce ACH Stripe Gateway is inactive%2$s. %3$sWooCommerce plugin %4$s must be active. Please %5$sinstall and activate WooCommerce &raquo;%6$s', 'woocommerce-gateway-ach-stripe'), '<strong>', '</strong>', '<a href="http://wordpress.org/extend/plugins/woocommerce/">', '</a>', '<a href="' . esc_url(admin_url('plugins.php')) . '">', '</a>');
                echo '</p></div>';
            }
        }

        /**
         * Initialize Log.
         */
        public static function log($message)
        {
            if (empty(self::$log)) {
                self::$log = new WC_Logger();
            }

            self::$log->add('woocommerce-gateway-ach-stripe', $message);
        }

        public function filter_customer_payment_page($payurl, $order)
        {
            $created_via = get_post_meta($order->get_id(), "_created_via", true);
            $payment_method = get_post_meta($order->get_id(), "_payment_method", true);

            if ($created_via == "ACH Stripe" || $created_via == "Plaid" || $payment_method == "ach_stripe" || $payment_method == "plaid")
                return $payurl . "&show_microdeps=true";

            return $payurl;
        }

        public function tiny_mce_not_remove_styles($options, $editor_id)
        {
            if (!isset($options['extended_valid_elements'])) {
                $options['extended_valid_elements'] = 'style';
            } else {
                $options['extended_valid_elements'] .= ',style';
            }

            if (!isset($options['valid_children'])) {
                $options['valid_children'] = '+body[style]';
            } else {
                $options['valid_children'] .= ',+body[style]';
            }

            if (!isset($options['custom_elements'])) {
                $options['custom_elements'] = 'style';
            } else {
                $options['custom_elements'] .= ',style';
            }

            return $options;
        }

        public function show_ach_fees($order_id)
        {
            $fees = get_post_meta($order_id, "_ach_fees", true);
            $net = get_post_meta($order_id, "_ach_net", true);
            ?>
            <?php if (!empty($fees)) : ?>
                <tr>
                    <td class="label"><?php esc_html_e('Stripe Fee', 'woocommerce-gateway-ach-stripe'); ?>:</td>
                    <td width="1%"></td>
                    <td class="ach-fee">
                        <span class="woocommerce-Price-amount amount">-<span class="woocommerce-Price-currencySymbol">$</span><?php echo $fees; // WPCS: XSS ok. 
                                                                                                                                ?></span>
                    </td>
                </tr>
            <?php endif; ?>
            <?php if (!empty($net)) : ?>
                <tr>
                    <td class="label"><?php esc_html_e('Stripe Payout', 'woocommerce-gateway-ach-stripe'); ?>:</td>
                    <td width="1%"></td>
                    <td class="ach-net">
                        <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span><?php echo $net; // WPCS: XSS ok. 
                                                                                                                                ?></span>
                    </td>
                </tr>
            <?php endif; ?>
    <?php
        }
    }
endif;