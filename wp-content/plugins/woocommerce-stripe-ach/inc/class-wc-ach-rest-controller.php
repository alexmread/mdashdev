<?php

if( !defined( "ABSPATH" ) )
    exit();

class WC_ACH_REST_Controller {
    
    // Here initialize our namespace and resource name.
    public function __construct() {
        $this->namespace     = 'ach/v1';
        $this->resource_name = '/charge';
        $this->resource_name_additional = '/charge/';
    }

    public function register_routes() {
        register_rest_route( $this->namespace, $this->resource_name, array(
            array(
                'methods'         => "POST",
                'callback'        => array( $this, 'charge' )
            ),
        )  );
        register_rest_route( $this->namespace, $this->resource_name_additional, array(
            array(
                'methods'         => "POST",
                'callback'        => array( $this, 'charge' )
            ),
        )  );
    }
     
    // Register our REST Server
    public function hook_rest_server(){
        add_action( 'rest_api_init', array( $this, 'register_routes' ) );
    }
    
    public function charge( WP_REST_Request $request ){
        $params = $request->get_json_params();
        $process_on_pending = isset( get_option( "woocommerce_ach_stripe_settings" )["process_on_pending"] ) ? get_option( "woocommerce_ach_stripe_settings" )["process_on_pending"] : "no";
        $trustworthy_customers_string = isset( get_option( "woocommerce_ach_stripe_settings" )["trustworthy_customers"] ) ? get_option( "woocommerce_ach_stripe_settings" )["trustworthy_customers"] : "";
        $t_customers = WC_ACH_Stripe_API::trustworthy_customers_to_array( $trustworthy_customers_string );
       
        WC_ACH_Stripe_API::log( "Got request from Stripe: " . $params["type"] );

        if( $params["type"] == "charge.pending" || $params["type"] == "charge.succeeded" ) {
            WC_ACH_Stripe_API::set_order_as_paid( $params, str_replace( "charge.", "", $params["type"] ) );
            WC_ACH_Stripe_API::log( "Set order as paid: " . $params["type"] );
        }
        
        if( ( $params["type"] == "charge.pending" || $params["type"] == "charge.succeeded" ) && $process_on_pending == "yes" )
            WC_ACH_Stripe_API::order_completed_from_stripe( $params, $t_customers );
        if( $params["type"] == "charge.succeeded" && $process_on_pending == "no" )
            WC_ACH_Stripe_API::order_completed_from_stripe( $params, $t_customers );
        
            return http_response_code(200); // PHP 5.4 or greater
    }

}

$ach_rest = new WC_ACH_REST_Controller();
$ach_rest->hook_rest_server();