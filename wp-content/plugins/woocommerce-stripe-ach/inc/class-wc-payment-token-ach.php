<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * WooCommerce Bitcoin Payment Token.
 *
 * Representation of a payment token for bitcoin.
 *
 * @class 		WC_Payment_Token_Bitcoin
 * @version     1.0.0
 * @since		1.0.0
 */
class WC_Payment_Token_ACH extends WC_Payment_Token {

	/** @protected string Token Type String. */
	protected $type = 'ACH';

	/**
	 * Stores payment token data.
	 *
	 * @var array
	 */
	protected $extra_data = array(
		'last4'        => '',
		'routing'        => '',
	);

	/**
	 * Get type to display to user.
	 *
	 * @since  2.6.0
	 * @param  string $deprecated Deprecated since WooCommerce 3.0
	 * @return string
	 */
	public function get_display_name( $deprecated = '' ) {
		return __( sprintf( "Bank account ending in %s", $this->get_last4() ), "woocommerce-gateway-ach-stripe" );
	}

	/**
	 * Hook prefix
	 *
	 * @since 3.0.0
	 */
	protected function get_hook_prefix() {
		return 'woocommerce_payment_token_ach_stripe_get_';
	}

	/**
	 * Validate credit card payment tokens.
	 *
	 * @return boolean True if the passed data is valid
	 */
	public function validate() {
		if ( false === parent::validate() ) {
			return false;
		}

		if ( ! $this->get_last4( 'edit' ) ) {
			return false;
		}
		if ( ! $this->get_routing( 'edit' ) ) {
			return false;
		}

		return true;
	}

	/**
	 * Returns the owner's last4.
	 *
	 * @param  string $context
	 * @return string last4
	 */
	public function get_last4( $context = 'view' ) {
		return $this->get_prop( 'last4', $context );
	}

	/**
	 * Set the owner's last4
	 * @param string $last4
	 */
	public function set_last4( $last4 ) {
		$this->set_prop( 'last4', $last4 );
	}

	/**
	 * Returns the owner's routing.
	 *
	 * @param  string $context
	 * @return string routing
	 */
	public function get_routing( $context = 'view' ) {
		return $this->get_prop( 'routing', $context );
	}

	/**
	 * Set the owner's routing
	 * @param string $routing
	 */
	public function set_routing( $routing ) {
		$this->set_prop( 'routing', $routing );
	}

	/**
	 * Hook.
	 *
	 * @since  2.6.0
	 * @param  string $context
	 * @return string Card type
	 */
	public function get_card_type( $context = 'view' ) {
		return "ach";
	}
}
