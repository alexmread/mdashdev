<?php
if (!defined('ABSPATH')) {
	exit;
}

/**
 * WC_Gateway_Stripe_Addons class.
 *
 * @extends WC_Gateway_Stripe
 */
class WC_Gateway_Plaid_Addons extends WC_Gateway_Plaid
{

	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();

		if (class_exists('WC_Subscriptions_Order')) {
			add_action('woocommerce_scheduled_subscription_payment_' . $this->id, array($this, 'scheduled_subscription_payment'), 10, 2);
			add_action('wcs_resubscribe_order_created', array($this, 'delete_resubscribe_meta'), 10);
			add_action('wcs_renewal_order_created', array($this, 'delete_renewal_meta'), 10);
			//add_action( 'woocommerce_subscription_failing_payment_method_updated_stripe', array( $this, 'update_failing_payment_method' ), 10, 2 );

			// display the credit card used for a subscription in the "My Subscriptions" table
			// add_filter( 'woocommerce_my_subscriptions_payment_method', array( $this, 'maybe_render_subscription_payment_method' ), 10, 2 );

			// allow store managers to manually set Stripe as the payment method on a subscription
			add_filter('woocommerce_subscription_payment_meta', array($this, 'add_subscription_payment_meta'), 10, 2);
			add_filter('woocommerce_subscription_validate_payment_meta', array($this, 'validate_subscription_payment_meta'), 10, 2);

			add_action("woocommerce_order_status_completed", array($this, "activate_subscription_on_processing_or_completed"));
			add_action("woocommerce_order_status_processing", array($this, "activate_subscription_on_processing_or_completed"));
		}
	}

	/**
	 * Is $order_id a subscription?
	 * @param  int  $order_id
	 * @return boolean
	 */
	protected function is_subscription($order_id)
	{
		WC_ACH_Stripe::log("Checking the order $order_id is subscription...");
		if (function_exists('wcs_order_contains_subscription') && (wcs_order_contains_subscription($order_id) || wcs_is_subscription($order_id) || wcs_order_contains_renewal($order_id)))
			WC_ACH_Stripe::log("OK!");
		else
			WC_ACH_Stripe::log("Not subscription!");
		return (function_exists('wcs_order_contains_subscription') && (wcs_order_contains_subscription($order_id) || wcs_is_subscription($order_id) || wcs_order_contains_renewal($order_id)));
	}

	/**
	 * Process the payment based on type.
	 * @param  int $order_id
	 * @return array
	 */
	public function process_payment($order_id, $retry = true, $force_customer = false, $amount = 0)
	{
		if ($this->is_subscription($order_id)) {
			// Regular payment with force customer enabled
			WC_ACH_Stripe::log("Let's process subscription payment...");
			return parent::process_payment($order_id, true, true);
		} else {
			WC_ACH_Stripe::log("Let's process product payment...");
			return parent::process_payment($order_id, $retry, $force_customer);
		}
	}

	/**
	 * Updates other subscription sources.
	 */
	protected function save_source($order, $source)
	{
		WC_ACH_Stripe::log("Begin saving sources of subscription...");
		parent::save_source($order, $source);

		$order_id = version_compare(WC_VERSION, '3.0.0', '<') ? $order->id : $order->get_id();

		// Also store it on the subscriptions being purchased or paid for in the order
		if (function_exists('wcs_order_contains_subscription') && wcs_order_contains_subscription($order_id)) {
			$subscriptions = wcs_get_subscriptions_for_order($order_id);
		} elseif (function_exists('wcs_order_contains_renewal') && wcs_order_contains_renewal($order_id)) {
			$subscriptions = wcs_get_subscriptions_for_renewal_order($order_id);
		} else {
			$subscriptions = array();
		}
		WC_ACH_Stripe::log("Updating each subscription post meta...");
		foreach ($subscriptions as $subscription) {
			update_post_meta($subscription->id, '_ach_stripe_customer_id', $source->customer);
			update_post_meta($subscription->id, '_ach_stripe_account_id', $source->source);
		}
	}

	/**
	 * process_subscription_payment function.
	 * @param mixed $order
	 * @param int $amount (default: 0)
	 * @param string $stripe_token (default: '')
	 * @param  bool initial_payment
	 */
	public function process_subscription_payment($order = '', $amount = 0)
	{
		WC_ACH_Stripe::log("Processing subscription payment...");
		if (empty($order) || is_null($order)) {
			WC_ACH_Stripe::log("The order is empty or null, can't process!");
			wc_add_notice("Cannot process payment of empty order", 'error');
		}

		$order_id = version_compare(WC_VERSION, '3.0.0', '<') ? $order->id : $order->get_id();

		return parent::process_payment($order_id, true, true, $amount);
	}

	/**
	 * Don't transfer Stripe customer/token meta to resubscribe orders.
	 * @param int $resubscribe_order The order created for the customer to resubscribe to the old expired/cancelled subscription
	 */
	public function delete_resubscribe_meta($resubscribe_order)
	{
		$order_id = version_compare(WC_VERSION, '3.0.0', '<') ? $resubscribe_order->id : $resubscribe_order->get_id();
		WC_ACH_Stripe::log("Deleting resubscribe meta of order {$order_id}...");
		delete_post_meta($order_id, '_ach_stripe_customer_id');
		delete_post_meta($order_id, '_ach_stripe_account_id');
		$this->delete_renewal_meta($resubscribe_order);
		WC_ACH_Stripe::log("Deleted!");
	}

	/**
	 * Don't transfer Stripe fee/ID meta to renewal orders.
	 * @param int $resubscribe_order The order created for the customer to resubscribe to the old expired/cancelled subscription
	 */
	public function delete_renewal_meta($renewal_order)
	{
		$order_id = version_compare(WC_VERSION, '3.0.0', '<') ? $renewal_order->id : $renewal_order->get_id();
		WC_ACH_Stripe::log("Deleting delete_renewal_meta meta of order {$order_id}...");
		delete_post_meta($order_id, 'ACH Stripe Fee');
		delete_post_meta($order_id, 'Net Revenue From ACH Stripe');
		delete_post_meta($order_id, 'ACH Stripe Payment ID');
		WC_ACH_Stripe::log("Deleted!");
		return $renewal_order;
	}

	/**
	 * scheduled_subscription_payment function.
	 *
	 * @param $amount_to_charge float The amount to charge.
	 * @param $renewal_order WC_Order A WC_Order object created to record the renewal payment.
	 */
	public function scheduled_subscription_payment($amount_to_charge, $renewal_order)
	{
		try {
			$order_id = version_compare(WC_VERSION, '3.0.0', '<') ? $renewal_order->id : $renewal_order->get_id();
			WC_ACH_Stripe::log("Processing sheduled payment of order {$order_id}...");
			$this->process_subscription_payment($renewal_order, $amount_to_charge);
		} catch (Exception $e) {
			wc_add_notice($e->getMessage(), 'error');
			WC_ACH_Stripe::log(sprintf(__('Error: %s', 'woocommerce-gateway-ach-stripe'), $e->getMessage()));
			$renewal_order->update_status('failed', sprintf(__('ACH Stripe Transaction Failed (%s)', 'woocommerce-gateway-ach-stripe'), $e->getMessage()));
			return;
		}
	}

	/**
	 * Remove order meta
	 * @param  object $order
	 */
	public function remove_order_source_before_retry($order)
	{
		$order_id = version_compare(WC_VERSION, '3.0.0', '<') ? $order->id : $order->get_id();
		WC_ACH_Stripe::log("Removing order $order_id source before retry...");
		delete_post_meta($order_id, '_ach_stripe_account_id');
		WC_ACH_Stripe::log("Removed!");
	}

	/**
	 * Remove order meta
	 * @param  object $order
	 */
	public function remove_order_customer_before_retry($order)
	{
		$order_id = version_compare(WC_VERSION, '3.0.0', '<') ? $order->id : $order->get_id();
		WC_ACH_Stripe::log("Removing order $order_id customer before retry...");
		delete_post_meta($order_id, '_ach_stripe_customer_id');
		WC_ACH_Stripe::log("Removed!");
	}

	/**
	 * Update the customer_id for a subscription after using Stripe to complete a payment to make up for
	 * an automatic renewal payment which previously failed.
	 *
	 * @access public
	 * @param WC_Subscription $subscription The subscription for which the failing payment method relates.
	 * @param WC_Order $renewal_order The order which recorded the successful payment (to make up for the failed automatic payment).
	 * @return void
	 */
	public function update_failing_payment_method($subscription, $renewal_order)
	{
		$renewal_order_id = version_compare(WC_VERSION, '3.0.0', '<') ? $renewal_order->id : $renewal_order->get_id();
		$subscription_id = version_compare(WC_VERSION, '3.0.0', '<') ? $subscription->id : $subscription->get_id();
		WC_ACH_Stripe::log("Updating failing payment method of renewal order {$renewal_order_id}...");
		update_post_meta($subscription->get_parent_id(), '_ach_stripe_customer_id', $renewal_order->_ach_stripe_customer_id);
		update_post_meta($subscription->get_parent_id(), '_ach_stripe_account_id', $renewal_order->_ach_stripe_account_id);
		WC_ACH_Stripe::log("Updated!");
	}

	/**
	 * Include the payment meta data required to process automatic recurring payments so that store managers can
	 * manually set up automatic recurring payments for a customer via the Edit Subscriptions screen in 2.0+.
	 *
	 * @since 2.5
	 * @param array $payment_meta associative array of meta data required for automatic payments
	 * @param WC_Subscription $subscription An instance of a subscription object
	 * @return array
	 */
	public function add_subscription_payment_meta($payment_meta, $subscription)
	{
		WC_ACH_Stripe::log("Adding subscription payment meta fields...");
		$payment_meta[$this->id] = array(
			'post_meta' => array(
				'_ach_stripe_customer_id' => array(
					'value' => get_post_meta($subscription->get_parent_id(), '_ach_stripe_customer_id', true),
					'label' => 'Stripe Customer ID',
				),
				'_ach_stripe_account_id' => array(
					'value' => get_post_meta($subscription->get_parent_id(), '_ach_stripe_account_id', true),
					'label' => 'Stripe Account ID',
				),
			),
		);
		WC_ACH_Stripe::log("Added!");
		return $payment_meta;
	}

	/**
	 * Validate the payment meta data required to process automatic recurring payments so that store managers can
	 * manually set up automatic recurring payments for a customer via the Edit Subscriptions screen in 2.0+.
	 *
	 * @since 2.5
	 * @param string $payment_method_id The ID of the payment method to validate
	 * @param array $payment_meta associative array of meta data required for automatic payments
	 * @return array
	 */
	public function validate_subscription_payment_meta($payment_method_id, $payment_meta)
	{
		if ($this->id === $payment_method_id) {
			WC_ACH_Stripe::log("Validating subscription payment meta fields...");

			// TEMPORARILY COMMENTED
			// TODO Turn on in later versions!

			/*if ( ! isset( $payment_meta['post_meta']['_ach_stripe_customer_id']['value'] ) || empty( $payment_meta['post_meta']['_ach_stripe_customer_id']['value'] ) ) {
				throw new Exception( 'A "_ach_stripe_customer_id" value is required.' );
			} elseif ( 0 !== strpos( $payment_meta['post_meta']['_ach_stripe_customer_id']['value'], 'cus_' ) ) {
				throw new Exception( 'Invalid customer ID. A valid "_ach_stripe_customer_id" must begin with "cus_".' );
			}

			if ( ! empty( $payment_meta['post_meta']['_ach_stripe_account_id']['value'] ) && 0 !== strpos( $payment_meta['post_meta']['_ach_stripe_account_id']['value'], 'ba_' ) ) {
				throw new Exception( 'Invalid card ID. A valid "_ach_stripe_account_id" must begin with "ba_".' );
			}*/
			WC_ACH_Stripe::log("Validated!");
		}
	}

	/**
	 * Render the payment method used for a subscription in the "My Subscriptions" table
	 *
	 * @since 1.7.5
	 * @param string $payment_method_to_display the default payment method text to display
	 * @param WC_Subscription $subscription the subscription details
	 * @return string the subscription payment method
	 */
	public function maybe_render_subscription_payment_method($payment_method_to_display, $subscription)
	{
		// bail for other payment methods
		if ($this->id !== $subscription->payment_method || !$subscription->customer_user) {
			return $payment_method_to_display;
		}

		$stripe_customer    = WC_ACH_Stripe_API::get_ach_stripe_customer( $subscription );
		$stripe_customer_id = get_post_meta($subscription->id, '_ach_stripe_customer_id', true);
		$stripe_account_id     = get_post_meta($subscription->id, '_ach_stripe_account_id', true);

		// If we couldn't find a Stripe customer linked to the subscription, fallback to the user meta data.
		if (!$stripe_customer_id || !is_string($stripe_customer_id)) {
			$user_id            = $subscription->customer_user;
			$stripe_customer_id = get_user_meta($user_id, '_ach_stripe_customer_id', true);
			$stripe_account_id     = get_user_meta($user_id, '_ach_stripe_account_id', true);
		}

		// If we couldn't find a Stripe customer linked to the account, fallback to the order meta data.
		if ((!$stripe_customer_id || !is_string($stripe_customer_id)) && false !== $subscription->order) {
			$stripe_customer_id = get_post_meta($subscription->order->id, '_ach_stripe_customer_id', true);
			$stripe_account_id     = get_post_meta($subscription->order->id, '_ach_stripe_account_id', true);
		}

		$accounts = $stripe_customer->sources->data;

		if ($accounts) {
			$found_acc = false;
			foreach ($accounts as $account) {
				if ($account->id === $stripe_account_id) {
					$found_acc                = true;
					$payment_method_to_display = sprintf(__('Via ACH Stripe Account (+PLAID) ending in %1$s', 'woocommerce-gateway-ach-stripe'), $account->last4);
					break;
				}
			}
			if (!$found_acc) {
				$payment_method_to_display = sprintf(__('Via ACH Stripe Account (+PLAID) ending in %1$s', 'woocommerce-gateway-ach-stripe'), $accounts[0]->last4);
			}
		}

		return $payment_method_to_display;
	}

	public function activate_subscription_on_processing_or_completed($order_id)
	{
		$order = wc_get_order($order_id);
		if ($order->get_status() == "processing" || $order->get_status() == "completed") {
			WC_Subscriptions_Manager::activate_subscriptions_for_order($order);
			self::log("Subscription activated!");
		}
	}

	/**
	 * Logs
	 *
	 * @since 3.1.0
	 * @version 3.1.0
	 *
	 * @param string $message
	 */
	public function log($message)
	{
		$options = get_option('woocommerce_ach_stripe_settings');

		if ('yes' === $options['logging']) {
			WC_ACH_Stripe::log($message);
		}
	}
}
