/**
 * Handles actions in admin panel.
 */

jQuery( function( $ ) {
	'use strict';

    // object to handle admin
    var wc_ach_stripe_admin = {

        /**
         * Initialization
         */
        init: function() {
            this.$plaid_checkbox = $( '#woocommerce_ach_stripe_plaid' );

            this.$plaid_client_id = $( '#woocommerce_ach_stripe_plaid_client_id' );
            this.$plaid_public_key = $( '#woocommerce_ach_stripe_plaid_public_key' );
            this.$plaid_secret = $( '#woocommerce_ach_stripe_plaid_secret' );

            this.$ach_stripe_test_secret = $( '#woocommerce_ach_stripe_test_secret_key' );
            this.$ach_stripe_test_publishable = $( '#woocommerce_ach_stripe_test_publishable_key' );
            this.$ach_stripe_live_secret = $( '#woocommerce_ach_stripe_secret_key' );
            this.$ach_stripe_live_publishable = $( '#woocommerce_ach_stripe_publishable_key' );

            this.plaidState();
            this.$plaid_checkbox.on( 'click', this.plaidState );
            this.webhookEndpointButton();
            this.emailTemplateEditorStyles();
            this.alert_of_no_account();

            $( "label[for='woocommerce_ach_stripe_process_on_pending'] ~ p.description" ).css({
                fontWeight: "600",
                fontStyle: "normal",
                color: "#333",
                fontSie: "14px",
                paddingTop: "15px",
                marginBottom: "-15px"
            });
        },

        /**
         * closest( 'tr' ).show/closest( 'tr' ).hide Plaid and Stripe
         */
        plaidState: function( e ) {
            if( wc_ach_stripe_admin.$plaid_checkbox.prop( 'checked' ) ) {

                wc_ach_stripe_admin.$plaid_client_id.closest( 'tr' ).show();
                wc_ach_stripe_admin.$plaid_public_key.closest( 'tr' ).show();
                wc_ach_stripe_admin.$plaid_secret.closest( 'tr' ).show();

                wc_ach_stripe_admin.$ach_stripe_live_publishable.closest( 'tr' ).hide();
                wc_ach_stripe_admin.$ach_stripe_live_secret.closest( 'tr' ).hide();
                wc_ach_stripe_admin.$ach_stripe_test_publishable.closest( 'tr' ).hide();
                wc_ach_stripe_admin.$ach_stripe_test_secret.closest( 'tr' ).hide();
            } else {
                wc_ach_stripe_admin.$plaid_client_id.closest( 'tr' ).hide();
                wc_ach_stripe_admin.$plaid_public_key.closest( 'tr' ).hide();
                wc_ach_stripe_admin.$plaid_secret.closest( 'tr' ).hide();

                wc_ach_stripe_admin.$ach_stripe_live_publishable.closest( 'tr' ).show();
                wc_ach_stripe_admin.$ach_stripe_live_secret.closest( 'tr' ).show();
                wc_ach_stripe_admin.$ach_stripe_test_publishable.closest( 'tr' ).show();
                wc_ach_stripe_admin.$ach_stripe_test_secret.closest( 'tr' ).show();
            }
        },

        webhookEndpointButton: function(e) {
            if($("input#woocommerce_ach_stripe_trustworthy_customers").length > 0) {
                $("input#woocommerce_ach_stripe_trustworthy_customers").closest("td")
                .append("<input id='ach-copy-endpoint-text' type='text' value='" + ext_data.endpoint + "' readonly size='40'><input type='button' id='ach-copy-endpoint' class='button-primary' value='Copy Stripe Webhooks endpoint to clipboard!' style='margin-left:10px;'>");

                var copyEndpointButton = document.querySelector('#ach-copy-endpoint');
                
                copyEndpointButton.addEventListener('click', function(event) {
                    var copyTextarea = document.querySelector('#ach-copy-endpoint-text');
                    copyTextarea.select();
                    
                    try {
                        var successful = document.execCommand('copy');
                        var msg = successful ? 'successful' : 'unsuccessful';
                        console.log('Copying text command was ' + msg);
                    } catch (err) {
                        console.log('Oops, unable to copy');
                    }
                });
            }
        },

        emailTemplateEditorStyles: function(e) {
            if($("input#woocommerce_ach_stripe_trustworthy_customers").length > 0) {
                $( "#ach_email_template" ).css( "width", "100%" );
            }
        },

        alert_of_no_account: function() {
            if( $( "input#woocommerce_ach_stripe_no_account_to_pay" ).length > 0 ) {
                $( "input#woocommerce_ach_stripe_no_account_to_pay" ).on( "change", function( e ) {
                    if( this.checked ) {
                        if( !confirm( "Do you take responsibility of any risks related to processing orders without any guarantee of receiving of money?" ) ) {
                            this.checked = false;
                        }
                    }
                } );
            }
        }
    
    };

    wc_ach_stripe_admin.init();

} );