jQuery(function($) {
  'use strict';
  /**
   * wc_ach_stripe_params from localizing.
   */
  if (wc_ach_stripe_params.key != 'no') {
    Stripe.setPublishableKey(wc_ach_stripe_params.key);
    Stripe.key = wc_ach_stripe_params.key;
  }

  /**
   * Object to handle Stripe payment forms.
   */
  var wc_ach_stripe_form = {
    /**
     * Initialize event handlers and UI state.
     */
    init: function() {
      // checkout page
      if ($('form.woocommerce-checkout').length) {
        this.form = $('form.woocommerce-checkout');
      }

      $('form.woocommerce-checkout')
        .on('checkout_place_order_ach_stripe', this.onSubmit)
        .on('checkout_place_order_plaid', this.onSubmit);

      // pay order page
      if ($('form#order_review').length) {
        this.form = $('form#order_review');
      }

      $('form#order_review').on('submit', this.onSubmit);

      // add payment method page
      if ($('form#add_payment_method').length) {
        this.form = $('form#add_payment_method');
      }

      // handle the checkout and saved methods
      if ($('.payment_method_ach_stripe ul.wc-saved-payment-methods').length) {
        $(document).on(
          'change',
          '.payment_method_ach_stripe ul.wc-saved-payment-methods li input',
          this.toggleSavedMethodsVisibility
        );
        $(
          '.payment_method_ach_stripe ul.wc-saved-payment-methods li:first-of-type input'
        ).change();
      }

      $('form#add_payment_method').on('submit', this.onSubmit);

      $(document)
        .on('change', '#ach-payment-form :input', this.onACHFormChange)
        .on('achStripeError', this.onError)
        .on('checkout_error', this.clearToken);

      if (
        $('form.woocommerce-checkout, form#order_review').length &&
        $('#plaid-place-order-img').length
      ) {
        var env = $('#plaid-payment-form').data('mode'); //sandbox, development or production
        var user_id = false;
        var link_token;
        if( localStorage.getItem( 'temp_user_id' ) != null ) {
          user_id = localStorage.getItem( 'temp_user_id' );
        }
        var data = {
          action: 'get_link_token',
          mode: env,
          user_id: user_id
        };
        jQuery.post(
          wc_ach_stripe_ajax.ajaxurl,
          data,
          function( response ) {
            if( user_id == false ) {
              if( response.new_user !== false ) {
                localStorage.setItem('temp_user_id', response.new_user);
              }
            }
            wc_ach_stripe_form.takeLinkHandler( response.link_token, env );
          }
        );
      }
      
      $(document).on('click', '#plaid-place-order-img', function(e) {
        wc_ach_stripe_form.linkHandler.open();
      });
    },

    takeLinkHandler: function( link_token, env ) {
      var data = {
        onSuccess: function( public_token, metadata ) {
          wc_ach_stripe_form.onPlaidResponse( public_token, metadata );
        },
        onExit: function( err, metadata ) {
          // The user exited the Link flow.
          wc_ach_stripe_form.unblock();
          if ( err != null ) {
            // The user encountered a Plaid API error prior to exiting.
            console.log( err );
          }
        }
      }
      if( $('#plaid-payment-form').data('public') != '' ) {
        data.apiVersion = 'v2';
        data.env = env;
        data.clientName = $('#plaid-payment-form').data('company');
        data.key = $('#plaid-payment-form').data('public');
        data.product = ['auth'];
        data.selectAccount = true;
      } else {
        data.token = link_token;
      }
      this.linkHandler = Plaid.create(data);
    },

    isACHStripeChosen: function() {
      return (
        $('#payment_method_ach_stripe').is(':checked') &&
        (!$('input[name="wc-ach-stripe-payment-token"]:checked').length ||
          'new' ===
            $('input[name="wc-ach-stripe-payment-token"]:checked').val())
      );
    },

    isPlaidChosen: function() {
      return $('#payment_method_plaid').is(':checked');
    },

    hasToken: function() {
      return 0 < $('input.ach_stripe_token').length;
    },

    hasPlaidToken: function() {
      return 0 < $('input.plaid_stripe_token').length;
    },

    toggleSavedMethodsVisibility: function(e) {
      if (
        $(this)
          .parent()
          .attr('class')
          .indexOf('new') >= 0
      ) {
        $('#ach-payment-form').show();
        $('label.wc-ach-saved-account-notice').hide();
      } else {
        $('#ach-payment-form').hide();
        $(this)
          .parent()
          .next('label.wc-ach-saved-account-notice')
          .show();
      }
    },

    validateAuth: function() {
      if (
        $('#ach-routing-number').val() == '' ||
        $('#ach-account-number').val() == ''
      ) {
        $(document).trigger('achStripeError', {
          response: {
            error: {
              message:
                'You must fill these fields: Bank Routing Number, Bank Account Number'
            }
          }
        });
        setTimeout(() => wc_ach_stripe_form.unblock(), 2000);
        return false;
      }
      if ($('#ach-auth:checked').length == 0) {
        $(document).trigger('achStripeError', {
          response: {
            error: {
              message: 'You should confirm the authorization checkbox'
            }
          }
        });
        setTimeout(() => wc_ach_stripe_form.unblock(), 2000);
        return false;
      } else {
        return true;
      }
    },

    block: function() {
      wc_ach_stripe_form.form.block({
        message: null,
        overlayCSS: {
          background: '#fff',
          opacity: 0.6
        }
      });
    },

    unblock: function() {
      wc_ach_stripe_form.form.unblock();
    },

    onError: function(e, responseObject) {
      var message = responseObject.response.error.message;

      $('.wc-ach-stripe-error, .ach_stripe_token').remove();
      $('#ach-country')
        .closest('p')
        .before(
          '<ul class="woocommerce_error woocommerce-error wc-ach-stripe-error"><li>' +
            message +
            '</li></ul>'
        );
      wc_ach_stripe_form.unblock();
    },

    onSubmit: function(e) {
      // if not https, prevent
      if (
        window.location.protocol != 'https:' &&
        wc_ach_stripe_ajax.mode == 'live'
      ) {
        e.preventDefault();
        return false;
      }

      // validate on front
      if (!wc_ach_stripe_form.validateFormForNotVerifiedAccount()) {
        e.preventDefault();
        return false;
      }

      //console.log("Submitting... Is Stripe chosen? " + wc_ach_stripe_form.isACHStripeChosen() + ". Has token? " + wc_ach_stripe_form.hasToken());
      if (
        wc_ach_stripe_form.isACHStripeChosen() &&
        !wc_ach_stripe_form.hasToken() &&
        !$('form#add_payment_method').length
      ) {
        e.preventDefault();
        wc_ach_stripe_form.block();
        // ACH Stripe
        var country = $('#ach-country option:selected').val(),
          currency = $('#ach-currency option:selected').val(),
          routing_number = $('#ach-routing-number').val(),
          account_number = $('#ach-account-number').val(),
          account_holder_name = $('#ach-account-holder-name').val(),
          account_holder_type = $(
            '#ach-account-holder-type option:selected'
          ).val(),
          billing_first_name = $('#billing_first_name').val(),
          billing_last_name = $('#billing_last_name').val(),
          data = {
            country: country,
            currency: currency,
            routing_number: routing_number,
            account_number: account_number,
            account_holder_name: account_holder_name,
            account_holder_type: account_holder_type
          };

        //console.log("Stripe data is " + JSON.stringify(data));

        if (wc_ach_stripe_form.validateAuth()) {
          Stripe.bankAccount.validateRoutingNumber(routing_number, country);
          Stripe.bankAccount.validateAccountNumber(account_number, country);
          //console.log("Customer data validated!");

          //console.log("Creating token...");
          Stripe.bankAccount.createToken(
            data,
            wc_ach_stripe_form.onACHStripeResponse
          );
        } else if (
          $(
            '.payment_method_ach_stripe ul.wc-saved-payment-methods li input:checked'
          ).attr('id') != 'wc-ach_stripe-payment-token-new'
        ) {
          e.preventDefault();
          wc_ach_stripe_form.block();
          // saved payment method is chosen to pay
          var token_id = $(
              '.payment_method_ach_stripe ul.wc-saved-payment-methods li input:checked'
            ).val(),
            order_key =
              wc_ach_stripe_form.getParameterByName('key') == null ||
              wc_ach_stripe_form.getParameterByName('key') == ''
                ? 0
                : wc_ach_stripe_form.getParameterByName('key');

          var data = {
            action: 'ach_payment_by_saved_token',
            token_id: token_id, // saved payment method token from DB

            order_key: order_key,
            payment_method: wc_ach_stripe_form.form
              .find('input[name="payment_method"]:checked')
              .val(),
            account_number: $('input#ach-account-number-' + token_id).val(),
            async: false
          };

          var shipping_methods = {};

          $(
            'select.shipping_method, input[name^="shipping_method"][type="radio"]:checked, input[name^="shipping_method"][type="hidden"]'
          ).each(function() {
            shipping_methods[$(this).data('index')] = $(this).val();
          });

          data.shipping_method = shipping_methods;

          var serializedForm = wc_ach_stripe_form.getFormData(
            jQuery('.checkout')
          );
          data = wc_ach_stripe_form.jsonConcat(data, serializedForm);

          jQuery.post(
            wc_ach_stripe_ajax.ajaxurl,
            data,
            wc_ach_stripe_form.handleServerResponse
          );
        }

        // Prevent form submitting
        return false;
      } else if (
        wc_ach_stripe_form.isPlaidChosen() &&
        !wc_ach_stripe_form.hasPlaidToken()
      ) {
        // Plaid
        e.preventDefault();
        wc_ach_stripe_form.linkHandler.open();
        // Prevent form submitting
        return false;
      } else if (
        $('form#add_payment_method input#payment_method_ach_stripe:checked')
          .length
      ) {
        e.preventDefault();

        var ext_data = {
          routing_number: $('#ach-routing-number').val(),
          account_number: $('#ach-account-number').val(),
          country: $('#ach-country option:selected').val(),
          currency: $('#ach-currency option:selected').val(),
          account_holder_name: $('#ach-account-holder-name').val(),
          account_holder_type: $(
            '#ach-account-holder-type option:selected'
          ).val(),
          billing_email: $('#billing_email').val()
        };

        if (wc_ach_stripe_form.validateAuth()) {
          Stripe.bankAccount.validateRoutingNumber(
            ext_data.routing,
            ext_data.country
          );
          Stripe.bankAccount.validateAccountNumber(
            ext_data.account,
            ext_data.country
          );
          //console.log("Customer data validated!");

          //console.log("Creating token...");
          Stripe.bankAccount.createToken(ext_data, function(status, response) {
            var data = Object.assign(
              {
                action: 'add_payment_method_ach',
                ach_stripe_token: response.id
              },
              ext_data
            );

            $.post(wc_ach_stripe_ajax.ajaxurl, data, function(e) {
              if (e.result == 'failure') {
                $(document).trigger('achStripeError', {
                  response: {
                    error: {
                      message: e.msg
                    }
                  }
                });
                return false;
              }

              window.location.href = e.redirect;
              return false;
            });
          });
        }

        return false;
      }
    },

    microdepsVerificationForm: function(token) {
      wc_ach_stripe_form.unblock();
      $('body').append(
        '<dialog id="ach-microdep-verification" title="Verification">' +
          '<div class="dialog-body">' +
          '<label for="ach-microdeps">Two verification microdeposits from your bank account</label>' +
          '<input type="text" id="ach-microdeps-1" class="dialog-input" placeholder="32">' +
          '<input type="text" id="ach-microdeps-2" class="dialog-input" placeholder="45">' +
          '<div class="dialog-actions">' +
          '<button id="achCancelButton">Cancel</button>' +
          '<button id="achOkButton">Ok</button>' +
          '</div>' +
          '</div>' +
          '</dialog>'
      );

      var okButton = document.getElementById('achOkButton');
      var cancelButton = document.getElementById('achCancelButton');
      var achDialog = document.getElementById('ach-microdep-verification');

      /* enable polyfill */
      dialogPolyfill.registerDialog(achDialog);
      achDialog.showModal();

      okButton.addEventListener('click', function() {
        if (
          +$('#ach-microdeps-1')
            .val()
            .trim() < 1
        )
          $('#ach-microdeps-1').val(
            +$('#ach-microdeps-1')
              .val()
              .trim() * 100
          );
        if (
          +$('#ach-microdeps-2')
            .val()
            .trim() < 1
        )
          $('#ach-microdeps-2').val(
            +$('#ach-microdeps-2')
              .val()
              .trim() * 100
          );

        var microdeps =
          $('#ach-microdeps-1')
            .val()
            .trim() +
          ' ' +
          $('#ach-microdeps-2')
            .val()
            .trim();

        wc_ach_stripe_form.form.append(
          "<input type='hidden' class='ach_stripe_token' name='ach_stripe_token' value='" +
            token +
            "'/>"
        );
        wc_ach_stripe_form.form.append(
          "<input type='hidden' class='ach_stripe_microdep' name='microdep' value='" +
            microdeps +
            "'/>"
        );
        wc_ach_stripe_form.form.submit();
        achDialog.close();
      });

      cancelButton.addEventListener('click', function() {
        achDialog.close();
      });
    },

    getParameterByName: function(name, url) {
      if (!url) url = window.location.href;
      name = name.replace(/[\[\]]/g, '\\$&');
      var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, ' '));
    },

    notVerificatedNotice: function(
      shopLink,
      popupHeader,
      popupText,
      popupTextWaiting,
      waiting = false
    ) {
      wc_ach_stripe_form.unblock();
      var message = popupText;
      if (waiting) message = popupTextWaiting;

      $('body').append(
        '<dialog id="ach-microdep-verification">' +
          '<div class="dialog-body">' +
          '<h3>' +
          popupHeader +
          '</h3>' +
          '<p>' +
          message +
          '</p>' +
          '<div class="dialog-actions">' +
          '<button id="achOkButton">Ok</button>' +
          '</div>' +
          '</div>' +
          '</dialog>'
      );

      var okButton = document.getElementById('achOkButton');
      var achDialog = document.getElementById('ach-microdep-verification');

      /* enable polyfill */
      dialogPolyfill.registerDialog(achDialog);
      achDialog.showModal();

      okButton.addEventListener('click', function() {
        achDialog.close();
        window.location.href = shopLink;
      });
    },

    onACHFormChange: function() {
      $('.wc-ach-stripe-error, .ach_stripe_token').remove();
    },

    onACHStripeResponse: function(status, response) {
      //console.log("Stripe response is " + JSON.stringify(response));
      if (response.error) {
        $(document).trigger('achStripeError', { response: response });
      } else {
        // token contains id, last4, and card type
        var token = response.id;

        var country = $('#ach-country option:selected').val(),
          currency = $('#ach-currency option:selected').val(),
          routing_number = $('#ach-routing-number').val(),
          account_number = $('#ach-account-number').val(),
          account_holder_name = $('#ach-account-holder-name').val(),
          account_holder_type = $(
            '#ach-account-holder-type option:selected'
          ).val(),
          save_account = $('#wc-ach_stripe-new-payment-method').is(':checked')
            ? 1
            : 0,
          // if user creates account on checkout
          account_username =
            $('#account_username').length > 0
              ? $('#account_username').val()
              : '',
          account_password =
            $('#account_password').length > 0
              ? $('#account_password').val()
              : '',
          order_key =
            wc_ach_stripe_form.getParameterByName('key') == null ||
            wc_ach_stripe_form.getParameterByName('key') == ''
              ? 0
              : wc_ach_stripe_form.getParameterByName('key');

        var data = {
          action: 'check_already_bank_verification',
          ach_stripe_token: token,
          country: country,
          currency: currency,
          routing_number: routing_number,
          account_number: account_number,
          account_holder_name: account_holder_name,
          account_holder_type: account_holder_type,
          save_account: save_account,
          account_username: account_username,
          account_password: account_password,

          order_key: order_key,
          async: false
        };

        var shipping_methods = {};

        $(
          'select.shipping_method, input[name^="shipping_method"][type="radio"]:checked, input[name^="shipping_method"][type="hidden"]'
        ).each(function() {
          shipping_methods[$(this).data('index')] = $(this).val();
        });

        data.shipping_method = shipping_methods;

        //console.log("Final step! Data is " + JSON.stringify(data));

        var serializedForm = wc_ach_stripe_form.getFormData(
          jQuery('.checkout')
        );
        data = wc_ach_stripe_form.jsonConcat(data, serializedForm);

        jQuery.post(
          wc_ach_stripe_ajax.ajaxurl,
          data,
          wc_ach_stripe_form.handleServerResponse
        );
      }
    },

    jsonConcat: function(o1, o2) {
      for (var key in o2) {
        o1[key] = o2[key];
      }
      return o1;
    },

    getFormData: function($form) {
      var unindexed_array = $form.serializeArray();
      var indexed_array = {};

      jQuery.map(unindexed_array, function(n, i) {
        /* concatenate multiselect values */
        /* TODO: ugly solution, this hack must be replaced */
        n['name'] = n['name'].replace(/\[]/g, '');
        if (indexed_array.hasOwnProperty(n['name'])) {
          indexed_array[n['name']] =
            indexed_array[n['name']] + ', ' + n['value'];
        } else {
          indexed_array[n['name']] = n['value'];
        }
      });

      return indexed_array;
    },

    onPlaidResponse: function(public_token, metadata) {
      wc_ach_stripe_form.form.append(
        "<input type='hidden' class='plaid_stripe_token' name='plaidToken' value='" +
          public_token +
          "'/>"
      );
      wc_ach_stripe_form.form.append(
        "<input type='hidden' class='plaid_stripe_token' name='plaidAccountID' value='" +
          metadata.account_id +
          "'/>"
      );
      // let data = {
      //   action: 'check_already_bank_verification',
      //   plaidToken: public_token,
      //   plaidAccountID: metadata.account_id
      // };
      // jQuery.post(
      //   wc_ach_stripe_ajax.ajaxurl,
      //   data,
      //   wc_ach_stripe_form.handlePlaidServerResponse
      // );
      wc_ach_stripe_form.form.submit();
    },
    handlePlaidServerResponse: function(response) {
      console.log(response);
    },
    handleServerResponse: function(response) {
      if (!response.verified) {
        // first purchase
        if (window.location.search.indexOf('show_microdeps') == -1) {
          wc_ach_stripe_form.notVerificatedNotice(
            response.redirection_after_checkout,
            response.popup_header,
            response.popup_text,
            response.popup_text_waiting
          );
        } else {
          wc_ach_stripe_form.fillFieldsForSavedAccount(response);
          wc_ach_stripe_form.microdepsVerificationForm(
            response.ach_stripe_token
          );
        }
      } else if (response.verified == 'waiting') {
        // not first purchase but a bank account us not vcerificated
        if (window.location.search.indexOf('show_microdeps') == -1) {
          wc_ach_stripe_form.notVerificatedNotice(
            response.redirection_after_checkout,
            response.popup_header,
            response.popup_text,
            response.popup_text_waiting,
            true
          );
        } else {
          wc_ach_stripe_form.fillFieldsForSavedAccount(response);
          wc_ach_stripe_form.microdepsVerificationForm(
            response.ach_stripe_token
          );
        }
      } else if (response.verified == 'fail') {
        // failed on the server side
        var error_elem =
          '<div class="woocommerce-NoticeGroup woocommerce-NoticeGroup-checkout"><ul class="woocommerce-error">' +
          '<li>' +
          response.msg +
          '</li>' +
          '</ul></div>';
        $('form.woocommerce-checkout, form#order_review').prepend(error_elem);
        wc_ach_stripe_form.unblock();
        $('html, body').animate(
          {
            scrollTop: $('.woocommerce-error').offset().top - 100
          },
          1000
        );
      } else {
        // make a payment because a customer's bank account is already verified

        if (response.bank) {
          $('#ach-routing-number').val(response.bank.routing_number);
          $('#ach-account-number').val(response.bank.last4);
          $('#ach-account-holder-name').val(response.bank.account_holder_name);
        }

        wc_ach_stripe_form.form.append(
          "<input type='hidden' class='ach_stripe_token' name='ach_stripe_token' value='" +
            response.ach_stripe_token +
            "'/>"
        );
        wc_ach_stripe_form.form.submit();
      }
    },

    // only for saved account
    fillFieldsForSavedAccount: function(response) {
      if (
        $(
          '.payment_method_ach_stripe ul.wc-saved-payment-methods li input:checked'
        ).attr('id') != 'wc-ach_stripe-payment-token-new'
      ) {
        var routing_number = $('#ach-routing-number').val(),
          account_number = $('#ach-account-number').val(),
          account_holder_name = $('#ach-account-holder-name').val(),
          account_holder_type = $(
            '#ach-account-holder-type option:selected'
          ).val();

        if (routing_number.length == 0) {
          $('#ach-routing-number').val(response.bank.routing_number);
        }

        if (account_number.length == 0) {
          $('#ach-account-number').val(response.bank.last4);
        }

        if (account_holder_name.length == 0) {
          $('#ach-account-holder-name').val(response.bank.account_holder_name);
        }

        $('#ach-account-holder-type option:selected').removeAttr('selected');
        $('#ach-account-holder-type')
          .find('option[value="' + response.bank.account_holder_type + '"]')
          .attr('selected', true);
      }
    },

    clearToken: function() {
      $('.ach_stripe_token').remove();
      $('.plaid_stripe_token').remove();
    },

    // if user's bank account is not verified we can't use internal WC verification
    validateFormForNotVerifiedAccount: function() {
      var formRequiredRows = wc_ach_stripe_form.form.find(
        '.form-row.validate-required'
      );
      wc_ach_stripe_form.block();
      var validated = true;
      var differentShipping =
        $('#ship-to-different-address-checkbox').attr('checked') == 'checked';
      var createAccount = $('#createaccount').attr('checked') == 'checked';

      var data = {
        action: 'validate_front_fields_for_not_verified_yet'
      };

      for (var i = 0; i < formRequiredRows.length; i++) {
        var $row = $(formRequiredRows[i]);
        var $input = $row.find('input');

        if (typeof $input.val() == 'string' && $input.val().length == 0) {
          var id = $input.attr('id');
          if (!id) {
            continue;
          }
          // skip shipping fields if the same shipping
          if (!differentShipping && id.indexOf('shipping_') !== -1) {
            continue;
          }
          if (!createAccount && id.indexOf('account_') !== -1) {
            continue;
          }
          data[id] = $input.val();
        }
      }

      for (var i = 0; i < formRequiredRows.length; i++) {
        var $row = $(formRequiredRows[i]);
        var $input = $row.find('input');
        if (
          typeof $input.val() == 'string' &&
          $input.val().length == 0 &&
          Object.keys(data).indexOf($input.attr('id')) !== -1
        ) {
          $row.addClass(
            'woocommerce-invalid woocommerce-invalid-required-field'
          );
          validated = false;
        }
      }

      if (!validated) {
        jQuery.post(wc_ach_stripe_ajax.ajaxurl, data, function(response) {
          if (response.length > 0) {
            $([document.documentElement, document.body]).animate(
              {
                scrollTop: wc_ach_stripe_form.form.offset().top - 100
              },
              1000
            );
            wc_ach_stripe_form.form.prepend(response);
            wc_ach_stripe_form.unblock();
          }
        });
      }

      return validated;
    }
  };

  wc_ach_stripe_form.init();
});
