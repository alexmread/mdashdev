# WooCommerce ACH Stripe Gateway

Version 1.4.9

Created 04/08/2017

Latest Update 06/11/2019

by Ruslan Askarov & Ildar Akhmetov

[Live Demo](http://festplugins.com/woocommerce-stripe-ach-gateway/)

email: [info@festagency.com](mailto:info@festagency.com)



## 1 About

Since [Stripe has accepted ACH payments](https://stripe.com/blog/accept-ach-payments) and WooCommerce has no such functionality we've made this plugin for you to easy accept payments from **bank accounts** via **ACH Stripe**. Also we've integrated **Plaid** to instantly pay via **Online Banking**.

You should understand that payments from a bank account are not instant. A payment will be done approximately in 5 days after purchasing. We provide regular ACH Stripe and Plaid because the first provides **not-instant** bank account verification and it takes 1-2 days. But the second one provides **instant** verification but it takes fees.

## 2 Installation

After you have purchased the plugin, you will receive plugin file in .zip package.

1. To install the plugin, go to Admin Panel >> Plugins.
2. Click the "Add New" button >> then "Upload Plugin" button.
3. Select .zip package & click "Install Now". After that activate the plugin and start to use it.

Remember that you should have the WooCommerce plugin working on your site!

## 3 Getting Started

After you have installed and activated the plugin you should go to WooCommerce >> Settings >> Payments >> ACH Stripe.

Let's introduce all the settings.

### Common Settings

![Common Settings](Common.png)

* Enable/Disable – enable or disable the ACH Stripe gateway
* Title – title of a payment method to be displayed on a checkout page
* Description – description of a payment method to be displayed on a checkout page
* Company Name – your company name. It will be displayed at the bottom of a payment form in accordance with [ACH Authorization](https://support.stripe.com/questions/accepting-ach-payments-with-stripe#ach-authorization)
* Process Orders Immediately - When the **verified** customer makes an order in your shop a charge is automatically created in your Stripe dashboard. 
This charge has "Pending" status and it means that the money will be on you account approximately in 7 days.
By default, the plugin change an orders' statuses after you received the money, i.e. in 7 days after purchase.
Check this option to process orders right after they were made to make your customer doesn't wait for 7 days.
*But remember that there is no money for this order right now!*

### Stripe API Settings

![Stripe API Settings](StripeAPI.png)

* Test Mode – enable test mode where you can test payments from some test bank account
* Test Publishable Key, Test Secret Key – necessary in a test mode. You can get it [from your Stripe dashboard](https://dashboard.stripe.com/)
* Live Publishable Key, Live Secret Key – necessary to receive a real payments. You can get it [from your Stripe dashboard](https://dashboard.stripe.com/)
* Redirection after checkout – the page where your customers will be redirected after a checkout

### Verification Popup Settings

![Verification Popup Settings](VerificationPopup.png)

* Verification Popup Header - The header of the popup appeared when customer needs its bank account to be verified
* Verification Popup Text - The text of the popup appeared only in the first time
* Verification Popup Text for Waiting - The text of the popup user sees in later times. It says that the current purchase requires the verification too and he should wait for the microdeposits

![Verificatoin Popup](verification popup.png)

### Other Useful Settings

![Other Useful Settings](Other.png)

* Saved Bank Accounts – allows customer to save their bank account data to not enter it again
* "Pay for Order" Filter - if enabled, the plugin will filter gateways that the customer can pay for the order with - if the order was created with the ACH than it will be the only gateway the customer can use. In other cases, the customer will not see this gateway. **Disable this option if you don't see the ACH gateway where you expect to see it!**
* Logging – enable/disable logging to a log file in WooCommerce >> System Status >> Logs
* Email template – when your customer makes a payment in first time without a verified band account he will receive an email with instructions of how to verify it. Here, you can customize this message. *Use [order-url] shortcode to put the payment url necessarily! For ex.: `<a href="[order-url]">Pay now</a>`*
* Pay without a Bank Account – WE KINDLY RECOMMEND TO NOT ENABLING THIS OPTION! When a customer makes a purchase at your website in the first time he doesn't have a bank account in your Stripe Dashboard. We create this bank account and make a request to verify it. But it can't be done immediately. And so the customer just has an order that is on-hold. It means that you have to wait until the customer has not verified his account in 5-7 days. When the verification happens the charge will be created in the Stripe and will be completed in 1-2 days. And after this the will begin to process an order. Well, enable this option if want to process orders immediately. But remember YOU WILL PROCESS ORDERS FACTICALY WITHOUT A BANK ACCOUNT AND ANY GUARANTEE OF MONEY!

## 4 Plaid Integration

If you want to let your customers pay via their Online Banking just go to WooCommerce >> Settings >> Payments >> Plaid.

Let's introduce all the settings.

![Plaid Settings](Plaid.png)

* Enable/Disable – enable or disable the ACH Stripe gateway
* Company Name – will be displayed before a checkout in popup
* Title – title of a payment method to be displayed on a checkout page
* Description – description of a payment method to be displayed on a checkout page
* Mode – payment mode in Plaid, one of *Sandbox, Development, Production*. [More about Testing in Plaid](https://plaid.com/docs/api/#sandbox)
* Client ID, Public Key, Plaid Secret – necessary to receive payments. You can get it [from your Plaid dashboard](https://dashboard.plaid.com/account/keys)
* "Pay for Order" Filter - If enabled, the plugin will filter gateways that the customer can pay for the order with - if the order was created with the Plaid than it will be the only gateway the customer can use. In other cases, the customer will not see this gateway.
**Disable this option if you don't see the Plaid gateway where you expect to see it!**
* Logging – enable/disable logging to a log file in WooCommerce >> System Status >> Logs

Remember that you should connect your Stripe and Plaid accounts [here.](https://dashboard.plaid.com/account/integrations)

**5 Setting up your Stripe Dashboard (READ ATTENTIVELY!)**

![](webhook1.png)

You have to set up the [Stripe Dashboard](https://dashboard.stripe.com/) to automate the processing of orders.

Follow the path from the image, but remember **to check in what mode (test or live) you are!**

When you click the Add endpoint button in the Stripe you will see the following popup:

![](webhook2.png)

First, go to your website admin panel to WooCommerce >> Settings >> Payments >> ACH Stripe.

![](processOrder.png)

Here you will see the blue button – click it and go to the Stripe Dashboard. Paste the copied endpoint to **URL to be called** field. The **Filter event** should be **Select types to send.** And you have to choose: 

![](webhook3.png)

and Add endpoint.

And voila!

![](webhook5.png)

This route is needed to send messages from the Stripe about different charges statuses – pending or succeeded.

ACH has its own advantages and disadvantages. The main minus is that money will be transferred to your account not immediately. Shortly, a few days should pass to change the payment status from *Pending* to *Succeeded*. [Read more here.](https://stripe.com/docs/ach#ach-payments-workflow)

So, the most part of popular payment gateways has no such a problem. Hence, we've got a dilemma: change an order status to *Processing* in advance considering that the money will be transferred in a few days or wait until the money will be received and then change an order status.

To solve this we have an option in Admin panel.

![](webhook4.png)

By default, all the orders will be processed only **after corresponding charges in the Stripe will be succeeded!** This is made to you not be worried that you have sold any product but haven't received the money.

But you should understand that such an approach can be confused for your customers. So, it is depending on your business.[Read more here.](https://stripe.com/docs/ach#ach-payments-workflow)

**6 Subscriptions**

The plugin provides the WooCommerce Subscriptions. Well, the Stripe has its own Subscriptions API. But the official Stripe implementation of the WooCommerce Subscription is the simple charge creation.

When you pay for some WooCommerce Subscription you will see the simple charge in your Stripe Dashboard.

**7 Deposits**

The plugin provides it like a charm. This is because Deposits in WooCommerce is the separate system that pass the already end amount for a charge. So it is no matter for Stripe – you just make a simple payment and then make an invoice for your customer.

**8 Payment Process**

The payment process differs in test and live modes. This is because of the Stripe has test bank account already verified and this allows us to avoid a verification via micro deposits. But with the live account your customers should first verify their bank accounts via micro deposits and then pay for their purchases.

First, customer chooses some products and places an order. After this an email will be sent to him with the later instruction. Also it will contain a link to the payment page.

**Note: be sure that you are able to send emails from your site!**

Two micro withdrawals will be done in 2-3 days and when it will be done your customer need to go by that link and make a payment. When he make a payment, he will have to enter those 2 micro withdrawals. So after this his bank account will be verified and he will be able to make any purchases without it.

Remember that after your customer has made a purchase he will receive the email with all the instructions.

**9 Conclusion**

We highly recommend you to read [ACH Authorization](https://support.stripe.com/questions/accepting-ach-payments-with-stripe#ach-authorization) to avoid any disputes about charges or fees. We provide a checkbox for customers to authorize you to electronically debit their accounts.

Also, the plugin provides WooCommerce **auto-refunds** via Stripe.



Hope, you will enjoy this plugin. And we also will be glad to help you!

[Our support](mailto:info@festagency.com)