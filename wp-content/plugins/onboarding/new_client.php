<?php
/**
 * Plugin Name: New Client Oboarding in One Step
 * Plugin URI: http://my.flyrise.io
 * Description: Creates User Account, (*no longer: Company), Everhour Client & Project,
 * & ContentSnare Client via Zapier
 * Version: 1.0
 * Author: Alex Frascona
 * Author URI: http://www.alexyz.com
 */


/*
*   Load Custom Everhour Plugin Clients Class
*/
$plugin_dir = ABSPATH . 'wp-content/plugins/custom-everhour/';
require_once( $plugin_dir . 'inc/everhour-clients.class.php' );

  

/*
*   Load Custom Zapier Plugin Class to Post the New User to ContentSnare
*/
$zap_dir = ABSPATH . 'wp-content/plugins/onboarding/';
require_once( $zap_dir . 'custom-zapier.class.php' );


if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}


// class Onboarding_List_Table extends WP_List_Table {

//     var $example_data = array();
    

//     function __construct(){
//         global $status, $page;
                
//         //Set parent defaults
//         parent::__construct( array(
//             'singular'  => 'project',     //singular name of the listed records
//             'plural'    => 'projects',    //plural name of the listed records
//             'ajax'      => false        //does this table support ajax?
//         ) );
        
//         /*
//             if not an admin
//             subscriber already can't view wp-admin
//             if ( !is_admin() ) {
//                 //
// 			}
//         */
//     }
// }


function at_add_menu_items(){
    add_menu_page('New Client', 'New Client', 'activate_plugins', 'new_client', 'at_render_create_page');
}
add_action('admin_menu', 'at_add_menu_items');


function at_render_create_page(){
    //$testListTable = new Onboarding_List_Table();
    //$testListTable->prepare_items();
    ?>
        <style>
            .wrapper {
                list-style-type: none;
                padding: 0;
                border-radius: 3px;
                display: flex;
                flex-direction: column;
            }
            .inner_wrap {
                background: #ECECEC;
                border: 1px solid #CCC;
                padding: 0 10px;
                margin-top: 5px;
                border-radius: 5px;
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
            }
            .form-row {
                padding: 0.5em;
                display: flex;
                flex-direction: row;
                justify-content: flex-start;
            }
            .form-row-result {

            }
            .form-row > label, .form-row-result > label {
                padding: .5em 1em .5em 0;
                font-weight: bold;
                width: 15%;
            }
            .form-row > input {
                flex: 7;
            }
            .page_title {
                font-size: 1.5em;
                font-weight: bold;
                justify-content: flex-start;
            }
            .page_subtitle {
                font-weight: bold;
                justify-content: flex-start;
            }
            .form-row > input,
            .form-row > button {
                padding: .5em;
            }
            .form-row > button {
                background: gray;
                color: white;
                border: 0;
            }
            .has-error {
                color: red;
                border-color: red;
                font-weight: bold;
            }
            .error {
                padding: 10px !important;
            }

            .wp-core-ui select {
                min-height: 44px;
                max-width: 100%;
                flex: 1;
            }
            .green-check{
                font-size: 1.5em;
                color: #219e00;
                margin: 7px 5px 0 0;
                display: none;
            }
            .red-x{
                display: none;
                font-size: 1.5em;
                color: #9e0000;
                margin: 7px 4px 0 -1px;
            }
            .next {
                font-size: 1.5em;
                color: #ff7d00;
                font-weight: bold;
            }
            
        </style>
    <?php
        //var_dump($_POST);
        //exit;
        if( isset($_POST['username']) && !isset($_POST['cleared']) ){

            /*
            *   Create New Client in Everhour
            */
            $EH_Clients = new Everhour_Clients();
            $EH_Clients->set_api_key(wp_cache_get( 'api_key'));
            if( !isset( $_GET['no_everhour'] ) ){
                $new_client = json_decode($EH_Clients->create_new_client($_POST['company']));
            }else{
                $new_client = false;
            }

            //var_dump($new_client);
            /* example:
            object(stdClass)#585 (13) { ["projects"]=> array(0) { } ["id"]=> int(6019413) ["name"]=> string(5) "New 6" ["createdAt"]=> string(19) "2022-04-11 01:24:41" ["lineItemMask"]=> string(37) "%MEMBER% :: %PROJECT% :: for %PERIOD%" ["paymentDueDays"]=> int(3) ["reference"]=> string(0) "" ["businessDetails"]=> string(0) "" ["invoicePublicNotes"]=> string(0) "" ["excludedLabels"]=> array(0) { } ["status"]=> string(6) "active" ["enableResourcePlanner"]=> bool(false) ["favorite"]=> bool(false) }
            */            

            if( $new_client != false && $new_client->name){
                //echo "New Everhour Client Created: " . $new_client->name . "<br />";
                //echo "New Everhour Client ID: " . $new_client->id . "<br />";
                //echo "New Everhour Client Active Status: " . $new_client->status . "<br />";
            }else{
                if( $new_client != false && $new_client->message ){
                    echo "Error creating New Client with Everhour: " . $new_client->message . "<br />";
                }else{
                    echo "Error creating new Client with Everhour, or disabled by url param no_everhour<br />";
                }
            }


            /*
            *   POST to Zapier
            *   Creates a new Client in ContentSnare
            */
            $ZAP = new Zapier();
            $fullname = $_POST['first_name'] . ' ' . $_POST['last_name'];
            if( !isset( $_GET['no_zapier'] ) ){
                $zap_result = $ZAP->post_to_zapier( $_POST['email'], $fullname, stripcslashes($_POST['company']) );
            }
            
            /* example:
            object(stdClass)#4700 (4) { ["id"]=> string(36) "0184ca46-0750-8bb6-24e8-64a0298791d5" ["attempt"]=> string(36) "0184ca46-0750-8bb6-24e8-64a0298791d5" ["request_id"]=> string(36) "0184ca46-0750-8bb6-24e8-64a0298791d5" ["status"]=> string(7) "success" }
            */


            // Create User and/or Customer

            //$result = wp_create_user($_POST['username'], 'password', $_POST['email']);
            // $user_id = wp_insert_user( array(
            //     'user_login' => $_POST['username'],
            //     'user_pass' => 'password',
            //     'user_email' => $_POST['email'],
            //     'first_name' => $_POST['first_name'],
            //     'last_name' => $_POST['last_name'],
            //     'display_name' => $_POST['first_name'] . $_POST['last_name'],
            //     'role' => 'subscriber',
            //     'company' => $_POST['company'],
            // ));
            $meta = array(
                'first_name' => $_POST['first_name'],
                'last_name' => $_POST['last_name'],
                // 'billing_first_name' => $_POST['first_name'],
                // 'billing_last_name' => $_POST['last_name'],
                // 'billing_address_1' => $_POST['line_1'],
                // 'billing_address_2' => $_POST['line_2'],
                // 'billing_country' => $_POST['country'],
                // 'billing_state' => $_POST['state'],
                // 'billing_postcode' => $_POST['zip'],
                // 'billing_city' => $_POST['city'],
                'billing_email' => $_POST['email'],
                //'billing_phone' => $_POST['phone'],
                'billing_company' => $_POST['company'],
                'brand' =>  $_POST['brand'],
                // 'shipping_first_name' => $_POST['first_name'],
                // 'shipping_last_name' => $_POST['last_name'],
                // 'shipping_address_1' => $_POST['line_1'],
                // 'shipping_address_2' => $_POST['line_2'],
                // 'shipping_country' => $_POST['country'],
                // 'shipping_state' => $_POST['state'],
                // 'shipping_postcode' => $_POST['zip'],
                // 'shipping_city' => $_POST['city'],
                'shipping_email' => $_POST['email'],
                //'shipping_phone' => $_POST['phone'],
                'shipping_company' => $_POST['company'],
                //'profileimg'    => array(0 => $_POST['profileimg']),
                //'_flyrise_user_profile' => array (0 => $_POST['profileimg']),
                //'profileimg' => array (0 => 'https://launch.flyrise.io/wp-content/uploads/flyrise-avatar.jpg'),
                //'_flyrise_user_profile' => array (0 => 'https://launch.flyrise.io/wp-content/uploads/flyrise-avatar.jpg'),
            );
              
            //let's create our user
            $id = wp_create_user( $_POST['username'], 'test', $_POST['email'] );
            $user_id_role = new WP_User($id);
            $user_id_role->set_role('customer');
            //$id = wc_create_new_customer( $_POST['email'], $_POST['username'], 'test' );
            
            

            //check if there are no errors
            if ( ! is_wp_error( $id ) ) {

                $uid = $id;

                /*
                *   upload image and attach to user
                */

                    if($_FILES['profileimg']['size'] != 0){
                        // echo "<pre>";
                        // print_r($_FILES);
                        // echo "</pre>";
                        // exit;
                        //$userID = $new_client->ID;
                      
                        $posted_data =  isset( $_POST ) ? $_POST : array();
                        $file_data = isset( $_FILES ) ? $_FILES : array();
                        $upload = $_FILES['profileimg'];

                        $data = array_merge( $posted_data, $file_data );

                        $uploads = wp_upload_dir(); /*Get path of upload dir of wordpress*/
                        if (is_writable($uploads['path']))  /*Check if upload dir is writable*/
                        {
                            if ((!empty($upload['tmp_name'])))  /*Check if uploaded image is not empty*/
                            {
                                if ($upload['tmp_name'])   /*Check if image has been uploaded in temp directory*/
                                {
                                    //require ABSPATH . 'wp-admin/includes/image.php';
                                    //require ABSPATH . 'wp-admin/includes/file.php';
                                    //require ABSPATH . 'wp-admin/includes/media.php';
                                    
                                    $overrides = array('test_form' => false);
                                    $attachment_id = wp_handle_upload($upload, $overrides);

                                    $fullsize_path = get_attached_file( $attachment_id );
                                    $url = $attachment_id['url'];
        
                                    update_user_meta($uid, '_flyrise_user_profile', $url);           
                                    update_user_meta($uid, 'profileimg', $url);               
                                }
                            }
                        }
                    }else{
                        if (isset($_POST['brand'])){
                            switch($_POST['brand']){
                                case 'flyrise':
                                    $url = 'https://launch.flyrise.io/wp-content/uploads/flyrise-avatar.jpg';
                                    update_user_meta($uid, '_flyrise_user_profile', $url);
                                    update_user_meta($uid, 'profileimg', $url);   
                                break;
                                case 'ridgecapmarketing':
                                    $url = 'https://launch.flyrise.io/wp-content/uploads/flyrise-avatar.jpg';
                                    update_user_meta($uid, '_flyrise_user_profile', $url);
                                    update_user_meta($uid, 'profileimg', $url);   
                                break;
                                default:
                                    $url = 'https://launch.flyrise.io/wp-content/uploads/flyrise-avatar.jpg';
                                    update_user_meta($uid, '_flyrise_user_profile', $url);
                                    update_user_meta($uid, 'profileimg', $url);   
                            }
                        }
                    }

                /*
                *   End Image Upload
                */


                foreach( $meta as $key => $val ) {
                    update_user_meta( $id, $key, $val ); 
                }
                if( !isset( $_GET['no_everhour'] ) ){
                    update_field( 'everhour_client_id', $new_client->id, 'user_'.$id );
                }
                update_field( 'qwilr_proposal_link', $_POST['qwilr_proposal_link'], 'user_'.$id );
                //update_field( 'billing_country', $_POST['country'], 'user_'.$id ); // doesn't work
                //update_field( 'shipping_country', $_POST['country'], 'user_'.$id );
                update_field( 'brand', $_POST['brand'], 'user_'.$id );
                // $customer = new WC_Customer( 'user_' . $id );

                // $customer->set_billing_country( $_POST['country'] );
                // $customer->set_shipping_country( $_POST['country'] );
                // $customer->save();

                // Update ACF Field for Zapier
                // $count = (int) get_field('company');
                update_field( 'company', $_POST['company'], 'user_' . $id );

                //WC()->customer->set_billing_country(wc_clean( $_POST['country'] )); 
            }else{
                $id_error = $id->get_error_message();
                echo "Error while creating a User:<br />";
                print_r($id_error);
            }


            // Create Company (custom post type)
            // insert the post and set the category
            /* We decided NOT to create a new Company any longer
            $new_company_id = wp_insert_post(array (
                'post_type' => 'companies',
                'post_title' => $_POST['company'],
                'post_content' => '',
                'post_status' => 'publish',
                'meta_input' => [
                    'companyname' => $_POST['company'],
                    'compaddress1' => $_POST['line_1'],
                    'compaddress2' => $_POST['line_2'],
                    'compcity' => $_POST['city'],
                    'comppostalcode' => $_POST['zip'],
                    'compstate' => $_POST['state'],
                ]
            ));

            // if ($new_company_id) {
            //     // insert post meta
            //     add_post_meta($new_company_id, 'companyname', $_POST['company']);
            //     add_post_meta($new_company_id, 'compaddress1', $_POST['line_1']);
            //     add_post_meta($new_company_id, 'compaddress2', $_POST['line_2']);
            //     add_post_meta($new_company_id, 'compcity', $_POST['city']);
            //     add_post_meta($new_company_id, 'comppostalcode', $_POST['zip']);
            //     add_post_meta($new_company_id, 'compstate', $_POST['state']);
            // }
            */

            //if( is_wp_error($id) || is_wp_error($new_company_id) ){
            if( is_wp_error($id) ){
                if( is_wp_error($id) ){
                    $error = $id->get_error_message();
                    echo "Error while creating a Customer:<br />";
                    print_r($error);
                }else{
                    //echo "Customer Created: " . $id . "<br />";
                }
                // if( is_wp_error($new_company_id) ){
                //     $company_error = $new_company_id->get_error_message();
                //     echo "Error while creating a Company:<br />";
                //     print_r($company_error);
                // }else{
                //     echo "Company Created: " . $new_company_id . "<br />";
                // }
                //exit;
            }else{
                //echo "Customer Created: " . $id . "<br />";
                //echo "Company Created: " . $new_company_id . "<br />";
                //print_r($user);
                //WP_User Object ( [data] => stdClass Object ( [ID] => 5029 [user_login] => franklin [user_pass] => $P$BZnP4jyxOHGgLm.q02oGibPWEf8f4l1 [user_nicename] => franklin [user_email] => franklin@newbiz.com [user_url] => [user_registered] => 2022-03-22 19:23:56 [user_activation_key] => [user_status] => 0 [display_name] => franklin ) [ID] => 5029 [caps] => Array ( [subscriber] => 1 ) [cap_key] => wp_capabilities [roles] => Array ( [0] => subscriber ) [allcaps] => Array ( [read] => 1 [level_0] => 1 [subscriber] => 1 ) [filter] => [site_id:WP_User:private] => 1 )
            }
            //echo $_POST['username'];



            /*
            *   Now let's create the Order or Subscription for the Admin
            */
            /*
            $order = wc_create_order( array( 'customer_id' => $id ) );
            $new_order_id = $order->get_id();

            $user = get_user_by( 'ID', $id );

            $fname     = $user->first_name;
            $lname     = $user->last_name;
            $email     = $user->user_email;
            // $address_1 = get_user_meta( $id, 'billing_address_1', true );
            // $address_2 = get_user_meta( $id, 'billing_address_2', true );
            // $city      = get_user_meta( $id, 'billing_city', true );
            // $postcode  = get_user_meta( $id, 'billing_postcode', true );
            // $country   = get_user_meta( $id, 'billing_country', true );
            // $state     = get_user_meta( $id, 'billing_state', true );
            

            $address         = array(
                'first_name' => $fname,
                'last_name'  => $lname,
                'email'      => $email,
                // 'address_1'  => $address_1,
                // 'address_2'  => $address_2,
                // 'city'       => $city,
                // 'state'      => $state,
                // 'postcode'   => $postcode,
                // 'country'    => $country,
            );

            $order->set_address( $address, 'billing' );
            $order->set_address( $address, 'shipping' );
            //$order->add_product( $product, 1 );

            if ( isset($_POST['order_type']) && $_POST['order_type'] == 'subscription' ){
                $product = wc_get_product(4684);

                $sub = wcs_create_subscription(array(
                    'order_id' => $order->get_id(),
                    'status' => 'pending', // Status should be initially set to pending to match how normal checkout process goes
                    'billing_period' => WC_Subscriptions_Product::get_period( $product ),
                    'billing_interval' => WC_Subscriptions_Product::get_interval( $product )
                ));

                write_log('My Inner Space Starts:');
                write_log( $sub );
                
                write_log('period:');
                write_log(WC_Subscriptions_Product::get_period( $product ));
                write_log('interval:');
                write_log(WC_Subscriptions_Product::get_interval( $product ));

                write_log('My Inner Space Ends');

                $new_sub_id = $sub->id;
            }
            */

            ?>
                <div class="wrap">
                    <div class="inner_wrap">
                        <ul class="wrapper">
                            <li class="form-row-result page_title">
                                <label><a href="?page=new_client&cleared">Create Another</a></label>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="wrap">
                    <div class="inner_wrap">
                        <form id="new_user_result" method="get">
                            <ul class="wrapper">
                                <!--<li class="form-row-result"><label>Created User: </label><a href="/wp-admin/user-edit.php?user_id=<?php //echo $user->id; ?>&wp_http_referer=%2Fwp-admin%2Fusers.php"><?php //echo $_POST['username']; ?></a></li>-->
                                <!--<li class="form-row-result"><label>Created User: </label><a href="/wp-admin/user-edit.php?user_id=<?php //echo $user_id; ?>&wp_http_referer=%2Fwp-admin%2Fusers.php"><?php //echo $_POST['username']; ?></a></li>-->
                                <li class="form-row-result"><label>Created Customer: </label><a target="_new" href="/wp-admin/user-edit.php?user_id=<?php echo $id; ?>&wp_http_referer=%2Fwp-admin%2Fusers.php"><?php echo $_POST['username'] . ", ID: " . $id; ?></a></li>
                                <li class="form-row-result"><label>Name: </label><?php echo $_POST['first_name'] . ' ' . $_POST['last_name']; ?></li>
                                <li class="form-row-result"><label>Email: </label><?php echo $_POST['email']; ?></li>
                                <!--<li class="form-row-result"><label>Created Company: </label><a href="/wp-admin/post.php?post=19340&action=edit"><?php //echo $_POST['company']; ?></a></li>-->
                                

                                <!-- <li class="form-row-result"><label>Address</label></li>
                                <li class="form-row-result"><label>Line 1: </label><?php echo $_POST['line_1']; ?></li>
                                <li class="form-row-result"><label>Line 2: </label><?php echo $_POST['line_2']; ?></li>
                                <li class="form-row-result"><label>City: </label><?php echo $_POST['city']; ?></li>
                                <li class="form-row-result"><label>Zip: </label><?php echo $_POST['zip']; ?></li>
                                <li class="form-row-result"><label>Country: </label><?php echo $_POST['country']; ?></li>
                                <li class="form-row-result"><label>State: </label><?php echo $_POST['state']; ?></li>
                                <li class="form-row-result"><label>Phone: </label><?php echo $_POST['phone']; ?></li> -->


                                <li class="form-row-result"><label>Everhour Client: </label>
                                    <?php
                                        if( $new_client != false ){ ?>
                                            <a href="https://app.everhour.com/#/clients/<?php echo $new_client->name . ", ID: " . $new_client->id; ?>" target="_blank"><?php echo $new_client->id; ?></a>
                                        <?php }else{
                                            echo "disabled by url param no_everhour";
                                        }
                                    ?>
                                </li>

                                <!--
                                    EH Client ID example: 2628937
                                    EH Project ID example: cl:43884674
                                -->
                                <li class="form-row-result"><label>Qwilr proposal: </label><a href="http://www.qwilr.com?post=19340" target="_blank"><?php echo $_POST['qwilr_proposal_link']; ?></a></li>
                                
                                <li class="form-row-result"><label>Brand: </label><?php echo $_POST['brand']; ?></li>

                                <li class="form-row-result">
                                    <label style="vertical-align: top;">Image: </label>
                                    <img width="50px" src="<?php echo $url; ?>">
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>

                <div class="wrap">
                    <div class="inner_wrap">
                        <ul class="wrapper">
                            <!-- <li class="form-row-result next">Next Create an
                                <a target="_new" href="/wp-admin/post-new.php?post_type=shop_order">Order</a> 
                                 or a
                                <a target="_new" href="/wp-admin/post-new.php?post_type=shop_subscription">Subscription</a> 
                                for this Client
                            </li> -->
                            <?php
                                if(isset($new_order_id)){
                            ?>
                                    <li class="form-row-result next">Next: Add Products to their Order: 
                                        <a target="_new" href="/wp-admin/post.php?post=<?php echo $new_order_id; ?>&action=edit"><?php echo $new_order_id; ?></a>
                                    </li>
                            <?php
                                }
                                if(isset($new_sub_id)){
                            ?>

                                    <li class="form-row-result next">Next: Add Products to their Subscription:
                                        <a target="_new" href="/wp-admin/post.php?post=<?php echo $new_sub_id; ?>&action=edit"><?php echo $new_sub_id; ?></a>
                                    </li>
                            <?php
                                }
                            ?>
                        </ul>
                    </div>
                </div>

                <div class="wrap">
                    <div class="inner_wrap">
                        <ul class="wrapper">
                            <li class="form-row-result"><label>Link to the Client's new Dashboard Account where they will continue their Onboarding:</label></li>
                            <?php
                                $url_base_brand_switch = '';
                                
                                //echo 'site_url: ' . Site_url() . "<br />";

                                switch(Site_url()){
                                    case 'https://mdashdev.local': $url_base_brand_switch = 'https://mdashdev.local'; break;
                                    case 'https://mdashdev.wpengine.com': $url_base_brand_switch = 'https://mdashdev.wpengine.com'; break;
                                    case 'https://my.flyrise.io':
                                    case 'https://my.ridgecapmarketing.com':
                                    case 'https://launch.flyrise.io':
                                        switch($_POST['brand']){
                                            case 'flyrise': $url_base_brand_switch = 'https://my.flyrise.io'; break;
                                            case 'ridgecapmarketing': $url_base_brand_switch = 'https://my.ridgecapmarketing.com'; break;
                                        }
                                        break;
                                }
                            ?>
                            <li class="form-row-result"><span id="copy"><?php echo $url_base_brand_switch; ?>/onboard?username=<?php echo $_POST['username']; ?></span> <button onclick="copyEvent('copy')">Copy</button></li>
                        </ul>
                    </div>
                </div>
                <script>
                    function copyEvent(id)
                    {
                        var str = document.getElementById(id);
                        window.getSelection().selectAllChildren(str);
                        document.execCommand("Copy");
                        alert("Copied to the Clipboard");
                    }
                </script>
                
                <!-- paste -> should look like this: http://my.flyrise.io/onboarding/ohmygoshtheerrorinputvalidationcouldreallysuckonthis -->

                <div class="wrap">
                    <div class="inner_wrap">
                        <ul class="wrapper">
                            <li class="form-row-result"><label>Posted to Zapier: </label>
                            <?php    
                                if(isset($zap_result)){
                                    print_r($zap_result);
                                }else{
                                    echo "either an error posting to zapier or dissabled via url param no_zapier<br />";
                                }    
                            ?></li>
                        </ul>
                    </div>
                </div>
            <?php
        }else{
            
        ?>
        <script src="https://kit.fontawesome.com/1b8197db00.js" crossorigin="anonymous"></script>
        <div class="wrap">
            <div class="inner_wrap">
                
                <form id="new_user" method="post" enctype="multipart/form-data">
                    <!-- the form posts back to our current page -->
                    <input type="hidden" name="create_new_user" value="<?php echo $_REQUEST['page'] ?>" />
                    <ul class="wrapper">
                        <li class="form-row page_title">New Client Onboarding:</li>
                        <li class="form-row page_subtitle">Here we will simultaneously create: a new User and associate it to a new Company</li>
                        <li class="form-row">
                            <span class="green-check"><i class="fa-regular fa-square-check"></i></span>
                            <span class="red-x" ><i class="fa-regular fa-circle-xmark"></i></span>

                            <label for="username">Username</label>
                            <input onselect="clearInput('username')" type="text" id="username" name="username" value="">
                        </li>
                        <li class="form-row">
                            <span class="green-check"><i class="fa-regular fa-square-check"></i></span>
                            <span class="red-x" ><i class="fa-regular fa-circle-xmark"></i></span>

                            <label for="first_name">First Name</label>
                            <input type="text" id="first_name" name="first_name">
                        </li>
                        <li class="form-row">
                            <span class="green-check"><i class="fa-regular fa-square-check"></i></span>
                            <span class="red-x" ><i class="fa-regular fa-circle-xmark"></i></span>

                            <label for="last_name">Last Name</label>
                            <input type="text" id="last_name" name="last_name">
                        </li>
                        <li class="form-row">
                            <span class="green-check"><i class="fa-regular fa-square-check"></i></span>
                            <span class="red-x" ><i class="fa-regular fa-circle-xmark"></i></span>

                            <label for="email">Email</label>
                            <input required pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" type="text" id="email" name="email">
                        </li>
                        <li class="form-row">
                            <span class="green-check"><i class="fa-regular fa-square-check"></i></span>
                            <span class="red-x" ><i class="fa-regular fa-circle-xmark"></i></span>

                            <label for="company">Company</label>
                            <input type="text" id="company" name="company">
                        </li>


                        <!-- 
                        <li class="form-row page_subtitle">User & Company Billing Address</li>
                        <li class="form-row">
                            <span class="green-check"><i class="fa-regular fa-square-check"></i></span>
                            <span class="red-x" ><i class="fa-regular fa-circle-xmark"></i></span>

                            <label for="line_1">Line 1</label>
                            <input type="text" id="line_1" name="line_1">
                        </li>
                        <li class="form-row">
                            <span class="green-check"><i class="fa-regular fa-square-check"></i></span>
                            <span class="red-x" ><i class="fa-regular fa-circle-xmark"></i></span>

                            <label for="line_2">Line 2</label>
                            <input type="text" id="line_2" name="line_2">
                        </li>
                        <li class="form-row">
                            <span class="green-check"><i class="fa-regular fa-square-check"></i></span>
                            <span class="red-x" ><i class="fa-regular fa-circle-xmark"></i></span>

                            <label for="city">City</label>
                            <input id="city" type="text" name="city">
                        </li>
                        <li class="form-row">
                            <span class="green-check"><i class="fa-regular fa-square-check"></i></span>
                            <span class="red-x" ><i class="fa-regular fa-circle-xmark"></i></span>

                            <label for="zip">Zip</label>
                            <input id="zip" type="text" name="zip">
                        </li>
                        <li class="form-row">
                            <span class="green-check"><i class="fa-regular fa-square-check"></i></span>
                            <span class="red-x" ><i class="fa-regular fa-circle-xmark"></i></span>

                            <label for="country">Country/Region</label>
                            <select id="country" name="country">
                                <option value="CA">Canada</option>
                                <option value="MX">Mexico</option>
                                <option value="US" selected>United States of America</option>
                            </select>
                        </li>
                        <li class="form-row">
                            <span class="green-check"><i class="fa-regular fa-square-check"></i></span>
                            <span class="red-x" ><i class="fa-regular fa-circle-xmark"></i></span>

                            <label for="state">State/County</label>
                            <select id="state" name="state">
                                <option value="AL">Alabama</option>
                                <option value="AK">Alaska</option>
                                <option value="AZ">Arizona</option>
                                <option value="AR">Arkansas</option>
                                <option value="CA">California</option>
                                <option value="CO">Colorado</option>
                                <option value="CT">Connecticut</option>
                                <option value="DE">Delaware</option>
                                <option value="DC">District Of Columbia</option>
                                <option value="FL">Florida</option>
                                <option value="GA">Georgia</option>
                                <option value="HI">Hawaii</option>
                                <option value="ID">Idaho</option>
                                <option value="IL">Illinois</option>
                                <option value="IN">Indiana</option>
                                <option value="IA">Iowa</option>
                                <option value="KS">Kansas</option>
                                <option value="KY">Kentucky</option>
                                <option value="LA">Louisiana</option>
                                <option value="ME">Maine</option>
                                <option value="MD">Maryland</option>
                                <option value="MA">Massachusetts</option>
                                <option value="MI">Michigan</option>
                                <option value="MN">Minnesota</option>
                                <option value="MS">Mississippi</option>
                                <option value="MO">Missouri</option>
                                <option value="MT">Montana</option>
                                <option value="NE">Nebraska</option>
                                <option value="NV">Nevada</option>
                                <option value="NH">New Hampshire</option>
                                <option value="NJ">New Jersey</option>
                                <option value="NM">New Mexico</option>
                                <option value="NY">New York</option>
                                <option value="NC">North Carolina</option>
                                <option value="ND">North Dakota</option>
                                <option value="OH">Ohio</option>
                                <option value="OK">Oklahoma</option>
                                <option value="OR" selected>Oregon</option>
                                <option value="PA">Pennsylvania</option>
                                <option value="RI">Rhode Island</option>
                                <option value="SC">South Carolina</option>
                                <option value="SD">South Dakota</option>
                                <option value="TN">Tennessee</option>
                                <option value="TX">Texas</option>
                                <option value="UT">Utah</option>
                                <option value="VT">Vermont</option>
                                <option value="VA">Virginia</option>
                                <option value="WA">Washington</option>
                                <option value="WV">West Virginia</option>
                                <option value="WI">Wisconsin</option>
                                <option value="WY">Wyoming</option>
                            </select>
                        </li>
                        <li class="form-row">
                            <span class="green-check"><i class="fa-regular fa-square-check"></i></span>
                            <span class="red-x" ><i class="fa-regular fa-circle-xmark"></i></span>

                            <label for="phone">Phone</label>
                            <input type="text" id="phone" name="phone">
                        </li> -->


                        <!-- <li class="form-row">
                            <label for="everhour">Everhour Client ID</label>
                            <input type="text" id="everhour" name="everhour">
                        </li> -->
                        <li class="form-row">
                            <span class="green-check"><i class="fa-regular fa-square-check"></i></span>
                            <span class="red-x" ><i class="fa-regular fa-circle-xmark"></i></span>

                            <label for="qwilr_proposal_link">Qwilr Proposal link</label>
                            <input type="text" id="qwilr_proposal_link" name="qwilr_proposal_link">
                        </li>

                        <li class="form-row">
                            <span class="green-check"><i class="fa-regular fa-square-check"></i></span>
                            <span class="red-x" ><i class="fa-regular fa-circle-xmark"></i></span>

                            <label for="brand">Brand</label>
                            <select id="brand" name="brand">
                                <option value="none" selected disabled hidden>Select a brand</option>
                                <option value="flyrise">flyrise</option>
                                <option value="ridgecapmarketing">ridgecapmarketing</option>
                            </select>
                        </li>

                        <li class="form-row">
                            <span class="green-check"><i class="fa-regular fa-square-check"></i></span>
                            <span class="red-x" ><i class="fa-regular fa-circle-xmark"></i></span>

                            <label for="order_type">Order Type</label>
                            <select id="order_type" name="order_type">
                                <option value="none" selected disabled hidden>Select the type of Order to Create for this Client</option>
                                <option value="order">Single Order</option>
                                <option value="subscription">Subscription</option>
                            </select>
                        </li>

                        <li class="form-row">
                            <span class="green-check"><i class="fa-regular fa-square-check"></i></span>
                            <span class="red-x" ><i class="fa-regular fa-circle-xmark"></i></span>

                            <label for="profileimg">Profile Image</label>
                            <input type="file" id="profileimg" ref="file" name="profileimg">
                        </li>
                        
                        <li class="form-row">
                            <input type="submit" value="Create" />
                        </li>
                    </ul>
                </form>
            </div>
        </div>
       
        <script>
            // manual validation:

            function clearInput(e){
                jQuery("#" + e).val("");
            }
            // function validate(e){
            //     if ( jQuery("#" + e).val().indexOf(" ") > 0 || jQuery("#" + e).val() == "" ){
            //         jQuery("#" + e).css('border-color','red');
            //         jQuery("#" + e).val("required and without spaces");
            //     }else{
            //         jQuery("#" + e).css('border-color','#8c8f94');
            //     }
            // }
            // function validate_not_empty(e){
            //     //return 0;
            //     //console.log(e.value.length);
                
            //     if ( jQuery(e).val() == "" ){
            //         return false;
            //         // jQuery("#" + e).css('border-color','red');
            //         // jQuery("#" + e).val("required");
            //     }else{
            //         //jQuery("#" + e).css('border-color','#8c8f94');
            //         return true;
            //     }
            // }
            // function validate_email(e){
            //     var userinput = $('#' + e).val();
            //     var pattern = "/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i";
            //     if(!pattern.test(userinput))
            //     {
            //         jQuery("#" + e).css('border-color','red');
            //         jQuery("#" + e).val("email invalid");
            //     }else{
            //         jQuery("#" + e).css('border-color','#8c8f94');
            //     }
            // }
            jQuery(function() {
        
                jQuery('#username').blur(function(){
                    //
                });

                /*
                    Only requireds:
                        email
                        username
                        company
                        qwilr
                        brand
                */
                // using jquery validate
                jQuery( "#new_user" ).validate({

                    rules: {
                        username: {
                            required: true,
                            minlength: 1,
                        },
                        first_name: {
                            required: false,
                            minlength: 1,
                            pattern: "^[a-zA-Z_]*$",
                        },
                        last_name: {
                            required: false,
                            minlength: 1,
                            pattern: "^[a-zA-Z_]*$",
                        },
                        emailfield: {
                            required: true,
                            email: true
                        },
                        // company: {
                        //     required: true,
                        //     minlength: 1,
                        // },
                        // line_1: {
                        //     required: false,
                        //     minlength: 5,
                        // },
                        // city: {
                        //     required: false,
                        //     minlength: 3,
                        // },
                        // zip: {
                        //     required: false,
                        //     digits: true,
                        //     minlength: 5
                        // },
                        // phone: {
                        //     required: false,
                        //     digits: true,
                        //     minlength: 7
                        // },
                        qwilr_proposal_link: {
                            required: true,
                            url: true
                        },
                        brand: {
                            required: true,
                            minlength: 1
                        }
                    },
                    highlight: function(element) {
                        jQuery(element).closest('.form-row').addClass('has-error');
                        jQuery(element).closest('.form-row').children('.red-x').css('display','block');
                        jQuery(element).closest('.form-row').children('.green-check').css('display','none');
                    },
                    unhighlight: function(element) {
                        jQuery(element).closest('.form-row').removeClass('has-error');
                        jQuery(element).closest('.form-row').children('.red-x').css('display','none');
                        jQuery(element).closest('.form-row').children('.green-check').css('display','block');
                    },
                    
                });
            });
            
        </script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
        <script>
            jQuery.extend(jQuery.validator.messages, {
                required: "This field is required.",
                email: "Please enter a valid email address.",
                url: "Please enter a valid URL.",
                number: "Please enter a valid number.",
                digits: "Please enter only digits.",                
            });
        </script>
    <?php
    }
}