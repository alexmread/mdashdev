<?php

class Zapier {

    //var $webhook_url = 'https://hooks.zapier.com/hooks/catch/2605075/bpeb8zx/';
    //var $all_report;

    // function set_api_key($api_key){
    //     $this->api_key = $api_key;
    // }    

    function post_to_zapier($email, $fullname, $company){
        
        // $request_report = wp_remote_get( 'https://api.everhour.com/dashboards/projects?date_gte=' . $start . '&date_lte=' . $end,
        //     array( 'timeout' => 10,
        //         'headers' => array( 'X-Api-Key' => $this->api_key ) 
        //     )
        // );

        $webhook_url = 'https://hooks.zapier.com/hooks/catch/2605075/bpeb8zx/';
        
        $body = array(
            'email'         => $email,
            'full_name'     => $fullname,
            'company_name'  => $company
        );

        $args = array(
            'method'      => 'POST',
            'timeout'     => 45,
            'sslverify'   => false,
            'headers'     => array(
                //'Authorization' => 'Bearer {token goes here}',
                'Content-Type'  => 'application/json',
            ),
            'body'        => json_encode($body),
        );

        $request = wp_remote_post( $webhook_url, $args );

        if( is_wp_error( $request ) ) {
            $error_string = $request->get_error_message();
            echo '<div id="message" class="error"><p>' . $error_string . '</p></div>';
        }

        $reportObj = wp_remote_retrieve_body( $request );
        $report_data = json_decode( $reportObj );
        //$this->all_report = $report_data;
        //var_dump($report_data);
        return $report_data;

        /*
        *   result example:
        *   object(stdClass)#4722 (4) { ["id"]=> string(36) "0184ca57-72e2-362d-0757-94a5fa5db7e2" ["attempt"]=> string(36) "0184ca57-72e2-362d-0757-94a5fa5db7e2" ["request_id"]=> string(36) "0184ca57-72e2-362d-0757-94a5fa5db7e2" ["status"]=> string(7) "success" }
        */
        
    }

    // function match_client_get_cost($client){
    //     $cost_to_return = 0;
    //     foreach($this->all_report as $client_obj){
    //         if( isset($client_obj->clientName) && $client_obj->clientName == $client && isset($client_obj->costs) ){
    //             $cost_to_return = number_format($client_obj->costs/100, 2);
    //         }
    //     }
    //     return $cost_to_return;
    // }
}

// $ZAP = new Zapier();
// $ZAP->post_new_client_to_zapier_and_on_to_contentsnare('test@yahoo.com');
