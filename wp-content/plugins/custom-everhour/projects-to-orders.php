<?php

if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Everhour_Projects_List_Table extends WP_List_Table {

    var $example_data = array();


    function __construct($api_key){
        global $status, $page;
                
        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'project',     //singular name of the listed records
            'plural'    => 'projects',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );        

        /*
        *   Everhour Projects Mapped to Woocommerce Orers
        */

        $request_projects = wp_remote_get( 'https://api.everhour.com/projects' ,
            array( 'timeout' => 10,
                'headers' => array( 'X-Api-Key' => $api_key ) 
            )
        );

        if( is_wp_error( $request_projects ) ) {
            $error_string = $request_projects->get_error_message();
            echo '<div id="message" class="error"><p>' . $error_string . '</p></div>';
        }

        $projects = wp_remote_retrieve_body( $request_projects );
        $projects_data = json_decode( $projects );
        if( ! empty( $projects_data ) ) {
            //print_r($projects_data);

            foreach($projects_data as $project){
                //print_r($project);

                /* Lookup Woocommerce Orders for/by Company Name */
                // 'wc-on-hold', 'wc-completed', 'wc-cancelled','wc-refunded','wc-failed' 
                $args = array(
                    'orderby' => 'modified',
                    'order' => 'DESC',
                    'status' => array('wc-processing', 'wc-pending'),
                    'limit' => 30,
                    'billing_company' => $project->name
                );
                $orders = wc_get_orders( $args );
                foreach($orders as $order){       
                    $order_data = $order->get_data();  
                    //echo "<li>ID: " . $order->id . ", " . $order->status . ", Total: " . $order->total . ", customer ID: " . $order->customer_id . ", Date Modified: " . $order->date_modified . "</li>";
                    //print_r($order);
                    $formatted_paid = new DateTime($order->get_date_paid());
                    
                    $an_order = array(
                        'ID'        => $order->get_id(),
                        'order'     =>  $order->get_id(),
                        'company'     => $order_data['billing']['company'],
                        'status'    => $order->get_status(),
                        'total'  => $order->get_formatted_order_total(),
                        'date'  =>  $formatted_paid->format('Y-m-d')
                    );
                    array_push($this->example_data, $an_order);

                };
            }
        }



        /*
        *   Everhour All Expenses Report
        */

        $request_report = wp_remote_get( 'https://api.everhour.com/expenses' ,
            array( 'timeout' => 10,
                'headers' => array( 'X-Api-Key' => $api_key ) 
            )
        );

        if( is_wp_error( $request_report ) ) {
            $error_string = $request_report->get_error_message();
            echo '<div id="message" class="error"><p>' . $error_string . '</p></div>';
        }

        $report_data = json_decode( $request_report['body'] );
        //var_dump($report_data);
        if( ! empty( $report_data ) ) {
            //echo "<pre>";
            //print_r( $request_report );
            //echo "</pre>";

            echo "<br /><table class='wp-list-table widefat fixed striped table-view-list reports'>
                    <thead>
                        <tr>
                            <th scope='col' id='report' class='manage-column column-project column-primary'>
                                <a href='h'>
                                    <span>Everhour projectId</span>
                                    <span class='sorting-indicator'></span>
                                </a>
                            </th>
                            <th scope='col' id='asId' class='manage-column column-name'>
                                <a href=''><span>Name</span></a>
                            </th>
                            <th scope='col' id='client' class='manage-column column-client'>
                                <a href=''><span>Client</span></a>
                            </th>
                            <th scope='col' id='expenses' class='manage-column column-expenses'>
                                <a href=''><span>Expenses</span></a>
                            </th>
                        </tr>
                    </thead>
                    <tbody id='the-list' data-wp-lists='list:report'>";

            foreach( $report_data as $report ){
                //echo "Everhour projectID: " . $report->projectId . ", Name: " . $report->projectName . ", Client: " . $report->clientName . ", Expenses: $" . $report->expenses/100 . "<br />";
                echo "<tr>
                    <td class='order column-project has-row-actions column-primary'>" . $report->project . "</td>
                    <td class='company column-name'>" . (isset($report->details)? $report->details: '') . "</td>
                    <td class='status column-client'>" . $report->date . "</td>
                    <td class='total column-expenses'>$" . $report->amount/100 . "</td>
                    </tr>";
                }
            echo "<tfoot>
                <tr>
                    <th scope='col' class='manage-column column-project column-primary'>
                        <a href=''><span>Everhour projectId</span></a>
                    </th>
                    <th scope='col' class='manage-column column-name'>
                        <a href=''><span>Name</span></a>
                    </th>
                    <th scope='col' class='manage-column column-client'>
                        <a href=''><span>Client</span></a>
                    </th>
                    <th scope='col' class='manage-column column-expenses'>
                        <a href=''><span>Expenses</span></a>
                    </th>
                </tr>
            </tfoot></table><br />";
            
        }else{
            echo "empty result";
        }
    }

    function column_default($item, $column_name){
        switch($column_name){
            case 'order':
            case 'status':
            case 'total':
            case 'date':
                return $item[$column_name];
            default:
                return print_r($item,true); //Show the whole array for troubleshooting purposes
        }
    }

    function column_company($item){
        
        //Build row actions
        $actions = array(
            // 'edit'      => sprintf('<a href="?page=%s&action=%s&project=%s">Edit</a>',$_REQUEST['page'],'edit',$item['ID']),
            // 'delete'    => sprintf('<a href="?page=%s&action=%s&project=%s">Delete</a>',$_REQUEST['page'],'delete',$item['ID']),
        );
        
        //Return the company contents
        return sprintf('%1$s <span style="color:silver">(id:%2$s)</span>%3$s',
            /*$1%s*/ $item['company'],
            /*$2%s*/ $item['ID'],
            /*$3%s*/ $this->row_actions($actions)
        );
    }

    function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label ("project")
            /*$2%s*/ $item['ID']                //The value of the checkbox should be the record's id
        );
    }

    function get_columns(){
        $columns = array(
            // 'cb'        => '<input type="checkbox" />', //Render a checkbox instead of text
            'order'        => 'Woo Order',
            'company'     => 'Everhour Project',
            'status'    => 'Status',
            'total'  => 'Total',
            'date' =>  'Date Paid'
        );
        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            'order'     => array('order',false),
            'company'     => array('company',false),     //true means it's already sorted
            'status'    => array('status',false),
            'total'  => array('total',false),
            'date'  =>  array('date',false)
        );
        return $sortable_columns;
    }

    function get_bulk_actions() {
        $actions = array(
            // 'delete'    => 'Delete'
        );
        return $actions;
    }

    function process_bulk_action() {
        
        //Detect when a bulk action is being triggered...
        if( 'delete'===$this->current_action() ) {
            wp_die('Items deleted (or they would be if we had items to delete)!');
        }
        
    }

    function prepare_items() {
        global $wpdb;

        $per_page = 50;

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden, $sortable);

        $this->process_bulk_action();

        $data = $this->example_data;

        function usort_reorder($a,$b){
            $orderby = (!empty($_REQUEST['orderby'])) ? $_REQUEST['orderby'] : 'company'; //If no sort, default to company
            $order = (!empty($_REQUEST['order'])) ? $_REQUEST['order'] : 'asc'; //If no order, default to asc
            $result = strcmp($a[$orderby], $b[$orderby]); //Determine sort order
            return ($order==='asc') ? $result : -$result; //Send final sort direction to usort
        }
        usort($data, 'usort_reorder');

        $current_page = $this->get_pagenum();

        $total_items = count($data);

        $data = array_slice($data,(($current_page-1)*$per_page),$per_page);

        $this->items = $data;

        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items/$per_page)   //WE have to calculate the total number of pages
        ) );
    }
}

function ep_render_list_page($api_key){
    $testListTable = new Everhour_Projects_List_Table($api_key);
    $testListTable->prepare_items();

    ?>
        <div class="wrap">
            <!-- <div style="background:#ECECEC;border:1px solid #CCC;padding:0 10px;margin-top:5px;border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;">
                <p>This plugin so far:</p>
                <ul>
                    <li>- Calls for and retrieves a list of Projects in Everhour</li>
                    <li>- Loops through those Projects for name matching Woo Orders and lists their Company, Order #, Status, Total & Date</li>
                </ul>
            </div> -->

            <form id="projects-filter" method="get">
                <!-- the form posts back to our current page -->
                <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                <!-- render the completed list table -->
                <?php $testListTable->display() ?>
            </form>
        </div>
    <?php
}
// echo "<h1>" . $this->everhour_options['x_api_key_0'] . "</h1>";
ep_render_list_page($this->everhour_options['x_api_key_0']);