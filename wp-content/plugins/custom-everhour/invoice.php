<?php

/*
*   *****
*   After Payment for an Order is Complete
*   Using the Everhour API
*   See if there is already an Invoice for this Company + Month
*   If one exists, update it
*   If one doesn't, create it, then update it
*   Then mark it paid
*   Chose to go with: 'woocommerce_payment_complete' action hook, but also considered: 'woocommerce_order_status_completed'
*   *****
*/

include_once 'inc/everhour-invoices.class.php';
include_once 'inc/everhour-clients.class.php';
include_once 'inc/woocommerce-functions.class.php';

$EH_Clients = new Everhour_Clients();
$EH_Clients->set_api_key($this->everhour_options['x_api_key_0']);
$EH_Clients->get_clients();

$EH_Invoice = new Everhour_Invoices();
$EH_Invoice->set_api_key($this->everhour_options['x_api_key_0']);

if(isset($_GET['delete'])){
    $delete_result = $EH_Invoice->delete($_GET['delete']);
    //var_dump($delete_result);
}

$WOO_Tools = new Woocommerce_Functions();
?>
<br />
<form class="woo_order_form">
    <input type="hidden" id="page" name="page" value="everhour">
    <input type="hidden" id="tab" name="tab" value="invoice">
    <!-- <input type="hidden" id="recache-invoices" name="recache-invoices" value="true"> -->
    <label for="orderId"></label><br>
    <input type="text" id="orderId" name="orderId" value="<?php if(isset($_GET['orderId'])){ echo $_GET['orderId']; }?>" placeholder="ex: 14586, 17916">
    <select name="action" id="action">
        <option value="lookup">Lookup a Woocommerce Order by ID</option>
        <option value="create">Create an Everhour Invoice for this Order</option>
    </select>
    <input type="submit" value="Go">
</form> 
<script>
    jQuery(document).ready(function(){
        jQuery( document ).ready(function() {
            jQuery('#action').change(function(){
                var selected_dept = jQuery(this).children(":selected").text(); // Get the selected option value

                switch(selected_dept){
                    case "CSE":
                    jQuery("#myform").attr('action','CSE.html');
                    // dynamically changing action attribute of form to cse.html
                    break;
                }
            });
        });
    });
</script>

<br />
<?php

    if( isset($_GET['orderId']) ){
        if(isset($_GET['orderId'])){
            $order = $_GET['orderId'];
        }else{
            $order = null;
        }
        if($order != null){
            $WOO_Tools->load_woocommerce_order($order);
            $matching_order = $WOO_Tools->get_matching_order();
        }
        if($matching_order == false){
            echo "No Order with that ID";
            exit;
        }
            
        echo "<br />";
        echo "Match: ";
        $EH_Clients->display_clients_dropdown($WOO_Tools->get_company());
        echo " " . $EH_Clients->get_client_list_status() . "<br />";
        $client_to_invoice = $EH_Clients->get_client_to_invoice();
    }
    if(isset($_GET['action'])){
        $selectedAction = $_GET['action'];
        switch ($selectedAction) {
            case 'lookup':
                //echo "Match: ";
                //echo 'Client: ' . $client_to_invoice->id . '<br />';
                //echo 'Project: ' . $client_to_invoice->projects[0] . '<br />';
                //echo ' Order Date: ' . $WOO_Tools->get_order_date() . '<br />';
                //print_r($client_to_invoice);
                break;
            case 'create':
                //echo '<br /><br />Invoice create selected<br/>';
                $create_result = $EH_Invoice->create($client_to_invoice->id, $client_to_invoice->projects[0]);
                echo "<br /><strong>Result: ";
                print_r($create_result);
                echo "</strong>";
                sleep(5);
                $update_result = $EH_Invoice->update($create_result, $matching_order);
                echo "<br /><strong>Update: ";
                print_r($update_result);
                echo "</strong>";
                break;
            default:
                break;
        }
    }
?>

<br /><br />
<strong>Everhour Invoices </strong>
<?php
    if(isset($_GET['recache-invoices'])){
        $recache = true;
    }else{
        $recache = false;
    }
    $EH_Invoice->get_invoices($recache);
    echo $EH_Invoice->invoices_status . "<br />";
    $EH_Invoice->display_invoices();
    echo "<br />";
?>

<?php
/*
*   Eventually connect this so that
*   Order Completion triggers it
*   For now, has a form to check or add an invoice manually
*/
function everhour_invoice($args){
    echo "Auto-generated an invoice for this Woocommerce Subscription in Everhour";
    print_r($args);
} 
//add_action( 'woocommerce_payment_complete', 'everhour_invoice' );


add_filter( 'woocommerce_admin_reports', 'eele_add_wc_reports' );