<hr>
<div style="background:#ECECEC;border:1px solid #CCC;padding:0 10px;margin-top:5px;border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;">
    <p>This plugin utilizes the Everhour API to:</p>
    <ul>
        <li>- Retrieve and display the Dashboard Expenses Report (see Projects to Orders tab)</li>
        <li>- Retrieve a list of all Projects in Everhour and maps them to Woocommerce Orders (see Projects to Orders tab)</li>
        <li>- Adds a "Cost" field to Woo Subscription Order Items (woocommerce_before_order_itemmeta)</li>
        <li>- Generates an Invoice in Everhour when a Woo Order is Paid/Completed (woocommerce_payment_complete)</li>
    </ul>
</div>
<hr>

<?php settings_errors(); ?>

<form method="post" action="options.php">
    <?php
        settings_fields( 'everhour_option_group' );
        do_settings_sections( 'everhour-admin' );
        submit_button();
    ?>
</form>