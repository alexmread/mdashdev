<?php
/*
 *  *****   
 *  ***** Admin Subscription Order List Item Pages *****
 *  *****
 */

 class Woocommerce_Add_Cost {

    var $api_key;

    function set_api_key($api_key){
        $this->api_key = $api_key;
    }

    function init($arg1, $arg2){
        echo "<!-- wc_alter_cost executed once Per Product Item -->";

        //$api_key = $this->everhour_options['x_api_key_0'];
        //echo "api key found: " . $api_key . "<br />";

        /* Get this Order */
        //$order_id = 18502;
        $order_id = get_the_id();
        $order = wc_get_order( $order_id );
        $order_data = $order->get_data();  
        
        //print_r($order_data['date_created']);
        //$order_date = $order->order_date;
        
        $datetime = new DateTime($order->get_date_created());
        echo "Order Date Created: " . $datetime->format('Y-m-d') . "<br />";
        $order_billing_month = $datetime->format('m');
        echo "Billing Month: " . $order_billing_month . "<br />";
        echo "<hr>";

        /* Get Company for this Order */
        $billing_company = get_post_meta( $order_id )['_billing_company'][0];
        //echo "Woo Order Billing Company: " . $billing_company['_billing_company'][0]; // example: Absolute Painting & Powerwashing

        /* Get All Everhour "Projects" and match to Woo Order "Company" */
        $everhour_company_id = $this->getAllEverhourProjects($billing_company);
        //echo "Everhour Company ID: " . $everhour_company_id . "<hr>";    

        /* Get All Everhour Tasks for this "Project" and match Tasks to Line-item title */
        $everhour_task_cost = $this->matchWooOrderItemToEverhourTask($everhour_company_id, $arg2['name'], $order_billing_month);
        wc_update_order_item_meta( $arg1, '_alg_wc_cog_item_cost', number_format($everhour_task_cost, 2) );
        ?>
        <script type='text/javascript'>
            jQuery(function($){
                $('.short').prop('readonly', true );
            });
        </script>
        <?php
    }

    /*
    *   Get All Everhour Projects Once
    */
    function getAllEverhourProjects($billing_company){
        echo "<!-- Matching Billing Company " . $billing_company . " executed -->";

        //$everhour_projects = wp_cache_get( 'everhour_projects' );
        $everhour_company_id = wp_cache_get( 'everhour_company_id' );
        $billing_company_temp = wp_cache_get( 'billing_company' );

        if( $billing_company == $billing_company_temp ){
            echo "<!-- getAllEverhourProjects remaining calls, id: " . $everhour_company_id . " -->";
            return $everhour_company_id;
        }else{
            echo "<!-- getAllEverhourProjects Call executed Once -->";

            //wp_cache_delete( 'everhour_projects' );
            //wp_cache_delete( 'everhour_company_id' );
            wp_cache_set( 'billing_company', $billing_company );   

            $request_projects = wp_remote_get( 'https://api.everhour.com/projects' ,
                    array( 'timeout' => 10,
                        'headers' => array( 'X-Api-Key' => $this->api_key ) 
                    )
                );
            
            if( is_wp_error( $request_projects ) ) {
                $error_string = $request_projects->get_error_message();
                echo '<div id="message" class="error"><p>' . $error_string . '</p></div>';
            }

            $projects = wp_remote_retrieve_body( $request_projects );
            $projects_data = json_decode( $projects );
            
            if( ! empty( $projects_data ) ) {
                //wp_cache_set( 'everhour_projects', $projects_data );
                foreach($projects_data as $project){ 
                    //print_r($project->name);
                
                    /* If Project Name & Order Company are = then this is our Everhour Project to loop through its Tasks */
                    if(strpos($billing_company, $project->name) !== false){
                        //echo "<br />Woo Company: " . $billing_company . "<br />";
                        //echo "Everhour Company: " . $project->name . "<br />";
                        $everhour_company_id = $project->id;
                        wp_cache_set( 'everhour_company_id', $project->id );
                        return $everhour_company_id;
                    } else{
                        // No Company Match
                    }
                }
            }
        }
    }

    /*
    *   Get All Everhour Users Once
    */
    function getUserCost($user_id, $hours){
        echo "<!-- Matching User " . $user_id . " executed -->";

        $everhour_users = wp_cache_get( 'everhour_users' );

        if( false != $everhour_users ){
            echo "<!-- Using Everhour Users cached, id: " . $user_id . " -->";
            $users_data = $everhour_users;
        }else{
            echo "<!-- Setting Everhour Users executed Once -->";
            wp_cache_set( 'check_user_id', $user_id );   

            $request_users = wp_remote_get( 'https://api.everhour.com/team/users' ,
                    array( 'timeout' => 10,
                        'headers' => array( 'X-Api-Key' => $this->api_key ) 
                    )
                );
            
            if( is_wp_error( $request_users ) ) {
                $error_string = $request_users->get_error_message();
                echo '<div id="message" class="error"><p>' . $error_string . '</p></div>';
            }

            $users = wp_remote_retrieve_body( $request_users );
            $users_data = json_decode( $users );
        }
        foreach($users_data as $user_single){ 
            //print_r($project->name);
        
            /* If Project Name & Order Company are = then this is our Everhour Project to loop through its Tasks */
            if(strpos($user_single->id, $user_id) !== false){
                //echo "Everhour User: " . $user_single->name . "<br />";
                $user_hourly = $user_single->cost/100;
                //echo "Everhour User Cost: $" . $user_hourly . "<br />";
                $total_cost_this_project_this_resource = ($user_single->cost * $hours)/100;
                //echo "Cost entry: $" . $total_cost_this_project_this_resource . "<br />";
                return $total_cost_this_project_this_resource;
            } else{
                // No User Match
            }
        }
    }

    /*
    *   Get All Everhour Tasks for our Company match
    *   Match them to each Woo Order line-item
    */
    function matchWooOrderItemToEverhourTask($everhour_company_id, $line_item, $billing_month) {
        $line_item = $line_item;

        echo "<!-- Getting Tasks for " . $everhour_company_id . " executed -->";

        $everhour_company_id_temp = wp_cache_get( 'everhour_company_id' );
       
        if( $everhour_company_id == $everhour_company_id_temp && false !== wp_cache_get( 'all_everhour_tasks ' ) ){
            echo "<!-- Using Cached Everhour Tasks -->";
            $tasks_data = wp_cache_get( 'all_everhour_tasks' );
        }else{
            echo "<!-- Setting Cached Everhour Tasks -->";
            wp_cache_set( 'everhour_company_id', $everhour_company_id );   

            $request_tasks = wp_remote_get( 'https://api.everhour.com/projects/' . $everhour_company_id . '/tasks' ,
                    array( 'timeout' => 10,
                        'headers' => array( 'X-Api-Key' => $this->api_key ) 
                    )
                );
            
            if( is_wp_error( $request_tasks ) ) {
                $error_string = $request_tasks->get_error_message();
                echo '<div id="message" class="error"><p>' . $error_string . '</p></div>';
            }

            $tasks = wp_remote_retrieve_body( $request_tasks );
            $tasks_data = json_decode( $tasks );
            wp_cache_set( 'all_everhour_tasks', $tasks_data);
        }
        //var_dump($tasks_data);
        
        $cost_total = 0;
        $datetime2 = null;
        $billing_month_task = null;
        // $single_cost_labor_item = null;
        // $label = null;
        foreach($tasks_data as $task_single){ 
            if(isset($task_single->labels))
            foreach($task_single->labels as $label){
                //echo "Check the label again: " . $label . "<br />";
                /*
                *   If Task Label = Order Line Item name
                *   AND date of task completed is the same as the month of the Order
                *   then evaluate cost and include
                */

                if(strpos($line_item, $label) !== false){
                    $everhour_task_id = $task_single->id;                
                    
                    $item_time = (isset($task_single->time) ? $task_single->time : false); // time is in seconds
                    if( $item_time !== false ){
                        foreach($item_time->users as $user_id => $hours){
                            
                            $completed_at = (isset($task_single->completedAt) ? $task_single->completedAt : false);
                            if( $completed_at !== false ){
                                foreach($task_single->completedAt as $date => $value){
                                    if($date == 'date'){
                                        $datetime2 = new DateTime($value);
                                        $billing_month_task = $datetime2->format('m');
                                        if($billing_month_task == $billing_month){
                                            //echo "Task Billing Month: " . $billing_month_task . "<br />";
                                            //echo "Work Completed: " . $value . "<br />";

                                            //echo "Woo Task: " . $line_item . "<br />";
                                            //echo "Everhour Task: " . $label . "<br />";
                                            echo "Task: " . $label . ", Completed: " . $datetime2->format('Y-m-d') . "<br />";
                                            
                                            //echo "User: " . $user_id . "<br />";
                                            $total_hours_this_user_this_item = $hours/60/60;
                                            echo "Hours: " . $total_hours_this_user_this_item . "<br />";
                                            
                                            $single_cost_labor_item = $this->getUserCost($user_id, $total_hours_this_user_this_item);
                                            echo "Everhour Cost: $" . number_format($single_cost_labor_item, 2) . "<br /><hr/>";
                                            $cost_total += $single_cost_labor_item;
                                        }
                                    }
                                }
                            }
                        };
                    }
                } else{
                    // No Task Cost
                }
            }
        }
        return $cost_total;
    }
}