<?php

class Everhour_Projects {

    var $api_key;
    var $projects;

    function set_api_key($api_key){
        $this->api_key = $api_key;
    }

    function get_projects(){
        return $this->projects;
    }


    function get_all_projects(){
        
        /* WP Transient Cache */
        // $this->projects = get_transient( 'everhour_projects' );

        // if( isset( $_GET['recache-projects'] ) ){
        //     $reset_projects_command = true;
        // }else{
        //     $reset_projects_command = false;
        // }

        // if( null === $this->projects || $reset_projects_command == true ) {

            $request_projects = wp_remote_get( 'https://api.everhour.com/projects',
                array( 'timeout' => 10,
                    'headers' => array( 'X-Api-Key' => $this->api_key ) 
                )
            );

            if( is_wp_error( $request_projects ) ) {
                $error_string = $request_projects->get_error_message();
                echo "Everhour Projects Call Failing<br />";
                echo '<div><p>' . $error_string . '</p></div>';
            }

            $projects_obj = wp_remote_retrieve_body( $request_projects );
            $projects_decoded = json_decode( $projects_obj );
            $this->projects = $projects_decoded;
            set_transient( 'everhour_projects', $projects_decoded, 1 * DAY_IN_SECONDS );
            
            // to fix this error: Warning: Invalid argument supplied for foreach()
            // occurrs when EH api fails
            if ( is_array($this->projects) || is_object($this->projects) ){
                foreach( $this->projects as $project ){
                    if(isset($project->workspaceName) ){
                        //
                    }
                }
            }
        //}
    }


    function output_all_projects(){
        echo "<pre>";
        print_r($this->projects);
        echo "</pre>";
    }
}