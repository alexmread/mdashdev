<?php

class Output_Division_Items {
    var $alternate = '';  

    var $items = array();

    var $total_time = 0;
    var $total_cost = 0;
    var $total_expenses = array();
    var $total_revenue = 0;
    var $total_profit = 0;
    var $total_percent = 0;
    var $total_rate = 0;

    var $divisors = array();
    var $companies_represented = array();


    public function add_a_line_item($label, $single_cat, $order, $item_data, $cost, $time, $expense){
        
        if(!in_array($order->company, $this->companies_represented)){
            array_push($this->companies_represented, $order->company);
        }

        $new_item = true;

        $this->total_revenue = 0;
        foreach ($this->items as $key => $line_item) {
    
            if ( $order->company == $line_item[2]->company
                &&
                ($order->company != 'N/A' && $order->company != 'All')
            ){
                // Product Name match
                if ( $line_item[1] == $single_cat ){
                    
                    if($this->items[$key][3]){
                        $this->items[$key][3]['total'] += $item_data['total'];
                        $new_item = false;
                    }
                    
                }
            }

            // Every time we add an item, recalculate the total revenue too
            if($new_item == true){
                if(isset($this->items[$key][3]['total'])){
                    $this->total_revenue += $this->items[$key][3]['total'];
                }
            }
        }

        if( $new_item == true ){
            $temp_label = $single_cat;
            
            if ( isset($this->divisors[$temp_label]) && !str_contains($this->divisors[$temp_label]['companies'], $order->company) ) {
                $this->divisors[$temp_label]['companies'] = $this->divisors[$temp_label]['companies'] . ', ' . $order->company;
                $this->divisors[$temp_label]['value'] = $this->divisors[$temp_label]['value'] + 1;
            }

            if ( !isset($this->divisors[$temp_label]) ) {
                $this->divisors[$temp_label] = array(
                    'value' => 1,
                    'companies' => $order->company
                );
            }
        }        
        
        if($new_item == true){           
                    
            array_push( $this->items, array($label, $single_cat, $order, $item_data, $cost, $time, $expense) );
            
            
            $this->total_time += $time;
            $this->total_cost += $cost;
            if($expense['amount'] != 0){
                $tempA = array($label, $expense['amount'], 'all');
                array_push($this->total_expenses, $tempA);
            }
            if($expense['single'] != 0){
                $tempB = array($label, $expense['single'], 'single');
                array_push($this->total_expenses, $tempB);
            }
           
        }
    }


    public function output(){
        
        $this->output_table_styles();
        $this->output_table_head();

        $this->display_totals();

        foreach($this->items as $one){
            $this->display_items($one[0],$one[1],$one[2],$one[3],$one[4],$one[5], $one[6]);
        }

        $this->output_table_close();
    }

    public function output_table_styles(){
        ?>
        <style> 
            /* td { 
                white-space: nowrap; 
            }  */
            .widefat td{
                padding: 2px 5px !important;
                /* border-bottom: 1px solid black; */
            }
            .under td{
                border-bottom: 1px solid #ccd0d4;
                background-color: #e6e6e6;
            }
            .noUnder{
                border-bottom: none !important;
            }
            td.purpleL a {
                color: purple;
            }
            .widefat tr{
                border-bottom: 1px solid black;
            }
        </style>
        <?php
    }

    public function output_table_head(){
        ?>
        <table class="widefat">
        <thead>
            <tr>
                <th id="order-id" class="manage-column column-company-name" scope="col">Order</th>
                <th id="company-name" class="manage-column column-company-name" scope="col">Company Subscription</th>
                <th id="service" class="manage-column column-service" scope="col">Service</th>
                <th id="eh-label" class="manage-column column-eh-label num" scope="col">Label</th>
                
                <th id="hours" class="manage-column column-hours num" scope="col">Hours Logged</th>
                <th id="labor-cost" class="manage-column column-labor-cost num" scope="col">Labor Cost</th>
                
                <th id="eh-label" class="manage-column column-expense num" scope="col">Expenses</th>
                <th id="expenses" class="manage-column column-expenses num" scope="col">Cost</th>

                <th id="expenses-total" class="manage-column column-expenses-total num" scope="col">Total Expenses</th>
                <th id="revenue" class="manage-column column-revenue num" scope="col">Revenue</th>
                <th id="profit" class="manage-column column-profit num" scope="col">Profit</th>
                <th id="profit-percent" class="manage-column column-profit-percent num" scope="col">Profit %</th>
                <th id="hourly-rate" class="manage-column column-hourly-rate num" scope="col">Effective Hourly Rate</th>
            </tr>
        </thead>
        <?php
    }

    public function display_totals() {
        $display_total_expenses = 0;
        foreach($this->total_expenses as $expense){
            
            if( isset($this->divisors[$expense[0]]) ){
                $divisor = $this->divisors[$expense[0]]['value'];
            }else{
                $divisor = 1;
            }
            if($expense[2] == 'all'){
                $display_total_expenses += $expense[1]/$divisor;
            }
            if($expense[2] == 'single'){
                $display_total_expenses += $expense[1];
            }
        }
        echo "<tr class='under'>";
        echo "<td class='column-order-id'>Totals</td>";
        echo "<td class='column-company-name'></td>";
        echo "<td class='column-service'></td>";
        echo "<td class='column-eh-label'></td>";
       
        echo "<td class='column-hours'>" . number_format( $this->total_time, 2) . "</td>";
        echo "<td class='column-labor-cost'><span style='color:red'>$" . $this->total_cost . "</span></td>";
        
        echo "<td class='column-expense'></td>";
        echo "<td class='column-expenses'><span style='color:red'>$" . number_format($display_total_expenses,2) . "</span></td>";

        echo "<td class='column-expenses-total'><span style='color:red'>$" . number_format(($this->total_cost + $display_total_expenses), 2) . "</span></td>";
        echo "<td class='column-revenue'><span style='color:green'>$" . number_format($this->total_revenue,2) . "</span></td>";
        
        if( number_format($this->total_revenue - ($this->total_cost + $display_total_expenses), 2) > 0 ){ $book = "green"; }else{ $book = "red"; }
        echo "<td class='column-profit'><span style='color:" . $book . "'>$" . number_format($this->total_revenue - ($this->total_cost + $display_total_expenses), 2) . "</td>";
        
        if( $this->total_revenue > 0 ){
            echo "<td class='column-profit-percent'>" . number_format( ($this->total_revenue - ($this->total_cost + $display_total_expenses) ) / $this->total_revenue, 2) * 100 . "%</td>";
        }else{
            echo "<td class='column-profit-percent'>" . number_format( ($this->total_revenue - ($this->total_cost + $display_total_expenses) ), 2) . "%</td>";
        }

        if( $this->total_time != 0){
            echo "<td class='column-hourly-rate'>$" . number_format( ($this->total_revenue - ($this->total_cost + $display_total_expenses) ) / $this->total_time, 2) . "</td>";
        }else{
            echo "<td class='column-hourly-rate'></td>";
        }

        echo "</tr>";
    }


    public function display_items($label, $single_cat, $order, $item_data, $cost, $time, $expense) {

        if($this->alternate == ''){
            $this->alternate = 'alternate';
        }else{
            $this->alternate = '';
        }
        
        if( isset($this->divisors[$label]) ){
            $divisor = $this->divisors[$label]['value'];
        }else{
            $divisor = 1;
        }


        $display_total_expenses = 0;
        if( isset($expense['amount']) && $expense['amount'] != 0 ){
            $display_total_expenses += $expense['amount']/$divisor;
        }
        if(isset($expense['single']) && $expense['single'] != 0 ){
            $display_total_expenses += $expense['single'];
        }
        
        echo "<tr class='" . $this->alternate . "'>";
        if(isset($order->id) && gettype($order->id)!== 'array'){
            echo "<td class='column-order-id'><a href='/wp-admin/post.php?post=" . $order->id . "&action=edit'>" . $order->id . "</a></td>";
        }else{
            echo "<td class='column-order-id'></td>";
        }
        echo "<td class='column-company-name'>" . $order->company . "</td>";
        echo "<td class='column-service'>" . strip_tags($single_cat);
        if($divisor > 1 && $single_cat != ''){ echo " (" . $divisor . ")"; }
        echo "</td>";
        echo "<td class='column-eh-label'>" . $label . "</td>";
        
        echo "<td class='column-hours'>" . number_format($time, 2) . "</td>";
        echo "<td class='column-labor-cost'><span style='color:red'>$" . number_format($cost, 2) . "</span></td>";

        echo "<td class='column-expense'>";
        if(isset($expense['description'])){ echo $expense['description']; }
        echo "</td>";
        echo "<td class='column-expenses'><span style='color:red'>$" . number_format($display_total_expenses, 2) . "</span></td>";

        echo "<td class='column-expenses-total'><span style='color:red'>$" . number_format(($cost + $display_total_expenses), 2) . "</span></td>";
        if( isset($item_data) ){
            if( number_format($item_data['total'], 2) > 0 ){ $booka = "green"; }else{ $booka = "red"; }
            echo "<td class='column-revenue'><span style='color:" . $booka . "'>$" . number_format($item_data['total'], 2) . "</span></td>";
            
            if( ( $item_data['total'] - $cost ) > 0 ){ $book2 = "green"; }else{ $book2 = "red"; }
            echo "<td class='column-profit'><span style='color:" . $book2 . "'>$" . number_format($item_data['total'] - ($cost + $display_total_expenses), 2) . "</td>";
            
            if($item_data['total'] != 0){
                echo "<td class='column-profit-percent'>" . sprintf("%.2f%%", (($item_data['total'] - ($cost + $display_total_expenses))/$item_data['total'])*100) . "</td>";
            }else{
                echo "<td class='column-profit-percent'>0</td>";
            }

            if($time != 0){
                echo "<td class='column-hourly-rate'>$" . number_format( ($item_data['total'] - ($cost + $display_total_expenses)) / $time, 2) . "</td>";
            }else{
                echo "<td class='column-hourly-rate'></td>";
            }
        }else{
            echo "<td class='column-revenue'><span>$0</span></td>";
            echo "<td class='column-profit'><span style='color:red'>$" . number_format(-$cost, 2) . "</span></td>";
            echo "<td class='column-profit-percent'><span style='color:red'>-100%</span></td>";
            echo "<td class='column-hourly-rate'></td>";
        }

        echo "</tr>";
    }

    public function output_table_close(){
        echo "</tbody></table>";
    }
}