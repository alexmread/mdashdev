<?php

/*
*   Example Return Object:
    [1] => stdClass Object
        (
            [id] => 59176679
            [date] => 2021-01-14
            [createdAt] => 2021-01-15 02:34:21
            [user] => 873055
            [time] => 420
            [task] => stdClass Object
                (
                    [id] => as:1199638553264604
                    [name] => Revamp - Implementation - Asana Task Creation | EMAIL CAMPAIGN MANAGEMENT
                    [type] => task
                    [status] => completed
                    [url] => https://app.asana.com/0/846309574829963/1199638553264604/f
                    [iteration] => Systems + Processes | Growth + Development
                    [projects] => Array
                        (
                            [0] => as:846309574829963
                        )

                    [parentName] => The Great 2020 Asana Task Revampification
                    [parentId] => as:1198195518657062
                    [createdAt] => 2020-12-18 02:27:38
                    [dueOn] => 2021-01-13
                    [labels] => Array
                        (
                        )

                    [time] => stdClass Object
                        (
                            [total] => 2460
                            [users] => stdClass Object
                                (
                                    [873055] => 2460
                                )

                            [timerTime] => 2160
                        )

                    [completed] => 1
                    [completedAt] => 2021-01-20 15:56:38
                )

            [history] => Array
                (
                    [0] => stdClass Object
                        (
                            [id] => 99664015
                            [createdAt] => 2021-01-15 02:34:21
                            [time] => 420
                            [action] => TIMER
                            [previousTime] => 0
                            [createdBy] => 873055
                        )

                )

            [lockReasons] => Array
                (
                )

            [isLocked] => 
            [cost] => 350
            [costRate] => 3000
        )
*/

class Everhour_Time_Records {

    var $api_key;
    //var $array_of_eh_labels;
    //var $eh_companies_with_cost = array();
    var $time_records = array();

    function set_api_key($api_key){
        $this->api_key = $api_key;
    }

    function get_time_records(){
        return $this->time_records;
    }

    // function get_array_of_eh_labels(){
    //     return $this->array_of_eh_labels;
    // }

    // function get_companies_that_have_not_been_unset(){
    //     return $this->eh_companies_with_cost;
    // }

    // function labelUniquitor($label_to_match){

    //     switch($label_to_match){
    //         case 'SEO': $label_to_return = "Search Engine Optimization"; break;
    //         case 'Search Engine Optimization': $label_to_return = "Search Engine Optimization"; break;
    //         case 'Email': $label_to_return = "Email Marketing"; break;
    //         case 'EMAIL': $label_to_return = "Email Marketing"; break;
    //         case 'Email Marketing': $label_to_return = "Email Marketing"; break;
    //         case 'Digital Marketing (Other)': $label_to_return = "Digital Marketing"; break;
    //         case 'Marketing Strategy': $label_to_return = "Marketing Strategy"; break;
    //         case 'STRATEGY': $label_to_return = "Marketing Strategy"; break;
    //         case 'Website Hosting & Maintenance': $label_to_return = "Hosting & Maintenance"; break;
    //         case 'Hosting & Maintenance': $label_to_return = "Hosting & Maintenance"; break;
    //         case 'Web Hosting and Maintenance': $label_to_return = "Hosting & Maintenance"; break;
    //         case 'Uncategorized': $label_to_return = "Uncategorized"; break;
    //         case 'Web Design': $label_to_return = "Web Design"; break;
    //         case 'Acct Mgmt': $label_to_return = "Account Management"; break;
    //         case 'ACCT MGMT': $label_to_return = "Account Management"; break;
    //         case 'CONTENT': $label_to_return = "Content"; break;
    //         case 'SALES': $label_to_return = "Sales funnels"; break;
    //         case '': $label_to_return = "Uncategorized"; break;
    //         default: $label_to_return = $label_to_match;
    //     }   
    //     //echo "Label to match: " . $label_to_match . ", Label returned: " . $label_to_return . "<br />";
    //     return $label_to_return;
    // }

    function call_to_retrieve_time_records($start_date, $end_date){
        echo "<span class='batch_titles'>Retrieving data from Everhour, from: " . $start_date . " to: " . $end_date . "</span><br />";
        /* WP Transient Cache */
        //$this->time_records = get_transient( 'everhour_time_records' );

        $request_time_records = wp_remote_get( 'https://api.everhour.com/team/time?from=' . $start_date . '&to=' . $end_date,
            array( 'timeout' => 10,
                'headers' => array( 'X-Api-Key' => $this->api_key ) 
            )
        );

        if( is_wp_error( $request_time_records ) ) {
            $error_string = $request_time_records->get_error_message();
            // echo "Everhour failing<br />";
            // echo "<pre>";
            // print_r($error_string);
            // echo "</pre>";
        }

        $time_records_obj = wp_remote_retrieve_body( $request_time_records );
        $time_records_data = json_decode( $time_records_obj );
        //set_transient( 'everhour_time_records', $time_records_data, 1 * DAY_IN_SECONDS );
        foreach($time_records_data as $new_record){
            // echo "<pre>";
            // print_r($new_record);
            // echo "</pre>";
            $this->time_records[] = $new_record;
        }
        //$this->time_records = $time_records_data;
        //array_merge($this->time_records, $time_records_data);
        
        // to fix this error: Warning: Invalid argument supplied for foreach()
        // occurrs when EH api fails
        if ( is_array($this->time_records) || is_object($this->time_records) ){
            foreach( $this->time_records as $record ){
                if(isset($record->cost) ){
                    //$this->eh_companies_with_cost[] = $record->task->id;
                    $this->eh_companies_with_cost[$record->task->id][] = $record->cost;
                }
            }
        }
        //echo "<pre>";
        //print_r($this->eh_companies_with_cost);
        //print_r($this->time_records);
        //echo "</pre>";
    }

    // function filter_time_records_by_project_ids($everhour_project_ids, $orderDateTime){
    //     // Empty this array of eh labels for each line item lookup
    //     $this->array_of_eh_labels = array();

    //     if ( is_array($this->time_records) || is_object($this->time_records) ){
    //     foreach ( $this->time_records as $key => $record ) {
            
    //         $short_date = substr($record->date,0,6);
            
    //         if (is_array($everhour_project_ids) || is_object($everhour_project_ids) && $short_date == $orderDateTime ){
    //             foreach ( $everhour_project_ids as $this_everhour_project_id ){
    //                 foreach( $record->task->projects as $project_single ){
    //                     if( $project_single == $this_everhour_project_id ){
                            
    //                         if( isset($record->task->labels[0]) ){
    //                             $this->array_of_eh_labels[strtoupper($record->task->labels[0])][] = array($record->cost, $record->time, $record->id);
    //                         }
    //                         else{
    //                             //if( isset($record->task->iteration) ) {
    //                                 //$this->array_of_eh_labels[$record->task->iteration][] = array($record->cost, $record->time);
    //                             //}else{
    //                                 if(isset($record->task->time) ){
    //                                     $this->array_of_eh_labels['Unlabeled'][] = array($record->cost, $record->time, $record->id);
    //                                 }
    //                             //}
    //                         }
    //                         //echo "task ID: " . $record->task->id . "<br />";
    //                         unset($this->time_records[$key]);
    //                     }
    //                 }
    //                 //echo $this_everhour_project_id . "<br />";
    //                 //unset($this->eh_companies_with_cost[$this_everhour_project_id]);
    //             }
    //         }else{
    //             //unset($this->time_records[$key]);
    //         }
    //     }
    //     }

    //     // echo "<pre>";
    //     // print_r($this->array_of_eh_labels);
    //     // echo "</pre>";
        
    //     return $this->array_of_eh_labels;
    // }


    // function filter_time_records_by_no_company(){
    //     // Empty this array of eh labels for each line item lookup
    //     $this->array_of_no_company = array();
    //     $this->array_of_no_company[0] = 0;
    //     $this->array_of_no_company[1] = 0;

    //     foreach ( $this->time_records as $record ) {        
    //         if(isset($record->task->time) &&  !isset($record->task->labels[0]) ){ //&& !isset($record->task->id) result none
    //             //echo "got one without labels<br />";
    //             $this->array_of_no_company[0] = $this->array_of_no_company[0] + $record->cost;
    //             $this->array_of_no_company[1] = $this->array_of_no_company[1] + $record->time;
    //         }
    //     }
    //     // echo "<pre>";
    //     // print_r($this->array_of_eh_labels);
    //     // echo "</pre>";
        
    //     return $this->array_of_no_company;
    // }


    // function find_service_costs($single_cat){
    //     $label_match = '';
    //     $cost = 0;
    //     $time = 0;

    //     /*
    //     *   Loop through all EH Labels Array,
    //     *   and when label matches line item,
    //     *   output and remove from EH Labels Array
    //     */
    //     // echo "<pre>";
    //     // print_r($this->array_of_eh_labels);
    //     // echo "</pre>";
    //     foreach($this->array_of_eh_labels as $label => $records){    

    //         $label_to_match1 = strip_tags(html_entity_decode($label));
    //         $label_to_match = $this->labelUniquitor($label_to_match1);

    //         //echo $label . "<br />";
    //         // switch($label_to_match){
    //         //     case 'SEO': $label_to_match = "Search Engine Optimization"; break;
    //         //     case 'Search Engine Optimization': $label_to_match = "Search Engine Optimization"; break;
    //         //     case 'Email': $label_to_match = "Email Marketing"; break;
    //         //     case 'EMAIL': $label_to_match = "Email Marketing"; break;
    //         //     case 'Email Marketing': $label_to_match = "Email Marketing"; break;
    //         //     case 'Digital Marketing (Other)': $label_to_match = "Digital Marketing"; break;
    //         //     case 'Marketing Strategy': $label_to_match = "Marketing Strategy"; break;
    //         //     case 'STRATEGY': $label_to_match = "Marketing Strategy"; break;
    //         //     case 'Website Hosting & Maintenance': $label_to_match = "Hosting & Maintenance"; break;
    //         //     case 'Hosting & Maintenance': $label_to_match = "Hosting & Maintenance"; break;
    //         //     case 'Web Hosting and Maintenance': $label_to_match = "Hosting & Maintenance"; break;
    //         //     case 'Uncategorized': $label_to_match = "Uncategorized"; break;
    //         //     case 'Web Design': $label_to_match = "Web Design"; break;
    //         //     case 'Acct Mgmt': $label_to_match = "Account Management"; break;
    //         //     case 'ACCT MGMT': $label_to_match = "Account Management"; break;
    //         //     case 'CONTENT': $label_to_match = "Content"; break;
    //         //     case 'SALES': $label_to_match = "Sales funnels"; break;
    //         //     case '': $label_to_match = "Uncategorized"; break;
    //         //     default: $label_to_match = $label_to_match;
    //         // }   

    //         // if( $single_cat == '' || $single_cat == null || $single_cat == false ){
    //         //     $single_cat = $label_to_match;
    //         // }
    //         /*
    //         *   Check for Woo Line Item Service to EH Label Match
    //         */
    //         //if ( strcmp($single_cat, $label_to_match) == 0 ) {                
    //         if ( strcasecmp($single_cat, $label_to_match) == 0 ) {                
    //             foreach($records as $key => $record){
    //                 $cost += $record[0]/100;
    //                 $time += $record[1]/60/60;
    //                 //unset($this->array_of_eh_labels[$label][$key]);
    //             }
    //             unset($this->array_of_eh_labels[$label]);
    //         }  
    //     }
    //     $costs_total = array(
    //         'label' => $single_cat,
    //         'cost' => $cost,
    //         'time' => $time 
    //     );
    //     return $costs_total;        
    // }

    // function find_remaining_costs(){
    //     $remaining_costs_array = array();
    //     $label_match = '';
    //     $cost = 0;
    //     $time = 0;

    //     /*
    //     *   Loop through all EH Labels Array,
    //     *   and return remaining items in an Array
    //     */
    //     foreach($this->array_of_eh_labels as $label => $records){  
    //         $cost = 0;
    //         $time = 0;

    //         $label_to_match2 = strip_tags(html_entity_decode($label));
    //         //echo $label . "<br />";
    //         $label_to_match = $this->labelUniquitor($label_match2);

    //         // switch($label_to_match){
    //         //     case 'SEO': $label_to_match = "Search Engine Optimization"; break;
    //         //     case 'Search Engine Optimization': $label_to_match = "Search Engine Optimization"; break;
    //         //     case 'Email': $label_to_match = "Email Marketing"; break;
    //         //     case 'EMAIL': $label_to_match = "Email Marketing"; break;
    //         //     case 'Email Marketing': $label_to_match = "Email Marketing"; break;
    //         //     case 'Digital Marketing (Other)': $label_to_match = "Digital Marketing"; break;
    //         //     case 'Marketing Strategy': $label_to_match = "Marketing Strategy"; break;
    //         //     case 'STRATEGY': $label_to_match = "Marketing Strategy"; break;
    //         //     case 'Website Hosting & Maintenance': $label_to_match = "Hosting & Maintenance"; break;
    //         //     case 'Hosting & Maintenance': $label_to_match = "Hosting & Maintenance"; break;
    //         //     case 'Web Hosting and Maintenance': $label_to_match = "Hosting & Maintenance"; break;
    //         //     case 'Uncategorized': $label_to_match = "Uncategorized"; break;
    //         //     case 'Web Design': $label_to_match = "Web Design"; break;
    //         //     case 'Acct Mgmt': $label_to_match = "Account Management"; break;
    //         //     case 'ACCT MGMT': $label_to_match = "Account Management"; break;
    //         //     case 'CONTENT': $label_to_match = "Content"; break;
    //         //     case 'SALES': $label_to_match = "Sales funnels"; break;
    //         //     case '': $label_to_match = "Uncategorized"; break;
    //         //     default: $label_to_match = $label_to_match;
    //         // }   

            
    //         foreach($records as $record){
    //             $cost += $record[0]/100;
    //             $time += $record[1]/60/60;
    //         } 
    //         // print_r($remaining_costs_array);
    //         // echo "<br /><br />";
    //         array_push($remaining_costs_array, array(
    //             "label" => $label_to_match,
    //             "cost" => $cost,
    //             "time" => $time,
    //         ));
    //         unset($this->array_of_eh_labels[$label]);  
    //     }
    //     return $remaining_costs_array;        
    // }


    function save_to_db($start_date, $end_date, $time_records, $display = false){
       
        if( $display==true ){
            echo "<pre>";
            print_r($time_records);
            echo "</pre>";
        }
            
        global $wpdb;
        // $sql = $wpdb->prepare(
        //     "INSERT INTO `wp_everhour`(`week`,`year`,`data`) values (`" . $week . "`, `" . $year . "`, `" . $data . "`)";
           
        //    $wpdb->query($sql);

        $wpdb->replace('wp_everhour', array("start_date" => $start_date, "end_date" => $end_date, "data" => serialize($time_records) ));
    }

    function retrieve_from_db($start_date, $end_date){
        //echo "time-records-class:<br />";
        //echo "start date: " . $start_date . "<br />";
        //echo "end date: " . $end_date . "<br />";
        global $wpdb;
        $query = $wpdb->get_results( "SELECT data FROM wp_everhour WHERE start_date = \"$start_date\" AND end_date = \"$end_date\"" );
        //var_dump($query);
        //return unserialize($query[0]->data);
        $add_these_entries = unserialize($query[0]->data);
        //echo $start_date . " to " . $end_date . ": " . count($add_these_entries) . " entries<br />";
        if($add_these_entries)
        foreach($add_these_entries as $new_record){
            // echo "<pre>";
            // print_r($new_record);
            // echo "</pre>";
            $this->time_records[] = $new_record;
        }
    }
}