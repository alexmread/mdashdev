<?php

class Company {

    var $name;
    var $eh_id;
    var $line_items = array();

    function __construct($ev_id, $name) {
        $this->name = $name;
        $this->eh_id = $ev_id;
    } 

    function add_line_item($order_info, $category_counter, $stopTheCounter = false){
        if(!isset($order_info->revenue)){ $order_info->revenue = 0;}
        if(!isset($order_info->category)){ $order_info->category = '';}

        //echo "<pre>";print_r($order_info);echo "</pre>";

        if( isset($order_info->eh_id) && $order_info->eh_id != 123456789 ){
            $productsonly = false;
        }else{
            $productsonly = true;
        }

        //if ( $productsonly == false ){
        if( array_key_exists(strtolower($order_info->category), $this->line_items) ){
            $this->line_items[strtolower($order_info->category)]->update($order_info);
        }else{
            $new_line_item = new Line_Item($order_info);
            $this->line_items[strtolower($order_info->category)] = $new_line_item;

            if($stopTheCounter == false)
            $category_counter->increment_category(strtolower($order_info->category), $order_info->revenue);
        }
        //}

        //if($stopTheCounter == false){
            if( array_key_exists('totals', $this->line_items) ){
                $this->line_items['totals']->update($order_info);
            }else{
                $new_totals_line = new Line_Item($order_info);
                $this->line_items['totals'] = $new_totals_line;
            }
        
            // if($order_info->expense){
            //     if( array_key_exists('expense_totals', $this->line_items) ){
            //         $this->line_items['expense_totals']->update($order_info);
            //     }else{
            //         $new_expense_totals_line = new Line_Item($order_info);
            //         $this->line_items['expense_totals'] = $new_expense_totals_line;
            //     }
            // }
        //}
    }
}