<?php

class Company_Category_Lookup {

    //var $name;
    var $companies_to_fix = array();
    var $categories_to_fix = array();
    var $fixed_company = null;
    var $fixed_category = null;

    function company($company_to_lookup){
        $company_to_lookup = strtolower($company_to_lookup);
        switch( $company_to_lookup ){
            case '1525': return '1525, Inc'; break;
            case 'zm performance': return 'zmperformance'; break;
            case 'seadon': return 'seadon llc'; break;
            case 'royalwise solutions': return 'royalwise'; break;
            case 'classic exhibits': return 'classic exhibits inc.'; break;
            default:
            return $company_to_lookup;
        }
    }

    function category($category_to_lookup){
        $category_to_lookup = strtolower($category_to_lookup);
        switch( $category_to_lookup ){
            case 'seo': return 'Search Engine Optimization'; break;
            case 'email': return 'email marketing'; break;
            case 'strategy': return 'marketing strategy'; break;
            default:
            return $category_to_lookup;
        }

    }
}