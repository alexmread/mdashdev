<?php

class Line_Item {

    var $revenue = 0;
    var $cost = 0;
    var $time = 0;
    var $order_id = array();
    var $product_ids = array();
    var $expense = 0;
    var $expense_description = array();

    var $occurrences = null;

    function __construct($itemObj) {
        if(isset($itemObj->revenue)){
            $this->revenue = $itemObj->revenue;
        }
        if(isset($itemObj->cost)){
            $this->cost = $itemObj->cost;
        }
        if(isset($itemObj->time)){
            $this->time = $itemObj->time;
        }
        if(isset($itemObj->order_id)){
            $this->order_id[] = $itemObj->order_id;
        }
        if(isset($itemObj->product_id)){
            $this->product_ids[] = $itemObj->product_id;
        }

        if(isset($itemObj->expense)){
            $this->expense = $itemObj->expense;
        }        

        if(isset($itemObj->expense_description)){
            $this->expense_description[] = $itemObj->expense_description;
        }

        if(isset($itemObj->occurrences)){
            $this->occurrences = 1;
        }
    }

    function update($itemObj){
        if(isset($itemObj->revenue)){
            $this->revenue = $this->revenue + $itemObj->revenue;
        }
        if(isset($itemObj->cost)){
            $this->cost = $this->cost + $itemObj->cost;
        }
        if(isset($itemObj->time)){
            $this->time = $this->time + $itemObj->time;
        }
        if(isset($itemObj->order_id)){
            $this->order_id[] = $itemObj->order_id;
        }
        if(isset($itemObj->product_id)){
            $this->product_ids[] = $itemObj->product_id;
        }

        if(isset($itemObj->expense)){
            $this->expense = $this->expense + $itemObj->expense;
        }

        if(isset($itemObj->expense_description)){
            $this->expense_description[] = $itemObj->expense_description;
        }
        if(isset($itemObj->occurrences)){
            $this->occurrences++;
        }
    }

}