<?php

/*
*   !!!
*   ***
*   This is no longer leveraged.
*   Replaced by the Google Sheet Class
*   ***
*   ***
*/

class Expenses {

    var $expenses_company_match_category_match = array();
    var $expenses_company_all_category_match = array();
    var $expenses_company_match_category_all = array();
    var $expenses_company_all_category_all = array();
    var $expenses_company_na_category_na = array();

    var $companies_in_woo = array();
    var $divisors = array("Empty"=>1);
    //var $start_date;
    //var $end_date;


    /* Check selected billing date range Helper */
    public function check_in_range($start_date, $end_date, $billing_date, $billing_date_end, $repeats)
    {
        // Convert to timestamp
        $start_date_filter_ts = strtotime($start_date);
        $end_date_filter_ts = strtotime($end_date);
        $billing_date_ts = strtotime($billing_date);
        if($billing_date_end == 'undefined' || $billing_date_end == '' || $billing_date_end == 0 || $billing_date_end == null){
            $billing_date_end_ts = 0;
        }else{
            $billing_date_end_ts = strtotime($billing_date_end);
        }
        
        if( $repeats == "Monthly" ){
            if( $end_date_filter_ts <= $billing_date_end_ts || $billing_date_end_ts == 0 ){
                //echo "passes Monthly<br />";
                return true;
            }
        }
        if( $repeats == "Yearly" ){
            if( ($end_date_filter_ts <= $billing_date_end_ts || $billing_date_end_ts == 0) ){
                //echo "passes Yearly<br />";
                return true;
            }
        }
        if( $repeats == "One Time" ){
            if( ($billing_date_ts >= $start_date_filter_ts) && ($billing_date_end_ts >= $end_date_filter_ts || $billing_date_end_ts == 0) ){
                //echo "passes One Time<br />";
                return true;
            }
        }
        //echo "this should almost never fire";
        return false;
    }


    /* Check selected billing date range Helper */
    public function check_in_range_new($start_date, $end_date, $billing_date)
    {
        // Convert to timestamp
        $start_date_filter_ts = strtotime($start_date);
        $end_date_filter_ts = strtotime($end_date);
        $billing_date_ts = strtotime($billing_date);
        
        if( ($billing_date_ts >= $start_date_filter_ts) && ($billing_date_ts <= $end_date_filter_ts) ){
            return true;
        }else{
            return false;
        }
    }


    // function get_divisors(){
    //     return $this->divisors;
    // }

    function get_expenses_company_all_category_all(){
        return $this->expenses_company_all_category_all;
    }

    function get_expenses_company_na_category_na(){
        return $this->get_expenses_company_na_category_na;
    }


    function get_all_companies_in_woo(){
        $companies = get_posts(array(
            'post_type'   => 'companies',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'fields' => 'ids',
            )
        );
        //print_r($companies); // array of company IDs
        foreach($companies as $c){
            $temp_company = array();
            $temp_company['id'] = $c;
            $temp_company['name'] = get_the_title($c);
            
            $this->companies_in_woo[] = $temp_company;
        }

        //print_r($this->companies_in_woo);
        return $this->companies_in_woo;
    }


    /**
     * Setup query to get the 'expenses' post type
     */
    function get_all_expenses(){

        $posts = get_posts(array(
            'post_type'   => 'expenses',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'fields' => 'ids',
            )
        );

        foreach($posts as $p){

            $temp_item = array(
                "single"        =>  array(
                    "amount"    =>  0
                ),
                "all"           =>  array(
                    "amount"    =>  0
                ),
                "id"            =>  null,	
                "description"   =>  '',
                "amount"        =>  0,
                "repeats"       =>  '',
                "post"          =>  null,
                "billing_date"  =>  '',
                "tier"          =>  '',
                "sub_category"  =>  '',
                "company"       =>  '',
                "category"      =>  ''
            );
            
            //$term_list = wp_get_post_terms( $p, 'prod_cat', array( 'fields' => 'all' ) );
            $temp_post = wp_get_post_terms( $p, 'product_cat' );
            if($temp_post){
                $terms = wp_get_post_terms( $p, 'product_cat' )[0]->name;
            }else{
                $terms = '';
            }
            //print_r( $terms );
            // foreach ( $terms as $term ) {
            //     $categories[] = $term->name;
            // }

            // $temp_item['category'] = $categories[0];
            $temp_item['category'] = $terms;

            if( null !== get_post_meta($p,"expense-amount",true) ){
                $temp_item['amount'] = get_post_meta($p,"expense-amount",true);
            }else{
                $temp_item['amount'] = 0;
            }
            //echo "amount: " . $temp_item['amount'];

            $temp_company = get_post_meta($p,"expense-to-company",true);
            if($temp_company && $temp_company[0]){
                $temp_item['company'] = get_post_meta($p,"expense-to-company",true)[0];
                $temp_item['company-name'] = get_the_title($temp_item['company']);
            }

            /* no longer using duration currently */
            $temp_item['duration'] = 12;//get_post_meta($p,"expense-duration-months",true);

            $temp_item['billing_date'] = get_post_meta($p,"expense-billing-date",true);
            
            // !!!***!!!
            $temp_item['billing_date_end'] = get_post_meta($p, "expense-billing-date-end",true);
            // !!!***!!!

            $temp_item['repeats'] = get_the_terms($p, 'repeats')[0]->name;
            $temp_item['post'] = $p;

            //if( null !== get_the_title($p)){
                $temp_item['description'] = get_the_title($p);
                //echo get_the_title($p) . ", " . $temp_item['description'] . "<br />";
            //}

            // Company == 'All && Category == 'All'
            // Examples 2: Typeform, Close
            if ( $temp_item['company-name'] == 'All' && $temp_item['category'] == 'All' ){
                array_push($this->expenses_company_all_category_all, $temp_item);
            }
            
            // Company == 'All' && Category != 'All'
            // Examples 3: Adobe Creative Cloud, AHREFs SEO Software, WP Engine Bulk
            if ( $temp_item['company-name'] == 'All' && $temp_item['category'] != 'All' ){
                array_push($this->expenses_company_all_category_match, $temp_item);
            }

            // Company != 'All' && Category == 'All
            // Example 1: Farmers, Classic Exhibits
            if ( $temp_item['company-name'] != 'All' && $temp_item['category'] == 'All' ){
                array_push($this->expenses_company_match_category_all, $temp_item);
            }

            // Company != "All && Category != 'All' && Company != 'N/A' && Category != 'N/A'
            // Examples 3: 	
            // AHREFs SEO Software/PECAA/SEO
            // Cloudflare – Noble/Noble/Hosting & Maintenance
            // WP Engine – Josh Bersin/Josh Bersin/Hosting & Maintenance
            if ( $temp_item['company-name'] != 'All' && $temp_item['category'] != 'All' ){
                if ( $temp_item['company-name'] != 'N/A' && $temp_item['category'] != 'N/A'){
                    array_push($this->expenses_company_match_category_match, $temp_item);
                }
            }

            // Company == 'N/A' && Category == 'N/A'
            // Example:
            // Amy Northard CPA
            if ( $temp_item['company-name'] == 'N/A' && $temp_item['category'] == 'N/A'){
                array_push($this->expenses_company_na_category_na, $temp_item);
            }
        }

        //$this->output_expenses_readable();
        //$this->output_expenses_object();
        return array(
            'expenses_company_match_category_match' =>  $this->expenses_company_match_category_match,
            'expenses_company_all_category_match'   =>  $this->expenses_company_all_category_match,
            'expenses_company_match_category_all'   =>  $this->expenses_company_match_category_all,
            'expenses_company_all_category_all'     =>  $this->expenses_company_all_category_all,
            'expenses_company_na_category_na'       =>  $this->expenses_company_na_category_na
        );
    }


    function find_expenses_company_match_OR_category_match($order, $single_cat, $start_date, $end_date){
        $single_cat = strip_tags(html_entity_decode($single_cat));
        $expenses_obj = array(
            "single"        =>  array(
                "amount"    => 0
            ),
            "all"           =>  array(
                "amount"    =>  0
            ),
            "na"            =>  array(
                "amount"    =>  0
            ),
            "description"   =>  ''
        );

        /*
        *   Woo Expenses for Company && Category Match
        */
        foreach( $this->expenses_company_match_category_match as $key => $single_expense ){

            if(isset($single_expense['category'])){
                $single_expense['category'] = strip_tags(html_entity_decode($single_expense['category']));
            }
            //echo "Company & Category Match: " . $single_expense['repeats'] . "<br />";

            /*
            *   If Category/Service matches
            */
            if( isset($single_expense['category']) && strcmp($single_expense['category'], $single_cat) == 0 ){

                /*
                *   If Company matches
                */
                foreach ($this->companies_in_woo as $company){
                    //echo $order->company . ':' . $company['name'] . ', id: ' . $company['id'] . ', ' . $single_expense['company'] . "<br />";
                    if ( $order->company == 'Josh Bersin Academy'){
                        $order->company = 'Josh Bersin';
                    }
                    if ( $company['name'] == 'Josh Bersin Academy' ){
                        $company['name'] = 'Josh Bersin';
                    }
                    if ( $order->company == $company['name'] && $company['id'] == $single_expense['company'] ){
                        //echo $order->company . "<br />";
                        /*
                        *   If "One Time" must be within date range
                        *   If "Monthly" or "Yearly" only needs to be Before end date
                        */                                            
                        //if( $this->check_in_range($start_date, $end_date, $single_expense['billing_date'], $single_expense['billing_date_end'], $single_expense['repeats']) ){
                        if( $this->check_in_range_new($start_date, $end_date, $single_expense['billing_date']) ){
                            //print_r($single_expense);
                            $expenses_obj['single']['amount'] += $single_expense['amount'];
                            if($expenses_obj['description'] != ''){
                                $expenses_obj['description'] .= "<br />";
                            }
                            $expenses_obj['description'] .= "Company & Category match:<br />" . $single_expense['description'];
                            //unset($this->expenses[$key]);  

                            
                        }
                    }
                }
            }
        }

        /*
        *   Woo Expenses for Company All && Category Match
        */
        foreach( $this->expenses_company_all_category_match as $key => $single_expense ){ 

            if(isset($single_expense['category'])){
                $single_expense['category'] = strip_tags(html_entity_decode($single_expense['category']));
            }

            /*
            *   If Category/Service matches
            */
            if( isset($single_expense['category']) && strcmp($single_expense['category'], $single_cat) == 0 ){

                $date1 = $start_date;
                $date2 = $end_date;
                $d1=new DateTime($date2); 
                $d2=new DateTime($date1);                                  
                $Months = $d2->diff($d1); 
                $number_of_months_of_this_expense = (($Months->y) * 12) + ($Months->m);
                if($number_of_months_of_this_expense == 0){
                    $number_of_months_of_this_expense = 1;
                }
                //echo $number_of_months_of_this_expense . "<br />"; // 1


                /*
                *   If "One Time", must be Within date range
                *   If "Monthly" or "Yearly" must be Before end date
                */                                             
                //if($this->check_in_range($start_date, $end_date, $single_expense['billing_date'], $single_expense['billing_date_end'], $single_expense['repeats']) && isset($single_expense['amount']) ){
                if($this->check_in_range_new($start_date, $end_date, $single_expense['billing_date']) && isset($single_expense['amount']) ){
                    //print_r($single_expense);
                    
                    //print_r($single_expense['repeats']);
                    if( $single_expense['repeats'] == 'Monthly'){
                        $expenses_obj['all']['amount'] += $single_expense['amount'] * $number_of_months_of_this_expense;
                    }
                    if( $single_expense['repeats'] == 'Yearly'){
                        //print_r($single_expense['amount']); // enters, and outputs 7728
                        $expenses_obj['all']['amount'] += ($single_expense['amount']/12) * $number_of_months_of_this_expense;
                    }
                    if ( $single_expense['repeats'] == "One Time" ){
                        echo "One Time Expense";
                        $expense_obj['single']['amount'] += $single_expense['amount'];
                    }

                    if($expenses_obj['description'] != ''){
                        $expenses_obj['description'] .= "<br />";
                    }
                    $expenses_obj['description'] .= "Company All & Category Match:<br />" . $single_expense['description'];
                    //unset($this->expenses[$key]); 
                }
            }
        }
        return $expenses_obj;
    }


    function find_expenses_company_match_category_all($company_check, $single_cat, $start_date, $end_date){
        $expenses_obj = array(
            "single"        =>  array(
                "amount"    =>  0
            ),
            "all"           => array(
                "amount"   =>  0
            ),
            "na"            =>  array(
                "amount"    =>  0
            ),
            "description"   =>  ''
        );
        foreach( $this->expenses_company_match_category_all as $key => $single_expense ){ 
            //echo "sd: " . $start_date . ", ed: " . $end_date . ", bd: " . $single_expense['billing_date'] . ", bde: " . $single_expense['billing_date_end'] . "<br />";
            //echo $single_expense['repeats'] . "<br />";
            /*
            *   If Company matches
            */
            foreach ($this->companies_in_woo as $company){
                if ( $company_check == $company['name'] && $company['id'] == $single_expense['company'] ){
                    //echo "check: " . $company_check . ':' . $company['name'] . ', id: ' . $company['id'] . ', ' . $single_expense['company'] . "<br />";
                    
                    /*
                    *   If within date range
                    */                   
                    //echo "<br />OUTSIDE<br />";
                    //print_r($single_expense);                          
                    //if( $this->check_in_range($start_date, $end_date, $single_expense['billing_date'], $single_expense['billing_date_end'], $single_expense['repeats'])  ){
                    if( $this->check_in_range_new($start_date, $end_date, $single_expense['billing_date'])  ){
                        // Farmer's doesn't make it into here becaust it's not within January
                        //echo "<br />INSIDE<br />";
                        //print_r($single_expense);
                        $expenses_obj['all']['amount'] += $single_expense['amount'];
                        if($expenses_obj['description'] != ''){
                            $expenses_obj['description'] .= "<br />";
                        }
                        $expenses_obj['description'] .= "Company Match & Category All:<br />" . $single_expense['description'];
                        //unset($this->expenses[$key]);  
                    }
                }
            }
        }
        return $expenses_obj;
    }


    function output_expenses_readable(){
        foreach( $this->expenses as $expense){
            print_r($expense);

            //get the meta you need form each post
            echo "<br />Amount: " . $expense['amount'] . "<br />";
            $company = $expense['company'];
            echo "Company ID: " . $company . "<br />";
            //echo "Frequency: " . $expense['duration'] . "<br />";
            echo "Repeats: " . $expense['repeats'] . "<br />";
            $str = substr($expense['billing_date'], 0, strrpos($expense['billing_date'], ' '));
            echo "Date: " . $str . "<br />";
        }
    }

    function output_expenses_object(){
        echo "expenses_company_match_category_match<br />";
        var_dump($this->expenses_company_match_category_match);

        echo "<br />expenses_company_all_category_match<br />";
        var_dump($this->expenses_company_all_category_match);

        echo "<br />expenses_company_match_category_all<br />";
        var_dump($this->expenses_company_match_category_all);

        echo "<br />expenses_company_all_category_all<br />";
        var_dump($this->expenses_company_all_category_all);

        echo "<br />expenses_company_na_cateogry_na<br />";
        var_dump($this->expenses_company_na_category_na);
    }
}