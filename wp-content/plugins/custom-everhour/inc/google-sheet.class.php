<?php

class Google_Sheet {

    var $expenses_company_match_category_match      = array();
    var $expenses_company_all_category_match        = array();
    var $expenses_company_match_category_all        = array();
    var $expenses_company_all_category_all          = array();
    var $expenses_company_na_category_na            = array();
    var $expenses_company_na_category_na_filtered   = array();
    var $expenses_company_all_category_matched      = array();
    var $expenses_company_all_category_no_match     = array();
    var $expenses_all                               = array();
    
    var $expenses_all_new                           = array();

    var $companies_in_woo = array();
    var $divisors = array("Empty"=>1);

    var $temp_expenses_object = array(
        "amount"        =>  0,
        "description"   =>  '',
        "single"        =>  0,
    );

    public function reset_temp_expenses_object(){
        $this->temp_expenses_object = array(
            "amount"        =>  0,
            "description"   =>  '',
            "single"        =>  0
        ); 
    }

    public function get_expenses_object_built(){
        return $this->temp_expenses_object;
    }

    /* Check selected billing date range Helper */
    public function check_in_range($start_date, $end_date, $billing_date, $billing_date_end, $repeats)
    {
        // Convert to timestamp
        $start_date_filter_ts = strtotime($start_date);
        $end_date_filter_ts = strtotime($end_date);
        $billing_date_ts = strtotime($billing_date);
        if($billing_date_end == 'undefined' || $billing_date_end == '' || $billing_date_end == 0 || $billing_date_end == null || $billing_date_end == 'ongoing' ){
            $billing_date_end_ts = 0;
        }else{
            $billing_date_end_ts = strtotime($billing_date_end);
        }
        
        /*
        *   Monthly
        *   !HAD! Expense End Date must be later than the Filter End Date
        *   New: Expense Start Date should be earlier than the Filter End Date
        *   New: Expense End Date must be later than the Filter Start Date (or 'ongoing')
        */
        if( $repeats == "Monthly" ){
            if( $billing_date_ts <= $end_date_filter_ts && ($billing_date_end_ts == 0 || $billing_date_end_ts >= $start_date_filter_ts ) ){
                //echo "passes Monthly<br />";
                return true;
            }
        }

        /*
        *   Yearly
        *   !HAD! Filter End Date must be earlier than the Expense Start Date
        *   New: Expense Start Date must be earlier than the Filter End Date
        *   New: Expense End Date must be later than the Filter Start Date (or 'ongoing')
        */
        if( $repeats == "Yearly" ){
            if( $billing_date_ts <= $end_date_filter_ts && ($billing_date_end_ts == 0 || $billing_date_end_ts >= $start_date_filter_ts) ){
                //echo "passes Yearly<br />";
                return true;
            }
        }

        /*
        *   One time
        *   !HAD! Expense Start Date must be later than the Filter Start Date
        *   !HAD! Expense End Date must be later than the Filter End Date
        *   New: Expense Start Date must be later than the Filter Start Date
        *   New: Expense End Date must be earlier than the Filter End Date
        */
        if( $repeats == "One Time" ){
            if ( $billing_date_ts >= $start_date_filter_ts && $billing_date_end_ts <= $end_date_filter_ts ){
                //echo "passes One Time<br />";
                return true;
            }
        }
        //echo "this should almost never fire";
        return false;
    }


    public function check_in_range_new($start_date, $end_date, $billing_date)
    {
       // Convert to timestamp
       $start_date_filter_ts = strtotime($start_date);
       $end_date_filter_ts = strtotime($end_date);
       $billing_date_ts = strtotime($billing_date);
       //echo "billing: " . $billing_date_ts . "<br />";
       if( ($billing_date_ts >= $start_date_filter_ts) && ($billing_date_ts <= $end_date_filter_ts) ){
           //echo "true<br />";
           return true;
       }else{
           //echo "false<br />";
           return false;
       }
    }


    function get_expenses_company_all_category_all($start_date, $end_date){
        $filtered_by_date = array();
        foreach($this->expenses_company_all_category_all as $single_expense){
            //echo "start: " . $start_date . ", end: " . $end_date . "<br />";
            //print_r($single_expense);
            //echo "<br />";
            if( $this->check_in_range_new($start_date, $end_date, $single_expense['billing_date']) ){
                array_push($filtered_by_date, $single_expense);
            }
        }
        return $filtered_by_date;
    }

    function get_expenses_company_na_category_na_filtered(){
        return $this->expenses_company_na_category_na_filtered;
    }

    function get_expenses_company_all_category_no_match(){
        return $this->expenses_company_all_category_no_match;
    }

    function get_expenses_all() {
        return $this->expenses_all;
    }


    function get_all_companies_in_woo(){
        $companies = get_posts(array(
            'post_type'   => 'companies',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'fields' => 'ids',
            )
        );
        //print_r($companies); // array of company IDs
        foreach($companies as $c){
            $temp_company = array();
            $temp_company['id'] = $c;
            $temp_company['name'] = get_the_title($c);
            
            $this->companies_in_woo[] = $temp_company;
        }

        //print_r($this->companies_in_woo);
        return $this->companies_in_woo;
    }


    /**
     * WAS query to get 'expenses' post type
     * NOW uses rows from Google Sheet
     */
    function get_all_expenses(){
        //echo "Year: " . $_GET['years'] . "<br/ >";
        if(isset($_GET['years']) && $_GET['years'] == 2021){
            $start_year = 2021;
        }
        if(isset($_GET['years']) && $_GET['years'] == 2022){
            $start_year = 2022;
        }
        if(isset($_GET['years']) && $_GET['years'] == 2023){
            $start_year = 2023;
        }
        
        $start_date = $_GET['start_date'];
        //echo "Start Date: " . $start_date . "<br/ >";
        $arr = explode("-", $start_date);
        $start_year = intval($arr[0]);
        if( $start_year == 2021 ){
            $start_year = 2021;
        }
        if( $start_year == 2022 ){
            $start_year = 2022;
        }
        if( $start_year == 2023 ){
            $start_year = 2023;
        }
        //echo "Start Year extracted: " . $start_year . "<br />";

        /* WP Transient Cache */
        //$this->time_records = get_transient( 'google_sheet' );
        switch($start_year){
            case 2021:
            $expenses_spreadsheet = wp_remote_get(
                'https://docs.google.com/spreadsheets/d/e/2PACX-1vS9p6b_q5Jjp0ozJTkGu1syHbs3WXN3SZHDIrB4IFl0HRr6doyhMzAcCrFEmfEfoWWHgY64NKnztSyV/pub?gid=0&single=true&output=csv',
                array( 'timeout' => 10,
                    //'headers' => array( 'X-Api-Key' => $this->api_key ) 
                )
            );
            break;
            case 2022:
                $expenses_spreadsheet = wp_remote_get(
                    'https://docs.google.com/spreadsheets/d/e/2PACX-1vSXvLEiiiO6_1nbZ-kn9Rg7-OHte9zNJFSCsy9bTFICX0bR8KfItSaSQRUyBrv8JfOpNc8vHj0Ph3mA/pub?gid=2122580481&single=true&output=csv',
                    array( 'timeout' => 10,
                        //'headers' => array( 'X-Api-Key' => $this->api_key ) 
                    )
                );
            break;
            case 2023:
                $expenses_spreadsheet = wp_remote_get(
                    'https://docs.google.com/spreadsheets/d/e/2PACX-1vQYTuFH5MoCHnOHwRKFBKpgzEtDNSJMvRGR60TNtWOk8TQmW6BVwnIbsdJg419NtJcnAUGq1M2LgFJ6/pub?gid=2122580481&single=true&output=csv',
                    array( 'timeout' => 10,
                        //'headers' => array( 'X-Api-Key' => $this->api_key ) 
                    )
                );
            break;
        }

        if( is_wp_error( $expenses_spreadsheet ) ) {
            $error_string = $expenses_spreadsheet->get_error_message();
            echo '<div id="message" class="error"><p>' . $error_string . '</p></div>';
        }

        $expenses_obj = wp_remote_retrieve_body( $expenses_spreadsheet );
        $expenses_data = array_map("str_getcsv", explode("\n", $expenses_obj));
        //$expenses_data = json_encode($array);
        //print_r($expenses_data);
        // remove title row of spreadsheet
        $remove = array_shift($expenses_data);

        /*
        *   Example Object from GS Sheet:
        *   Array (
                [0] => Description
                [1] => Transaction Date // Ignored here
                [2] => Amount
                [3] => Date Start
                [4] => Tier
                [5] => Sub Category
                [6] => Company
                [7] => Service
                [8] => // Ignored here
                [9] => // Original (for full Yearly amount reference) // Ignored here
            )
        */
        foreach($expenses_data as $p){
            // print_r($p);
            // echo "<br >";
            $temp_item = array(
                "description"   =>  '', // 0
                "amount"        =>  0, // 2
                "billing_date"  =>  '', // 3
                "tier"          =>  '', // 4
                "sub-category"  =>  '', // 5
                "company"       =>  '', // 6
                "company-name"  =>  '', // 6
                "category"      =>  '', // aka Service 7
                "post"          =>  null, // 8
                //"accounted"     =>  false,
            );
            
            // Paid Advertising
            $temp_item['description'] = (isset($p[0])) ? $p[0]: null; // Description
            // $p[1] unused
            $temp_amount = (isset($p[2])) ? str_replace("$",'',$p[2]): 0; // Amount
            $temp_amount2 = str_replace(",",'',$temp_amount);
            $temp_item['amount'] = (float)$temp_amount2;

            $temp_item['billing_date'] = (isset($p[3])) ? $p[3]: ''; // Date Start 

            $temp_item['tier'] = isset($p[3]) ? $p[4]: ''; // Tier

            $temp_item['sub-category'] = isset($p[5]) ? $p[5]: ''; // Sub-Category

            $temp_item['company'] = (isset($p[6])) ? $p[6]: ''; // Company
            $temp_item['company-name'] = (isset($p[6])) ? $p[6]: '';

            $temp_item['category'] = (isset($p[7])) ? $p[7]: ''; // Service

            $temp_item['post'] = (isset($p)) ? $p: null;  // Full Post Object

            //print_r($temp_item);

            if( $temp_item['tier'] != "Hidden" ){

                if ( $temp_item['company-name'] == 'All' && $temp_item['category'] == 'All' ){
                    //echo "item added to expenses_company_all_category_all<br />";
                    array_push($this->expenses_company_all_category_all, $temp_item);
                }
                
                /*
                *   Company == 'All' && Category != 'All'
                *   Examples 3: Adobe Creative Cloud, AHREFs SEO Software, WP Engine Bulk
                *   !we need to track each of these as they're represented
                *   so as to diff this later and get those that don't get associated to line items
                */
                if ( $temp_item['company-name'] == 'All' && $temp_item['category'] != 'All' ){
                    //echo "item added to expenses_company_all_category_match<br />";
                    array_push($this->expenses_company_all_category_match, $temp_item);
                }

                // Company != 'All' && Category == 'All
                // Example 1: Farmers, Classic Exhibits
                if ( $temp_item['company-name'] != 'All' && $temp_item['category'] == 'All' ){
                    //echo "item added to expenses_company_match_category_all<br />";
                    array_push($this->expenses_company_match_category_all, $temp_item);
                }

                // Company != "All && Category != 'All' && Company != 'N/A' && Category != 'N/A'
                // Examples 3: 	
                // AHREFs SEO Software/PECAA/SEO
                // Cloudflare – Noble/Noble/Hosting & Maintenance
                // WP Engine – Josh Bersin/Josh Bersin/Hosting & Maintenance
                if ( $temp_item['company-name'] != 'All' && $temp_item['category'] != 'All' ){
                    if ( $temp_item['company-name'] != 'N/A' && $temp_item['category'] != 'N/A'){
                        //echo "item added to expenses_company_match_category_match<br />";
                        $temp_item['description'] .= '*';
                        array_push($this->expenses_company_match_category_match, $temp_item);
            
                    }
                }

                // Company == 'N/A' && Category == 'N/A'
                // Example:
                // Amy Northard CPA
                if ( $temp_item['company-name'] == 'N/A' && $temp_item['category'] == 'N/A'){
                    //echo "got one<br />"; // count of 13 which matches Google Doc
                    array_push($this->expenses_company_na_category_na, $temp_item);
                }

                // Array of All Expenses regardless of Company or Category, tier->"Hidden" still excluded even from this!
                array_push( $this->expenses_all, $temp_item );
            }
        }

        //$this->output_expenses_readable();
        //$this->output_expenses_object();
        return array(
            'expenses_company_match_category_match'     =>  $this->expenses_company_match_category_match,
            'expenses_company_all_category_match'       =>  $this->expenses_company_all_category_match,
            'expenses_company_match_category_all'       =>  $this->expenses_company_match_category_all,
            'expenses_company_all_category_all'         =>  $this->expenses_company_all_category_all,
            'expenses_company_na_category_na'           =>  $this->expenses_company_na_category_na,
            'expenses_company_na_category_na_filtered'  =>  $this->expenses_company_na_category_na_filtered,
            'expenses_all'                              =>  $this->expenses_all
        );
    }


    /*
    *   Get All Expenses NEW
    */
    /**
     * WAS query to get 'expenses' post type
     * NOW uses rows from Google Sheet
     */
    function get_all_expenses_new(){
        
        if(isset($_GET['years']) && $_GET['years'] == 2021){
            $start_year = 2021;
        }
        if(isset($_GET['years']) && $_GET['years'] == 2022){
            $start_year = 2022;
        }
        if(isset($_GET['years']) && $_GET['years'] == 2023){
            $start_year = 2023;
        }
        
        $default_current = new DateTime();
        $start_date_default = $default_current->format('Y') . "-" . $default_current->format('m') . "-" . $default_current->format('d');
        $start_date = (isset($_GET['start_date']))?$_GET['start_date']:$start_date_default;
        
        $arr = explode("-", $start_date);
        $start_year = intval($arr[0]);
        if( $start_year == 2021 ){
            $start_year = 2021;
        }
        if( $start_year == 2022 ){
            $start_year = 2022;
        }
        if( $start_year == 2023 ){
            $start_year = 2023;
        }
        
        switch($start_year){
            case 2021:
            $expenses_spreadsheet = wp_remote_get(
                'https://docs.google.com/spreadsheets/d/e/2PACX-1vS9p6b_q5Jjp0ozJTkGu1syHbs3WXN3SZHDIrB4IFl0HRr6doyhMzAcCrFEmfEfoWWHgY64NKnztSyV/pub?gid=0&single=true&output=csv',
                array( 'timeout' => 10,
                    //'headers' => array( 'X-Api-Key' => $this->api_key ) 
                )
            );
            break;
            case 2022:
                $expenses_spreadsheet = wp_remote_get(
                    'https://docs.google.com/spreadsheets/d/e/2PACX-1vSXvLEiiiO6_1nbZ-kn9Rg7-OHte9zNJFSCsy9bTFICX0bR8KfItSaSQRUyBrv8JfOpNc8vHj0Ph3mA/pub?gid=2122580481&single=true&output=csv',
                    array( 'timeout' => 10,
                        //'headers' => array( 'X-Api-Key' => $this->api_key ) 
                    )
                );
            break;
            case 2023:
                $expenses_spreadsheet = wp_remote_get(
                    'https://docs.google.com/spreadsheets/d/e/2PACX-1vQYTuFH5MoCHnOHwRKFBKpgzEtDNSJMvRGR60TNtWOk8TQmW6BVwnIbsdJg419NtJcnAUGq1M2LgFJ6/pub?gid=2122580481&single=true&output=csv',
                    array( 'timeout' => 10,
                        //'headers' => array( 'X-Api-Key' => $this->api_key ) 
                    )
                );
            break;
        }

        if( is_wp_error( $expenses_spreadsheet ) ) {
            $error_string = $expenses_spreadsheet->get_error_message();
            echo '<div id="message" class="error"><p>' . $error_string . '</p></div>';
        }

        $expenses_obj = wp_remote_retrieve_body( $expenses_spreadsheet );
        $expenses_data = array_map("str_getcsv", explode("\n", $expenses_obj));
        
        // remove title row of spreadsheet
        $remove = array_shift($expenses_data);

        /*
        *   Example Object from GS Sheet:
        *   Array (
                [0] => Description
                [1] => Transaction Date // Ignored here
                [2] => Amount
                [3] => Date Start
                [4] => Tier
                [5] => Sub Category
                [6] => Company
                [7] => Service
                [8] => // Ignored here
                [9] => // Original (for full Yearly amount reference) // Ignored here
            )
        */
        foreach($expenses_data as $p){
        //    echo "<pre>";
        //    print_r($p);
        //    echo "</pre>";

            $temp_item = array(
                "description"   =>  '', // 0
                "amount"        =>  0, // 2
                "billing_date"  =>  '', // 3
                "tier"          =>  '', // 4
                "sub-category"  =>  '', // 5
                "company"       =>  '', // 6
                "category"      =>  '', // aka Service 7,
                "eh_id"         =>  null
            );
            

            $temp_item['description'] = (isset($p[0])) ? $p[0]: null; // Description

            $temp_amount = (isset($p[2])) ? str_replace("$",'',$p[2]): 0; // Amount
            $temp_amount2 = str_replace(",",'',$temp_amount);
            $temp_item['amount'] = (float)$temp_amount2;

            $temp_item['billing_date'] = (isset($p[3])) ? $p[3]: ''; // Date Start 

            $temp_item['tier'] = isset($p[3]) ? $p[4]: ''; // Tier

            $temp_item['sub-category'] = isset($p[5]) ? $p[5]: ''; // Sub-Category

            //$temp_item['company'] = (isset($p[6])) ? strtolower($p[6]): ''; // Company
            //$temp_item['eh_id'] = (isset($p[6])) ? explode(":", strtolower($p[6]))[1]: ''; // Everhour ID
            if(isset($p[6])){
                $explode_it = explode(":", strtolower($p[6]));
                if(isset($explode_it[0])){
                    $temp_item['company'] = $explode_it[0]; // Company
                }
                if(isset($explode_it[1])){
                    $temp_item['eh_id'] = $explode_it[1];
                }
            }
            

            $temp_item['category'] = (isset($p[7])) ? strtolower($p[7]): ''; // Service

            if( $temp_item['tier'] != "Hidden" ){
                // Array of All Expenses regardless of Company or Category, tier->"Hidden" excluded!
                array_push( $this->expenses_all_new, $temp_item );
            }
        }

        return $this->expenses_all_new;
    }


    function find_expenses_company_match_AND_category_match($order, $single_cat, $start_date, $end_date){
        $single_cat = strip_tags(html_entity_decode($single_cat));

        /*
        *   Expenses for Company && Category Match
        */
        foreach( $this->expenses_company_match_category_match as $key => $single_expense ){
            
            // $mystring = $single_expense['description'];
            // $findme   = 'Facebook';
            // $pos = strpos($mystring, $findme);
            // if ($pos === false) {
            // }else{
            //     print_r($single_expense);
            // }

            if(isset($single_expense['category'])){
                $single_expense['category'] = strip_tags(html_entity_decode($single_expense['category']));
            }
            //echo "Company & Category Match: " . $single_expense['company'] . "<br />";

            /*
            *   If Category/Service matches
            */
            if( isset($single_expense['category']) && strcmp($single_expense['category'], $single_cat) == 0 ){

                // $mystring = $single_expense['description'];
                // $findme   = 'Facebook';
                // $pos = strpos($mystring, $findme);
                // if ($pos === false) {
                // }else{
                //     print_r($single_expense);
                // }

                /*
                *   If Company matches
                */
                foreach ($this->companies_in_woo as $company){

                    // if ( $order->company == 'Josh Bersin Academy'){
                    //     $order->company = 'Josh Bersin';
                    // }
                    // if ( $company['name'] == 'Josh Bersin Academy' ){
                    //     $company['name'] = 'Josh Bersin';
                    // }
                    
                    // $mystring = $single_expense['description'];
                    // $findme   = 'Facebook';
                    // $pos = strpos($mystring, $findme);
                    // if ($pos === false) {
                    // }else{
                    //     echo $company['name'] . ', single_expense company: ' . $single_expense['company'] . "<br />";
                    //     print_r($single_expense);
                    //     echo "<br /><br />";
                    // }

                    if ( $order->company == $company['name'] && $company['name'] == $single_expense['company'] ){
                        
                        // $mystring = $single_expense['description'];
                        // $findme   = 'Facebook';
                        // $pos = strpos($mystring, $findme);
                        // if ($pos === false) {
                        // }else{
                        //     print_r($single_expense);
                        // }

                        /*
                        *   If "One Time" must be within date range
                        *   If "Monthly" or "Yearly" only needs to be Before end date
                        */                                            
                        //if( $this->check_in_range_new($start_date, $end_date, $single_expense['billing_date'], $single_expense['billing_date_end'], $single_expense['repeats']) ){
                        if( $this->check_in_range_new($start_date, $end_date, $single_expense['billing_date']) ){
                            
                            $this->temp_expenses_object['single'] += $single_expense['amount'];
                            if($this->temp_expenses_object['description'] != ''){
                                $this->temp_expenses_object['description'] .= "<br />";
                            }
                            //$this->temp_expenses_object['description'] .= $single_expense['description'] . ": $" . $single_expense['amount']. ": " . $single_expense['repeats'];                            
                            $this->temp_expenses_object['description'] .= $single_expense['description'] . ": $" . $single_expense['amount'];                            
                        }
                    }
                }
            }
        }


        /*
        *   Expenses for Company All && has a Category
        */
        //print_r($this->expenses_company_all_category_match);
        foreach( $this->expenses_company_all_category_match as $key => $single_expense ){ 

            if(isset($single_expense['category'])){
                $single_expense['category'] = strip_tags(html_entity_decode($single_expense['category']));
            }

            /*
            *   If Category/Service matches
            */
            if( isset($single_expense['category']) && strcmp($single_expense['category'], $single_cat) == 0 ){

                $date1 = $start_date;
                $date2 = $end_date;
                $d1=new DateTime($date2); 
                $d2=new DateTime($date1);                                  
                $Months = $d2->diff($d1); 
                $number_of_months_of_this_expense = (($Months->y) * 12) + ($Months->m);
                if($number_of_months_of_this_expense == 0){
                    $number_of_months_of_this_expense = 1;
                }
                //echo $number_of_months_of_this_expense . "<br />"; // 1

                /*
                *   If "One Time", must be Within date range
                *   If "Monthly" or "Yearly" must be Before end date
                */                                             
                //if($this->check_in_range($start_date, $end_date, $single_expense['billing_date'], $single_expense['billing_date_end'], $single_expense['repeats']) && isset($single_expense['amount']) ){
                if( $this->check_in_range_new($start_date, $end_date, $single_expense['billing_date']) ){
                    //print_r($single_expense);
                    
                    //print_r($single_expense['repeats']);
                    // if( $single_expense['repeats'] == 'Monthly'){
                    //     $this->temp_expenses_object['amount'] += $single_expense['amount'] * $number_of_months_of_this_expense;
                    // }
                    // if( $single_expense['repeats'] == 'Yearly'){
                    //     //print_r($single_expense['amount']); // enters, and outputs 7728
                    //     $this->temp_expenses_object['amount'] += ($single_expense['amount']/12) * $number_of_months_of_this_expense;
                    // }
                    //if ( $single_expense['repeats'] == "One Time" ){
                        //echo "One Time Expense";
                        $this->temp_expenses_object['amount'] += $single_expense['amount'];
                    //}

                    if($this->temp_expenses_object['description'] != ''){
                        $this->temp_expenses_object['description'] .= "<br />";
                    }
    
                    //$this->temp_expenses_object['description'] .= $single_expense['description'] . "| $" . $single_expense['amount']. "| " . $single_expense['repeats'];
                    $this->temp_expenses_object['description'] .= $single_expense['description'] . " | $" . $single_expense['amount'];
                    //unset($this->expenses[$key]); 
                }
                /*
                *   Add this expense from expenses_company_all_category_match to
                *   expenses_company_all_category_no_match
                *   After looping through Order line items
                *   Dif against expenses_company_all_category_match
                *   to get expenses with All & Category that Daven't match a category
                */
                array_push($this->expenses_company_all_category_matched, $single_expense);
            }
        }

        if( $this->temp_expenses_object['amount'] == 0 && $this->temp_expenses_object['single'] == 0 ){
            return false;
        }else{
            return true;
        }
    }


    /*
    *   Expenses with a Company, but no Invoice in Woo
    */
    function find_remaining_expenses_with_companies($array_of_companies_already_matched, $start_date, $end_date){
        $new_array_of_expenses = [];
        foreach($this->expenses_company_match_category_match as $remaining_expense){

            if( !in_array( $remaining_expense['company'], $array_of_companies_already_matched ) && $remaining_expense['company'] != '' && $remaining_expense['company'] != null ){
                
                if( $this->check_in_range_new($start_date, $end_date, $remaining_expense['billing_date']) ){
                    array_push( $new_array_of_expenses, $remaining_expense);
                }
            }
        }
        return $new_array_of_expenses;
    }


    /*
    *   Expenses with a Company and invoice, but no Line Item matches an Expense's category
    */
    function find_remaining_expenses_additional_line_items($company, $expenses_company_categories_encountered, $start_date, $end_date){
        $new_array_of_expenses = [];
        foreach($this->expenses_company_match_category_match as $remaining_expense){

            if( !in_array( $remaining_expense['category'], $expenses_company_categories_encountered ) && $remaining_expense['company'] == $company ){
                
                if( $this->check_in_range_new($start_date, $end_date, $remaining_expense['billing_date']) ){
                    array_push( $new_array_of_expenses, $remaining_expense);
                }
            }
        }
        return $new_array_of_expenses;
    }



    /*
    *   Expenses for Company All && Category No Match
    */
    function find_expenses_company_all_category_no_match($start_date, $end_date){
        
        //print_r($this->expenses_company_all_category_matched);
        foreach( $this->expenses_company_all_category_match as $key => $single_expense ){ 
        
            /*
            *   If Expense's Category/Service is NOT in expenses_company_all_category_matched
            */
            if( !in_array($single_expense, $this->expenses_company_all_category_matched) ){

                // echo "<pre>";
                // print_r($single_expense);
                // echo "</pre><br /><br />";

                $date1 = $start_date;
                $date2 = $end_date;
                $d1=new DateTime($date2); 
                $d2=new DateTime($date1);                                  
                $Months = $d2->diff($d1); 
                // $number_of_months_of_this_expense = (($Months->y) * 12) + ($Months->m);
                // if($number_of_months_of_this_expense == 0){
                //     $number_of_months_of_this_expense = 1;
                // }

                /*
                *   If "One Time", must be Within date range
                *   If "Monthly" or "Yearly" must be Before end date
                */                                             
                //if($this->check_in_range($start_date, $end_date, $single_expense['billing_date'], $single_expense['billing_date_end'], $single_expense['repeats']) && isset($single_expense['amount']) ){
                if( $this->check_in_range_new($start_date, $end_date, $single_expense['billing_date']) ){
                    //print_r($single_expense);
                    
                    //print_r($single_expense['repeats']);
                    // if( $single_expense['repeats'] == 'Monthly'){
                    //     $this->temp_expenses_object['amount'] += $single_expense['amount'] * $number_of_months_of_this_expense;
                    // }
                    // if( $single_expense['repeats'] == 'Yearly'){
                    //     //print_r($single_expense['amount']); // enters, and outputs 7728
                    //     $this->temp_expenses_object['amount'] += ($single_expense['amount']/12) * $number_of_months_of_this_expense;
                    // }
                    // if ( $single_expense['repeats'] == "One Time" ){
                        //echo "One Time Expense";
                        $this->temp_expenses_object['amount'] += $single_expense['amount'];
                    //}

                    if($this->temp_expenses_object['description'] != ''){
                        $this->temp_expenses_object['description'] .= "<br />";
                    }
    
                    //$this->temp_expenses_object['description'] .= $single_expense['description'] . ": $" . $single_expense['amount'] . ": " . $single_expense['repeats'];
                    $this->temp_expenses_object['description'] .= $single_expense['description'] . ": $" . $single_expense['amount'];
                    //unset($this->expenses[$key]); 
                
                    array_push($this->expenses_company_all_category_no_match, $single_expense);
                }
            }
        }
    }

    function find_expenses_company_match_category_all($company_check, $single_cat, $start_date, $end_date){
        
        foreach( $this->expenses_company_match_category_all as $key => $single_expense ){ 
            /*
            *   If Company matches
            */
            foreach ($this->companies_in_woo as $company){
                if ( $company_check == $company['name'] && $company['id'] == $single_expense['company'] ){
                    
                    /*
                    *   If within date range
                    */                   
                                   
                    //if( $this->check_in_range($start_date, $end_date, $single_expense['billing_date'], $single_expense['billing_date_end'], $single_expense['repeats'])  ){
                    if( $this->check_in_range_new($start_date, $end_date, $single_expense['billing_date']) ){

                        $this->temp_expenses_object['amount'] += $single_expense['amount'];
                        if($this->temp_expenses_object['description'] != ''){
                            $this->temp_expenses_object['description'] .= "<br />";
                        }
                       
                        //$this->temp_expenses_object['description'] .= $single_expense['description'] . ": $" . $single_expense['amount']. ": " . $single_expense['repeats'];
                        $this->temp_expenses_object['description'] .= $single_expense['description'] . ": $" . $single_expense['amount'];
                    }
                }
            }
        }
        //return $this->temp_expenses_object;
    }


    function filter_expenses_company_na_category_na($start_date, $end_date){
        /*
        *   If within date range
        */                   
        foreach($this->expenses_company_na_category_na as $single_expense){

            //if( $this->check_in_range($start_date, $end_date, $single_expense['billing_date'], $single_expense['billing_date_end'], $single_expense['repeats'])  ){
            if( $this->check_in_range_new($start_date, $end_date, $single_expense['billing_date']) ){

                // if( $single_expense['repeats'] == 'Yearly'){
                //     $single_expense['amount'] = $single_expense['amount']/12;
                // }

                array_push($this->expenses_company_na_category_na_filtered, $single_expense);
            }
        }
    }


    function output_expenses_readable(){
        foreach( $this->expenses as $expense){
            //print_r($expense);

            //get the meta you need form each post
            echo "<br />Amount: " . $expense['amount'] . "<br />";
            $company = $expense['company'];
            echo "Company ID: " . $company . "<br />";
            //echo "Frequency: " . $expense['duration'] . "<br />";
            echo "Repeats: " . $expense['repeats'] . "<br />";
            $str = substr($expense['billing_date'], 0, strrpos($expense['billing_date'], ' '));
            echo "Date: " . $str . "<br />";
        }
    }

    function output_expenses_object(){
        echo "expenses_company_match_category_match<br />";
        var_dump($this->expenses_company_match_category_match);

        echo "<br />expenses_company_all_category_match<br />";
        var_dump($this->expenses_company_all_category_match);

        echo "<br />expenses_company_match_category_all<br />";
        var_dump($this->expenses_company_match_category_all);

        echo "<br />expenses_company_all_category_all<br />";
        var_dump($this->expenses_company_all_category_all);

        echo "<br />expenses_company_na_cateogry_na<br />";
        var_dump($this->expenses_company_na_category_na);
    }

}

