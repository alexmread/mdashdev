<?php

class Everhour_Reports {

    var $api_key;
    var $all_report;

    function set_api_key($api_key){
        $this->api_key = $api_key;
    }    

    function get_report_all_date_range($start, $end){
        $request_report = wp_remote_get( 'https://api.everhour.com/dashboards/projects?date_gte=' . $start . '&date_lte=' . $end,
            array( 'timeout' => 10,
                'headers' => array( 'X-Api-Key' => $this->api_key ) 
            )
        );

        if( is_wp_error( $request_report ) ) {
            $error_string = $request_report->get_error_message();
            echo '<div id="message" class="error"><p>' . $error_string . '</p></div>';
        }

        $reportObj = wp_remote_retrieve_body( $request_report );
        $report_data = json_decode( $reportObj );
        $this->all_report = $report_data;
        //var_dump($report_data);
        //match either projectName or clientName
        //set_transient( 'everhour_report', $invoices_data, 1 * DAY_IN_SECONDS );
    }

    function match_client_get_cost($client){
        $cost_to_return = 0;
        foreach($this->all_report as $client_obj){
            if( isset($client_obj->clientName) && $client_obj->clientName == $client && isset($client_obj->costs) ){
                $cost_to_return = number_format($client_obj->costs/100, 2);
            }
        }
        return $cost_to_return;
    }
}