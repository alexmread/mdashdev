<?php
/*
*   The Goal is to only count each category once per invoice
*   And to end up with the number of companies responsible for a category expense (divisor)
*/

class Category_Counter {

    var $category_collection = array();

    function increment_category($category, $revenue){
        //echo "this even fires<br />";
        //settype($revenue, "integer");
        if( array_key_exists($category, $this->category_collection) ){
            $this->category_collection[$category]['count']++;
            $this->category_collection[$category]['revenue'] = $this->category_collection[$category]['revenue'] + $revenue;
        }else{
            $this->category_collection[$category]['count'] = 1;
            //echo "type: " . gettype($revenue) . "<br />"; // string
            $this->category_collection[$category]['revenue'] = $revenue;
        }
    }

    function get_category_collection(){
        return $this->category_collection;
    }
    
    function output_category_collection(){
        echo "<pre>";
        print_r($this->category_collection);
        echo "</pre>";
    }



    /*
    invoices [
        order_id,
        categories[
            'category_string1',
            'category_string2',
        ]
    ]
    */
    //var $invoices = array();

    /*
    categories [
        category_string1,
        count: int
    ]
    */
    //var $categories = array();



    // function log_category($order_id, $category){
    //     if( array_key_exists($order_id, $this->invoices) ){
    //         $this->invoices[$order_id][] = $category;
    //     }else{
    //         $this->invoices[$order_id] = array($category);
    //     }
    // }   

    // function tally_categories(){
    //     foreach ( $this->invoices as $invoice ){
    //         $invoice = array_unique($invoice);
    //         foreach ($invoice as $category){
    //             if( array_key_exists($category, $this->categories) ){
    //                 $this->categories[$category]++;
    //             }else{
    //                 $this->categories[$category] = 1;
    //             }
    //         }            
    //         //if( array_key_exists($order_id, $this->categories) ){
    //         // $new_category = new stdClass();
    //         // $new_category->name = $single_category;
    //         // $new_category->count = 1;
    //         // $this->categories[] = $new_category;
    //     }
    //     return $this->categories;
    // }

    // function output_category_totals(){
    //     echo "<pre>";
    //     print_r($this->categories);
    //     echo "</pre>";
    // }

    // function invoices_to_categories(){
    //     echo "<pre>";
    //     print_r($this->invoices);
    //     echo "</pre>";
    // }
    
}