<?php
/*
*   This filename is a misnomer now,
*   as it outputs the lines for the whole report,
*   Not just Everhour
*/

class Output_Line_Items {
    var $alternate = '';  

    var $items = array();
    var $company_filter = 'xyz';
    var $category_filter = '';

    var $total_time = 0;
    var $total_cost = 0;
    var $total_expenses = array();
    var $total_revenue = 0;
    var $total_profit = 0;
    var $total_percent = 0;
    var $total_rate = 0;

    var $divisors = array();
    var $companies_represented = array();

    public function get_companies_represented(){
        return count($this->companies_represented);
    }

    public function set_company_dropdown($setTo){
        $this->company_filter = $setTo;
    }

    public function set_category_dropdown($setTo){
        $this->category_filter = $setTo;
    }

    public function add_a_line_item($label, $single_cat, $order, $item_data, $cost, $time, $expense){
        //echo "company: " . $order->company . ", label: " . $label . ", cat: " . $single_cat . "<br />";
        //if($label == ''){ $label = $single_cat; }
        //if($single_cat == ''){ $single_cat = $label; }
        // if($order == null){
        //     $order = array(
        //         'company' => ''
        //     );
        // }
        if(!in_array($order->company, $this->companies_represented)){
            array_push($this->companies_represented, $order->company);
        }

        $new_item = true;
        
        /*
            $item_data passed in looks like this:
                [id] => 3582
                [order_id] => 18829
                [name] => Sales Funnels & Lead Generation
                [product_id] => 17324
                [variation_id] => 0
                [quantity] => 1
                [tax_class] => 
                [subtotal] => 212.5
                [subtotal_tax] => 0
                [total] => 212.5
                [total_tax] => 0
                [taxes] => ...
                [meta_data] => ...
        */

        $this->total_revenue = 0;
        foreach ($this->items as $key => $line_item) {
            
            //if( $item_data!=null && isset($item_data['id']) && $line_item[3]!=null && isset($item_data['product_id']) ){
                // order companies match
                if ( $order->company == $line_item[2]->company
                    &&
                    ($order->company != 'N/A' && $order->company != 'All')
                ){
                    // Product Name match
                    if ( $line_item[1] == $single_cat ){
                        // Order ID
                        //if ( $order->id == $line_item[3]['order_id']){
                            // Item Total
                            //if( $item_data['total'] != $line_item[3]['total']){
                            // echo "dupe found:<br />";
                            // print_r($item_data);
                            // echo "<br />REPEATS: " . $order->amount['repeats'] . "<br />";
                            // echo "<br /><br />";
                        if($this->items[$key][3]){
                            $this->items[$key][3]['total'] += $item_data['total'];
                            $new_item = false;
                        }
                        //}
                    }
                }
            //}

            // Every time we add an item, recalculate the total revenue too
            if($new_item == true){
                if(isset($this->items[$key][3]['total'])){
                    $this->total_revenue += $this->items[$key][3]['total'];
                }
            }
        }
        
        /*
            [0]-$label, [1]-$single_cat, [2]-$order, [3]-$item_data, [4]-$cost, [5]-$time, [6]-$expense
            
            [0] => Web Design, [1] => Web Design, [2] => stdClass Object (
                    [id] => 18802
                    [date] => 2021-04-01 09:36:11
                    [company] => Carolina Kustoms
                    [order_total] => 900
                )
            [3] => Array (
                [id] => 3505
                [order_id] => 18802
                [name] => Website Design Project
                [product_id] => 3207
                [variation_id] => 0
                [quantity] => 1
                [tax_class] =>
                [subtotal] => 900
                [subtotal_tax] => 0
                [total] => 900
                [total_tax] => 0
                [taxes] => Array (
                    [total] => Array ( )
                    [subtotal] => Array ( )
                )
                [meta_data] => Array ( )
            )
            [4] => 79.75
            [5] => 2.4166666666667
            [6] => Array (
                [amount] => 14.53
                [description] => Lambda Test: $174.36 [single] => 0
            )
        */


        /*
        *   Increment divisor tally
            $divisors_test = array(
                "Hosting & Maintenance" => array(
                    'value' => 0,
                    'companies' => 'Pacific Truck Colors, Heartwarmers'
                ),
                "Content" => array(
                    'value' => 1,
                    'companies' => 'Midas Touch, Bananapay'
                )
            );
        */
        // echo "<pre>";
        // print_r($item_data);
        // echo "</pre><br />";
        if( $new_item == true && $single_cat != ''){
            $temp_label = $single_cat;
            
            if ( isset($this->divisors[$temp_label]) && !str_contains($this->divisors[$temp_label]['companies'], $order->company) ) {
                $this->divisors[$temp_label]['companies'] = $this->divisors[$temp_label]['companies'] . ', ' . $order->company;
                $this->divisors[$temp_label]['value'] = $this->divisors[$temp_label]['value'] + 1;
            }

            if ( !isset($this->divisors[$temp_label]) ) {
                $this->divisors[$temp_label] = array(
                    'value' => 1,
                    'companies' => $order->company
                );
            }
            //echo "Company: " . $order->company . ", Service: " . $temp_label . ", count: " . $this->divisors[$temp_label]['value'] . "<br />";
        }        


        // if the company dropdown filter was selected,
        // only show that company
        if( $this->company_filter != '0 Companies' && strpos( $order->company, $this->company_filter ) !== 0 ){
            $new_item = false;
        }
        if( ($label == 'All') ){//&& ($order->company == 'All' || $order->company == 'N/A') ){
            $new_item = true;
        }

        // if the category dropdown filter was selected,
        // only show those lines
        if( $this->category_filter != 'Product Categories' && $this->category_filter != strip_tags($single_cat) ){
            $new_item = false;
        }
        
        if($new_item == true){
            
            //if(!in_array(array($label, $single_cat, $order, $item_data, $cost, $time, $expense), $this->items)){

                //if( $cost == 0 && $time == 0 && $expense['amount'] == 0 ){
                    //echo "never returns false"; // prints many
                //    return false;
                //}else{
                    
                array_push( $this->items, array($label, $single_cat, $order, $item_data, $cost, $time, $expense) );
                //}
                
                $this->total_time += $time;
                $this->total_cost += $cost;
                if($expense['amount'] != 0){
                    $tempA = array($label, $expense['amount'], 'all');
                    array_push($this->total_expenses, $tempA);
                }
                if($expense['single'] != 0){
                    $tempB = array($label, $expense['single'], 'single');
                    array_push($this->total_expenses, $tempB);
                }
            //}
        }
    }


    public function output($company_filter){
        $this->company_filter = $company_filter;
        //echo "company filter: " . $company_filter . "<br />";
        $this->output_table_styles();
        $this->output_table_head();

        $this->display_totals();

        // echo "<pre>";
        // print_r($this->items);
        // echo "</pre>";

        foreach($this->items as $one){
            //print_r($one);
            //echo "<br /><br />";
            if($company_filter == '0 Companies'){
                $this->display_items($one[0],$one[1],$one[2],$one[3],$one[4],$one[5], $one[6]);
            }else{
                if($one[2]->company == $company_filter || ($one[0] == 'N/A') || ($one[0] == 'All') ){
                    // if($one[6]['amount'] != 0){
                    //     $one[6]['amount'] = $one[6]['amount']/$this->get_companies_represented();
                    // }
                    // if($one[6]['single'] != 0){
                    //     $one[6]['single'] = $one[6]['single']/$this->get_companies_represented();
                    // }
                    $this->display_items($one[0],$one[1],$one[2],$one[3],$one[4],$one[5], $one[6]);
                }
            }
        }

        $this->output_table_close();

        // echo "<pre>";
        // print_r($this->divisors);
        // echo "</pre>";
    }

    public function output_table_styles(){
        ?>
        <style> 
            /* td { 
                white-space: nowrap; 
            }  */
            .widefat td{
                padding: 2px 5px !important;
                /* border-bottom: 1px solid black; */
            }
            .under td{
                border-bottom: 1px solid #ccd0d4;
                background-color: #e6e6e6;
            }
            .noUnder{
                border-bottom: none !important;
            }
            td.purpleL a {
                color: purple;
            }
            .widefat tr{
                border-bottom: 1px solid black;
            }
        </style>
        <?php
    }

    public function output_table_head(){
        ?>
        <table class="widefat">
        <thead>
            <tr>
                <th id="order-id" class="manage-column column-company-name" scope="col">Order</th>
                <!-- <th><strong>Date</strong></th> -->
                <th id="company-name" class="manage-column column-company-name" scope="col">Company Subscription</th>
                <!-- <th><strong>Client</strong></th> -->
                <th id="service" class="manage-column column-service" scope="col">Service</th>
                <th id="eh-label" class="manage-column column-eh-label num" scope="col">Label</th>
                
                <th id="hours" class="manage-column column-hours num" scope="col">Hours Logged</th>
                <th id="labor-cost" class="manage-column column-labor-cost num" scope="col">Labor Cost</th>
                
                <th id="eh-label" class="manage-column column-expense num" scope="col">Expenses</th>
                <th id="expenses" class="manage-column column-expenses num" scope="col">Cost</th>

                <th id="expenses-total" class="manage-column column-expenses-total num" scope="col">Total Expenses</th>
                <th id="revenue" class="manage-column column-revenue num" scope="col">Revenue</th>
                <th id="profit" class="manage-column column-profit num" scope="col">Profit</th>
                <th id="profit-percent" class="manage-column column-profit-percent num" scope="col">Profit %</th>
                <th id="hourly-rate" class="manage-column column-hourly-rate num" scope="col">Effective Hourly Rate</th>
                <!-- <th><strong>Profit</strong></th> -->
            </tr>
        </thead>
        <?php
    }

    public function display_totals() {
        $display_total_expenses = 0;
        foreach($this->total_expenses as $expense){
            
            if( isset($this->divisors[$expense[0]]) ){//&& $this->divisors[$expense[0]] != null ){
                //echo "divisor: "; print_r($this->divisors[$expense[0]]);
                $divisor = $this->divisors[$expense[0]]['value'];
            }else{
                $divisor = 1;
            }
            if($expense[2] == 'all'){
                $display_total_expenses += $expense[1]/$divisor;
            }
            if($expense[2] == 'single'){
                $display_total_expenses += $expense[1];
            }
            //$display_total_expenses += ($expense[1]['all']['amount']/$divisor) + $expense[1]['single']['amount'];
            //$display_total_expenses += ($expense[1]/$divisor);// + $expense[1];
        }
        echo "<tr class='under'>";
        echo "<td class='column-order-id'>Totals</td>";
        echo "<td class='column-company-name'></td>";
        echo "<td class='column-service'></td>";
        echo "<td class='column-eh-label'></td>";
       
        echo "<td class='column-hours'>" . number_format( $this->total_time, 2) . "</td>";
        echo "<td class='column-labor-cost'><span style='color:red'>$" . $this->total_cost . "</span></td>";
        
        echo "<td class='column-expense'></td>";
        echo "<td class='column-expenses'><span style='color:red'>$" . number_format($display_total_expenses,2) . "</span></td>";

        echo "<td class='column-expenses-total'><span style='color:red'>$" . number_format(($this->total_cost + $display_total_expenses), 2) . "</span></td>";
        echo "<td class='column-revenue'><span style='color:green'>$" . number_format($this->total_revenue,2) . "</span></td>";
        
        if( number_format($this->total_revenue - ($this->total_cost + $display_total_expenses), 2) > 0 ){ $book = "green"; }else{ $book = "red"; }
        echo "<td class='column-profit'><span style='color:" . $book . "'>$" . number_format($this->total_revenue - ($this->total_cost + $display_total_expenses), 2) * 100 * 100 . "</td>";
        
        if( $this->total_revenue > 0 ){
            echo "<td class='column-profit-percent'>" . number_format( (($this->total_revenue - ($this->total_cost + $display_total_expenses) ) / $this->total_revenue) * 100 * 100, 2) . "%</td>";
        }else{
            echo "<td class='column-profit-percent'>" . number_format( ($this->total_revenue - ($this->total_cost + $display_total_expenses) ), 2) * 100 * 100 . "%</td>";
        }

        if( $this->total_time != 0){
            echo "<td class='column-hourly-rate'>$" . number_format( ($this->total_revenue - ($this->total_cost + $display_total_expenses) ) / $this->total_time, 2) . "</td>";
        }else{
            echo "<td class='column-hourly-rate'></td>";
        }

        echo "</tr>";
    }


    public function display_items($label, $single_cat, $order, $item_data, $cost, $time, $expense) {
        // echo "<pre>";
        // print_r($order);
        // echo "</pre>";
        //echo "Label: " . $label . "<br />";

        if($this->alternate == ''){
            $this->alternate = 'alternate';
        }else{
            $this->alternate = '';
        }
        
        if( $label != '' && isset($this->divisors[$label]) ){//&& $this->divisors[$label] !== 0 ){
            $divisor = $this->divisors[$label]['value'];
            //echo "divisor: " . $divisor;
        }else{
            $divisor = 1;
        }

        if( $this->company_filter != '0 Companies' && ($label == 'All' || $label == 'N/A') ){
            $divisor = $this->get_companies_represented();
        }

        $display_total_expenses = 0;
        if( isset($expense['amount']) && $expense['amount'] != 0 ){
            $display_total_expenses += $expense['amount']/$divisor;
        }
        if(isset($expense['single']) && $expense['single'] != 0 ){
            $display_total_expenses += $expense['single'];
        }
        
        //echo "total: " . $expense['amount'] . ", /" . $divisor . "= " . $display_total_expenses . "<br />";

        echo "<tr class='" . $this->alternate . "'>";
        if(isset($order->id) && gettype($order->id)!== 'array'){
            echo "<td class='column-order-id'><a href='/wp-admin/post.php?post=" . $order->id . "&action=edit'>" . $order->id . "</a></td>";
        }else{
            echo "<td class='column-order-id'></td>";
        }
        echo "<td class='column-company-name'>" . $order->company . "</td>";
        echo "<td class='column-service'>" . strip_tags($single_cat);
        if($divisor > 1 && $single_cat != ''){ echo " (" . $divisor . ")"; }
        echo "</td>";
        //if( $single_cat == $label || $single_cat == 'Monthly' || $single_cat == 'Yearly' ){ $label = ''; }
        echo "<td class='column-eh-label'>" . $label . "</td>";
        
        echo "<td class='column-hours'>" . number_format($time, 2) . "</td>";
        echo "<td class='column-labor-cost'><span style='color:red'>$" . number_format($cost, 2) . "</span></td>";

        echo "<td class='column-expense'>";
        if(isset($expense['description'])){ echo $expense['description']; }
        echo "</td>";
        echo "<td class='column-expenses'><span style='color:red'>$" . number_format($display_total_expenses, 2) . "</span></td>";

        echo "<td class='column-expenses-total'><span style='color:red'>$" . number_format(($cost + $display_total_expenses), 2) . "</span></td>";
        if( isset($item_data) ){
            if( number_format($item_data['total'], 2) > 0 ){ $booka = "green"; }else{ $booka = "red"; }
            echo "<td class='column-revenue'><span style='color:" . $booka . "'>$" . number_format($item_data['total'], 2) . "</span></td>";
            
            if( ( $item_data['total'] - $cost ) > 0 ){ $book2 = "green"; }else{ $book2 = "red"; }
            echo "<td class='column-profit'><span style='color:" . $book2 . "'>$" . number_format($item_data['total'] - ($cost + $display_total_expenses), 2) * 100 * 100 . "</td>";
            
            if($item_data['total'] != 0){
                echo "<td class='column-profit-percent'>" . sprintf("%.2f%%", (($item_data['total'] - ($cost + $display_total_expenses))/$item_data['total']) * 100 * 100) . "</td>";
            }else{
                echo "<td class='column-profit-percent'>0</td>";
            }

            if($time != 0){
                echo "<td class='column-hourly-rate'>$" . number_format( ($item_data['total'] - ($cost + $display_total_expenses)) / $time, 2) . "</td>";
            }else{
                echo "<td class='column-hourly-rate'></td>";
            }
        }else{
            echo "<td class='column-revenue'><span>$0</span></td>";
            echo "<td class='column-profit'><span style='color:red'>$" . number_format(-$cost, 2) * 100 * 100 . "</span></td>";
            echo "<td class='column-profit-percent'><span style='color:red'>-100%</span></td>";
            echo "<td class='column-hourly-rate'></td>";
        }

        echo "</tr>";
    }

    public function output_table_close(){
        echo "</tbody></table>";
    }
}