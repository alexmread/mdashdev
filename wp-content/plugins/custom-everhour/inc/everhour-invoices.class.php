<?php

class Everhour_Invoices {

    var $invoices_status = "";
    var $api_key;
    var $invoices;

    function set_api_key($api_key){
        $this->api_key = $api_key;
    }

    function get_invoices_status(){
        return $this->invoices_status;
    }

    function get_invoices($recache = false){
        //echo "RECACHE received: " . $recache . "<br />";
        /* WP Transient Cache */
        $this->invoices = get_transient( 'everhour_invoices' );

        if( $recache == true ){
            $reset_command = true;
        }else{
            $reset_command = false;
        }

        if( false === $this->invoices || $reset_command ) {

            //$this->invoices_status = "Reloaded Invoices from Everhour";
            $this->invoices_status = "";

            $request_invoices = wp_remote_get( 'https://api.everhour.com/invoices' ,
                array( 'timeout' => 10,
                    'headers' => array( 'X-Api-Key' => $this->api_key ) 
                )
            );

            if( is_wp_error( $request_invoices ) ) {
                $error_string = $request_invoices->get_error_message();
                echo '<div id="message" class="error"><p>' . $error_string . '</p></div>';
            }

            $invoicesObj = wp_remote_retrieve_body( $request_invoices );
            $invoices_data = json_decode( $invoicesObj );
            set_transient( 'everhour_invoices', $invoices_data, 1 * DAY_IN_SECONDS );
            $this->invoices = $invoices_data;
        } else {
            $this->invoices_status = "(<a href='?page=everhour&tab=invoice&recache-invoices'>Reload</a>)"; 
        }
    }


    function lookup_invoice(){
        /* Lookup Woocommerce Orders for/by Company Name */
        // 'wc-on-hold', 'wc-completed', 'wc-cancelled','wc-refunded','wc-failed' 
        // $args = array(
        //     'orderby' => 'modified',
        //     'order' => 'DESC',
        //     'status' => array('wc-processing', 'wc-pending'),
        //     'limit' => 30,
        //     'billing_company' => $project->name
        // );
        // $orders = wc_get_orders( $args );
        // foreach($orders as $order){         
        //     //echo "<li>ID: " . $order->id . ", " . $order->status . ", Total: " . $order->total . ", customer ID: " . $order->customer_id . ", Date Modified: " . $order->date_modified . "</li>";
        //     //print_r($order);

        //     $an_order = array(
        //         'ID'        => $order->id,
        //         'order'     =>  $order->id,
        //         'company'     => $project->name,
        //         'status'    => $order->status,
        //         'total'  => $order->total,
        //         'date'  =>  $order->date_modified->format ('Y-m-d')
        //     );
        //     array_push($this->example_data, $an_order);

        // };
    }


    function create ($ev_client_id, $ev_project_id){
        $args = array(
            'timeout' => 10,
            'headers' => array(
                'X-Api-Key' => $this->api_key,
                'Content-Type' => 'application/json'
            ),
            'body'  => json_encode(
                array(
                    "includeExpenses" => false,
                    "includeTime" => false
                ))
        );
        $create_invoice = wp_remote_post( 'https://api.everhour.com/clients/' . $ev_client_id . '/invoices' , $args );
        if( is_wp_error( $create_invoice ) ) {
            $error_string = $create_invoice->get_error_message();
            return '<div id="message" class="error"><p>' . $error_string . '</p></div>';
        }
        
        $invoiceObj = wp_remote_retrieve_body( $create_invoice );
        $invoices_data = json_decode( $invoiceObj );
        
        return $invoices_data->id;
    }


    function update ($ev_invoice, $order){
        
        $numItems = count($order->get_items());
        $i = 0;
        $items_obj = array();
        //print_r($order->get_items());
        foreach ( $order->get_items() as  $item_key => $item_values ) {
            $item_data = $item_values->get_data();
            $object = new stdClass();
            $object->name = $item_data['name'];
            $object->listAmount = (int)$item_data['total'];
            $items_obj[] = $object;
        }
        // echo "<pre>";
        // print_r($items_obj);
        // echo "</pre>";

        // $args = array(
        //     'headers' => array(
        //         'X-Api-Key' => $this->api_key,
        //         'Content-Type' => 'application/json',
        //         'method' => 'PUT'
        //     ),
        //     'body' => json_encode(array(
        //         "issueDate" => $order->get_date_created()->format('Y-m-d'),
        //         "reference" => (string)$order->get_id(),
        //         "invoiceItems" => $items_obj
        //     ))
        // );

        // $update_invoice = wp_remote_request( 'https://api.everhour.com/invoices/' . $ev_invoice , $args );
        // if( is_wp_error( $update_invoice ) ) {
        //     $error_string = $update_invoice->get_error_message();
        //     return '<div id="message" class="error"><p>' . $error_string . '</p></div>';
        // }
        
        // $update_invoice_obj = wp_remote_retrieve_body( $update_invoice );
        // $update_invoice_data = json_decode( $update_invoice_obj );
        
        //return $update_invoice_data;

        if($order->get_date_created() != null){
            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.everhour.com/invoices/' . $ev_invoice,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS =>'{"issueDate":"' . $order->get_date_created()->format('Y-m-d') . '","reference":' . $order->get_id() . ',"invoiceItems":' . json_encode($items_obj) . '}',
            CURLOPT_HTTPHEADER => array(
                'X-Api-Key: 7c66-53f3-f5e94a-274171-9357471c',
                'Content-Type: application/json'
            ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            return $response['valid'];
        }
    }


    function delete ($everhour_invoice_id){
        $delete_invoice = wp_remote_request( 'https://api.everhour.com/invoices/' . $everhour_invoice_id,
            array(
                'timeout' => 10,
                'headers' => array(
                    'X-Api-Key' => $this->api_key
                ),
                'method' => 'DELETE'
            )
        );

        if( is_wp_error( $delete_invoice ) ) {
            $error_string = $delete_invoice->get_error_message();
            return '<div id="message" class="error"><p>' . $error_string . '</p></div>';
        }

        $deleteObj = wp_remote_retrieve_body( $delete_invoice );
        $delete_data = json_decode( $deleteObj );
        return $delete_data;
    }

    function display_invoices(){
        if(false == get_transient( 'everhour_invoices' )){
            $invoices = $this->get_invoices();
        }else{
            $invoices = get_transient( 'everhour_invoices' );
        }
                //print_r($projects_data);
        ?>
        <table class="widefat fixed" cellspacing="0">
            <thead>
                <tr>
                    <th id="e_order_id" class="manage-column column-e_order_id" scope="col">EH Invoice ID</th>
                    <th id="e_company" class="manage-column column-e_company" scope="col">Client</th>
                    <th id="e_issue_date" class="manage-column column-e_issue_date" scope="col">Issued</th>
                    <th id="e_status" class="manage-column column-e_status" scope="col">Status</th>
                    <th id="e_items" class="manage-column column-e_items" scope="col">Items</th>
                    <th id="e_total" class="manage-column column-e_total" scope="col">Total</th>
                    <th id="e_reference" class="manage-column column-e_reference" scope="col">Woocommerce</th>
                    <th id="e_delete" class="manage-column column-e_delete" scope="col"></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th class="manage-column column-e_order_id" scope="col"></th>
                    <th class="manage-column column-e_company" scope="col"></th>
                    <th class="manage-column column-e_issue_date" scope="col"></th>
                    <th class="manage-column column-e_status" scope="col"></th>
                    <th class="manage-column column-e_items" scope="col"></th>
                    <th class="manage-column column-e_total" scope="col"></th>
                    <th class="manage-column column-e_reference" scope="col"></th>
                    <th class="manage-column column-e_delete" scope="col"></th>
                </tr>
            </tfoot>

            <tbody>        
            <?php       
                if($invoices) {
                    foreach($invoices as $invoice){
                        $line_items = $invoice->invoiceItems;
                        //print_r($line_items);
                        $line_items_output = "<ul>";
                        foreach($line_items as $line_item){
                            // echo "<pre>";
                            // print_r($line_item);
                            if($line_item != null && $line_item->listAmount != null){
                                $line_items_output .= "<li>" . $line_item->name . ": $" . number_format($line_item->listAmount) . "</li>";
                            }
                        }
                        $line_items_output .= "</ul>";

                        if(isset($invoice->reference)){
                            $reference = "<a href='post.php?post=" . $invoice->reference . "&action=edit'>" . $invoice->reference . "</a>";
                        }else{
                            $reference = '';
                        }
                        if($invoice->totalAmount != null){
                            $total_amount_checked = $invoice->totalAmount;
                        }else{ $total_amount_checked = 0; }
                        ?>
                        <tr class="alternate">
                            <td class="column-e_order_id" scope="row"><?php echo $invoice->id; ?></td>
                            <td class="column-e_company" scope="row"><?php echo $invoice->client->name; ?></td>
                            <td class="column-e_issue_date" scope="row"><?php echo $invoice->issueDate; ?></td>
                            <td class="column-e_status" scope="row"><?php echo $invoice->status; ?></td>
                            <td class="column-e_items" scope="row"><?php echo $line_items_output; ?></td>
                            <td class="column-e_total" scope="row">$<?php echo number_format($total_amount_checked); ?></td>
                            <td class="column-e_reference" scope="row"><?php echo $reference; ?></td>
                            <td class="column-e_delete" scope="row"><a href="?page=everhour&tab=invoice&delete=<?php echo $invoice->id; ?>&recache-invoices">delete</a></td>
                        </tr>
                        <?php
                    }
                }
            ?>
            </tbody>
        </table><?php
    }

    function get_report_all_date_range($start, $end){
        $request_report = wp_remote_get( 'https://api.everhour.com/dashboards/projects?date_gte=' . $start . '&date_lte=' . $end,
            array( 'timeout' => 10,
                'headers' => array( 'X-Api-Key' => $this->api_key ) 
            )
        );

        if( is_wp_error( $request_report ) ) {
            $error_string = $request_report->get_error_message();
            echo '<div id="message" class="error"><p>' . $error_string . '</p></div>';
        }

        $reportObj = wp_remote_retrieve_body( $request_report );
        return $report_data = json_decode( $reportObj );
        //print_r($report_data);
        //match either projectName or clientName
        //set_transient( 'everhour_report', $invoices_data, 1 * DAY_IN_SECONDS );
    }
}