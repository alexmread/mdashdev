<?php

class Everhour_Users {

    var $api_key;
    var $users;   
    
    function set_api_key($api_key){
        $this->api_key = $api_key;
    }  
    
    /*
    *   Get All Everhour Users
    */
    function getUserCost($user_id, $hours){
        //echo "<!-- Matching User " . $user_id . " executed -->";

        $this->users = get_transient( 'everhour_users' );

        if( false != $this->users ){
            //echo "<!-- Using Everhour Users cached, id: " . $user_id . " -->";
        }else{
            //echo "<!-- Setting Everhour Users executed Once -->";

            $request_users = wp_remote_get( 'https://api.everhour.com/team/users' ,
                    array( 'timeout' => 10,
                        'headers' => array( 'X-Api-Key' => $this->api_key ) 
                    )
                );
            
            if( is_wp_error( $request_users ) ) {
                $error_string = $request_users->get_error_message();
                echo '<div id="message" class="error"><p>' . $error_string . '</p></div>';
            }

            $users = wp_remote_retrieve_body( $request_users );
            $users_data = json_decode( $users );
            set_transient( 'everhour_users', $users_data, 1 * DAY_IN_SECONDS );
            $this->users = $users_data;
        }

        foreach($this->users as $user_single){ 
            if(strpos($user_single->id, $user_id) !== false){
                //echo "Everhour User: " . $user_single->name . "<br />";
                $user_hourly = $user_single->cost;
                //echo "Everhour User Cost: $" . $user_hourly . "<br />";
                $total_cost_this_project_this_resource = ($user_single->cost/100 * $hours/60/60);
                //echo "Cost entry: $" . $total_cost_this_project_this_resource . "<br />";
                return $total_cost_this_project_this_resource;
            } else{
                // No User Match
            }
        }
    }
}