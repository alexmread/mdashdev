<?php

class Woocommerce_Functions {

    var $matching_order;
    var $company;
    var $order_date;

    /* used by New Reporting approach only */
    function get_all_product_categories_for_wc_admin(){

        $args = array(
                'taxonomy'     => 'product_cat',
                'orderby'      => 'name',
        );
        $all_categories = get_categories( $args );
        
        $cats = array();
        array_push($cats,
            array(
                'label' => 'Product Categories',
                'value' => 'Product Categories',
            )
        );

        foreach ($all_categories as $cat) {
            array_push($cats,
                array(
                    'label' => $cat->name,
                    'value' => $cat->name
                )
            );
        }

        return $cats;
    }

    function get_all_product_categories($optional_category = false){
        $taxonomy     = 'product_cat';
        $orderby      = 'name';  
        $show_count   = 0;      // 1 for yes, 0 for no
        $pad_counts   = 0;      // 1 for yes, 0 for no
        $hierarchical = 1;      // 1 for yes, 0 for no  
        $title        = '';  
        $empty        = 0;

        $args = array(
                'taxonomy'     => $taxonomy,
                'orderby'      => $orderby,
                'show_count'   => $show_count,
                'pad_counts'   => $pad_counts,
                'hierarchical' => $hierarchical,
                'title_li'     => $title,
                'hide_empty'   => $empty
        );
        $all_categories = get_categories( $args );
        echo "<select name='category' value='custom'><option>Product Categories</option>";
        foreach ($all_categories as $cat) {
            if($cat->category_parent == 0) {
                $category_id = $cat->term_id;       
                //echo '<br /><a href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a>';
                if($optional_category == $cat->name){
                    $category_selection = 'selected';
                }else{
                    $category_selection = '';
                }
                //if( isset($cat->name) ){
                    echo "<option " . $category_selection . ">" . $cat->name . "</option>";
                //}

                $args2 = array(
                        'taxonomy'     => $taxonomy,
                        'child_of'     => 0,
                        'parent'       => $category_id,
                        'orderby'      => $orderby,
                        'show_count'   => $show_count,
                        'pad_counts'   => $pad_counts,
                        'hierarchical' => $hierarchical,
                        'title_li'     => $title,
                        'hide_empty'   => $empty
                );
                $sub_cats = get_categories( $args2 );
                if($sub_cats) {
                    foreach($sub_cats as $sub_category) {
                        //echo  $sub_category->name ;
                    }   
                }
            }       
        }
        echo "</select>";
    }

    function get_company(){
        return $this->company;
    }

    function get_order_date(){
        return $this->order_date;
    }

    function get_matching_order(){
        return $this->matching_order;
    }

    function load_woocommerce_order( $orderId ){
        $order = wc_get_order( $orderId );
        if(!$order){ $this->matching_order = false; return; }

        $this->matching_order = $order;

        $order_data = $order->get_data();

        $this->company = $order_data['billing']['company'];
        if($order->get_date_paid() != null){
            $this->order_date = $order->get_date_paid()->format('Y-m-d');
        }else{
            $this->order_date = '';
        }

        //print_r($order);  
        $user = $order->get_user();
        if( $user ){
            //
        }
        ?>
        <table class="widefat fixed" cellspacing="0">
            <thead>
                <tr>
                    <th id="w_order_id" class="manage-column column-w_order_id" scope="col">Woocommerce Order ID</th>
                    <th id="w_company" class="manage-column column-w_company" scope="col">Company</th>
                    <th id="w_status" class="manage-column column-w_status" scope="col">Status</th>
                    <th id="w_items" class="manage-column column-w_items" scope="col">Items</th>
                    <th id="w_date_created" class="manage-column column-w_date_created" scope="col">Date Completed</th>
                    <th id="w_total" class="manage-column column-w_total" scope="col">Total</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th class="manage-column column-w_order_id" scope="col"></th>
                    <th class="manage-column column-w_company" scope="col"></th>
                    <th class="manage-column column-w_status" scope="col"></th>
                    <th class="manage-column column-w_items" scope="col"></th>
                    <th class="manage-column column-w_date_created" scope="col"></th>
                    <th class="manage-column column-w_total" scope="col"></th>
                </tr>
            </tfoot>

            <tbody>
                <tr class="alternate">
                    <th class="column-w_order_id" scope="row"><?php echo $order->get_id(); ?></th>
                    <td class="column-w_company"><?php echo $order_data['billing']['company']; ?></td>
                    <td class="column-w_status"><?php echo $order->get_status(); ?></td>
                    <td class="column-w_items">
                        <?php
                            foreach ( $order->get_items() as  $item_key => $item_values ) {
                                $item_data = $item_values->get_data();
                                echo $product_name = $item_data['name'] . ": $" . $line_total = $item_data['total'] . "<br />";
                            }
                        ?>
                    </td>
                    <td class="column-w_date_created"><?php echo $this->order_date; ?></td>
                    <td class="column-w_date_created"><?php echo $order->get_formatted_order_total(); ?></td>
                </tr>
            </tbody>
        </table>
        <?php
    }
}