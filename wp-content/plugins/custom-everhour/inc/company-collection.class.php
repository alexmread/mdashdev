<?php

class Company_Collection {

    var $companies = array(); //this is now full of eh ids not company names
    var $company_counter = 0;
    
    function check_company($companyName, $everhourID, $stopTheCounter = false){
        //echo "Check Company invoked: " . $companyName . ", count it: " . $stopTheCounter . "<br />";
        
        // if (gettype($companyName) != 'string'){
        //     echo "Offending Type: " . gettype($companyName) . "<br />";

        //     echo "<pre>";
        //     print_r($companyName);
        //     echo "</pre>" . "<br />";
        // }
        $companyName = strtolower($companyName);

        //if ( array_key_exists($companyName, $this->companies) ){
        if ( array_key_exists($everhourID, $this->companies) ){
            //echo "Company exists: " . $companyName . "<br />";
            return $this->companies[$everhourID];
        }else{
            //echo "Company created: " . $companyName . "<br />";
            $newCompany = new Company($everhourID, $companyName);
            $this->companies[$everhourID] = $newCompany;
            // only count companies invoiced, not companies created by EH or Expenses
            if($stopTheCounter == true){
                $this->company_counter++;
            }
            return $newCompany;
        }
    }   

    function output_companies(){
        echo "<pre>";
        print_r($this->companies);
        echo "</pre>";
    }
}