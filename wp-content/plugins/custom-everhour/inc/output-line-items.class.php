<?php
/*
*   This filename is a misnomer now,
*   as it outputs the lines for the whole report,
*   Not just Everhour
*/

class Output_Line_Items {
    var $alternate = '';  

    var $company_collection = array();
    var $company_filter = null;
    var $category_filter = null;

    var $total_time = 0;
    var $total_cost = 0;
    var $total_expenses = 0;
    var $total_revenue = 0;
    var $total_profit = 0;
    var $total_percent = 0;
    var $total_rate = 0;

    var $divisors = array();

    public function set_company_dropdown($setTo){
        //echo "set company dropdown: " . $setTo . "<br />";
        $this->company_filter = $setTo;
    }

    public function set_category_dropdown($setTo){
        $this->category_filter = $setTo;
    }


 
    public function submit_collection($company_collection, $divisors, $totals_only = false, $products_only = false){
        $this->company_collection = $company_collection;
        $this->divisors = $divisors;
        // echo "<pre>";
        // print_r($divisors);
        // echo "</pre>";
        //exit;

        $this->total_revenue = 0;
        //echo $company_collection->company_counter . " Companies Invoiced<br />"; 
        //$company_collection->companies['seadon llc']->line_items['search engine optimization']->revenue
        foreach ($this->company_collection->companies as $company_key => $company) {
            
            //echo $company_key);
            //echo "<pre>";print_r($company);echo "</pre>";
            // line items looks like this
            /*
            [totals] => Line_Item Object
                (
                    [revenue] => 0
                    [cost] => 88
                    [time] => 2.7666666666667
                    [order_id] => Array
                        (
                        )

                    [product_ids] => Array
                        (
                        )

                    [expense] => 284.5
                    [expense_description] => Array
                        (
                            [0] => LinksforPros - ROIHigh: $162
                            [1] => SMR Digital Limited | RhinoRank: $122.5
                        )

                    [occurrences] => 
                )
            */

            // if ( $company->eh_id == 123456789 ){
            //     continue;
            // }
            // Totals is selected, so we don't want to go through line items, just the total 
            if ( $totals_only == true ) {
                //echo "Totals Only<br />";
                //echo "<pre>"; print_r($company); echo "</pre>";
                if(isset($company->line_items['totals']) && $company->eh_id == 123456789){
                    //$this->display_row($company->name, 'totals', $company->line_items['totals']);
                    $this->total_revenue += $company->line_items['totals']->revenue;
                    $this->total_time += $company->line_items['totals']->time;
                    $this->total_cost += $company->line_items['totals']->cost;
                    $this->total_expenses += $company->line_items['totals']->expense;;
                }
                //$this->display_row($company->name, 'totals', $company->data);
            }else{
            
            //echo "<pre>";print_r($company);echo "</pre>";

            foreach($company->line_items as $category_key => $item){

                //echo "category_key: " . $category_key . "<br />";
                if ( $totals_only == false && $category_key != 'totals' && $company->eh_id != 123456789 ){


                // if ( array_key_exists( $category_key, $divisors ) ){
                //     $divisor = $divisors[$category_key]['count'];
                // }else{
                //     $divisor = 1;
                // }
                
                // No Filter, all Companies & Categories
                if ( $this->company_filter == '0 Companies' && $this->category_filter == 'product categories' ){
                    $this->total_revenue += $item->revenue;
                    $this->total_time += $item->time;
                    $this->total_cost += $item->cost;
                    $this->total_expenses += $item->expense;
                }
                else{
                    // Category Dropdown Filter
                    if ( $this->company_filter == '0 Companies' && $this->category_filter != 'product categories'){
                        if ( $category_key == $this->category_filter ){
                            $this->total_revenue += $item->revenue;
                            $this->total_time += $item->time;
                            $this->total_cost += $item->cost;
                            $this->total_expenses += $item->expense;
                        }
                    }else{
                        // Company Filter
                        if ( $this->company_filter != '0 Companies' && $this->category_filter == 'product categories' ){
                            
                            //echo $item->revenue . "<br />";
                            //if ( $company_key == $this->company_filter ){
                            if ( $company->eh_id == $this->company_filter ){
                                //echo "company eh_id: " . $company->eh_id . ", company filter: " . $this->company_filter . "<br />";
                                $this->total_revenue += $item->revenue;
                                $this->total_time += $item->time;
                                $this->total_cost += $item->cost;
                                $this->total_expenses += $item->expense;
                            }
                            if ( $company_key == 'all' ) {
                                $this->total_expenses += $item->expense/$company_collection->company_counter;
                            }
                            
                        }else{
                            // Both Filters
                            if ( $this->company_filter != '0 Companies' && $this->category_filter != 'product categories' ){
                                //if ( $company_key == $this->company_filter && $category_key == $this->category_filter ){
                                if ( $company->eh_id == $this->company_filter && $category_key == $this->category_filter ){
                                    $this->total_revenue += $item->revenue;
                                    $this->total_time += $item->time;
                                    $this->total_cost += $item->cost;
                                    $this->total_expenses += $item->expense;
                                }
                            }
                        }
                    }
                    
                }
                }
                
            }
            }
        }
    }


    public function output($company_collection, $totals_only = false, $products_only = false ){
        $this->company_collection = $company_collection;

        echo "Needs their EH id:";
        foreach( $company_collection->companies as $company ){
            if(!$company->eh_id){
                // echo "<pre>";
                // print_r($company);
                // echo "</p>";
                echo $company->name;
            }
        }
        

        $this->output_table_styles();
        $this->output_table_head();

        $this->display_totals();

        //echo "Company Filter: " . $this->company_filter . "<br />";
        //echo "Category Filter: " . $this->category_filter . "<br />";

        foreach($this->company_collection->companies as $company_key => $company){
            
            //echo "<pre>";print_r($company);echo "</pre>";
            //echo "totals_only: " . $totals_only . "<br />";
            //echo "products_only: " . $products_only . "<br />";
            //echo $company->name . ": " . $company->eh_id . "<br />";
            //echo gettype($company->eh_id) . "<br />";

            // Products Only is Not checked, Totals Only is checked (01)
            if ( $totals_only == true ) {
                //echo "<pre>"; print_r($company); echo "</pre>";
                if(isset($company->line_items['totals']) && $company->eh_id != 123456789){
                    $this->display_row($company->name, 'totals', $company->line_items['totals']);
                }
                //$this->display_row($company->name, 'totals', $company->data);
            }else{
                //echo "shouldn't even get here in this scenario!<br />";
                foreach($company->line_items as $category_key => $data){
                    // echo 'category_key: ' . $category_key . "<br />";
                    // echo 'company_filter: ' . $this->company_filter . "<br />";
                    // echo 'category_filter: ' . $this->category_filter . "<br />";

                    // Products Only is checked, Total Only is checked, just act as though they both weren't (11)

                    // Products Only is checked, Totals Only is Not checked, We don't want to show any Companies but the aggregate Company (10)
                    if ( $products_only == true && $company->eh_id != 123456789 && $totals_only == false ){
                        continue;
                    }

                    // Products Only is Not checked, Totals Only is Not checked, so don't show the aggregate Company (00)
                    if ( $products_only == false && $company->eh_id == 123456789 && $totals_only == false ){
                        continue;
                    }
                    // Normal report, Neither checked, don't show Totals Line Item
                    if ( $category_key == 'totals' ){
                        continue;
                    }


                    // echo "<pre>";
                    // print_r($data);
                    // echo "</pre>";
                    if ( $this->company_filter == '0 Companies' && $this->category_filter == 'product categories' ){
                        //echo "No Filters Applied<br />";
                        $this->display_row($company->name, $category_key, $data);
                    }else{
                        if ( $this->company_filter == '0 Companies' && $this->category_filter != 'product categories' && $this->category_filter != null ){
                            //echo "Category Filter Applied<br />";
                            if ( $category_key == $this->category_filter ){
                                $this->display_row($company->name, $category_key, $data);
                            }
                        }else{
                            if ( $this->company_filter != '0 Companies' && $this->company_filter != null && $this->category_filter == 'product categories' ){
                                
                                //echo "Company Filter Applied, company id: " . $company_key . ", filter: " . $this->company_filter . "<br />";
                                if ( $company->eh_id == $this->company_filter ){
                                //if ( $company_key == $this->company_filter){
                                    //echo "everhour id match<br />";
                                    $this->display_row($company->name, $category_key, $data);
                                }
                                if ( $company->name == 'all' ){
                                    $data->expense = $data->expense/$company_collection->company_counter;
                                    foreach($data->expense_description as $single_key => $single_description){
                                        $data->expense_description[$single_key] = $single_description . '/' . $company_collection->company_counter;
                                    }
                                    $this->display_row($company->name, $category_key, $data);
                                }
                            }else{
                                if ( $this->company_filter != '0 Companies' && $this->company_filter != null && $this->category_filter != 'product categories' && $this->category_filter != null ){
                                    //echo "Company & Category Filters Applied<br />";
                                    if ( $company->name == $this->company_filter && $category_key == $this->category_filter ){
                                        $this->display_row($company->name, $category_key, $data);
                                    }
                                }
                            }
                        }
                        
                    }
                }
            }
        }

        $this->output_table_close();

        // echo "<pre>";
        // print_r($this->divisors);
        // echo "</pre>";
    }

    public function output_table_styles(){
        ?>
        <style> 
            /* td { 
                white-space: nowrap; 
            }  */
            .widefat td{
                padding: 2px 5px !important;
                /* border-bottom: 1px solid black; */
            }
            .under td{
                border-bottom: 1px solid #ccd0d4;
                background-color: #e6e6e6;
            }
            .noUnder{
                border-bottom: none !important;
            }
            td.purpleL a {
                color: purple;
            }
            .widefat tr{
                border-bottom: 1px solid black;
            }
        </style>
        <?php
    }

    public function output_table_head(){
        ?>
        <table class="widefat">
        <thead>
            <tr>
                <!-- Order Ids, column 1 -->
                <th id="order-id" class="manage-column column-order-id" scope="col">Order</th> 
                
                <!-- Company, column 2 -->
                <th id="company-name" class="manage-column column-company-name" scope="col">Company</th>
                
                <!-- Category, column 3 -->
                <th id="service" class="manage-column column-service" scope="col">Service</th>
                <!-- <th id="eh-label" class="manage-column column-eh-label num" scope="col">Label</th> -->
                
                <!-- Hours, column 4 -->
                <th id="hours" class="manage-column column-hours num" scope="col">Hours</th>
                
                <!-- EH Cost, column 5 -->
                <!-- <th id="labor-cost" class="manage-column column-labor-cost num" scope="col">Cost</th> -->
                <th id="labor-cost" class="manage-column column-labor-cost num" scope="col">Labor</th>
                
                <!-- Expenses Description, column 6 -->
                <th id="eh-label" class="manage-column column-expense num" scope="col">Expenses</th>
                
                <!-- Expenses Cost, column 7 -->
                <th id="expenses" class="manage-column column-expenses num" scope="col">Cost</th>

                <!-- Cost Total, column 8 -->
                <th id="expenses-total" class="manage-column column-expenses-total num" scope="col">Total Cost</th>

                <!-- Revenue, column 9 -->
                <th id="revenue" class="manage-column column-revenue num" scope="col">Revenue</th>
                
                <!-- Profit, column 10 -->
                <th id="profit" class="manage-column column-profit num" scope="col">Profit</th>
                
                <!-- Profit percent, column 11 -->
                <th id="profit-percent" class="manage-column column-profit-percent num" scope="col">Profit %</th>
                
                <!-- Effective Hourly, column 12 -->
                <th id="hourly-rate" class="manage-column column-hourly-rate num" scope="col">Effective Hourly Rate</th>
            </tr>
        </thead>
        <?php
    }

    public function display_totals() {
        // $display_total_expenses = 0;
        // foreach($this->total_expenses as $expense){
            
        //     if( isset($this->divisors[$expense]) ){//&& $this->divisors[$expense[0]] != null ){
        //         //echo "divisor: "; print_r($this->divisors[$expense[0]]);
        //         $divisor = $this->divisors[$expense[0]]['value'];
        //     }else{
        //         $divisor = 1;
        //     }
        //     if($expense[2] == 'all'){
        //         $display_total_expenses += $expense[1]/$divisor;
        //     }
        //     if($expense[2] == 'single'){
        //         $display_total_expenses += $expense[1];
        //     }
        //     //$display_total_expenses += ($expense[1]['all']['amount']/$divisor) + $expense[1]['single']['amount'];
        //     //$display_total_expenses += ($expense[1]/$divisor);// + $expense[1];
        // }
        echo "<tr class='under'>";

            /* 1, 'Totals' */
            echo "<td class='column-order-id'>Totals</td>";

            /* 2, empty */
            echo "<td class='column-company-name'></td>";

            /* 3, empty */
            echo "<td class='column-service'></td>";
            // echo "<td class='column-eh-label'></td>";

            /* Hours, column 4 */
            echo "<td class='column-hours'>" . number_format( $this->total_time, 2) . "</td>";

            /* EH Cost, column 5 */
            echo "<td class='column-labor-cost'><span style='color:red'>$" . number_format( $this->total_cost, 2) . "</span></td>";
            
            /* Expenses Description, column 6 */
            echo "<td class='column-expense'></td>";

            /* Expenses Cost, column 7 */
            echo "<td class='column-expenses'><span style='color:red'>$" . number_format($this->total_expenses,2) . "</span></td>";

            /* Cost Total, column 8 */
            echo "<td class='column-expenses-total'><span style='color:red'>$" . number_format(($this->total_cost + $this->total_expenses), 2) . "</span></td>";
            
            /* Revenue, column 9 */
            echo "<td class='column-revenue'><span style='color:green'>$" . number_format($this->total_revenue,2) . "</span></td>";
            
            /* Profit, column 10 */
            echo "<td class='column-profit'>";
                if( number_format($this->total_revenue - ($this->total_cost + $this->total_expenses), 2) > 0 ){ $book = "green"; }else{ $book = "red"; }
                echo "<span style='color:" . $book . "'>$" . number_format($this->total_revenue - ($this->total_cost + $this->total_expenses), 2) . "</span>";
            echo "</td>";
            
            echo "<td class='column-profit-percent'>";
            /* Profit percent, column 11 */
            if( $this->total_revenue > 0 ){
                echo number_format( (($this->total_revenue - ($this->total_cost + $this->total_expenses) ) / $this->total_revenue)*100) . "%";
            }else{
                echo number_format( ($this->total_revenue - ($this->total_cost + $this->total_expenses) )) . "%";
            }
            echo "</td>";

            /* Effective Hourly, column 12 */
            echo "<td class='column-hourly-rate'>$";
                if( $this->total_time != 0){
                    echo number_format( ($this->total_revenue - ($this->total_cost + $this->total_expenses) ) / $this->total_time, 2);
                }
            echo "</td>";
        echo "</tr>";
    }


    public function display_row($company, $category, $data) {

        // echo "company: " . $company;
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";

        if($this->alternate == ''){
            $this->alternate = 'alternate';
        }else{
            $this->alternate = '';
        }
        
        if(
            $category != null &&
            $category != 'n/a' &&
            $category != 'all' &&
            isset($this->divisors[$category])
        ){
            $divisor = $this->divisors[$category]['count'];
            if($this->divisors[$category]['revenue'] != 0){
                $divisor_percentage = number_format(($data->revenue/$this->divisors[$category]['revenue'])* 100, 0) . '%';
            }else{
                $divisor_percentage = '0%';
            }
            //echo "divisor: " . $divisor . "<br/ >";
        }else{
            $divisor = 1;
        }


        echo "<tr class='" . $this->alternate . "'>";

            /* Order IDs, column 1 */
            echo "<td class='column-order-id'>";
                if(isset($data->order_id)){
                foreach($data->order_id as $single_invoice){
                    echo "<a href='/wp-admin/post.php?post=" . $single_invoice . "&action=edit'>" . $single_invoice . "</a> ";
                }
                }
            echo "</td>";

            /* Company, column 2 */
            echo "<td class='column-company-name'>" . ucwords($company) . "</td>";

            /* Category, column 3 */
            echo "<td class='column-service'>" . ucwords($category);
                if($divisor > 1){
                    echo " (" . $divisor . ", " . $divisor_percentage . ")";
                }
            echo "</td>";
            
            /* Hours, column 4 */
            echo "<td class='column-hours'>" . number_format($data->time, 2) . "</td>";

            /* EH Cost, column 5 */
            echo "<td class='column-labor-cost'><span style='color:red'>$" . number_format($data->cost, 2) . "</span></td>";

            /* Expenses Description, column 6 */
            echo "<td class='column-expense'>";
                if(isset($data->expense_description) && sizeOf($data->expense_description) > 0){
                    echo '<div class=\'display_descriptions_full\' onclick="jQuery(this).next(\'.descriptions_full\').slideToggle();">&#9432;</div>';
                    echo "<div style='display:none' class='descriptions_full'>";
                        //$data->expense_description = array_unique($data->expense_description); // this really deceives, there ARE identical but both valid occurrences, if hidden things don't add up correctly.
                        foreach($data->expense_description as $single_description){
                            echo $single_description . "<br/ >";
                        }
                    echo "</div>";
                }
            echo "</td>";

            /* Expenses Cost, column 7 */
            echo "<td class='column-expenses'><span style='color:red'>$" . number_format($data->expense, 2) . "</span></td>";

            /* Cost Total, column 8 */
            echo "<td class='column-expenses-total'><span style='color:red'>$" . number_format(($data->cost + $data->expense), 2) . "</span></td>";
            
            /* Revenue, column 9 */
            echo "<td class='column-revenue'>";
                if( number_format($data->revenue, 2) > 0 ){ $booka = "green"; }else{ $booka = "red"; }
                echo "<span style='color:" . $booka . "'>$" . number_format($data->revenue, 2) . "</span>";
            echo "</td>";
            
            /* Profit, column 10 */
            echo "<td class='column-profit'>";
                if( ( $data->revenue - ($data->cost + $data->expense) ) > 0 ){
                    $book2 = "green";
                }else{
                    $book2 = "red";
                }
                echo "<span style='color:" . $book2 . "'>$" . number_format($data->revenue - ($data->cost + $data->expense), 2) . "</span>";
            echo "</td>";
            
            /* Profit percent, column 11 */
            echo "<td class='column-profit-percent'>";
                if($data->revenue != 0){
                 echo number_format((($data->revenue - ($data->cost + $data->expense))/$data->revenue)* 100) . "%";
                }
            echo "</td>";

            /* Effective Hourly, column 12 */
            echo "<td class='column-hourly-rate'>";
                if($data->time != 0){
                    echo "$" . number_format( ($data->revenue - ($data->cost + $data->expense)) / $data->time, 2);
                }
            echo "</td>";

        echo "</tr>";
    }

    public function output_table_close(){
        echo "</tbody></table>";
    }
}