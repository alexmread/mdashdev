<?php

class Everhour_Clients {

    var $client_list_status = "";
    var $api_key;
    var $clients;
    var $client_to_invoice = '';
    var $project_ids;

    function set_api_key($api_key) {
        $this->api_key = $api_key;
    }

    function create_new_client($name){
        //echo "We have the Everhour_Clients Class<br />";

        $new_client = array( 'name' => stripcslashes($name) );

        $request_clients = wp_remote_post( 'https://api.everhour.com/clients' ,
            array(
                'timeout' => 10,
                'headers' => array(
                    'X-Api-Key' => $this->api_key,
                    'Content-Type' => 'application/json',
                ),
                'body' => json_encode($new_client)
            )
        );

        if( is_wp_error( $request_clients ) ){
            return false;
        }

        $clientsObj = wp_remote_retrieve_body( $request_clients );
    
        if( ! empty( $clientsObj ) ) {
            //var_dump($clientsObj);
            /* example return:
            {
                "projects":[],
                "id":6019405,
                "name":"New Client 3",
                "createdAt":"2022-04-11 01:19:15",
                "lineItemMask":"%MEMBER% :: %PROJECT% :: for %PERIOD%",
                "paymentDueDays":3,
                "reference":"",
                "businessDetails":"",
                "invoicePublicNotes":"",
                "excludedLabels":[],
                "status":"active",
                "enableResourcePlanner":false,
                "favorite":false
            }*/
            return $clientsObj;
        }
    }

    function get_client_list_status() {
        return $this->client_list_status;
    }

    function get_client_to_invoice() {
        return $this->client_to_invoice;
    }

    function get_project_ids_array(){
        return $this->project_ids;
    }

    function get_clients_object(){
        return $this->clients;
    }

    function get_clients(){

        /* WP Transient Cache */
        $this->clients = false;//get_transient( 'everhour_clients' );

        if( isset($_GET['recache-clients']) ){
            $reset_clients_command = true;
        }else{
            $reset_clients_command = false;
        }

        if( false === $this->clients || $reset_clients_command == true ) {

            $this->client_list_status = "";

            $request_clients = wp_remote_get( 'https://api.everhour.com/clients' ,
                array( 'timeout' => 10,
                    'headers' => array( 'X-Api-Key' => $this->api_key ) 
                )
            );

            if( is_wp_error( $request_clients ) ){
                return false;
            }

            $clientsObj = wp_remote_retrieve_body( $request_clients );
            $clients_data = json_decode( $clientsObj );

            if( ! empty( $clients_data ) ) {
                set_transient( 'everhour_clients', $clients_data, 1 * DAY_IN_SECONDS );
                $this->clients = $clients_data;
            }
        } else {
            $this->client_list_status = "(<a href='?page=everhour&tab=invoice&recache-clients'>Recache Everhour Clients</a>)"; 
        }
        return $this->clients;
    }

    function get_company_projects( $woo_order_company ){
        foreach($this->clients as $client){
            if(strpos($woo_order_company, $client->id) !== false){
                $this->client_to_invoice = $client;
                $this->project_ids = $client->projects;
            }else{
                $this->project_ids = null;
                $this->client_to_invoice = '';
            }        
        }
    }

    function display_clients_dropdown($woo_order_company, $forOrderReports = false, $return_errors = true, $optional_selection = false){
        $unset = 'unset';
        if($forOrderReports == true){
            foreach($this->clients as $client){
                if(strpos($woo_order_company, $client->name) !== false){
                    $this->client_to_invoice = $client;
                    $this->project_ids = $client->projects;
                    $unset = 'set';
                }
                if($unset == 'set'){
                    return "match";
                }else{
                    $this->project_ids = null;
                    $this->client_to_invoice = '';
                }        
            }
        }else{

            echo "<select id='eh_company_list' name='company' value='custom'>";
            echo "<option value='0 Companies'>0 Companies</option>";
            //echo "<select id='eh_company_list' name='company' value='custom'><option>All</option>";
            foreach($this->clients as $client){
                // echo "client name: " . $client->name . "<br />";
                // echo "optional argument: " . $optional_selection . "<br />";
                if($client->status != 'archived'){
                    if( strpos($woo_order_company, $client->name) !== false || $client->name == $optional_selection ){
                        $this->client_to_invoice = $client;
                        if($client->projects){
                            $this->project_ids = $client->projects;
                        }
                        //echo "<option selected>" . $client->name . ", client: " . $client->id . ", project: " . $client->projects[0] . "</option>";
                        echo "<option selected value='" . $client->id . "'>" . $client->name . "</option>";
                    }else{
                        echo "<option value='" . $client->id . "'>" . $client->name . "</option>";
                    }
                }
            }
            echo "</select>";
            if($this->client_to_invoice == '' && $return_errors == true){
                echo " <span style='color:red'><strong>NOTICE: There is no match for this company in Everhour!</strong></span>";
            }
            ?>
            <script>
                jQuery( document ).ready(function() {
                    console.log('sorting company dropdown');
                    jQuery("#eh_company_list").html(jQuery("#eh_company_list option").sort(function (a, b) {
                        return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
                    }));
                    // jQuery('#everhour_company_list').remove();
                    // jQuery('#eh_company_list').prepend(jQuery('<option selected>', {
                    //     value: '0 Companies',
                    //     text: '0 Companies'
                    // }));
                });
                
            </script>
            <?php
        }
    }


    function project_to_client_lookup($project_to_lookup){
        foreach( $this->clients as $client ){
            foreach( $client->projects as $key => $project ){
                if( $project == $project_to_lookup ){
                    return $client->name;
                }
            }
        }
    }
}