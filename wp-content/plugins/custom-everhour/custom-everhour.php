<?php
/**
 * Plugin Name: Everhour
 * Plugin URI: http://my.flyrise.io
 * Description: Pulls data from Everhour
 * Version: 1.0
 * Author: Alex Frascona
 * Author URI: http://www.alexyz.com
 * Documentation: https://everhour.docs.apiary.io/
 */

class Everhour {
    private $everhour_options;

	public function __construct() {
		add_action( 'admin_menu', array( $this, 'everhour_add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'everhour_page_init' ) );
	}

    public function get_api_key(){
        return get_option( 'everhour_option_name' )['x_api_key_0'];
    }

	public function everhour_add_plugin_page() {
		add_menu_page(
			'Everhour', // page_title
			'Everhour', // menu_title
			'manage_options', // capability
			'everhour', // menu_slug
			array( $this, 'everhour_create_admin_page' ), // function
			'dashicons-hammer', // icon_url
			3 // position
		);
	}

	public function everhour_create_admin_page() {

        $default_tab = null;
        $tab = isset($_GET['tab']) ? $_GET['tab'] : $default_tab;

		$this->everhour_options = get_option( 'everhour_option_name' ); ?>

        <div class="wrap">
            <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
            <nav class="nav-tab-wrapper">
                <a href="?page=everhour" class="nav-tab <?php if($tab===null):?>nav-tab-active<?php endif; ?>">Settings</a>
                <a href="?page=everhour&tab=projects" class="nav-tab <?php if($tab==='projects'):?>nav-tab-active<?php endif; ?>">Projects to Orders</a>
                <a href="?page=everhour&tab=invoice" class="nav-tab <?php if($tab==='invoice'):?>nav-tab-active<?php endif; ?>">Invoice Generator</a>
            </nav>

            <div class="tab-content">
                <?php switch($tab) :
                    case 'projects':
                        /*
                        *   Load Everhour Projects, match them to Woocommerce Orders, display as WP List Table Class
                        *   Also has examples for getting Everhour Clients & Expenses
                        */
                        include_once 'projects-to-orders.php';
                    break;
                    case 'invoice':
                        /*
                        *   Tool to add an invoice to Everhour */
                        include_once 'invoice.php';
                    break;
                    default:
                        include_once 'initial.php';
                    break;
                endswitch; ?>
            </div>
        </div>
        <?php        
	}

	public function everhour_page_init() {
		register_setting(
			'everhour_option_group', // option_group
			'everhour_option_name', // option_name
			array( $this, 'everhour_sanitize' ) // sanitize_callback
		);

		add_settings_section(
			'everhour_setting_section', // id
			'Settings', // title
			array( $this, 'everhour_section_info' ), // callback
			'everhour-admin' // page
		);

		add_settings_field(
			'x_api_key_0', // id
			'X-Api-Key', // title
			array( $this, 'x_api_key_0_callback' ), // callback
			'everhour-admin', // page
			'everhour_setting_section' // section
        );
	}

	public function everhour_sanitize($input) {
		$sanitary_values = array();
		if ( isset( $input['x_api_key_0'] ) ) {
			$sanitary_values['x_api_key_0'] = sanitize_text_field( $input['x_api_key_0'] );
		}

		return $sanitary_values;
	}

	public function everhour_section_info() {
		
	}

	public function x_api_key_0_callback() {
		printf(
			'<input class="regular-text" type="text" name="everhour_option_name[x_api_key_0]" id="x_api_key_0" value="%s">',
			isset( $this->everhour_options['x_api_key_0'] ) ? esc_attr( $this->everhour_options['x_api_key_0']) : ''
        );
    }
}



if ( is_admin() ){
    $everhour = new Everhour();
    //echo "***** 1: " . $everhour->get_api_key();
    wp_cache_set( 'api_key', $everhour->get_api_key() );
}



/*
*   Add Cost field to Woocommerce Subscription Orders Page Line Items
*/
include_once 'cost.php';

function wc_alter_cost ( $arg1, $arg2 ){
    //$everhour = new Everhour();
    //echo "************************ 2: " . $everhour->get_api_key();
    
    $Cost_init = new Woocommerce_Add_Cost();
    $Cost_init->set_api_key( wp_cache_get( 'api_key' ) );
    $Cost_init->init($arg1, $arg2);
}
//add_action( 'woocommerce_before_order_itemmeta', 'wc_alter_cost', 10, 2);





/*
 *  *****   
 *  ***** Misc / Later Use
 *  *****
 */
        

// function everhour_load_plugin_css() {
//     echo "LOADED THE CSS FUNCTION";
//     $plugin_url = plugin_dir_url( __FILE__ );
//     wp_enqueue_style( 'style1', $plugin_url . 'custom-everhour.css' );
// }
