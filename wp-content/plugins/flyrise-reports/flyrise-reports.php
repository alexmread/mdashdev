<?php
/**
 * Plugin Name: WooCommerce Admin Add Report Example
 *
 * @package WooCommerce\Admin
 */


$plugin_dir = ABSPATH . 'wp-content/plugins/custom-everhour/';
require_once( $plugin_dir . 'inc/expenses.class.php');
require_once( $plugin_dir . 'inc/woocommerce-functions.class.php' );


/**
 * Register the JS.
 */
function add_report_register_script() {

	if ( ! class_exists( 'Automattic\WooCommerce\Admin\Loader' ) || ! \Automattic\WooCommerce\Admin\Loader::is_admin_page() ) {
		return;
	}

	wp_register_script(
		'add-report',
		plugins_url( '/build/index.js', __FILE__ ),
		array(
			'wp-hooks',
			'wp-element',
			'wp-i18n',
			'wc-components',
		),
		filemtime( dirname( __FILE__ ) . '/build/index.js' ),
		true
	);

	wp_enqueue_script( 'add-report' );
}
add_action( 'admin_enqueue_scripts', 'add_report_register_script' );

/**
 * Add "Example" as a Analytics submenu item.
 *
 * @param array $report_pages Report page menu items.
 * @return array Updated report page menu items.
 */
function add_report_add_report_menu_item( $report_pages ) {
	$report_pages[] = array(
		'id'     => 'example-analytics-report',
		'title'  => __( 'Example', 'woocommerce-admin' ),
		'parent' => 'woocommerce-analytics',
		'path'   => '/analytics/example',
	);

	return $report_pages;
}
add_filter( 'woocommerce_analytics_report_menu_items', 'add_report_add_report_menu_item' );


function populate_dropdowns() {

	$Expenses = new Expenses();
    $all_companies_in_woo = $Expenses->get_all_companies_in_woo();
	$clients = array(
        array(
            'label' => __( '0 Companies', 'example-analytics-report' ),
            'value' => '0 Companies',
		)
	);
	foreach ($all_companies_in_woo as $single_company){
		$add_a_company = array(
			'label' => $single_company['name'],
			'value' => $single_company['name'],
		);
		array_push($clients, $add_a_company);
	}

	$WOO_OBJ = new Woocommerce_Functions();
	$services = $WOO_OBJ->get_all_product_categories_for_wc_admin();

 
    $data_registry = Automattic\WooCommerce\Blocks\Package::container()->get(
        Automattic\WooCommerce\Blocks\Assets\AssetDataRegistry::class
    );
 
	$data_registry->add( 'clients', $clients );
	$data_registry->add( 'services', $services );
}
add_action( 'init', 'populate_dropdowns' );


function apply_url_args( $args ) {
	$client = '0 Companies';
	$service = 'Product Categories';

	if ( isset( $_GET['client'] ) ) {
        $client = sanitize_text_field( wp_unslash( $_GET['client'] ) );
    }

	if ( isset( $_GET['service'] ) ) {
        $service = sanitize_text_field( wp_unslash( $_GET['service'] ) );
    }
 
    $args['client'] = $client;
    $args['service'] = $service;
 
    return $args;
}
add_filter( 'woocommerce_analytics_orders_query_args', 'apply_url_args' );
add_filter( 'woocommerce_analytics_orders_stats_query_args', 'apply_url_args' );