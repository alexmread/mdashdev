<?php
/**
 * Plugin Name: flyrise-reports
 *
 * @package WooCommerce\Admin
 */

$plugin_dir = ABSPATH . 'wp-content/plugins/custom-everhour/';
require_once( $plugin_dir . 'inc/expenses.class.php');
require_once( $plugin_dir . 'inc/woocommerce-functions.class.php' );


function add_report_register_script() {

	if ( ! class_exists( 'Automattic\WooCommerce\Admin\Loader' ) || ! \Automattic\WooCommerce\Admin\Loader::is_admin_page() ) {
		return;
	}

	wp_register_script(
		'flyrise-reports',
		plugins_url( '/build/index.js', __FILE__ ),
		array(
			'wp-hooks',
			'wp-element',
			'wp-i18n',
			'wc-components',
		),
		filemtime( dirname( __FILE__ ) . '/build/index.js' ),
		true
	);

	wp_enqueue_script( 'flyrise-reports' );
}
add_action( 'admin_enqueue_scripts', 'add_report_register_script' );

/**
 * Add "Example" as a Analytics submenu item.
 *
 * @param array $report_pages Report page menu items.
 * @return array Updated report page menu items.
 */
function add_report_add_report_menu_item( $report_pages ) {
	$report_pages[] = array(
		'id'     => 'flyrise-reports',
		'title'  => __( 'Expenses', 'woocommerce-admin' ),
		'parent' => 'woocommerce-analytics',
		'path'   => '/analytics/expenses',
	);

	return $report_pages;
}
add_filter( 'woocommerce_analytics_report_menu_items', 'add_report_add_report_menu_item' );


/**
 * Register the JS.
 */
function add_extension_register_script() {
	if ( ! class_exists( 'Automattic\WooCommerce\Admin\Loader' ) || ! \Automattic\WooCommerce\Admin\Loader::is_admin_or_embed_page() ) {
		return;
	}
	
	$script_path       = '/build/index.js';
	$script_asset_path = dirname( __FILE__ ) . '/build/index.asset.php';
	$script_asset      = file_exists( $script_asset_path )
		? require( $script_asset_path )
		: array( 'dependencies' => array(), 'version' => filemtime( $script_path ) );
	$script_url = plugins_url( $script_path, __FILE__ );

	wp_register_script(
		'flyrise-reports',
		$script_url,
		$script_asset['dependencies'],
		$script_asset['version'],
		true
	);

	wp_register_style(
		'flyrise-reports',
		plugins_url( '/build/index.css', __FILE__ ),
		// Add any dependencies styles may have, such as wp-components.
		array(),
		filemtime( dirname( __FILE__ ) . '/build/index.css' )
	);

	wp_enqueue_script( 'flyrise-reports' );
	wp_enqueue_style( 'flyrise-reports' );
}
add_action( 'admin_enqueue_scripts', 'add_extension_register_script' );


function populate_dropdowns() {

	$Expenses = new Expenses();
    $all_companies_in_woo = $Expenses->get_all_companies_in_woo();
	$clients = array(
        array(
            'label' => __( 'Everhour Company List', 'flyrise-reports' ),
            'value' => 'Everhour Company List',
		)
	);
	foreach ($all_companies_in_woo as $single_company){
		$add_a_company = array(
			'label' => $single_company['name'],
			'value' => $single_company['name'],
		);
		array_push($clients, $add_a_company);
	}

	$WOO_OBJ = new Woocommerce_Functions();
	$services = $WOO_OBJ->get_all_product_categories_for_wc_admin();

 
    $data_registry = Automattic\WooCommerce\Blocks\Package::container()->get(
        Automattic\WooCommerce\Blocks\Assets\AssetDataRegistry::class
    );
 
	$data_registry->add( 'clients', $clients );
	$data_registry->add( 'services', $services );
}
add_action( 'init', 'populate_dropdowns' );


function apply_url_args( $args ) {
	$client = 'Everhour Company List';
	$service = 'Product Categories';

	if ( isset( $_GET['client'] ) ) {
        $client = sanitize_text_field( wp_unslash( $_GET['client'] ) );
    }

	if ( isset( $_GET['service'] ) ) {
        $service = sanitize_text_field( wp_unslash( $_GET['service'] ) );
    }
 
    $args['client'] = $client;
    $args['service'] = $service;
 
    return $args;
}
add_filter( 'woocommerce_analytics_orders_query_args', 'apply_url_args' );
add_filter( 'woocommerce_analytics_orders_stats_query_args', 'apply_url_args' );



// function add_join_subquery( $clauses ) {
//     global $wpdb;
 
//     $clauses[] = "JOIN {$wpdb->postmeta} currency_postmeta ON {$wpdb->prefix}wc_order_stats.order_id = currency_postmeta.post_id";
 
//     return $clauses;
// }
 
// add_filter( 'woocommerce_analytics_clauses_join_orders_subquery', 'add_join_subquery' );
// add_filter( 'woocommerce_analytics_clauses_join_orders_stats_total', 'add_join_subquery' );
// add_filter( 'woocommerce_analytics_clauses_join_orders_stats_interval', 'add_join_subquery' );


// function add_where_subquery( $clauses ) {
//     $currency = 'USD';
 
//     if ( isset( $_GET['currency'] ) ) {
//         $currency = sanitize_text_field( wp_unslash( $_GET['currency'] ) );
//     }
 
//     $clauses[] = "AND currency_postmeta.meta_key = '_order_currency' AND currency_postmeta.meta_value = '{$currency}'";
 
//     return $clauses;
// }
 
// add_filter( 'woocommerce_analytics_clauses_where_orders_subquery', 'add_where_subquery' );
// add_filter( 'woocommerce_analytics_clauses_where_orders_stats_total', 'add_where_subquery' );
// add_filter( 'woocommerce_analytics_clauses_where_orders_stats_interval', 'add_where_subquery' );


// function add_select_subquery( $clauses ) {
//     $clauses[] = ', currency_postmeta.meta_value AS currency';
 
//     return $clauses;
// }
 
// add_filter( 'woocommerce_analytics_clauses_select_orders_subquery', 'add_select_subquery' );
// add_filter( 'woocommerce_analytics_clauses_select_orders_stats_total', 'add_select_subquery' );
// add_filter( 'woocommerce_analytics_clauses_select_orders_stats_interval', 'add_select_subquery' );


// function add_join_subquery2( $clauses ) {
//     global $wpdb;
 
//     $clauses[] = "JOIN {$wpdb->postmeta} billing_company_postmeta ON {$wpdb->prefix}wc_order_stats.company_id = billing_company_postmeta.post_id";
 
//     return $clauses;
// }
 
// add_filter( 'woocommerce_analytics_clauses_join_orders_subquery', 'add_join_subquery2' );
// add_filter( 'woocommerce_analytics_clauses_join_orders_stats_total', 'add_join_subquery2' );
// add_filter( 'woocommerce_analytics_clauses_join_orders_stats_interval', 'add_join_subquery2' );

// function add_where_subquery2( $clauses ) {
//     $client = 'Everhour Company List';
 
//     if ( isset( $_GET['client'] ) ) {
//         $currency = sanitize_text_field( wp_unslash( $_GET['client'] ) );
//     }
 
//     $clauses[] = "AND billing_company_postmeta.meta_key = '_billing_company' AND billing_company_postmeta.meta_value = '{$client}'";
 
//     return $clauses;
// }
 
// add_filter( 'woocommerce_analytics_clauses_where_orders_subquery', 'add_where_subquery2' );
// add_filter( 'woocommerce_analytics_clauses_where_orders_stats_total', 'add_where_subquery2' );
// add_filter( 'woocommerce_analytics_clauses_where_orders_stats_interval', 'add_where_subquery2' );

// function add_select_subquery2( $clauses ) {
//     $clauses[] = ', billing_company_postmeta.meta_value AS client';
 
//     return $clauses;
// }
 
// add_filter( 'woocommerce_analytics_clauses_select_orders_subquery', 'add_select_subquery2' );
// add_filter( 'woocommerce_analytics_clauses_select_orders_stats_total', 'add_select_subquery2' );
// add_filter( 'woocommerce_analytics_clauses_select_orders_stats_interval', 'add_select_subquery2' );
