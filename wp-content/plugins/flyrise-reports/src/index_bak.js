import { addFilter } from '@wordpress/hooks';
import { __ } from '@wordpress/i18n';
 
// const addCurrencyFilters = ( filters ) => {
//     return [
//         {
//             label: __( 'Currency', 'flyrise-reports' ),
//             staticParams: [],
//             param: 'currency',
//             showFilters: () => true,
//             defaultValue: 'USD',
//             filters: [ ...( wcSettings.multiCurrency || [] ) ],
//         },
//         ...filters,
//     ];
// };

const addClientFilters = ( filters ) => {
    //console.log(wcSettings.clients); //fires, exists, populated
    return [
        {
            label: __( 'Client', 'flyrise-reports' ),
            staticParams: [],
            param: 'client',
            showFilters: () => true,
            defaultValue: 'Everhour Company List',
            filters: [ ...( wcSettings.clients || [] ) ],
        },
        ...filters,
    ];
};

const addServiceFilters = ( filters ) => {
    return [
        {
            label: __( 'Services', 'flyrise-reports' ),
            staticParams: [],
            param: 'service',
            showFilters: () => true,
            defaultValue: 'Product Categories',
            filters: [ ...( wcSettings.services || [] ) ],
        },
        ...filters,
    ];
};
 
// addFilter(
//     'woocommerce_admin_orders_report_filters',
//     'flyrise-reports',
//     addCurrencyFilters
// );

addFilter(
    'woocommerce_admin_orders_report_filters',
    'flyrise-reports',
    addClientFilters
);

addFilter(
    'woocommerce_admin_orders_report_filters',
    'flyrise-reports',
    addServiceFilters
);



// const addTableColumn = reportTableData => {
//     if ( 'orders' !== reportTableData.endpoint ) {
//         return reportTableData;
//     }
 
//     const newHeaders = [
//         {
//             label: 'Currency',
//             key: 'currency',
//         },
//         ...reportTableData.headers,
//     ];
//     const newRows = reportTableData.rows.map( ( row, index ) => {
//         const item = reportTableData.items.data[ index ];
//         const newRow = [
//             {
//                 display: item.currency,
//                 value: item.currency,
//             },
//             ...row,
//         ];
//         return newRow;
//     } );
 
//     reportTableData.headers = newHeaders;
//     reportTableData.rows = newRows;
 
//     return reportTableData;
// };
 
// addFilter( 'woocommerce_admin_report_table', 'flyrise-reports', addTableColumn );


// const addTableColumn2 = reportTableData => {
//     if ( 'orders' !== reportTableData.endpoint ) {
//         return reportTableData;
//     }
 
//     const newHeaders = [
//         {
//             label: 'Client',
//             key: 'client',
//         },
//         ...reportTableData.headers,
//     ];
//     const newRows = reportTableData.rows.map( ( row, index ) => {
//         const item = reportTableData.items.data[ index ];
//         const newRow = [
//             {
//                 display: item.client,
//                 value: item.client,
//             },
//             ...row,
//         ];
//         return newRow;
//     } );
 
//     reportTableData.headers = newHeaders;
//     reportTableData.rows = newRows;
    
//     return reportTableData;
// };
 
// addFilter( 'woocommerce_admin_report_table', 'flyrise-reports', addTableColumn2 );

//console.dir(reportTableData); doesn't work outside the filter


// const currencies = {
//     MXN: {
//         code: 'MXN',
//         symbol: '$MXN', // For the sake of the example.
//         symbolPosition: 'left',
//         thousandSeparator: ',',
//         decimalSeparator: '.',
//         precision: 2,
//     },
//     NZD: {
//         code: 'NZD',
//         symbol: '$NZ',
//         symbolPosition: 'left',
//         thousandSeparator: ',',
//         decimalSeparator: '.',
//         precision: 2,
//     },
// };

// const clients = {
//     'testing company': {
//         label: 'testing company',
//         value: 'testing company',
//     }
// };

//const clients = wcSettings.clients;
//clients.forEach(populateClients);
// function populateClients(item, index) {
//   document.getElementById("demo").innerHTML += index + ":" + item + "<br>";
// }

// const updateReportCurrencies = ( config, { currency } ) => {
//     if ( currency && currencies[ currency ] ) {
//         return currencies[ currency ];
//     }
//     return config;
// };
 
// addFilter(
//     'woocommerce_admin_report_currency',
//     'flyrise-reports',
//     updateReportCurrencies
// );


// const updateReportClient = ( config, { client } ) => {
//     console.log(client);
//     if ( client && clients[ client ] ) {
//         return currencies[ client ];
//     }
//     return config;
// };
 
// addFilter(
//     'woocommerce_admin_report_currency',
//     'flyrise-reports',
//     updateReportClient
// );