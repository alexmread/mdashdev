<?php

/**
 * Plugin Name: ContentSnare
 * Author: Alex Frascona
 * Author URI: https://alexyz.com
 * Description: REST API endpoint to receive data from ContentSnare Webhook
 * Version: 1.0
 * License: GPL2 or later
 * License URL: http://www.gnu.org/licenses/gpl-2.0.txt
 * text-domain: content-snare-webhook
*/


add_action( 'rest_api_init', 'contentsnare_add_callback_url_endpoint' );

function contentsnare_add_callback_url_endpoint(){
    register_rest_route(
        'contentsnare/v1',
        '/receive',
        array(
            'methods'  => 'POST', //'THODS,
            'callback' => 'contentsnare_receive_callback',
            'permission_callback' => '__return_true'
        )
    );
}


function contentsnare_receive_callback( $parameters ) {

    $body = $parameters->get_params();
    //return $body;
    //return $body['request__client__email'];

    $updated_time = date("h:i:s");
    $data['updated'] = $updated_time;

    $myfile = file_put_contents('requests.txt', 'Endpoint called: ' . $updated_time . PHP_EOL , FILE_APPEND | LOCK_EX );

    $event = $body['event_name'];

    $id = $name = $status = $due = $percent_complete = $link = $email = null;

    if( $event == 'request_published' || $event == 'request_completed' ){
        $id                 = $body['id']; // request ID! use this in their 'journey'
        $name               = $body['name']; // This is the Request Title!
        $status             = $body['status']; // published or completed
        $due                = $body['due_date'];
        $percent_complete   = $body['completion_percentage'];
        $link               = $body['share_link'];
        $company            = $body['client']['company_name'];
        $email              = $body['client']['email'];
        $client_id          = $body['client']['id']; // we'll use this for lookup rather than email?
        $client_name        = $body['client']['full_name'];
    }

    if ( $event == 'all_fields_completed' ){
        $id                 = $body['request']['id']; // request ID! use this in their 'journey'
        $name               = $body['request']['name']; // This is the Request Title!
        $status             = $body['request']['status']; // published or completed
        $due                = $body['request']['due_date'];
        $percent_complete   = $body['request']['completion_percentage'];
        $link               = $body['request']['share_link'];
        $company            = $body['request']['client']['company_name'];
        $email              = $body['request']['client']['email'];
        $client_id          = $body['request']['client']['id'];
        $client_name        = $body['request']['client']['full_name'];
    }

    if ( $event == 'client_created' ) {
        $id = $name = $status = $due = $percent_complete = $link = ''; // not in object
        $company            = $body['company_name'];
        $email              = $body['email'];
        $client_id          = $body['id']; // Client id!
        $client_name        = $body['full_name'];
    }
    
    
    $data['email'] = $email;
    

    //if ( isset($email) ) {
    //if ( isset($email) && ($event == 'request_published') ) {
    
    if( $event == 'client_created' ){

        //$userdata = get_user_by( 'login', $name );
        $userdata = get_user_by( 'email', $email );

    }else{ // 'request_published' || 'request_completed' || 'all_fields_completed'
       $userdata = get_users(array(
            'meta_key' => 'contentsnare_client_id',
            'meta_value' => $client_id
        ))[0];
       // print_r($userdata);
       // echo gettype($userdata);
       // echo count($userdata);
    }
    //exit;
        
        if ( $userdata ) {
            
            //$wp_check_password_result = wp_check_password( $password, $userdata->user_pass, $userdata->ID );
            
            //if ( $wp_check_password_result ) {

                $journey = json_decode(get_user_meta( $userdata->ID, 'journey', true ));

                if( is_null($journey) ){
                    $journey = new stdClass;
                }
                $journey->updated = $updated_time;

                $data['journey_found'] = $journey;
                

                /* This adds it to our Journey object
                if($journey->contentsnare_client_id == false){
                    $journey->contentsnare_client_id = $client_id;
                }*/
                /*
                *   Instead we want it to live in it's own custom meta field
                *   so that we can look them up by that rather than by their email
                */
                if($event == 'client_created'){
                $contentsnare_client_id = get_user_meta( $userdata->ID, 'contentsnare_client_id', true );
                $data['clientsnare_client_id'] = $contentsnare_client_id;
                //if($contentsnare_client_id == false){
                    $contentsnare_client_id_update = update_user_meta( $userdata->ID, 'contentsnare_client_id', $client_id );
                
                    if( $contentsnare_client_id_update ) {
                        $data['contentsnare_client_id_updated_status'] = 'ClientSnare ID status OK';
                        $data['contentsnare_client_id_message'] = 'ClientSnare Client ID Updated';
                        $myfile = file_put_contents('requests.txt', 'ClientSnare Client ID Updated: ' . $email . PHP_EOL , FILE_APPEND | LOCK_EX );
                    }else{
                        $data['contentsnare_client_id_updated_status'] = 'ClientSnare Client ID Meta Not Updated';
                        $myfile = file_put_contents('requests.txt', 'ClientSnare Client ID Not Updated: ' . $email . PHP_EOL , FILE_APPEND | LOCK_EX );
                    }
                //}
                /* End ClientSnare Client ID Custom Meta Field Updates */
                }

                /* Don't update Journey when it's just a New Client Creation Hook */
                if($event != 'client_created'){

                    $journey_obj_match = false;
                    if($journey->requests){ // if they already have some requests
                    //if(undefined != $journey->requests){
                    //if( isset($journey->requests) && ($journey->requests !== null) ){

                      foreach($journey->requests as $journey_obj){

                        if( $journey_obj->id == $id){ // if they already have this one, update it
                            if($id != null){
                                $journey_obj->id = $id;
                            }
                            if($name != null){
                              $journey_obj->name    = $name;
                            }
                            if($link != null){
                              $journey_obj->link    = $link;
                            }
                            if($due != null){
                              $journey_obj->due     = $due;
                            }
                            $journey_obj->event   = $event;
                            $journey_obj->status  = $status;
                            $journey_obj->percent_complete = $percent_complete;

                            $data['journey_updated'] = $journey;
                            $journey_obj_match = true;
                        }
                      }

                      if($journey_obj_match == false){ // if they have some, but not this one, add it

                        $new_journey_item               = new stdClass();
                        $new_journey_item->id           = $id;
                        $new_journey_item->name         = $name;
                        $new_journey_item->link         = $link;
                        $new_journey_item->due          = $due;
                        $new_journey_item->event        = $event;
                        $new_journey_item->status       = $status;
                        $new_journey_item->percent_complete = $percent_complete;

                        $journey->requests->$id = $new_journey_item;
                        $data['journey_updated'] = $new_journey_item;
                      }
                    } else { // no requests yet, add a requests object, and this request
                      $journey_requests_obj = new stdClass();
                      $journey->requests = $journey_requests_obj;

                      $new_journey_item               = new stdClass();
                      $new_journey_item->id           = $id;
                      $new_journey_item->name         = $name;
                      $new_journey_item->link         = $link;
                      $new_journey_item->due          = $due;
                      $new_journey_item->event        = $event;
                      $new_journey_item->status       = $status;
                      $new_journey_item->percent_complete = $percent_complete;

                      $journey->requests->$id = $new_journey_item;
                      $data['journey_updated'] = $new_journey_item;
                    }
                    
                    
                    $journey_update = update_user_meta( $userdata->ID, 'journey', json_encode($journey) );
                    
                    if( $journey_update ) {
                        $data['status'] = 'OK';
                        $data['message'] = 'Journey Updated';
                        $myfile = file_put_contents('requests.txt', 'Journey Updated: ' . $email . PHP_EOL , FILE_APPEND | LOCK_EX );
                    }else{
                        //return $journey_update; // just returns: false
                        $data['status'] = 'Journey Meta Not Updated';
                        $myfile = file_put_contents('requests.txt', 'Journey Not Updated: ' . $email . PHP_EOL , FILE_APPEND | LOCK_EX );
                    }
                }
                
            // } else {
            //     $data['status'] = 'OK';
            //     $data['message'] = 'You are not authenticated to login!';
            // }
            
            
        } else {
            $myfile = file_put_contents('requests.txt', 'email unfound' . $email . PHP_EOL , FILE_APPEND | LOCK_EX);

            $data['status'] = 'FAILED';
            $data['message'] = 'The current user does not exist!';
        }
        
        
    // } else {
    //     $myfile = file_put_contents('requests.txt', 'email or event missing'.PHP_EOL , FILE_APPEND | LOCK_EX);
    //     $data['status'] = 'Failed';
    //     $data['message'] = 'Parameters Missing!';
        
    // }
    
    return $data;
}