<?php 
require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');
wp_head(); 

$filepath = $_SERVER['REQUEST_URI'];
if ($filepath != '/my-account/payment-methods/') {
	$html = file_get_contents(home_url() . '/wp-content/themes/midas-dashboard/vue-dist' . $filepath . '/index.html');
	$html = str_replace('/_nuxt/', '/wp-content/themes/midas-dashboard/vue-dist/_nuxt/' , $html);
	echo $html;

	?>
	<!-- section -->
	<div id="__nuxt">
	</div>
	<!-- /section -->
	<?php
}
else {
    while(have_posts()) : the_post(); ?>

	title: <?php the_title(); ?><br />
	ID: <?php the_ID(); ?><br />
	time: <?php the_time(get_option('date_format')); ?><br />
	excerpt: <?php the_excerpt(); ?><br />
	link: <a href="<?php the_permalink(); ?>">link</a><br />
	<hr />

<?php endwhile; 
}
 get_footer(); 
 ?>
