<?php /* Template Name: resetpass Template */ 

wp_head(); ?>

<div class="container">
	<div class="row justify-content-center vertical-center">
		<div class="col col-sm-8 col-lg-4">
			
			<div class="row justify-content-center mb-4">
				<div class="col col-6 text-center">
					<img src="/wp-content/themes/midas-dashboard/assets/images/flyrise-full-color-logo.svg" alt="">
				</div>
			</div>

			<div class="card ds-default">
				<div class="card-header border-bottom-0">
					<div class="text-center">Reset Password</div>
				</div>

				<div class="card-body">

					<div role="form" id="login">
						<form method="post" action="<?php echo site_url('wp-login.php?action=lostpassword', 'login_post') ?>" class="wp-user-form">
							<div class="text-center">
								<?php $reset = $_GET['reset']; if($reset == false) { echo '<small>Please enter your username or email address. You will receive an email message with instructions on how to reset your password.</small>'; } ?>
								<?php $reset = $_GET['reset']; if($reset == true) { echo '<small>A message will be sent to your email address.</small>'; } ?>
							</div>
							<div class="form-group text-center">
								<label class="form-control-label">Email Address or Username 9</label>
								<div class="has-label username">
									<input type="text" name="user_login" value="" id="user_login" class="form-control text-center" tabindex="1001" />
								</div>
							</div>

				  		<div class="text-center login_fields">
				  			<?php do_action('login_form', 'resetpass'); ?>
				  			<input type="submit" name="user-submit" value="<?php _e('Reset my password'); ?>" class="user-submit btn btn-primary" tabindex="1002" />
				  			<input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>?reset=true" />
				  			<input type="hidden" name="user-cookie" value="1" />
				  		</div>
						</form>
					</div>

				</div>
			</div>
			<div class="row mt-3">
				<div class="col-12 text-center">
					<h5>Return to the login page <a href="/login" class="login-reset-pw">here</a></h5>
				</div>
			</div>
		</div>
	</div>
</div>


<?php get_footer(); ?>
