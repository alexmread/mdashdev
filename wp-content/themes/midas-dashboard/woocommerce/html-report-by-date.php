<?php
/**
 * Admin View: Report by Date (with date filters)
 *
 * @package WooCommerce\Admin\Reporting
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

require_once( WP_PLUGIN_DIR . '/custom-everhour/inc/everhour-clients.class.php' );
$EH_Clients = new Everhour_Clients();
$EH_Clients->set_api_key(wp_cache_get( 'api_key'));
$EH_Clients->get_clients();

require_once( WP_PLUGIN_DIR . '/custom-everhour/inc/woocommerce-functions.class.php' );
$WOO_OBJ = new Woocommerce_Functions();

?>

<div id="poststuff" class="woocommerce-reports-wide">
	<div class="postbox">

	<?php if ( 'custom' === $current_range && isset( $_GET['start_date'], $_GET['end_date'] ) ) : ?>
		<h3 class="screen-reader-text">
			<?php
			/* translators: 1: start date 2: end date */
			printf(
				esc_html__( 'From %1$s to %2$s', 'woocommerce' ),
				esc_html( wc_clean( wp_unslash( $_GET['start_date'] ) ) ),
				esc_html( wc_clean( wp_unslash( $_GET['end_date'] ) ) )
			);
			?>
		</h3>
	<?php else : ?>
		<h3 class="screen-reader-text"><?php echo esc_html( $ranges[ $current_range ] ); ?></h3>
	<?php endif; ?>

		<div class="stats_range">
			<?php $this->get_export_button(); ?>
			<ul>
				<?php
				foreach ( $ranges as $range => $name ) {
					echo '<li class="' . ( $current_range == $range ? 'active' : '' ) . '"><a href="' . esc_url( remove_query_arg( array( 'start_date', 'end_date' ), add_query_arg( 'range', $range ) ) ) . '">' . esc_html( $name ) . '</a></li>';
				}
				?>
				<li class="custom <?php echo ( 'custom' === $current_range ) ? 'active' : ''; ?>">
					<?php
						//esc_html_e( 'Custom:', 'woocommerce' );
					?>
					<form method="GET">
						<div>
							<?php
							// Maintain query string.
							foreach ( $_GET as $key => $value ) {
								if ( is_array( $value ) ) {
									foreach ( $value as $v ) {
										echo '<input type="hidden" name="' . esc_attr( sanitize_text_field( $key ) ) . '[]" value="' . esc_attr( sanitize_text_field( $v ) ) . '" />';
									}
								} else {
									echo '<input type="hidden" name="' . esc_attr( sanitize_text_field( $key ) ) . '" value="' . esc_attr( sanitize_text_field( $value ) ) . '" />';
								}
							}
							?>
							
							<!-- midas-dashboard/woocommerce/html-report-by-date.php -->
							<script type="text/javascript">
						        jQuery(function() {
									jQuery('.date-picker').datepicker({
										dateFormat: "yy-mm-dd",
										changeMonth: true,
										changeYear: true,
										showButtonPanel: true,
										onClose: function(dateText, inst) {


										  function isDonePressed() {
										    return (jQuery('#ui-datepicker-div').html().indexOf('ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover') > -1);
										  }

										  if (isDonePressed()) {
										  	console.log('isDonePressed 1');
										    var month = jQuery("#ui-datepicker-div .ui-datepicker-month :selected").val();
										    console.log('month:' + month);
										    var year = jQuery("#ui-datepicker-div .ui-datepicker-year :selected").val();
										    console.log('year: ' + year);
										    jQuery(this).datepicker('setDate', new Date(year, month, 1)).trigger('change');

										    jQuery('.date-picker').focusout() //Added to remove focus from datepicker input box on selecting date
										  }
										},
										beforeShow: function(input, inst) {
											console.log('beforeShow 1');
										  	inst.dpDiv.addClass('month_year_datepicker')

											if ((datestr = jQuery(this).val()).length > 0) {
												year = datestr.substring(datestr.length - 4, datestr.length);
												console.log('year: ' + year);
												month = datestr.substring(0, 2);
												console.log('month: ' + month);
												//jQuery(this).datepicker('option', 'defaultDate', new Date(year, month - 1, 1));
												//jQuery(this).datepicker('setDate', new Date(year, month - 1, 1));
												jQuery(".ui-datepicker-calendar").hide();
											}
										}
									})
								});
								jQuery(function() {
									jQuery('.date-picker-2').datepicker({
										dateFormat: "yy-mm-dd",
										changeMonth: true,
										changeYear: true,
										showButtonPanel: true,
										onClose: function(dateText, inst) {


										  function isDonePressed() {
										    return (jQuery('#ui-datepicker-div').html().indexOf('ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover') > -1);
										  }

										  if (isDonePressed()) {
										  	console.log('isDonePressed 2');
										    var month = parseInt(jQuery("#ui-datepicker-div .ui-datepicker-month :selected").val()) + 1;
										    console.log('month: ' + month);
										    var year = jQuery("#ui-datepicker-div .ui-datepicker-year :selected").val();
										    console.log('year: ' + year);
										    jQuery(this).datepicker('setDate', new Date(year, month, 0)).trigger('change');

										    jQuery('.date-picker-2').focusout() //Added to remove focus from datepicker input box on selecting date
										  }
										},
										beforeShow: function(input, inst) {
											console.log('beforeShow 2');
											inst.dpDiv.addClass('month_year_datepicker')

											if ((datestr = jQuery(this).val()).length > 0) {
												console.log('datestr: ' + datestr);
												//year = datestr.substring(datestr.length - 4, datestr.length);
												year = datestr.slice(0, 4);
												console.log('year: ' + year);

												//month = datestr.substring(0, 2);
												month = parseInt(datestr.split("-")[1]) + 1;
												console.log('month: ' + month);
												//jQuery(this).datepicker('option', 'defaultDate', new Date(year + "-" + month, 0));
												//jQuery(this).datepicker('setDate', new Date(year + "-" + month + "-" + 0));
												jQuery(".ui-datepicker-calendar").hide();
											}
										}
									})
								});
						    </script>
							<style>
							    .ui-datepicker-calendar {
							        display: none;
							    }
							    </style>
							<input type="hidden" name="range" value="custom" />
							<input type="text" size="11" placeholder="yyyy-mm-dd" value="<?php echo ( ! empty( $_GET['start_date'] ) ) ? esc_attr( wp_unslash( $_GET['start_date'] ) ) : ''; ?>" name="start_date" id="start_date" class="date-picker" autocomplete="off" /><?php //@codingStandardsIgnoreLine ?>

							<!-- <label for="startDate">Date :</label>
    						<input name="startDate" id="startDate" class="date-picker" /> -->

							<span>&ndash;</span>

							<input type="text" size="11" placeholder="yyyy-mm-dd" value="<?php echo ( ! empty( $_GET['end_date'] ) ) ? esc_attr( wp_unslash( $_GET['end_date'] ) ) : ''; ?>" name="end_date" id="end_date" class="date-picker-2" autocomplete="off" /><?php //@codingStandardsIgnoreLine ?>
                            
                            <?php
                                echo $EH_Clients->display_clients_dropdown(null, false, false, ( ! empty( $_GET['company'] ) ) ? esc_attr( wp_unslash( $_GET['company'] ) ) : false);
								if(! empty( $_GET['category'] ) ){
									$category_selection = esc_attr( wp_unslash( $_GET['category'] ) );
								 }else{
									 $category_selection = false;
								 }
								$WOO_OBJ->get_all_product_categories($category_selection);
							?>
                            
                            Exclude:
                            <?php
                            	if ( isset($_GET['exclude_flyrise']) &&  $_GET['exclude_flyrise']=='on') {
                            		$exclude_flyrise = "checked='checked'";
                            	}
                            	if ( isset($_GET['exclude_ridgecapmarketing']) &&  $_GET['exclude_ridgecapmarketing']=='on') {
                            		$exclude_ridgecapmarketing = "checked='checked'";
                            	}
                        	?> 
							<input type="hidden" name="exclude_flyrise" value="off">
							<input name="exclude_flyrise" type="checkbox" value="on" <?php if(isset($exclude_flyrise)){ echo $exclude_flyrise; }?>>
							<label for="exclude_flyrise">Flyrise </label>

							<input type="hidden" name="exclude_ridgecapmarketing" value="off">
							<input name="exclude_ridgecapmarketing" type="checkbox" value="on" <?php if(isset($exclude_ridgecapmarketing)){echo $exclude_ridgecapmarketing; }?>>
							<label for="exclude_ridgecapmarketing">Ridgecapmarketing </label>
							

                            <button type="submit" class="button" value="<?php esc_attr_e( 'Go', 'woocommerce' ); ?>"><?php esc_html_e( 'Go', 'woocommerce' ); ?></button>
							<?php wp_nonce_field( 'custom_range', 'wc_reports_nonce', false ); ?>


							<?php
								if ( isset($_GET['totals_only']) && $_GET['totals_only'] == 'on' ) {
									$totals_only = "checked='checked'";
								}
							?>
							<input type="hidden" name="totals_only" type="checkbox" value="off">
							<input name="totals_only" type="checkbox" value="on" <?php if(isset($totals_only)){ echo $totals_only; }?>>
							<label for="totals_only">Totals Only </label>


							<?php
								if ( isset($_GET['products_only']) && $_GET['products_only'] == 'on' ) {
									$products_only = "checked='checked'";
								}
							?>
							<input type="hidden" name="products_only" type="checkbox" value="off">
							<input name="products_only" type="checkbox" value="on" <?php if(isset($products_only)){ echo $products_only; }?>>
							<label for="products_only">Products Only </label>

						</div>
					</form>
				</li>
			</ul>
		</div>
		<?php if ( empty( $hide_sidebar ) ) : ?>
			<div class="inside chart-with-sidebar">
				<div class="chart-sidebar">
					<?php if ( $legends = $this->get_chart_legend() ) : ?>
						<ul class="chart-legend">
							<?php foreach ( $legends as $legend ) : ?>
								<?php // @codingStandardsIgnoreStart ?>
								<li style="border-color: <?php echo $legend['color']; ?>" <?php if ( isset( $legend['highlight_series'] ) ) echo 'class="highlight_series ' . ( isset( $legend['placeholder'] ) ? 'tips' : '' ) . '" data-series="' . esc_attr( $legend['highlight_series'] ) . '"'; ?> data-tip="<?php echo isset( $legend['placeholder'] ) ? $legend['placeholder'] : ''; ?>">
									<?php echo $legend['title']; ?>
								</li>
								<?php // @codingStandardsIgnoreEnd ?>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
					<ul class="chart-widgets">
						<?php foreach ( $this->get_chart_widgets() as $widget ) : ?>
							<li class="chart-widget">
								<?php if ( $widget['title'] ) : ?>
									<h4><?php echo esc_html( $widget['title'] ); ?></h4>
								<?php endif; ?>
								<?php call_user_func( $widget['callback'] ); ?>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
				<div class="main">
					<?php $this->get_main_chart(); ?>
				</div>
			</div>
		<?php else : ?>
			<div class="inside">
				<?php $this->get_main_chart(); ?>
			</div>
		<?php endif; ?>
	</div>
</div>
