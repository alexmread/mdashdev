<?php

function workspaces_to_cost_callback() {

    $report = new WC_Report_Everhour_Workspaces();
    $report->output_report();
}

class WC_Report_Everhour_Workspaces extends WC_Admin_Report {

	/**
	 * Chart colors.
	 *
	 * @var array
	 */
	public $chart_colours = array();

	var $range = '';
    var $total_revenue;
	var $total_operations;
    var $total_clients;
    var $total_marketing;
    var $total_prospects;
    var $total_sales_development;
    var $total_dashboard;
    var $total_cost;
	var $total_cost_without_filters;
	//var $total_misc;

    var $array_of_revenue		= array();
    var $array_of_operations	= array();
    var $array_of_clients		= array();
    var $array_of_prospects		= array();
    var $array_of_marketing		= array();
    var $array_of_sales			= array();
    var $array_of_dashboard		= array();
	//var $array_of_misc			= array();

	var $array_of_marketing_tasks = array();
	var $array_of_sales_tasks = array();

    /**
	 * The report data.
	 *
	 * @var stdClass
	 */
	private $report_data;

	/**
	 * Get report data.
	 *
	 * @return stdClass
	 */
	public function get_report_data() {
		if ( empty( $this->report_data ) ) {
			$this->query_report_data();
		}
		return $this->report_data;
		// echo "<pre>";
		// print_r($this->report_data);
		// echo "</pre>";
	}

	/**
	 * Get all data needed for this report and store in the class.
	 */
	private function query_report_data() {
		echo "Range from: " . $_GET['start_date'] . "<br />";
		echo "Range to: " . $_GET['end_date'] . "<br />";
		echo "Range itself: " . $_GET['range'] . "<br />";

		$start_date = ! empty( $_GET['start_date'] ) ? sanitize_text_field( $_GET['start_date'] ) : $start_date_default;
        $end_date = ! empty( $_GET['end_date'] ) ? sanitize_text_field( $_GET['end_date'] ) : $end_date_default;
        $arr = explode("-", $start_date);
        $start_year = intval($arr[0]);
        $month_from = intval($arr[1]);
        
        $arr2 = explode("-", $end_date);
        $end_year = $arr2[0];
        $month_to = intval($arr2[1]);


		$this->report_data = new stdClass();

		$this->report_data->order_counts = (array) $this->get_order_report_data(
			array(
				'data'         => array(
					'ID'        => array(
						'type'     => 'post_data',
						'function' => 'COUNT',
						'name'     => 'count',
						'distinct' => true,
					),
					'post_date' => array(
						'type'     => 'post_data',
						'function' => '',
						'name'     => 'post_date',
					),
				),
				'group_by'     => $this->group_by_query,
				'order_by'     => 'post_date ASC',
				'query_type'   => 'get_results',
				'filter_range' => true,
				'nocache'       =>  true,
				'order_types'  => wc_get_order_types( 'order-count' ),
				'order_status' => array( 'completed', 'processing', 'on-hold', 'refunded' ),
				'date_query' => array(
	                array(
	                    'after'     => date('Y-m-d', strtotime($start_date)),
	                    'before'    => date('Y-m-d', strtotime($end_date)),
	                    'inclusive' => true,
	                ),
	            ),
			)
		);

		$this->report_data->coupons = (array) $this->get_order_report_data(
			array(
				'data'         => array(
					'order_item_name' => array(
						'type'     => 'order_item',
						'function' => '',
						'name'     => 'order_item_name',
					),
					'discount_amount' => array(
						'type'            => 'order_item_meta',
						'order_item_type' => 'coupon',
						'function'        => 'SUM',
						'name'            => 'discount_amount',
					),
					'post_date'       => array(
						'type'     => 'post_data',
						'function' => '',
						'name'     => 'post_date',
					),
				),
				'where'        => array(
					array(
						'key'      => 'order_items.order_item_type',
						'value'    => 'coupon',
						'operator' => '=',
					),
				),
				'group_by'     => $this->group_by_query . ', order_item_name',
				'order_by'     => 'post_date ASC',
				'query_type'   => 'get_results',
				'filter_range' => true,
				'order_types'  => wc_get_order_types( 'order-count' ),
				'order_status' => array( 'completed', 'processing', 'on-hold', 'refunded' ),
				'date_query' => array(
	                array(
	                    'after'     => date('Y-m-d', strtotime($start_date)),
	                    'before'    => date('Y-m-d', strtotime($end_date)),
	                    'inclusive' => true,
	                ),
	            ),
			)
		);

		// All items from orders - even those refunded.
		$this->report_data->order_items = (array) $this->get_order_report_data(
			array(
				'data'         => array(
					'_qty'      => array(
						'type'            => 'order_item_meta',
						'order_item_type' => 'line_item',
						'function'        => 'SUM',
						'name'            => 'order_item_count',
					),
					'post_date' => array(
						'type'     => 'post_data',
						'function' => '',
						'name'     => 'post_date',
					),
				),
				'where'        => array(
					array(
						'key'      => 'order_items.order_item_type',
						'value'    => 'line_item',
						'operator' => '=',
					),
				),
				'group_by'     => $this->group_by_query,
				'order_by'     => 'post_date ASC',
				'query_type'   => 'get_results',
				'filter_range' => true,
				'order_types'  => wc_get_order_types( 'order-count' ),
				'order_status' => array( 'completed', 'processing', 'on-hold', 'refunded' ),
				'date_query' => array(
	                array(
	                    'after'     => date('Y-m-d', strtotime($start_date)),
	                    'before'    => date('Y-m-d', strtotime($end_date)),
	                    'inclusive' => true,
	                ),
	            ),
			)
		);

		/**
		 * Get total of fully refunded items.
		 */
		$this->report_data->refunded_order_items = absint(
			$this->get_order_report_data(
				array(
					'data'         => array(
						'_qty' => array(
							'type'            => 'order_item_meta',
							'order_item_type' => 'line_item',
							'function'        => 'SUM',
							'name'            => 'order_item_count',
						),
					),
					'where'        => array(
						array(
							'key'      => 'order_items.order_item_type',
							'value'    => 'line_item',
							'operator' => '=',
						),
					),
					'query_type'   => 'get_var',
					'filter_range' => true,
					'order_types'  => wc_get_order_types( 'order-count' ),
					'order_status' => array( 'refunded' ),
					'date_query' => array(
		                array(
		                    'after'     => date('Y-m-d', strtotime($start_date)),
		                    'before'    => date('Y-m-d', strtotime($end_date)),
		                    'inclusive' => true,
		                ),
		            ),
				)
			)
		);

		/**
		 * Order totals by date. Charts should show GROSS amounts to avoid going -ve.
		 */
		$this->report_data->orders = (array) $this->get_order_report_data(
			array(
				'data'         => array(
					'_order_total'        => array(
						'type'     => 'meta',
						'function' => 'SUM',
						'name'     => 'total_sales',
					),
					'_order_shipping'     => array(
						'type'     => 'meta',
						'function' => 'SUM',
						'name'     => 'total_shipping',
					),
					'_order_tax'          => array(
						'type'     => 'meta',
						'function' => 'SUM',
						'name'     => 'total_tax',
					),
					'_order_shipping_tax' => array(
						'type'     => 'meta',
						'function' => 'SUM',
						'name'     => 'total_shipping_tax',
					),
					'post_date'           => array(
						'type'     => 'post_data',
						'function' => '',
						'name'     => 'post_date',
					),
				),
				'group_by'     => $this->group_by_query,
				'order_by'     => 'post_date ASC',
				'query_type'   => 'get_results',
				'filter_range' => true,
				'order_types'  => wc_get_order_types( 'sales-reports' ),
				'order_status' => array( 'completed', 'processing', 'on-hold', 'refunded' ),
				'date_query' => array(
	                array(
	                    'after'     => date('Y-m-d', strtotime($start_date)),
	                    'before'    => date('Y-m-d', strtotime($end_date)),
	                    'inclusive' => true,
	                ),
	            ),
			)
		);

		/**
		 * If an order is 100% refunded we should look at the parent's totals, but the refunds dates.
		 * We also need to ensure each parent order's values are only counted/summed once.
		 */
		$this->report_data->full_refunds = (array) $this->get_order_report_data(
			array(
				'data'                => array(
					'_order_total'        => array(
						'type'     => 'parent_meta',
						'function' => '',
						'name'     => 'total_refund',
					),
					'_order_shipping'     => array(
						'type'     => 'parent_meta',
						'function' => '',
						'name'     => 'total_shipping',
					),
					'_order_tax'          => array(
						'type'     => 'parent_meta',
						'function' => '',
						'name'     => 'total_tax',
					),
					'_order_shipping_tax' => array(
						'type'     => 'parent_meta',
						'function' => '',
						'name'     => 'total_shipping_tax',
					),
					'post_date'           => array(
						'type'     => 'post_data',
						'function' => '',
						'name'     => 'post_date',
					),
				),
				'group_by'            => 'posts.post_parent',
				'query_type'          => 'get_results',
				'filter_range'        => true,
				'order_status'        => false,
				'parent_order_status' => array( 'refunded' ),
				'date_query' => array(
	                array(
	                    'after'     => date('Y-m-d', strtotime($start_date)),
	                    'before'    => date('Y-m-d', strtotime($end_date)),
	                    'inclusive' => true,
	                ),
	            ),
			)
		);

		foreach ( $this->report_data->full_refunds as $key => $order ) {
			$total_refund       = is_numeric( $order->total_refund ) ? $order->total_refund : 0;
			$total_shipping     = is_numeric( $order->total_shipping ) ? $order->total_shipping : 0;
			$total_tax          = is_numeric( $order->total_tax ) ? $order->total_tax : 0;
			$total_shipping_tax = is_numeric( $order->total_shipping_tax ) ? $order->total_shipping_tax : 0;

			$this->report_data->full_refunds[ $key ]->net_refund = $total_refund - ( $total_shipping + $total_tax + $total_shipping_tax );
		}

		/**
		 * Partial refunds. This includes line items, shipping and taxes. Not grouped by date.
		 */
		$this->report_data->partial_refunds = (array) $this->get_order_report_data(
			array(
				'data'                => array(
					'ID'                  => array(
						'type'     => 'post_data',
						'function' => '',
						'name'     => 'refund_id',
					),
					'_refund_amount'      => array(
						'type'     => 'meta',
						'function' => '',
						'name'     => 'total_refund',
					),
					'post_date'           => array(
						'type'     => 'post_data',
						'function' => '',
						'name'     => 'post_date',
					),
					'order_item_type'     => array(
						'type'      => 'order_item',
						'function'  => '',
						'name'      => 'item_type',
						'join_type' => 'LEFT',
					),
					'_order_total'        => array(
						'type'     => 'meta',
						'function' => '',
						'name'     => 'total_sales',
					),
					'_order_shipping'     => array(
						'type'      => 'meta',
						'function'  => '',
						'name'      => 'total_shipping',
						'join_type' => 'LEFT',
					),
					'_order_tax'          => array(
						'type'      => 'meta',
						'function'  => '',
						'name'      => 'total_tax',
						'join_type' => 'LEFT',
					),
					'_order_shipping_tax' => array(
						'type'      => 'meta',
						'function'  => '',
						'name'      => 'total_shipping_tax',
						'join_type' => 'LEFT',
					),
					'_qty'                => array(
						'type'      => 'order_item_meta',
						'function'  => 'SUM',
						'name'      => 'order_item_count',
						'join_type' => 'LEFT',
					),
				),
				'group_by'            => 'refund_id',
				'order_by'            => 'post_date ASC',
				'query_type'          => 'get_results',
				'filter_range'        => true,
				'order_status'        => false,
				'parent_order_status' => array( 'completed', 'processing', 'on-hold' ),
			)
		);

		foreach ( $this->report_data->partial_refunds as $key => $order ) {
			$this->report_data->partial_refunds[ $key ]->net_refund = $order->total_refund - ( $order->total_shipping + $order->total_tax + $order->total_shipping_tax );
		}

		/**
		 * Refund lines - all partial refunds on all order types so we can plot full AND partial refunds on the chart.
		 */
		$this->report_data->refund_lines = (array) $this->get_order_report_data(
			array(
				'data'                => array(
					'ID'                  => array(
						'type'     => 'post_data',
						'function' => '',
						'name'     => 'refund_id',
					),
					'_refund_amount'      => array(
						'type'     => 'meta',
						'function' => '',
						'name'     => 'total_refund',
					),
					'post_date'           => array(
						'type'     => 'post_data',
						'function' => '',
						'name'     => 'post_date',
					),
					'order_item_type'     => array(
						'type'      => 'order_item',
						'function'  => '',
						'name'      => 'item_type',
						'join_type' => 'LEFT',
					),
					'_order_total'        => array(
						'type'     => 'meta',
						'function' => '',
						'name'     => 'total_sales',
					),
					'_order_shipping'     => array(
						'type'      => 'meta',
						'function'  => '',
						'name'      => 'total_shipping',
						'join_type' => 'LEFT',
					),
					'_order_tax'          => array(
						'type'      => 'meta',
						'function'  => '',
						'name'      => 'total_tax',
						'join_type' => 'LEFT',
					),
					'_order_shipping_tax' => array(
						'type'      => 'meta',
						'function'  => '',
						'name'      => 'total_shipping_tax',
						'join_type' => 'LEFT',
					),
					'_qty'                => array(
						'type'      => 'order_item_meta',
						'function'  => 'SUM',
						'name'      => 'order_item_count',
						'join_type' => 'LEFT',
					),
				),
				'group_by'            => 'refund_id',
				'order_by'            => 'post_date ASC',
				'query_type'          => 'get_results',
				'filter_range'        => true,
				'order_status'        => false,
				'parent_order_status' => array( 'completed', 'processing', 'on-hold', 'refunded' ),
			)
		);

		/**
		 * Total up refunds. Note: when an order is fully refunded, a refund line will be added.
		 */
		$this->report_data->total_tax_refunded          = 0;
		$this->report_data->total_shipping_refunded     = 0;
		$this->report_data->total_shipping_tax_refunded = 0;
		$this->report_data->total_refunds               = 0;

		$this->report_data->refunded_orders = array_merge( $this->report_data->partial_refunds, $this->report_data->full_refunds );

		foreach ( $this->report_data->refunded_orders as $key => $value ) {
			$this->report_data->total_tax_refunded          += floatval( $value->total_tax < 0 ? $value->total_tax * -1 : $value->total_tax );
			$this->report_data->total_refunds               += floatval( $value->total_refund );
			$this->report_data->total_shipping_tax_refunded += floatval( $value->total_shipping_tax < 0 ? $value->total_shipping_tax * -1 : $value->total_shipping_tax );
			$this->report_data->total_shipping_refunded     += floatval( $value->total_shipping < 0 ? $value->total_shipping * -1 : $value->total_shipping );

			// Only applies to parial.
			if ( isset( $value->order_item_count ) ) {
				$this->report_data->refunded_order_items += floatval( $value->order_item_count < 0 ? $value->order_item_count * -1 : $value->order_item_count );
			}
		}

		// Totals from all orders - including those refunded. Subtract refunded amounts.
		$this->report_data->total_tax          = wc_format_decimal( array_sum( wp_list_pluck( $this->report_data->orders, 'total_tax' ) ) - $this->report_data->total_tax_refunded, 2 );
		$this->report_data->total_shipping     = wc_format_decimal( array_sum( wp_list_pluck( $this->report_data->orders, 'total_shipping' ) ) - $this->report_data->total_shipping_refunded, 2 );
		$this->report_data->total_shipping_tax = wc_format_decimal( array_sum( wp_list_pluck( $this->report_data->orders, 'total_shipping_tax' ) ) - $this->report_data->total_shipping_tax_refunded, 2 );

		// Total the refunds and sales amounts. Sales subract refunds. Note - total_sales also includes shipping costs.
		$this->report_data->total_sales = wc_format_decimal( array_sum( wp_list_pluck( $this->report_data->orders, 'total_sales' ) ) - $this->report_data->total_refunds, 2 );
		$this->report_data->net_sales   = wc_format_decimal( $this->report_data->total_sales - $this->report_data->total_shipping - max( 0, $this->report_data->total_tax ) - max( 0, $this->report_data->total_shipping_tax ), 2 );

		// Calculate average based on net.
		$this->report_data->average_sales       = wc_format_decimal( $this->report_data->net_sales / ( $this->chart_interval + 1 ), 2 );
		$this->report_data->average_total_sales = wc_format_decimal( $this->report_data->total_sales / ( $this->chart_interval + 1 ), 2 );

		// Total orders and discounts also includes those which have been refunded at some point.
		$this->report_data->total_coupons         = number_format( array_sum( wp_list_pluck( $this->report_data->coupons, 'discount_amount' ) ), 2, '.', '' );
		$this->report_data->total_refunded_orders = absint( count( $this->report_data->full_refunds ) );

		// Total orders in this period, even if refunded.
		$this->report_data->total_orders = absint( array_sum( wp_list_pluck( $this->report_data->order_counts, 'count' ) ) );

		// Item items ordered in this period, even if refunded.
		$this->report_data->total_items = absint( array_sum( wp_list_pluck( $this->report_data->order_items, 'order_item_count' ) ) );

		// 3rd party filtering of report data
		$this->report_data = apply_filters( 'woocommerce_admin_report_data', $this->report_data );

		//echo "total sales: " . $this->report_data->total_sales . "<br />"; matches
		
		// echo "total refunds: " . $this->report_data->total_refunds . "<br />";
		// echo "total shipping: " . $this->report_data->total_shipping . "<br />";
		// echo "total tax: " . $this->report_data->total_tax . "<br />";
		// echo "total shipping tax: " . $this->report_data->total_shipping_tax . "<br />";
		// *! these all return zeros !*


	}


	/**
	 * Constructor.
	 */
	public function __construct() {

        $default_current = new DateTime();
        $filter_start_date_default = $default_current->format('Y') . "-" . $default_current->format('m') . "-01";
        $filter_end_date_default = $default_current->format('Y') . "-" . $default_current->format('m') . "-" . cal_days_in_month(CAL_GREGORIAN, $default_current->format('m'), $default_current->format('y'));

        $this->start_date = ! empty( $_GET['start_date'] ) ? sanitize_text_field( $_GET['start_date'] ) : '';
        $this->end_date = ! empty( $_GET['end_date'] ) ? sanitize_text_field( $_GET['end_date'] ) : '';
        //echo "tasks from: " . $this->start_date . " to: " . $this->end_date . "<br />";  

        $this->range = ! empty( $_GET['range'] ) ? sanitize_text_field( $_GET['range'] ) : 'custom';
		$this->years = ! empty( $_GET['years'] ) ? sanitize_text_field( $_GET['years'] ) : 'custom';

        if ( $this->range == 'q1'){
            //$this->range = 'q1';
            $this->chart_groupby = 'month';
			//$this->start_date = $default_current->format('Y') . "-01-01";
			$this->start_date = $this->years . "-01-01";
            $this->end_date = $this->years . "-03-31";
            $_GET['start_date'] = $this->years . "-01-01";
            $_GET['end_date'] = $this->years . "-03-31";
        }
		if ( $this->range == 'Jan' ){
			//$this->range = 'Jan';
            $this->chart_groupby = 'day';
			$this->start_date = $this->years . "-01-01";
            $this->end_date = $this->years . "-01-31";
			//echo $this->start_date . " " . $this->end_date . "<br />";
            $_GET['start_date'] = $this->years . "-01-01";
            $_GET['end_date'] = $this->years . "-01-31";
		}
		if ( $this->range == 'Feb' ){
			//$this->range = 'Feb';
            $this->chart_groupby = 'day';
			$this->start_date = $this->years . "-02-01";
            $this->end_date = $this->years . "-02-28";
            $_GET['start_date'] = $this->years . "-02-01";
            $_GET['end_date'] = $this->years . "-02-28";
		}
		if ( $this->range == 'Mar' ){
			//$this->range = 'Mar';
            $this->chart_groupby = 'day';
			$this->start_date = $this->years . "-03-01";
            $this->end_date = $this->years . "-03-31";
            $_GET['start_date'] = $this->years . "-03-01";
            $_GET['end_date'] = $this->years . "-03-31";
		}

        if ( $this->range == 'q2' ){
			//$this->range = 'q2';
            $this->chart_groupby = 'month';
            $this->start_date = $this->years . "-04-01";
            $this->end_date = $this->years . "-06-30";
            $_GET['start_date'] = $this->years . "-04-01";
            $_GET['end_date'] = $this->years . "-06-30";
        }
		if ( $this->range == 'Apr' ){
			//$this->range = 'Apr';
            $this->chart_groupby = 'day';
			$this->start_date = $this->years . "-04-01";
            $this->end_date = $this->years . "-04-30";
            $_GET['start_date'] = $this->years . "-04-01";
            $_GET['end_date'] = $this->years . "-04-30";
		}
		if ( $this->range == 'May' ){
			//$this->range = 'May';
            $this->chart_groupby = 'day';
			$this->start_date = $this->years . "-05-01";
            $this->end_date = $this->years . "-05-31";
            $_GET['start_date'] = $this->years . "-05-01";
            $_GET['end_date'] = $this->years . "-05-31";
		}
		if ( $this->range == 'Jun' ){
			//$this->range = 'Jun';
            $this->chart_groupby = 'day';
			$this->start_date = $this->years . "-06-01";
            $this->end_date = $this->years . "-06-30";
            $_GET['start_date'] = $this->years . "-06-01";
            $_GET['end_date'] = $this->years . "-06-30";
		}

        if ( $this->range == 'q3' ){
			//$this->range = 'q3';
            $this->chart_groupby = 'month';
            $this->start_date = $this->years . "-07-01";
            $this->end_date = $this->years . "-09-30";
            $_GET['start_date'] = $this->years . "-07-01";
            $_GET['end_date'] = $this->years . "-09-30";
        }
		if ( $this->range == 'Jul' ){
			//$this->range = 'Jul';
            $this->chart_groupby = 'day';
			$this->start_date = $this->years . "-07-01";
            $this->end_date = $this->years . "-07-31";
            $_GET['start_date'] = $this->years . "-07-01";
            $_GET['end_date'] = $this->years . "-07-31";
		}
		if ( $this->range == 'Aug' ){
			//$this->range = 'Aug';
            $this->chart_groupby = 'day';
			$this->start_date = $this->years . "-08-01";
            $this->end_date = $this->years . "-08-31";
            $_GET['start_date'] = $this->years . "-08-01";
            $_GET['end_date'] = $this->years . "-08-31";
		}
		if ( $this->range == 'Sep' ){
			//$this->range = 'Sep';
            $this->chart_groupby = 'day';
			$this->start_date = $this->years . "-09-01";
            $this->end_date = $this->years . "-09-30";
            $_GET['start_date'] = $this->years . "-09-01";
            $_GET['end_date'] = $this->years . "-09-30";
		}

        if ( $this->range == 'q4' ){
			//$this->range = 'q4';
            $this->chart_groupby = 'month';
            $this->start_date = $this->years . "-10-01";
            $this->end_date = $this->years . "-12-31";
            $_GET['start_date'] = $this->years . "-10-01";
            $_GET['end_date'] = $this->years . "-12-31";
        }
		if ( $this->range == 'Oct' ){
			//$this->range = 'Oct';
            $this->chart_groupby = 'day';
			$this->start_date = $this->years . "-10-01";
            $this->end_date = $this->years . "-10-31";
            $_GET['start_date'] = $this->years . "-10-01";
            $_GET['end_date'] = $this->years . "-10-31";
		}
		if ( $this->range == 'Nov' ){
			//$this->range = 'Nov';
            $this->chart_groupby = 'day';
			$this->start_date = $this->years . "-11-01";
            $this->end_date = $this->years . "-11-30";
            $_GET['start_date'] = $this->years . "-11-01";
            $_GET['end_date'] = $this->years . "-11-30";
		}
		if ( $this->range == 'Dec' ){
			//$this->range = 'Dec';
            $this->chart_groupby = 'day';
			$this->start_date = $this->years . "-12-01";
            $this->end_date = $this->years . "-12-31";
            $_GET['start_date'] = $this->years . "-12-01";
            $_GET['end_date'] = $this->years . "-12-31";
		}

        // if ( $range == 'month' ){
        //     $this->chart_groupby = 'day';
        //     $this->start_date = $filter_start_date_default;
        //     $this->end_date = $filter_end_date_default;
        // }
        if ( $this->range == 'year' ){
			$this->range = 'custom';
			//echo "range: " . $this->years . "<br />"; // works fine ...
            $this->chart_groupby = 'month';
            $this->start_date = $this->years . "-01-01";
            $this->end_date = $this->years . "-12-31";
            $_GET['start_date'] = $this->start_date;
            $_GET['end_date'] = $this->end_date;

			//&range=custom&start_date=2021-01-01&end_date=2021-12-31
        }

        if ( ( $this->range == '' ) && empty( $_GET['start_date'] ) && empty( $_GET['end_date'] ) ){
            $this->range = 'custom';
			$this->chart_groupby = 'month';
            $this->start_date = $filter_start_date_default;
            $this->end_date = $filter_end_date_default;
        }


        /*
        *   Everhour Costs
        *   API limited to 9 Months or 1000 objects returned
        */
        $EH_Projects = new Everhour_Projects();
        $EH_Projects->set_api_key( wp_cache_get( 'api_key' ) );
        $EH_Projects->get_all_projects();
        //$EH_Projects->output_all_projects();
        $all_projects = $EH_Projects->get_projects();

        /* Not cached because parameters (dates) change */
        $EH_Time_Records = new Everhour_Time_Records();
        $EH_Time_Records->set_api_key(wp_cache_get( 'api_key'));

        $filter_start_date_temp = date_create($this->start_date);
        //echo "start_date_temp: " . $filter_start_date_temp . ", ";
        $filter_start_date_month = date_format($filter_start_date_temp,"m");
		

		if ( $this->range == 'Jan' || $this->range == 'q1' || $this->range == 'year' || $this->range == 'custom'){
            //$this->chart_groupby = 'day';
			echo "Year: " . $this->years . "<br />";
			$time_records1 = $EH_Time_Records->retrieve_from_db( $this->years . '-01-01', $this->years . '-01-14');
			$time_records2 = $EH_Time_Records->retrieve_from_db( $this->years . '-01-15', $this->years . '-01-31');				
		}
		if ( $this->range == 'Feb' || $this->range == 'q1' || $this->range == 'year' || $this->range == 'custom' ){
            //$this->chart_groupby = 'day';
			$time_records3 = $EH_Time_Records->retrieve_from_db( $this->years . '-02-01', $this->years . '-02-14');
			$time_records4 = $EH_Time_Records->retrieve_from_db( $this->years . '-02-15', $this->years . '-02-28');
		}
		if ( $this->range == 'Mar' || $this->range == 'q1' || $this->range == 'year' || $this->range == 'custom' ){
            //$this->chart_groupby = 'day';
			$time_records5 = $EH_Time_Records->retrieve_from_db( $this->years . '-03-01', $this->years . '-03-14');
			$time_records6 = $EH_Time_Records->retrieve_from_db( $this->years . '-03-15', $this->years . '-03-31');
        }

		if ( $this->range == 'Apr' || $this->range == 'q2' || $this->range == 'year' || $this->range == 'custom' ){
            $time_records7 = $EH_Time_Records->retrieve_from_db( $this->years . '-04-01', $this->years . '-04-14');
			$time_records8 = $EH_Time_Records->retrieve_from_db( $this->years . '-04-15', $this->years . '-04-30');
		}
		if ( $this->range == 'May' || $this->range == 'q2' || $this->range == 'year' || $this->range == 'custom' ){
			$time_records9 = $EH_Time_Records->retrieve_from_db( $this->years . '-05-01', $this->years . '-05-14');
			$time_records10 = $EH_Time_Records->retrieve_from_db( $this->years . '-05-15', $this->years . '-05-31');
		}
		if ( $this->range == 'Jun' || $this->range == 'q2' || $this->range == 'year' || $this->range == 'custom' ){
			$time_records11 = $EH_Time_Records->retrieve_from_db( $this->years . '-06-01', $this->years . '-06-14');
			$time_records12 = $EH_Time_Records->retrieve_from_db( $this->years . '-06-15', $this->years . '-06-30');
		}

		if ( $this->range == 'Jul' || $this->range == 'q3' || $this->range == 'year' || $this->range == 'custom' ){
            $time_records13 = $EH_Time_Records->retrieve_from_db( $this->years . '-07-01', $this->years . '-07-14');
			$time_records14 = $EH_Time_Records->retrieve_from_db( $this->years . '-07-15', $this->years . '-07-31');
		}
		if ( $this->range == 'Aug' || $this->range == 'q3' || $this->range == 'year' || $this->range == 'custom'){
			$time_records15 = $EH_Time_Records->retrieve_from_db( $this->years . '-08-01', $this->years . '-08-14');
			$time_records16 = $EH_Time_Records->retrieve_from_db( $this->years . '-08-15', $this->years . '-08-31');
		}
		if ( $this->range == 'Sep' || $this->range == 'q3' || $this->range == 'year' || $this->range == 'custom' ){
			$time_records17 = $EH_Time_Records->retrieve_from_db( $this->years . '-09-01', $this->years . '-09-14');
			$time_records18 = $EH_Time_Records->retrieve_from_db( $this->years . '-09-15', $this->years . '-09-30');
		}

		if ( $this->range == 'Oct' || $this->range == 'q4' || $this->range == 'year' || $this->range == 'custom' ){
            $time_records19 = $EH_Time_Records->retrieve_from_db( $this->years . '-10-01', $this->years . '-10-14');
			$time_records20 = $EH_Time_Records->retrieve_from_db( $this->years . '-10-15', $this->years . '-10-31');
		}
		if ( $this->range == 'Nov' || $this->range == 'q4' || $this->range == 'year' || $this->range == 'custom' ){
			$time_records21 = $EH_Time_Records->retrieve_from_db( $this->years . '-11-01', $this->years . '-11-14');
			$time_records22 = $EH_Time_Records->retrieve_from_db( $this->years . '-11-15', $this->years . '-11-30');
		}
		if ( $this->range == 'Dec' || $this->range == 'q4' || $this->range == 'year' || $this->range == 'custom' ){
			$time_records23 = $EH_Time_Records->retrieve_from_db( $this->years . '-12-01', $this->years . '-12-14');
			$time_records24 = $EH_Time_Records->retrieve_from_db( $this->years . '-12-15', $this->years . '-12-31');
		}

		if ( $this->range == 'q1' || $this->range == 'q2' || $this->range == 'q3' || $this->range == 'q4' || $this->range == 'year' ){
			//$this->range = 'custom';
            $this->chart_groupby = 'month';
		}
		//$this->range = 'custom';

		$time_records = $EH_Time_Records->get_time_records();
		//$time_records = $EH_Time_Records->retrieve_from_db($this->start_date, $this->end_date);
        
		//echo "<pre>";
        //print_r($time_records);
		//print_r($all_projects);
		//print_r(unserialize($data_retrived_from_db[0]->data));
		//echo "</pre><br />";

		$marketing_entries = 0;
		$sales_entries = 0;
        // Loop through all task time entries, adjust the date, add workspace
        // Call to API limited dates already for us so all costs returned are used
        if( $time_records != null )
		echo count($time_records) . " records <br />";

		echo '<label for="show_costs">EH Costs Details</label> ';
		if( !isset($_GET['show_costs']) ) {
			echo '<input onchange="location = this.value;" type="checkbox" id="show_costs" name="show_costs" value="' . esc_url( remove_query_arg( array( 'start_date', 'end_date' ), add_query_arg( array( 'range' => $this->range, 'show_costs' => 'true' ) ) ) ) . '"><br />';
		}else{
			echo '<input checked onchange="location = this.value;" type="checkbox" id="show_costs" name="show_costs" value="' . esc_url( remove_query_arg( array( 'start_date', 'end_date', 'show_costs' ), add_query_arg( array( 'range' => $this->range ) ) ) ) . '"><br />';
			echo "<table id='eh_costs_table'>";
			echo	"<tr>
						<th>Date</th>
						<th>Name</th>
						<th>Project ID</th>
						<th>Task ID</th>
						<th>Cost</th>
						<th>Workspace</th>
					</tr>";

		}
		?>
		<style>
			#eh_costs_table {
			font-family: arial, sans-serif;
			border-collapse: collapse;
			width: 100%;
			}

			#eh_costs_table td, th {
			border: 1px solid #dddddd;
			text-align: left;
			padding: 8px;
			}

			#eh_costs_table tr:nth-child(even) {
			background-color: #dddddd;
			}
		</style>
		<?php

        foreach ( $time_records as $record ){
            if( gettype($record) == 'object' ){
				$workspaceMatched = false;
				//echo "project: " . $record->task->projects[0];
				$i = 1;
				foreach($all_projects as $project){
					//if( gettype($record) == 'object' )
					if ( $record->task->projects[0] ==  $project->id ) {
						$record->workspace = $project->workspaceName;
						//echo "workspace set: " . $record->workspace . ", " . $i . " time <br />";
						//break;
						$i++;
					}
				}

				//echo ", date: " . $record->date;
				if( $this->chart_groupby == 'month' ) {
					$record->dateSave = $record->date;
					// $record->recordTemp = substr($record->date, 0, strrpos($record->date, '-', )) . "-01";
					//echo "Record date pre: " . $record->date . ", Record date post: " . $record->recordTemp . "<br />";
					$record->date = strtotime($record->recordTemp) . '000';
					//$record->date = strtotime($record->date->format('Y-m-01')) . '000';
				}else{
					if( $this->chart_groupby == 'day' ) {
						$record->dateSave = $record->date;
						$record->date = strtotime($record->date) . '000';
					}else{
						$this->chart_groupby = 'month';
						$record->dateSave = $record->date;
						//echo "chart groupby isn't month NOR day!? " . $this->chart_groupby . "<br />";
					}
				}
				
				//echo ", sec: " . $record->date . "<br />";
				//echo "<pre>";
				//print_r($record);
				//echo "</pre><br />";

				//echo "workspace to match: " . $record->workspace . "<br />";
				$amount_to_add = $record->cost/100;
				if(isset($_GET['eh_costs'])){
					if( isset($_GET['show_costs']) ){
						echo "<tr><td>" . $record->dateSave . "</td><td>" . $record->task->name . "</td><td>" . $record->task->projects[0] . "</td><td>" . $record->task->id . "</td><td>" . wc_price($amount_to_add) . "</td><td>" . $record->workspace . "</td></tr>";
					}
				switch($record->workspace){
					case 'Operations':
						if( !isset( $this->array_of_operations[$record->date] ) ){
							$this->array_of_operations[$record->date] = array( $record->date, $record->cost/100 );
						}else{
							$date_to_add_to = $this->array_of_operations[$record->date][0];
							$amount_to_add_to = $this->array_of_operations[$record->date][1];
							$new_total = $amount_to_add_to + $amount_to_add;
							$this->array_of_operations[$record->date] = array( $record->date, $new_total );
						}
						$this->total_operations = $this->total_operations + $amount_to_add;
						break;
					case 'Clients':
						if( !isset( $this->array_of_clients[$record->date] ) ){
							$this->array_of_clients[$record->date] = array( $record->date, $record->cost/100 );
						}else{
							$date_to_add_to = $this->array_of_clients[$record->date][0];
							$amount_to_add_to = $this->array_of_clients[$record->date][1];
							$new_total = $amount_to_add_to + $amount_to_add;
							$this->array_of_clients[$record->date] = array( $record->date, $new_total );
						}
						$this->total_clients = $this->total_clients + $amount_to_add;
						break;
					case 'Prospects':
						
						if( !isset( $this->array_of_prospects[$record->date] ) ){
							$this->array_of_prospects[$record->date] = array( $record->date, $record->cost/100 );
						}else{
							$date_to_add_to = $this->array_of_prospects[$record->date][0];
							$amount_to_add_to = $this->array_of_prospects[$record->date][1];
							$new_total = $amount_to_add_to + $amount_to_add;
							$this->array_of_prospects[$record->date] = array( $record->date, $new_total );
						}
						$this->total_prospects = $this->total_prospects + $amount_to_add;
						break;
					case 'Marketing':
						
						if( !isset( $this->array_of_marketing[$record->date] ) ){
							$this->array_of_marketing[$record->date] = array( $record->date, $record->cost/100 );
						}else{
							//print_r($this->array_of_marketing);
							//echo "<br />";
							$date_to_add_to = $this->array_of_marketing[$record->date][0];
							$amount_to_add_to = $this->array_of_marketing[$record->date][1];
							$new_total = $amount_to_add_to + $amount_to_add;
							//echo "date to add to: " . $date_to_add_to . ", amount already saved: " . $amount_to_add_to . ", amount to add: " . $amount_to_add . ", new total should be: " . $new_total . "<br />";
							$this->array_of_marketing[$record->date] = array( $record->date, $new_total );
							//print_r($this->array_of_marketing);
						}
						$this->total_marketing = $this->total_marketing + $amount_to_add;
						break;
					case 'Sales Development':
						// echo "<pre>";
						// print_r($record);
						// echo "</pre>";
						
						if( !isset( $this->array_of_sales[$record->date] ) ){
							$this->array_of_sales[$record->date] = array( $record->date, $record->cost/100 );
						}else{
							$date_to_add_to = $this->array_of_sales[$record->date][0];
							$amount_to_add_to = $this->array_of_sales[$record->date][1];
							$new_total = $amount_to_add_to + $amount_to_add;
							$this->array_of_sales[$record->date] = array( $record->date, $new_total );
						}
						$this->total_sales_development = $this->total_sales_development + $amount_to_add;
						break;
					case 'Dashboard':
						
						if( !isset( $this->array_of_dashboard[$record->date] ) ){
							$this->array_of_dashboard[$record->date] = array( $record->date, $record->cost/100 );
						}else{
							$date_to_add_to = $this->array_of_dashboard[$record->date][0];
							$amount_to_add_to = $this->array_of_dashboard[$record->date][1];
							$new_total = $amount_to_add_to + $amount_to_add;
							$this->array_of_dashboard[$record->date] = array( $record->date, $new_total );
						}
						$this->total_dashboard = $this->total_dashboard + $amount_to_add;
						//add_to_arrays($this->array_of_dashboard, $record->date, $this->total_dashboard, $record->cost/100, $this->end_date );
						break;
					// case 'Misc':
						
					// 	if( !isset( $this->array_of_misc[$record->date] ) ){
					// 		$this->array_of_misc[$record->date] = array( $record->date, $record->cost/100 );
					// 	}else{
					// 		$date_to_add_to = $this->array_of_misc[$record->date][0];
					// 		$amount_to_add_to = $this->array_of_misc[$record->date][1];
					// 		$new_total = $amount_to_add_to + $amount_to_add;
					// 		$this->array_of_misc[$record->date] = array( $record->date, $new_total );
					// 	}
					// 	$this->total_misc = $this->total_misc + $amount_to_add;
					// 	//add_to_arrays($this->array_of_misc, $record->date, $this->total_misc, $record->cost/100, $this->end_date );
					// 	break;
					// default:
						// echo "<pre>";
						// print_r($record);
						// echo "</pre>";
						// echo ", this didn't get added to any Workspace array!?<br />";
				}}

				if(isset($total_cost_without_filters)){
					$total_cost_without_filters = $total_cost_without_filters + $record->cost/100;
				}else{
					$total_cost_without_filters = $record->cost/100;
				}
            }else{
				echo "non object!?<br />"; // none hit this
				print_r($record);
			}
        }
		if( isset($_GET['show_costs']) ){
			echo "</table>";
		}
		//echo "Total marketing projects found: " . count($array_of_marketing_tasks) . "<br />";
		//if(isset($array_of_sales_tasks)){ echo "Total sales projects found: " . count($array_of_sales_tasks) . "<br />"; }
		//if(isset($total_cost_without_filters)){ echo "Total Cost Without Filters and Without Expenses: " . $total_cost_without_filters . "<br />"; }
        /*
        *   Everhour Costs END
        */



        /*
        *   Expenses from Google Sheet
        */
		echo '<label for="show_expenses">GS Expenses Details</label> ';
		if( !isset($_GET['show_expenses']) ) {
			echo '<input onchange="location = this.value;" type="checkbox" id="show_expenses" name="show_expenses" value="' . esc_url( remove_query_arg( array( 'start_date', 'end_date' ), add_query_arg( array( 'range' => $this->range, 'show_expenses' => 'true' ) ) ) ) . '"><br />';
		}else{
			echo '<input checked onchange="location = this.value;" type="checkbox" id="show_expenses" name="show_expenses" value="' . esc_url( remove_query_arg( array( 'start_date', 'end_date', 'show_expenses' ), add_query_arg( array( 'range' => $this->range ) ) ) ) . '"><br />';
			echo "<table id='gs_expenses_table'>";
			echo	"<tr>
						<th>Description</th>
						<th>Amount</th>
						<th>Date</th>
						<th>Sub Category</th>
						<th>Company</th>
					</tr>";

		}
		?>
		<style>
			#gs_expenses_table {
			font-family: arial, sans-serif;
			border-collapse: collapse;
			width: 100%;
			}

			#gs_expenses_table td, th {
			border: 1px solid #dddddd;
			text-align: left;
			padding: 8px;
			}

			#gs_expenses_table tr:nth-child(even) {
			background-color: #dddddd;
			}
		</style>

		<?php
        $Expenses = new Google_Sheet();
        $all_expenses = $Expenses->get_all_expenses();

        foreach ( $all_expenses['expenses_all'] as $expense ){

			if ( !isset( $expense['sub-category'] ) || $expense['sub-category'] == '' ) {
				//Array ( [description] => Wells Fargo - CC Fee [amount] => 4.49 [billing_date] => 9/1/2021 [tier] => [sub-category] => [company] => [company-name] => [category] => [post] => Array ( [0] => Wells Fargo - CC Fee [1] => 9/28/21 [2] => $4.49 [3] => 9/1/2021 [4] => [5] => [6] => [7] => [8] => WF Credit Card | October [9] => Finance charge. [10] => ) )
				//echo $expense['description'] . ": " . $expense['amount'] . "<br />";
				//$expense['sub-category'] = 'Misc';
			}
			
			$check_dates = $Expenses->check_in_range_new($this->start_date, $this->end_date, $expense['billing_date']);
			if( $check_dates && isset($_GET['gs_expenses']) ){

				// example: start date: 2021-09-01
				$filter_start_date_temp = date_create($this->start_date);
				$filter_start_date_month = date_format($filter_start_date_temp,"m");
				$filter_start_date_year = date_format($filter_start_date_temp, "Y");
				//echo "expense_billing_date_month_start: " . $expense_billing_date_month_start . ", ";

				// example: billing date: 1/1/2021
				//echo "expense billing date: " . $expense['billing_date'] . ", ";
				$expense_billing_date_start_original = date_create($expense['billing_date']);
				//echo "expense_billing_date: " . $expense_billing_date_start_original . ", ";
				$expense_billing_date_day = date_format($expense_billing_date_start_original, "d");
				$expense_billing_date_month_start = date_format($expense_billing_date_start_original, "m");

				$expense_billing_date_year = date_format($expense_billing_date_start_original, "Y");
				
				//$expense_billing_date_end_original = $expense['billing_date_end'];

				if( $this->chart_groupby == 'month' ) {
					$expense_billing_date_day = "01";
				}

				$combined = $expense_billing_date_year . "-" . $expense_billing_date_month_start . "-" . $expense_billing_date_day;
				//echo "combined: " . $combined . "<br />";
				$combinedJS = strtotime($combined) . '000';
				// echo $combinedJS . "<br />";
				// echo "amount: " . $expense['amount'];

				
				if( isset($_GET['show_expenses']) ){
					//echo "<tr><td>" . $expense['description'] . "</td><td>$" . $expense['amount'] . "</td><td>" . date_format($expense_billing_date_start_original, "Y-m-d") . "</td><td>" . $expense_billing_date_end_original . "</td><td>" . $combined . "</td><td>" . $expense['repeats'] ."</td><td>" . $expense['sub-category'] . "</td><td>" . $divideIt . "</td><td>" . wc_price($expense['amount']/$divideIt) . "</td></tr>";
					echo "<tr><td>" . $expense['description'] . "</td><td>$" . $expense['amount'] . "</td><td>" . date_format($expense_billing_date_start_original, "Y-m-d") . "</td><td>" . $expense['sub-category'] . "</td><td>" . $expense['company'] . "</td></tr>";
				}

				switch($expense['sub-category']){

					case 'Operations':
						if( !isset( $this->array_of_operations[$combinedJS] ) ){
							$this->array_of_operations[$combinedJS] = array( $combinedJS, $expense['amount'] );
						}else{
							$date_to_add_to = $this->array_of_operations[$combinedJS][0];
							$amount_to_add_to = $this->array_of_operations[$combinedJS][1];
							$new_total = $amount_to_add_to + $expense['amount'];
							$this->array_of_operations[$combinedJS] = array( $combinedJS, $new_total );
						}
						$this->total_operations = $this->total_operations + $expense['amount'];
						break;

					case 'Clients':
						if( !isset( $this->array_of_clients[$combinedJS] ) ){
							$this->array_of_clients[$combinedJS] = array( $combinedJS, $expense['amount'] );
						}else{
							$date_to_add_to = $this->array_of_clients[$combinedJS][0];
							$amount_to_add_to = $this->array_of_clients[$combinedJS][1];
							$new_total = $amount_to_add_to + $expense['amount'];
							$this->array_of_clients[$combinedJS] = array( $combinedJS, $new_total );
						}
						$this->total_clients = $this->total_clients + $expense['amount'];
						//add_to_arrays($this->array_of_clients, $combinedJS, $this->total_clients, $expense['amount']/$divideIt, $this->end_date);
						break;

					case 'Prospects':
						//echo $expense['amount'] . "<br />";
						if( !isset( $this->array_of_prospects[$combinedJS] ) ){
							//echo "new" . "<br />";
							//echo $combined . "<br />";
							$this->array_of_prospects[$combinedJS] = array( $combinedJS, $expense['amount'] );
						}else{
							//echo "exists" . "<br />";
							//echo $combined . "<br />";
							$date_to_add_to = $this->array_of_prospects[$combinedJS][0];
							$amount_to_add_to = $this->array_of_prospects[$combinedJS][1];
							$new_total = $amount_to_add_to + $expense['amount'];
							$this->array_of_prospects[$combinedJS] = array( $combinedJS, $new_total );
						}
						$this->total_prospects = $this->total_prospects + $expense['amount'];
						break;

					case 'Marketing':
						if( !isset( $this->array_of_marketing[$combinedJS] ) ){
							$this->array_of_marketing[$combinedJS] = array( $combinedJS, $expense['amount'] );
						}else{
							$date_to_add_to = $this->array_of_marketing[$combinedJS][0];
							$amount_to_add_to = $this->array_of_marketing[$combinedJS][1];
							$new_total = $amount_to_add_to + $expense['amount'];
							$this->array_of_marketing[$combinedJS] = array( $combinedJS, $new_total );
						}
						$this->total_marketing = $this->total_marketing + $expense['amount'];
						break;

					case 'Sales Development':
						if( !isset( $this->array_of_sales[$combinedJS] ) ){
							$this->array_of_sales[$combinedJS] = array( $combinedJS, $expense['amount'] );
						}else{
							$date_to_add_to = $this->array_of_sales[$combinedJS][0];
							$amount_to_add_to = $this->array_of_sales[$combinedJS][1];
							$new_total = $amount_to_add_to + $expense['amount'];
							$this->array_of_sales[$combinedJS] = array( $combinedJS, $new_total );
						}
						$this->total_sales_development = $this->total_sales_development + $expense['amount'];
						break;

					case 'Dashboard':
						if( !isset( $this->array_of_dashboard[$combinedJS] ) ){
							$this->array_of_dashboard[$combinedJS] = array( $combinedJS, $expense['amount'] );
						}else{
							$date_to_add_to = $this->array_of_dashboard[$combinedJS][0];
							$amount_to_add_to = $this->array_of_dashboard[$combinedJS][1];
							$new_total = $amount_to_add_to + $expense['amount'];
							$this->array_of_dashboard[$combinedJS] = array( $combinedJS, $new_total );
						}
						$this->total_dashboard = $this->total_dashboard + $expense['amount'];
						break;

					// case 'Misc':
					// 	if( !isset( $this->array_of_misc[$combinedJS] ) ){
					// 		$this->array_of_misc[$combinedJS] = array( $combinedJS, $expense['amount'] );
					// 	}else{
					// 		$date_to_add_to = $this->array_of_misc[$combinedJS][0];
					// 		$amount_to_add_to = $this->array_of_misc[$combinedJS][1];
					// 		$new_total = $amount_to_add_to + $expense['amount'];
					// 		$this->array_of_misc[$combinedJS] = array( $combinedJS, $new_total );
					// 	}
					// 	$this->total_misc = $this->total_misc + $expense['amount'];
					// 	break;
				}
			}
        }
		if( isset($_GET['show_expenses']) ){
			echo "</table>";
		}

        $this->total_cost = $this->total_operations +
                            $this->total_clients +
                            $this->total_prospects +
                            $this->total_marketing +
                            $this->total_sales_development +
                            $this->total_dashboard;
							//+ $this->total_misc;		
	}

	/**
	 * Get the legend for the main chart sidebar.
	 *
	 * @return array
	 */
	public function get_chart_legend() {

		$legend = array();
        $data   = $this->get_report_data();
		if($data->net_sales == 0){
			$data->net_sales = 1;
		}

        $legend[] = array(
			'title'            =>	'<strong>' . wc_price( $data->net_sales ) . '</strong>Revenue<br />' .
									'Total Cost: ' . wc_price( $this->total_cost ) . '<br />' .
									number_format( (float)$this->total_cost/$data->net_sales * 100, 2, '.', '' ) . '% of revenue<br />' .
									'Profit: ' . wc_price( $data->net_sales - $this->total_cost ),
			'placeholder'      =>	__( 'This is the sum of the order totals after any refunds and excluding shipping and taxes.', 'woocommerce' ),
			'color'            =>	$this->chart_colours['net_sales_amount'],
			'highlight_series' =>	0,
		);
		// if ( $data->average_sales > 0 ) {
		// 	$legend[] = array(
		// 		'title'            => $average_sales_title,
		// 		'color'            => $this->chart_colours['net_average'],
		// 		'highlight_series' => 3,
		// 	);
		// }


        if($data->net_sales != 0 && $this->total_cost != 0)
        $legend[] = array(
			/* translators: %s: discount amount */
			'title'            =>   '<strong>' . wc_price( $this->total_operations ) . '</strong>Operations<br />' .
                                    number_format( (float)$this->total_operations/$data->net_sales * 100, 2, '.', '' ) . '% of revenue<br />' .
                                    number_format( (float)$this->total_operations/$this->total_cost * 100, 2, '.', '' ) . '% of cost',
			'color'            => $this->chart_colours['operations'],
			'highlight_series' => 1,
		);

        if($data->net_sales != 0 && $this->total_cost != 0)
        $legend[] = array(
			'title'            =>   '<strong>' . wc_price( $this->total_clients ) . '</strong>Clients<br />' .
                                    number_format( (float)$this->total_clients/$data->net_sales * 100, 2, '.', '' ) . '% of revenue<br />' .
                                    number_format( (float)$this->total_clients/$this->total_cost * 100, 2, '.', '' ) . '% of cost',
			'color'            => $this->chart_colours['clients'],
			'highlight_series' => 2,
		);

        if($data->net_sales != 0 && $this->total_cost != 0)
        $legend[] = array(
			'title'            =>   '<strong>' . wc_price( $this->total_prospects ) . '</strong>Prospects<br />' .
                                    number_format( (float)$this->total_prospects/$data->net_sales * 100, 2, '.', '' ) . '% of revenue<br />' .
                                    number_format( (float)$this->total_prospects/$this->total_cost * 100, 2, '.', '' ) . '% of cost',
			'color'            => $this->chart_colours['prospects'],
			'highlight_series' => 3,
		);

        if($data->net_sales != 0 && $this->total_cost != 0)
        $legend[] = array(
			'title'            =>   '<strong>' . wc_price( $this->total_marketing ) . '</strong>Marketing<br />' .
                                    number_format( (float)$this->total_marketing/$data->net_sales * 100, 2, '.', '' ) . '% of revenue<br />' .
                                    number_format( (float)$this->total_marketing/$this->total_cost * 100, 2, '.', '' ) . '% of cost',
			'color'            => $this->chart_colours['marketing'],
			'highlight_series' => 4,
		);

        if($data->net_sales != 0 && $this->total_cost != 0)
        $legend[] = array(
			'title'            =>   '<strong>' . wc_price( $this->total_sales_development ) . '</strong>Sales<br />' .
                                    number_format( (float)$this->total_sales_development/$data->net_sales * 100, 2, '.', '' ) . '% of revenue<br />' .
                                    number_format( (float)$this->total_sales_development/$this->total_cost * 100, 2, '.', '' ) . '% of cost',
			'color'            => $this->chart_colours['sales'],
			'highlight_series' => 5,
		);

        if($data->net_sales != 0 && $this->total_cost != 0)
        $legend[] = array(
			'title'            =>   '<strong>' . wc_price( $this->total_dashboard ) . '</strong>Dashboard<br />' .
                                    number_format( (float)$this->total_dashboard/$data->net_sales * 100, 2, '.', '' ) . '% of revenue<br />' .
                                    number_format( (float)$this->total_dashboard/$this->total_cost * 100, 2, '.', '' ) . '% of cost',
			'color'            => $this->chart_colours['dashboard'],
			'highlight_series' => 6,
		);

		// if($data->net_sales != 0 && $this->total_cost != 0)
		// $legend[] = array(
		// 	'title'            =>   '<strong>' . wc_price( $this->total_misc ) . '</strong>Misc<br />' .
        //                             number_format( (float)$this->total_misc/$data->net_sales * 100, 2, '.', '' ) . '% of revenue<br />' .
        //                             number_format( (float)$this->total_misc/$this->total_cost * 100, 2, '.', '' ) . '% of cost',
		// 	'color'            => $this->chart_colours['misc'],
		// 	'highlight_series' => 7,
		// );

		return $legend;
	}


    /**
	 * Get the current range and calculate the start and end dates.
     * **!
     * Overwritten from class-wc-admin-report to suport Quarters
	 * **!
	 * @param  string $current_range
	 */
	public function calculate_current_range( $current_range ) {
		//echo "current range: " . $current_range . "<br />";
		switch ( $current_range ) {

			case 'q1':case'q2':case'q3':case 'q4':
			case 'Jan': case 'Feb': case 'Mar': case 'Apr': case 'May': case'Jun':
			case 'Jul': case 'Aug': case 'Sep': case 'Oct': case 'Nov': case 'Dec':
			case 'custom':
				$this->start_date = max( strtotime( '-20 years' ), strtotime( $this->start_date ) );

				$this->end_date = strtotime( 'midnight', strtotime( $this->end_date ) );

				$interval = 0;
				$min_date = $this->start_date;

				while ( ( $min_date = strtotime( '+1 MONTH', $min_date ) ) <= $this->end_date ) {
					$interval ++;
				}

				//echo "interval: " . $interval . "<br />";
				// 3 months max for day view
				if ( $interval > 1 ) {
					$this->chart_groupby = 'month';
					//echo "chart_groupby set line 1155 to month";
				} else {
					$this->chart_groupby = 'day';
					//echo "chart_groupby set line 1158 to day";
				}
                // echo "custom: <br />";
                // echo "interval result: " . $interval . "<br />";
                // echo "grouby:" . $this->chart_groupby . "<br />";
				break;

			case 'year':
				$this->start_date    = strtotime( date( 'Y-01-01', current_time( 'timestamp' ) ) );
				$this->end_date      = strtotime( 'midnight', current_time( 'timestamp' ) );
				$this->chart_groupby = 'month';
                // echo "year: <br />";
                // echo "interval result: " . $interval . "<br />";
                // echo "grouby:" . $this->chart_groupby . "<br />";
				break;

			// case 'last_month':
			// 	$first_day_current_month = strtotime( date( 'Y-m-01', current_time( 'timestamp' ) ) );
			// 	$this->start_date        = strtotime( date( 'Y-m-01', strtotime( '-1 DAY', $first_day_current_month ) ) );
			// 	$this->end_date          = strtotime( date( 'Y-m-t', strtotime( '-1 DAY', $first_day_current_month ) ) );
			// 	$this->chart_groupby     = 'day';
			// 	break;

			// case 'month':
			// 	$this->start_date    = strtotime( date( 'Y-m-01', current_time( 'timestamp' ) ) );
			// 	$this->end_date      = strtotime( 'midnight', current_time( 'timestamp' ) );
			// 	$this->chart_groupby = 'day';
            //     // echo "month: <br />";
            //     // echo "interval result: " . $interval . "<br />";
            //     // echo "grouby:" . $this->chart_groupby . "<br />";
			// 	break;

			// case '7day':
			// 	$this->start_date    = strtotime( '-6 days', strtotime( 'midnight', current_time( 'timestamp' ) ) );
			// 	$this->end_date      = strtotime( 'midnight', current_time( 'timestamp' ) );
			// 	$this->chart_groupby = 'day';
			// 	break;
		}

		// Group by
		switch ( $this->chart_groupby ) {

			case 'day':
				$this->group_by_query = 'YEAR(posts.post_date), MONTH(posts.post_date), DAY(posts.post_date)';
				$this->chart_interval = absint( ceil( max( 0, ( $this->end_date - $this->start_date ) / ( 60 * 60 * 24 ) ) ) );
				$this->barwidth       = 60 * 60 * 24 * 1000;
                //echo "chart interval: " . $this->chart_interval . "<br />";
				break;

			case 'month':
				$this->group_by_query = 'YEAR(posts.post_date), MONTH(posts.post_date)';
				$this->chart_interval = 0;
				$min_date             = strtotime( date( 'Y-m-01', $this->start_date ) );

				while ( ( $min_date = strtotime( '+1 MONTH', $min_date ) ) < $this->end_date ) {
					$this->chart_interval ++;
				}

				$this->barwidth = 60 * 60 * 24 * 7 * 4 * 1000;
                //echo "chart interval: " . $this->chart_interval . "<br />";
				break;
		}
	}

	/**
	 * Output the report.
	 */
	public function output_report() {
        
		$ranges = array(
			'year'  => __( 'Year', 'woocommerce' ),
            'q1'    => __( 'Jan - Mar', 'woocommerce' ),
            'q2'    => __( 'Apr - Jun', 'woocommerce' ),
			'q3'    => __( 'Jul - Sept', 'woocommerce' ),
            'q4'    =>  __( 'Oct - Dec', 'woocommerce' ),
			// 'month' => __( 'This month', 'woocommerce' ),
		);
		$ranges_months = array(
			'Jan'	=>	__( 'Jan', 'woocommerce' ),
			'Feb'	=>	__( 'Feb', 'woocommerce' ),
			'Mar'	=>	__( 'Mar', 'woocommerce' ),
			'Apr'	=>	__( 'Apr', 'woocommerce' ),
			'May'	=>	__( 'May', 'woocommerce' ),
			'Jun'	=>	__( 'Jun', 'woocommerce' ),
			'Jul'	=>	__( 'Jul', 'woocommerce' ),
			'Aug'	=>	__( 'Aug', 'woocommerce' ),
			'Sep'	=>	__( 'Sep', 'woocommerce' ),
			'Oct'	=>	__( 'Oct', 'woocommerce' ),
			'Nov'	=>	__( 'Nov', 'woocommerce' ),
			'Dec'	=>	__( 'Dec', 'woocommerce' ),
		);
		$ranges_years = array(
			'2021'	=>	__( '2021', 'woocommerce' ),
			'2022'	=>	__( '2022', 'woocommerce' ),
			'2023'	=>	__( '2023', 'woocommerce' ),
		);


		$this->chart_colours = array(
            'net_sales_amount' => '#F2EFEB',
			'net_average'      => '#3498db',
			'operations'    =>  '#2F2D66',
			'clients'       =>  '#FADA3D',
            'prospects'     =>  '#FFB915',
            'marketing'     =>  '#56D7AF',
            'sales'         =>  '#3DC7F2',
            'dashboard'     =>  '#7B68EE',
			//'misc'			=>	'#00FC2F',
		);
		?>
		<style>
			.woocommerce-reports-wide .postbox .chart-legend li:hover, .woocommerce-reports-wrap .postbox .chart-legend li:hover {
    			border-right: 5px solid #FB5F82!important;
			}
		</style>

	<?php
		$current_range = $this->range;

		echo "{ ";
		echo "group by " . $this->chart_groupby . ", ";
		echo $current_range . ", ";
		echo $this->start_date . "->";
		echo $this->end_date . "}<br />";

		//$this->check_current_range_nonce( $current_range );
		$this->calculate_current_range( $current_range, $this->start_date, $this->end_date );

		require_once( get_template_directory() . '/woocommerce/html-report-by-date-workspaces-to-sub-categories.php');
	}

	
	

	/**
	 * Output an export link.
	 */
	public function get_export_button() {
		$current_range = ! empty( $_GET['range'] ) ? sanitize_text_field( wp_unslash( $_GET['range'] ) ) : '7day';
		?>
		<a
			href="#"
			download="report-<?php echo esc_attr( $current_range ); ?>-<?php echo esc_attr( date_i18n( 'Y-m-d', current_time( 'timestamp' ) ) ); ?>.csv"
			class="export_csv"
			data-export="chart"
			data-xaxes="<?php esc_attr_e( 'Date', 'woocommerce' ); ?>"
			data-groupby="<?php echo esc_attr( $this->chart_groupby ); ?>"
		>
			<?php esc_html_e( 'Export CSV', 'woocommerce' ); ?>
		</a>
		<?php
	}


    /**
	 * Round our totals correctly.
	 *
	 * @param array|string $amount Chart total.
	 *
	 * @return array|string
	 */
	private function round_chart_totals( $amount ) {
		if ( is_array( $amount ) ) {
			return array( $amount[0], wc_format_decimal( $amount[1], wc_get_price_decimals() ) );
		} else {
			return wc_format_decimal( $amount, wc_get_price_decimals() );
		}
	}


	/**
	 * Get the main chart.
	 */
	public function get_main_chart() {
		global $wp_locale;
        

        $data = array(
			'order_counts'         => $this->prepare_chart_data( $this->report_data->order_counts, 'post_date', 'count', $this->chart_interval, $this->start_date, $this->chart_groupby ),
			'order_item_counts'    => $this->prepare_chart_data( $this->report_data->order_items, 'post_date', 'order_item_count', $this->chart_interval, $this->start_date, $this->chart_groupby ),
			'order_amounts'        => $this->prepare_chart_data( $this->report_data->orders, 'post_date', 'total_sales', $this->chart_interval, $this->start_date, $this->chart_groupby ),
			'coupon_amounts'       => $this->prepare_chart_data( $this->report_data->coupons, 'post_date', 'discount_amount', $this->chart_interval, $this->start_date, $this->chart_groupby ),
			'shipping_amounts'     => $this->prepare_chart_data( $this->report_data->orders, 'post_date', 'total_shipping', $this->chart_interval, $this->start_date, $this->chart_groupby ),
			'refund_amounts'       => $this->prepare_chart_data( $this->report_data->refund_lines, 'post_date', 'total_refund', $this->chart_interval, $this->start_date, $this->chart_groupby ),
			'net_refund_amounts'   => $this->prepare_chart_data( $this->report_data->refunded_orders, 'post_date', 'net_refund', $this->chart_interval, $this->start_date, $this->chart_groupby ),
			'shipping_tax_amounts' => $this->prepare_chart_data( $this->report_data->orders, 'post_date', 'total_shipping_tax', $this->chart_interval, $this->start_date, $this->chart_groupby ),
			'tax_amounts'          => $this->prepare_chart_data( $this->report_data->orders, 'post_date', 'total_tax', $this->chart_interval, $this->start_date, $this->chart_groupby ),
			'net_order_amounts'    => $this->prepare_chart_data( $this->report_data->orders, 'post_date', 'total_sales', $this->chart_interval, $this->start_date, $this->chart_groupby ), //array(),
			'gross_order_amounts'  => array(),
		);

        foreach ( $data['order_amounts'] as $order_amount_key => $order_amount_value ) {
			$data['gross_order_amounts'][ $order_amount_key ]     = $order_amount_value;
			$data['gross_order_amounts'][ $order_amount_key ][1] -= $data['refund_amounts'][ $order_amount_key ][1];

			$data['net_order_amounts'][ $order_amount_key ] = $order_amount_value;
			// Subtract the sum of the values from net order amounts.
			$data['net_order_amounts'][ $order_amount_key ][1] -=
				$data['net_refund_amounts'][ $order_amount_key ][1] +
				$data['shipping_amounts'][ $order_amount_key ][1] +
				$data['shipping_tax_amounts'][ $order_amount_key ][1] +
				$data['tax_amounts'][ $order_amount_key ][1];
		}

        ksort($this->array_of_operations);
        ksort($this->array_of_clients);
        ksort($this->array_of_prospects);
        ksort($this->array_of_marketing);
        ksort($this->array_of_sales);
        ksort($this->array_of_dashboard);
		//ksort($this->array_of_misc);
		
		// array_walk($this->array_of_operations, function (&$el) {
		// 	$el = round($el, 2);
		// });

        // Encode in json format.
		$chart_data = wp_json_encode(
			array(               
                'order_counts'        	=> array_values( $data['order_counts'] ),
				'order_item_counts'   	=> array_values( $data['order_item_counts'] ),
				'order_amounts'       	=> array_map( array( $this, 'round_chart_totals' ), array_values( $data['order_amounts'] ) ),
				'gross_order_amounts' 	=> array_map( array( $this, 'round_chart_totals' ), array_values( $data['gross_order_amounts'] ) ),
				'net_order_amounts'   	=> array_map( array( $this, 'round_chart_totals' ), array_values( $data['net_order_amounts'] ) ),
				'shipping_amounts'    	=> array_map( array( $this, 'round_chart_totals' ), array_values( $data['shipping_amounts'] ) ),
				'coupon_amounts'      	=> array_map( array( $this, 'round_chart_totals' ), array_values( $data['coupon_amounts'] ) ),
				'refund_amounts'      	=> array_map( array( $this, 'round_chart_totals' ), array_values( $data['refund_amounts'] ) ),
				'array_of_operations' 	=> array_map( array( $this, 'round_chart_totals' ), array_values( $this->array_of_operations ) ),
                'array_of_clients'    	=> array_map( array( $this, 'round_chart_totals' ), array_values( $this->array_of_clients ) ),
                'array_of_prospects'  	=> array_map( array( $this, 'round_chart_totals' ), array_values( $this->array_of_prospects ) ),
                'array_of_marketing'  	=> array_map( array( $this, 'round_chart_totals' ), array_values( $this->array_of_marketing ) ),
                'array_of_sales'      	=> array_map( array( $this, 'round_chart_totals' ), array_values( $this->array_of_sales ) ),
                'array_of_dashboard'  	=> array_map( array( $this, 'round_chart_totals' ), array_values( $this->array_of_dashboard ) ),
				//'array_of_misc'			=> array_map( array( $this, 'round_chart_totals' ), array_values( $this->array_of_misc ) )
			)
		);

		?>
		<div class="chart-container">
			<div class="chart-placeholder main"></div>
		</div>
		<script type="text/javascript">
			var main_chart;

			jQuery(function(){
				var order_data = JSON.parse( decodeURIComponent( '<?php echo rawurlencode( $chart_data ); ?>' ) );

				var drawGraph = function( highlight ) {
					var series = [
                        {
							label: "<?php echo esc_js( __( 'Revenue', 'woocommerce' ) ); ?>",
							data: order_data.net_order_amounts,
							color: '<?php echo esc_js( $this->chart_colours['net_sales_amount'] ); ?>',
							bars: { fillColor: '<?php echo esc_js( $this->chart_colours['net_sales_amount'] ); ?>', fill: true, show: true, lineWidth: 0, barWidth: <?php echo esc_js( $this->barwidth ); ?> * 0.5, align: 'center' },
							points: { show: true, radius: 5, lineWidth: 3, fillColor: '#fff', fill: true },
                            shadowSize: 0,
                            hoverable: true,
						},
						{
							label: "<?php echo esc_js( __( 'Operations', 'woocommerce' ) ); ?>",
							data: order_data.array_of_operations,
                            yaxis: 2,
							color: '<?php echo esc_js( $this->chart_colours['operations'] ); ?>',
							points: { show: true, radius: 5, lineWidth: 3, fillColor: '#fff', fill: true },
							lines: { show: true, lineWidth: 4, fill: false },
                            shadowSize: 0,
                            <?php echo $this->get_currency_tooltip(); ?><?php // @codingStandardsIgnoreLine ?>
						},
                        {
							label: "<?php echo esc_js( __( 'Clients', 'woocommerce' ) ); ?>",
							data: order_data.array_of_clients,
							yaxis: 2,
							color: '<?php echo esc_js( $this->chart_colours['clients'] ); ?>',
							points: { show: true, radius: 5, lineWidth: 3, fillColor: '#fff', fill: true },
							lines: { show: true, lineWidth: 4, fill: false },
							shadowSize: 0,
							<?php echo $this->get_currency_tooltip(); ?><?php // @codingStandardsIgnoreLine ?>
						},
                        {
							label: "<?php echo esc_js( __( 'Prospects', 'woocommerce' ) ); ?>",
							data: order_data.array_of_prospects,
							yaxis: 2,
							color: '<?php echo esc_js( $this->chart_colours['prospects'] ); ?>',
							points: { show: true, radius: 5, lineWidth: 3, fillColor: '#fff', fill: true },
							lines: { show: true, lineWidth: 4, fill: false },
							shadowSize: 0,
							<?php echo $this->get_currency_tooltip(); ?><?php // @codingStandardsIgnoreLine ?>
						},
                        {
							label: "<?php echo esc_js( __( 'Marketing', 'woocommerce' ) ); ?>",
							data: order_data.array_of_marketing,
							yaxis: 2,
							color: '<?php echo esc_js( $this->chart_colours['marketing'] ); ?>',
							points: { show: true, radius: 5, lineWidth: 3, fillColor: '#fff', fill: true },
							lines: { show: true, lineWidth: 4, fill: false },
							shadowSize: 0,
							<?php echo $this->get_currency_tooltip(); ?><?php // @codingStandardsIgnoreLine ?>
						},
                        {
							label: "<?php echo esc_js( __( 'Sales', 'woocommerce' ) ); ?>",
							data: order_data.array_of_sales,
							yaxis: 2,
							color: '<?php echo esc_js( $this->chart_colours['sales'] ); ?>',
							points: { show: true, radius: 5, lineWidth: 3, fillColor: '#fff', fill: true },
							lines: { show: true, lineWidth: 4, fill: false },
							shadowSize: 0,
							<?php echo $this->get_currency_tooltip(); ?><?php // @codingStandardsIgnoreLine ?>
						},
                        {
							label: "<?php echo esc_js( __( 'Dashboard', 'woocommerce' ) ); ?>",
							data: order_data.array_of_dashboard,
							yaxis: 2,
							color: '<?php echo esc_js( $this->chart_colours['dashboard'] ); ?>',
							points: { show: true, radius: 5, lineWidth: 3, fillColor: '#fff', fill: true },
							lines: { show: true, lineWidth: 4, fill: false },
							shadowSize: 0,
							<?php echo $this->get_currency_tooltip(); ?><?php // @codingStandardsIgnoreLine ?>
						},
						// {
						// 	label: "<?php echo esc_js( __( 'Misc', 'woocommerce' ) ); ?>",
						// 	data: order_data.array_of_misc,
						// 	yaxis: 2,
						// 	color: '<?php echo esc_js( $this->chart_colours['misc'] ); ?>',
						// 	points: { show: true, radius: 5, lineWidth: 3, fillColor: '#fff', fill: true },
						// 	lines: { show: true, lineWidth: 4, fill: false },
						// 	shadowSize: 0,
						// 	<?php echo $this->get_currency_tooltip(); ?><?php // @codingStandardsIgnoreLine ?>
						// }
					];

					if ( highlight !== 'undefined' && series[ highlight ] ) {
						highlight_series = series[ highlight ];

						highlight_series.color = '#FB5F82';

						if ( highlight_series.bars )
							highlight_series.bars.fillColor = '#FB5F82';

						if ( highlight_series.lines ) {
							highlight_series.lines.lineWidth = 5;
						}
					}

					main_chart = jQuery.plot(
						jQuery('.chart-placeholder.main'),
						series,
						{
							legend: {
								show: false
							},
							grid: {
								color: '#aaa',
								borderColor: 'transparent',
								borderWidth: 0,
								hoverable: true
							},
							xaxes: [ {
								color: '#aaa',
								position: "bottom",
								tickColor: 'transparent',
								mode: "time",
								timeformat: "<?php echo ( 'day' === $this->chart_groupby ) ? '%d %b' : '%b'; ?>",
								monthNames: JSON.parse( decodeURIComponent( '<?php echo rawurlencode( wp_json_encode( array_values( $wp_locale->month_abbrev ) ) ); ?>' ) ),
								tickLength: 1,
								minTickSize: [1, "<?php echo esc_js( $this->chart_groupby ); ?>"],
								font: {
									color: "#aaa"
								}
							} ],
							yaxes: [
								{
									min: 0,
									minTickSize: 1,
									tickDecimals: 0,
									color: '#ecf0f1',
									font: { color: "#aaa" }
								},
								{
									position: "right",
									min: 0,
									tickDecimals: 2,
									alignTicksWithAxis: 1,
									color: 'transparent',
									font: { color: "#aaa" }
								}
							],
						}
					);

					jQuery('.chart-placeholder').trigger( 'resize' );
				}

				drawGraph();

				jQuery('.highlight_series').on( 'mouseenter',
					function() {
						drawGraph( jQuery(this).data('series') );
					} ).on( 'mouseleave',
					function() {
						drawGraph();
					}
				);
			});
		</script>
		<?php
	}
}

