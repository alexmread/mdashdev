<?php
/*
*   Custom Woocommerce Report to add a column and show Costs from Everhour
*/

$plugin_dir = ABSPATH . 'wp-content/plugins/custom-everhour/';
//require_once( $plugin_dir . 'inc/everhour-invoices.class.php' );
require_once( $plugin_dir . 'inc/everhour-reports.class.php' );
require_once( $plugin_dir . 'inc/everhour-clients.class.php' );
require_once( $plugin_dir . 'inc/everhour-projects.class.php' );
require_once( $plugin_dir . 'inc/everhour-time-records.class.php' );
require_once( $plugin_dir . 'inc/everhour-users.class.php' );
require_once( $plugin_dir . 'inc/expenses.class.php');
//require_once( $plugin_dir . 'inc/everhour-output-profitability-table.class.php' ); // renamed output-line-items.class.php below
require_once( $plugin_dir . 'inc/everhour-output-divisions-table.class.php' );
//require_once( $plugin_dir . 'inc/woocommerce-functions.class.php' );
require_once( $plugin_dir . 'inc/google-sheet.class.php');


require_once( $plugin_dir . '/inc/company-collection.class.php');
require_once( $plugin_dir . '/inc/company.class.php');
require_once( $plugin_dir . '/inc/line-item.class.php');
require_once( $plugin_dir . '/inc/category-counter.class.php');
require_once( $plugin_dir . '/inc/output-line-items.class.php' );
require_once( $plugin_dir . '/inc/company-category-lookup.class.php' );


include_once WC()->plugin_path().'/includes/admin/reports/class-wc-admin-report.php';

add_filter( 'woocommerce_admin_reports', 'woocommerce_profitability', 10, 1 );
function woocommerce_profitability( $reports ) {
    $reports['profitability'] = array(
        'title'  => __( 'Profitability', 'woocommerce' ),
        'reports' => array(
            // 'order_cost' => array(
            //     'title'       => __( 'Order to Cost', 'woocommerce' ),
            //     'description' => '',
            //     'hide_title'  => true,
            //     'callback'    => 'order_to_cost_callback',
            // ),
            'custom_report' => array(
                'title'       => __( 'Profitability', 'woocommerce' ),
                'description' => '',
                'hide_title'  => true,
                'callback'    => 'profitability_to_cost_callback',
            ),
        ),
    );

    return $reports;
}

add_filter( 'woocommerce_admin_reports', 'woocommerce_workspaces', 10, 1 );
function woocommerce_workspaces( $reports ) {
    $reports['workspaces'] = array(
        'title'  => __( 'Divisions', 'woocommerce' ),
        'reports' => array(
            'custom_report' => array(
                'title'       => __( 'Workspaces', 'woocommerce' ),
                'description' => '',
                'hide_title'  => true,
                'callback'    => 'workspaces_to_cost_callback',
            ),
        ),
    );

    return $reports;
}

add_filter( 'woocommerce_admin_reports', 'woocommerce_batch', 10, 1 );
function woocommerce_batch( $reports ) {
    $reports['batch'] = array(
        'title'  => __( 'Batch', 'woocommerce' ),
        'reports' => array(
            'custom_report' => array(
                'title'       => __( 'Batch', 'woocommerce' ),
                'description' => '',
                'hide_title'  => true,
                'callback'    => 'batch_callback',
            ),
        ),
    );

    return $reports;
}

//require_once( get_template_directory() . '/woocommerce/order-cost.php');
require_once( get_template_directory() . '/woocommerce/profitability.php');
require_once( get_template_directory() . '/woocommerce/workspaces.php');
require_once( get_template_directory() . '/woocommerce/batch.php');