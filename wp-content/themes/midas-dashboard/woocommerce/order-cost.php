<?php

function order_to_cost_callback() {

    $report = new WC_Report_Everhour_Cost();
    $report->output_report();
}

include_once(WC()->plugin_path().'/includes/admin/reports/class-wc-admin-report.php');

class WC_Report_Everhour_Cost extends WC_Admin_Report {

    /**
     * Output the report.
     */
    public function output_report() {

      $ranges = array(
        'year'         => __( 'Year', 'woocommerce' ),
        'last_month'   => __( 'Last month', 'woocommerce' ),
        'month'        => __( 'This month', 'woocommerce' ),
      );
  
      $current_range = ! empty( $_GET['range'] ) ? sanitize_text_field( $_GET['range'] ) : 'month';
  
      if ( ! in_array( $current_range, array( 'custom', 'year', 'last_month', '7day' ) ) ) {
        $current_range = 'month';
      }
  
      $this->check_current_range_nonce( $current_range );
      $this->calculate_current_range( $current_range );
  
      $hide_sidebar = true;
  
      include( WC()->plugin_path() . '/includes/admin/views/html-report-by-date.php' );
    }
    
    /**
     * Get the main chart.
     */
    public function get_main_chart() {
        global $wpdb;

        $default_current = new DateTime();
        $start_date_default = $default_current->format('Y') . "-" . $default_current->format('m') . "-01";
        $end_date_default = $default_current->format('Y') . "-" . $default_current->format('m') . "-" . cal_days_in_month(CAL_GREGORIAN, $default_current->format('m'), $default_current->format('y'));
        //echo "from: " . $start_date_default . ", to: " . $end_date_default . "<br />";

        $start_date = ! empty( $_GET['start_date'] ) ? sanitize_text_field( $_GET['start_date'] ) : $start_date_default;
        $end_date = ! empty( $_GET['end_date'] ) ? sanitize_text_field( $_GET['end_date'] ) : $end_date_default;
        echo "from: " . $start_date . " to: " . $end_date;

        /* Cached via daily Transient. Clearable */
        $EH_Clients = new Everhour_Clients();
        $EH_Clients->set_api_key(wp_cache_get( 'api_key'));
        $EH_Clients->get_clients();
        
        /* Not cached because parameters (dates) change */
        $EH_Reports = new Everhour_Reports();
        $EH_Reports->set_api_key(wp_cache_get( 'api_key'));
        $EH_Reports->get_report_all_date_range($start_date, $end_date);

        /* Not cached because parameters (dates) change */
        $EH_Time_Records = new Everhour_Time_Records();
        $EH_Time_Records->set_api_key(wp_cache_get( 'api_key'));
        $EH_Time_Records->call_to_retrieve_time_records($start_date, $end_date);
        $time_records = $EH_Time_Records->get_time_records();

        /* Cached via daily Transient. */
        $EH_Users = new Everhour_Users();
        $EH_Users->set_api_key(wp_cache_get( 'api_key' ));

        $query_data = array(
            'ID' => array(
                'type'     => 'post_data',
                'function' => '',
                'name'     => 'id',
                'distinct' => true,
            ),
            'post_date'  =>  array(
                'type'  =>  'post_data',
                'function'  =>  '',
                'name'  =>  'date',
            ),
            '_billing_company'   =>  array(
                'type'  =>  'meta',
                'function'  =>  '',
                'name'  =>  'company',
            ),
            '_order_total'   => array(
                'type'      => 'meta',
                'function'  => 'SUM',
                'name'      => 'order_total'
            ),
        );
      
        $orders = $this->get_order_report_data( array(
            'data'                  => $query_data,
            'query_type'            => 'get_results',
            'group_by'              => 'company',
            'filter_range'          => true,
            // 'order_types'           => wc_get_order_types( 'sales-reports' ),
            // 'order_status'          => array( 'processing' ),
            'parent_order_status'   => false,
        ) );
        ?>
        <style> 
            /* td { 
                white-space: nowrap; 
            }  */
            .widefat td{
                padding: 2px 5px !important;
                border-bottom: 1px solid black;
            }
            .noUnder{
                border-bottom: none !important;
            }
        </style>
        <table class="widefat">
        <thead>
            <tr>
                <th><strong>Order</strong></th>
                <th><strong>Date</strong></th>
                <th><strong>Company</strong></th>
                <th><strong>Client</strong></th>
                <th><strong>Line Items</strong></th>
                <th><strong>Invoice</strong></th>
                <th><strong>Cost</strong></th>
                <th><strong>Label/Hours/Cost</strong></th>
                <!-- <th><strong>Profit</strong></th> -->
            </tr>
        </thead>
        <tbody>
            <?php
            foreach( $orders as $order ) { 
              
                //print_r($order);                
                $woo_order = wc_get_order( $order->id );
                $dateTime = new DateTime($woo_order->get_date_paid());
                $dateTime = $dateTime->format('Y-m-d');
                ?>
                <tr>
                    <td><?php echo $order->id; ?></td>
                    <td><?php echo $dateTime; ?></td>
                    <td><?php echo $order->company; ?></td>
                    <td>
                        <?php
                            echo $EH_Clients->display_clients_dropdown($order->company, true);
                        ?>
                    </td>
                    <td>
                        <table class="widefat">
                        <?php
                            foreach ( $woo_order->get_items() as  $item_key => $item_values ) {
                                $item_data = $item_values->get_data();
                                //echo $product_name = $item_data['name'] . ": $" . $line_total = $item_data['total'] . "<br />";
                                echo "<tr><td class='noUnder'>" . $item_data['name'] . "</td><td class='noUnder'>$" . $item_data['total'] . "</td></tr>";
                            }
                        ?>
                        </table>
                    </td>
                    <td style="color:green"><?php echo '$' . number_format($order->order_total, 2); ?></td>
                    <td style="color:red">
                        <?php
                            $costVal = $EH_Reports->match_client_get_cost($order->company);
                            if($costVal != 0){
                                echo "$" . $costVal;
                            }
                        ?>
                    </td>
                    <td>
                        <?php
                            $this_everhour_project_id = $EH_Clients->get_project_id();
                            //echo $this_everhour_project_id;
                            $array_of_labels = array();
                            foreach ( $time_records as $record ) {
                                if( $record->task->projects[0] == $this_everhour_project_id ){
                                    //&& $record->task->status == 'completed'
                                    if( isset($record->task->labels[0]) ){
                                        //$array_of_labels[$record->task->labels[0]][] = $record->task->time->users; 
                                        $array_of_labels[$record->task->labels[0]][] = array($record->cost, $record->time);
                                        
                                        //$array_of_labels[$record->task->labels[0]] = array_map("unserialize", array_unique(array_map("serialize", $array_of_labels[$record->task->labels[0]])));
                                    }else{
                                        //echo "we're using the iteration tags: " . $record->task->iteration . "<br />";
                                        if( isset($record->task->iteration) ) {
                                            //$array_of_labels[$record->task->iteration][] = $record->task->time->users;
                                            $array_of_labels[$record->task->iteration][] = array($record->cost, $record->time);
                                            
                                            //$array_of_labels[$record->task->iteration] = array_map("unserialize", array_unique(array_map("serialize", $array_of_labels[$record->task->iteration] )));
                                        }else{
                                            //echo "unlabeled: " . $record->task->time->user . "<br />";
                                            if(isset($record->task->time) ){
                                                $array_of_labels['Unlabeled'][] = array($record->cost, $record->time);

                                                //$array_of_labels['Unlabeled'] = array_map("unserialize", array_unique(array_map("serialize", $array_of_labels['Unlabeled'] )));
                                            }
                                        }
                                    }
                                }
                            }
                            
                            // echo "<pre>";
                            // print_r($array_of_labels);
                            // echo "</pre>";
                            // example:
                            // Array(
                            //     [Acct Mgmt] => Array(
                            //         [0] => stdClass Object
                            //             (
                            //                 [839286] => 28560
                            //                 [847014] => 5220
                            //             )

                            //         [1] => stdClass Object
                            //             (
                            //                 [839286] => 28560
                            //                 [847014] => 5220
                            //             )

                            
                            $all_labels_cost = 0;
                            $all_labels_hours = 0;
                            echo "<table class='widefat'>";
                            foreach($array_of_labels as $label => $record){
                                $hours_sum = 0;
                                $cost_sum = 0;
                                //echo $label;
                                //var_dump($record);
                                foreach($record as $key => $hours){
                                    //foreach($user_time as $user => $hours){
                                        //echo "<tr><td class='noUnder'>" . $label . "</td><td class='noUnder'><span style='color:red'>$" . number_format($EH_Users->getUserCost($user, $hours), 2) . "</span></td></tr>";
                                    //echo "Time: " . $hours . ', type: ' . print_r($hours) . "<br />";
                                    //echo "Cost: " . $cost . ', type: ' . gettype($cost) . "<br />";
                                    $hours_sum += $hours[1]/60/60;//$hours;
                                    //$cost_sum += $EH_Users->getUserCost($user, $hours);
                                    $cost_sum += $hours[0]/100;
                                    //}
                                }
                            
                                echo "<tr><td class='noUnder'>" . $label . "</td><td class='noUnder'>" . number_format($hours_sum, 2) . "</td><td class='noUnder'><span style='color:red'>$" . number_format($cost_sum, 2) . "</span></td></tr>";
                                $all_labels_cost = $all_labels_cost + $cost_sum;
                                $all_labels_hours = $all_labels_hours + $hours_sum;   
                            }
                            echo "<tr><td class='noUnder'>Total</td><td class='noUnder'>" . number_format($all_labels_hours, 2) . "</td><td class='noUnder'><span style='font-weight:bold;color:red'>$" . number_format($all_labels_cost, 2) . "</span></td></tr>";
                            echo "</table>";
                        ?>
                    </td>
                    
                </tr>
            <?php
            }
            ?>
        </tbody>
        </table>
    <?php  
    }
}