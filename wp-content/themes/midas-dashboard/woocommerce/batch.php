<?php

function batch_callback() {

    $report = new WC_Report_Everhour_Batch();
    $report->output_data();
    $report->output_form();
    $report->output_styles();
}

class WC_Report_Everhour_Batch extends WC_Admin_Report {

	/**
	 * Constructor.
	 */
	public function __construct() {
        global $wpdb;
        
        $this->start_date = ! empty( $_GET['start_date'] ) ? sanitize_text_field( $_GET['start_date'] ) : '';
        $this->end_date = ! empty( $_GET['end_date'] ) ? sanitize_text_field( $_GET['end_date'] ) : '';

        if($this->start_date != '' && $this->end_date != ''){
            
            $checkDates = $wpdb->get_results( 
                "SELECT `key` FROM {$wpdb->prefix}everhour WHERE `start_date` = \"$this->start_date\""
            );
            
            if(empty($checkDates)){
                echo "<span class='batch_titles'>" . $this->start_date . " - " . $this->end_date . " was already deleted or is new</span><br />";
            }else{
                echo "<span class='batch_titles'>db had that range: " . $this->start_date . " - " . $this->end_date . "</span><br />";
                $delete_result = $wpdb->delete( 'wp_everhour', array( 'start_date' => $this->start_date ) );
                if(isset($delete_result)){
                    //print_r($delete_result);
                    echo "<span class='batch_titles'>deleted for replacement</span><br />";
                }else{
                    echo "<span class='batch_titles'>there was an error deleting that range</span><br />";
                }
            }

            /*
            *   Everhour Costs
            */
            $EH_Time_Records = new Everhour_Time_Records();
            $EH_Time_Records->set_api_key(wp_cache_get( 'api_key'));
            
            $EH_Time_Records->call_to_retrieve_time_records($this->start_date, $this->end_date);
            
            $time_records = $EH_Time_Records->get_time_records();

            $EH_Time_Records->save_to_db($this->start_date, $this->end_date, $time_records, false);
            $data_retrived_from_db = $EH_Time_Records->retrieve_from_db($this->start_date, $this->end_date);
            //echo "<pre>";
            //print_r(unserialize($data_retrived_from_db[0]->data));
            //echo "</pre><br />";

            // Loop through all task time entries, adjust the date, add workspace
            // Call to API limited dates already for us so all costs returned are used
            if( $time_records != null )
            echo "<span class='batch_titles'>" . count($time_records) . " records returned</span><br />";
            /*
            *   Everhour Costs END
            */
        }
	}



	/**
	 * Output the report.
	 */
	public function output_data() {
        
		//require_once( get_template_directory() . '/woocommerce/html-report-batch.php');
        global $wpdb;
        if(isset($_GET['key'])){
            $tempKey = $_GET['key'];
            $checkKey = $wpdb->get_results( 
                "SELECT `key` FROM {$wpdb->prefix}everhour WHERE `key` = $tempKey"
            );
            //echo "<span class='batch_titles'>checkKey results: </span>";
            //print_r($checkKey);
            if(empty($checkKey)){
                echo "<span class='batch_titles'>key " . $tempKey . " no longer exists</span><br />";
            }else{
                $delete_result = $wpdb->delete( 'wp_everhour', array( 'key' => $tempKey ) );
                if(isset($delete_result)){
                    //echo "<span class='batch_titles'>delete result: ";
                    //print_r($delete_result);
                    echo "<span class='batch_titles'>record deleted</span><br />";
                }
            }
        }

        $results = $wpdb->get_results( 
            "SELECT `key`,`start_date`,`end_date`,`batched`,OCTET_LENGTH(`data`) as size FROM {$wpdb->prefix}everhour ORDER BY start_date"
        );
        ?>
        <!-- <span class='batch_titles'>2 week blobs of Everhour data already saved in the DB:</span> -->
        <?php
        echo "<table class='everhour_table'><tr><th>key</th><th>start_date</td><th>end_date</th><td>batched date</th><th>size (KiB)</th><th></th><th></th></tr>";
        foreach($results as $row){
            echo "<tr><td>" . $row->key . "</td><td>" . $row->start_date . "</td><td>" . $row->end_date . "</td><td>" . $row->batched . "</td><td>" . floor($row->size/1000) . "<td><a href='?page=wc-reports&tab=batch&start_date=$row->start_date&end_date=$row->end_date'>reload</a><td><a href='?page=wc-reports&tab=batch&key=" . $row->key . "'>delete</a></td></tr>";
        };
	}


    public function output_form() {
        ?>
        <br /><span class='batch_titles'>Load and save a date range of timecards from Everhour:</span><br />
        <form>
            <input name="page" value="wc-reports" type="hidden">
            <input name="tab" value="batch" type="hidden">
            <label for="start_date">start_date: </label> <input name="start_date" value="2021-12-15" />
            <label for="end_date" > end_date: </label> <input name="end_date" value="2021-12-31" />
            <input type="submit" value="Submit">
        </form>
        <?php
    }

    public function output_styles() {
        ?>
        <style>
            .everhour_table{
                width: 100%;
                margin: 20px 0 2000px 0;
            }
            .everhour_table table td, td{
                text-align: center;
            }
            .batch_titles{
                font-size: 1.3em;
                font-weight: 500;
                margin: 0px 0;
                display: block;
            }
        </style>
        <?php
    }


	
}

