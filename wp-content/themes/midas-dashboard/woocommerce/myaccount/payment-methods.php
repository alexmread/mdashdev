<?php
/**
 * Payment methods
 *
 * Shows customer payment methods on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/payment-methods.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.6.0
 */

defined( 'ABSPATH' ) || exit;

$saved_methods = wc_get_customer_saved_methods_list( get_current_user_id() );
$has_methods   = (bool) $saved_methods;
$types         = wc_get_account_payment_methods_types();

do_action( 'woocommerce_before_account_payment_methods', $has_methods );


/* User, works: */
	// $user = wp_get_current_user();
	// $loggedin = is_user_logged_in();
	// echo "user: ";
	// print_r($user);
	// echo "<br />logged in: " . $loggedin . "<br />";
?>

<?php
	global $current_user;
	$user = wp_get_current_user();
	$brand_switch = get_user_meta($user->ID, 'brand', true);
	$user_first_last_name = get_user_meta( $user->ID, 'first_name', true ) . " " . get_user_meta( $user->ID, 'last_name', true );
	//echo $brand_switch . "<br />";
	if($brand_switch != null && $brand_switch != false && $brand_switch != 'undefined' && $brand_switch != ''){
		$brand_switch_var = $brand_switch;
	}else{
		$brand_switch_var = false;
	}
	//print_r($user);
	//exit;
	
	//print_r($_SERVER); echo "<br />";
	//$query = $_SERVER['REDIRECT_QUERY_STRING'];
	//echo 'query: ' . $query . "<br />";
	//if ( strpos( $query, 'ridgecapmarketing' ) !== false || isset($_GET['brand']) ){
		//echo 'REDIRECT_QUERY_STRING fires<br />';
		// $ridgecapmarketing = true;
		// $brand = '&brand=ridgecapmarketing';
		// $qbrand = '?brand=ridgecapmarketing';
	//};
	//$referer = $_SERVER['HTTP_REFERER'];
	//if ( strpos( $referer, 'ridgecapmarketing' ) !== false || isset($_GET['brand']) ){
		//echo 'HTTP_REFERER fires<br />';
	// 	$ridgecapmarketing = true;
	// 	$brand = '&brand=ridgecapmarketing';
	// 	$qbrand = '?brand=ridgecapmarketing';
	// }
	//echo 'referer: ' . $referer . "<br />";
	//print_r($referer); echo "<br />";
	
	//$server_var = $_SERVER['HTTP_ORIGIN'];
	//if ( strpos( $server_var, 'ridgecapmarketing' ) !== false || isset($_GET['brand']) ){
		//echo 'HTTP_ORIGIN fires<br />';
		// $ridgecapmarketing = true;
		// $brand = '&brand=ridgecapmarketing';
		// $qbrand = '?brand=ridgecapmarketing';
	//}
	
	// $server_var2 = $_SERVER['REDIRECT_QUERY_STRING'];
	// if ( strpos( $server_var2, 'ridgecapmarketing' ) !== false || isset($_GET['brand']) ){
		//echo 'REDIRECT_QUERY_STRING fires<br />';
	// 	$ridgecapmarketing = true;
	// 	$brand = '&brand=ridgecapmarketing';
	// 	$qbrand = '?brand=ridgecapmarketing';
	// }
	
	// $server_var3 = $_SERVER['QUERY_STRING'];
	// if ( strpos( $server_var3, 'ridgecapmarketing' ) !== false || isset($_GET['brand']) ){
		//echo 'QUERY_STRING fires<br />';
	// 	$ridgecapmarketing = true;
	// 	$brand = '&brand=ridgecapmarketing';
	// 	$qbrand = '?brand=ridgecapmarketing';
	// }
	
	
	// $host = parse_url($_SERVER['HTTP_HOST']);
	// print_r($host); echo "<br />";
	// echo 'host: ' . $host['path'] . "<br />";
	$server = parse_url($_SERVER['SERVER_NAME']);
	// print_r($server); echo "<br />";
	//echo 'server: ' . $server['path'] . "<br />";
	switch($server['path']){
		case 'mdashdev.local':
			//echo " local";
			$wpBase = 'https://mdashdev.local';
			if($brand_switch == 'flyrise'){
				$headlessBase = 'http://my.flyrise.local:3000';
			}else{
				if($brand_switch == 'ridgecapmarketing'){
					$headlessBase = 'http://my.ridgecapmarketing.local:3000';
				}else{
					$headlessBase = 'http://mdashdev.local';
				}
			}
			break;
		case 'mdashdev.wpengine.com':
			//echo " mdashdev.wpengine";
			$wpBase = 'https://mdashdev.wpengine.com';
			$headlessBase = 'https://my.ridgecapmarketing.com';
			break;
		case 'launch.flyrise.io':
			//echo " prod";
			$wpBase = 'https://launch.flyrise.io';
			$headlessBase = 'https://my.ridgecapmarketing.com';
			break;
	}
?>

<?php if ( $has_methods ) : ?>
	<div class="checkout-header">
		<?php
			if ( $brand_switch_var == 'ridgecapmarketing' ){
			?>
				<a href="https://my.ridgecapmarketing.com/dashboard"><img src="https://my.ridgecapmarketing.com/ridgecap-icon-40.png" class="checkout-logo"></a>
			<?php
			}else{
				?><a href="https://my.flyrise.io/dashboard"><img src="/wp-content/themes/midas-dashboard/assets/images/flyrise-icon-gradient.svg" class="checkout-logo"></a><?php
			}
		?>
		<h1>Payment Methods for <?php echo $user_first_last_name; ?></h1>
		<!-- <h1 class="card-preheader large">Payment Methods</h1> -->
	</div>
	<hr>

	<div id="saved-methods" class="card" type="default">
		<div class="border-0 card-header">
			<div class="row align-items-center">
				<div class="col-8">
					<h3 class="mb-0">Payment Methods</h3>
				</div>
			</div>
		</div>
		<div class="card-body">
			<div class="el-table table-responsive table-flush no-outside-column-padding no-bottom-row-border el-table--fit el-table--enable-row-hover el-table--enable-row-transition">

				<div class="el-table__body-wrapper">
					<table class="el-table__body" style="width: 100%;">
						<thead>
							<tr>

								<th><div class="cell">Method</div></th>
								<th><div class="cell">Expires</div></th>
								<th><div class="cell">Actions</div></th>
								
							</tr>
						</thead>

					
						
						<tbody>
							<?php foreach ( $saved_methods as $type => $methods ) : // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited ?>
								<?php foreach ( $methods as $method ) : ?>
									<tr class="payment-method<?php echo ! empty( $method['is_default'] ) ? ' default-payment-method' : ''; ?> el-table__row">
										<?php foreach ( wc_get_account_payment_methods_columns() as $column_id => $column_name ) : ?>
											<td class="woocommerce-PaymentMethod woocommerce-PaymentMethod--<?php echo esc_attr( $column_id ); ?> payment-method-<?php echo esc_attr( $column_id ); ?>" data-title="<?php echo esc_attr( $column_name ); ?>">
												<?php
												if ( has_action( 'woocommerce_account_payment_methods_column_' . $column_id ) ) {
													do_action( 'woocommerce_account_payment_methods_column_' . $column_id, $method );
												} elseif ( 'method' === $column_id ) {
													if ( ! empty( $method['method']['last4'] ) ) {
														/* translators: 1: credit card type 2: last 4 digits */
														echo sprintf( esc_html__( '%1$s ending in %2$s', 'woocommerce' ), esc_html( wc_get_credit_card_type_label( $method['method']['brand'] ) ), esc_html( $method['method']['last4'] ) );
													} else {
														echo esc_html( wc_get_credit_card_type_label( $method['method']['brand'] ) );
													}
												} elseif ( 'expires' === $column_id ) {
													echo esc_html( $method['expires'] );
												} elseif ( 'actions' === $column_id ) {
													foreach ( $method['actions'] as $key => $action ) { // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited
														echo '<a href="' . esc_url( $action['url'] ) . '&' . $brand_switch . '" class=" ' . sanitize_html_class( $key ) . ' btn btn-sm">' . esc_html( $action['name'] ) . '</a>&nbsp;';
													}
												}
												?>
											</td>
										<?php endforeach; ?>
									</tr>
								<?php endforeach; ?>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
				
				
			</div>
		</div>
	</div>


<?php else : ?>
	<div class="checkout-header">
		<?php
		if ( $brand_switch_var == 'ridgecapmarketing' ){
			?><img src="https://my.ridgecapmarketing.com/ridgecap-icon-40.png" class="checkout-logo"><?php
		}else{
			?><img src="/wp-content/themes/midas-dashboard/assets/images/flyrise-icon-gradient.svg" class="checkout-logo"><?php
		}
		?>
		<h1 class="card-preheader large">Payment Methods</h1>
	</div>
	<hr>
	<div class="woocommerce-notices-wrapper"><p class="woocommerce-Message woocommerce-Message--info woocommerce-info woocommerce-message"><?php esc_html_e( 'No saved methods found.', 'woocommerce' ); ?></p></div>
<?php endif; ?>

<?php do_action( 'woocommerce_after_account_payment_methods', $has_methods ); ?>


<?php if ( WC()->payment_gateways->get_available_payment_gateways() ) : ?>
	<a class="button" href="<?php echo esc_url( wc_get_endpoint_url( 'add-payment-method' ) ) ?>"><?php esc_html_e( 'Add payment method', 'woocommerce' ); ?></a>
<?php endif; ?>

<?php if ( $brand_switch_var == 'ridgecapmarketing' ) : ?>
		<br/><br /><a class="button" href="<?php echo $headlessBase; ?>/dashboard">Back to your Dashboard</a>
		<!-- <link rel="stylesheet" href="https://my.ridgecapmarketing.com/assets/sass/ridgecap/compiled/sass-style.css"> this gets overwritten doesn't work here-->
	<?php else : ?>
		<br/><br /><a class="button" href="http://my.flyrise.io/dashboard">Back to your Dashboard</a>
<?php endif; ?>