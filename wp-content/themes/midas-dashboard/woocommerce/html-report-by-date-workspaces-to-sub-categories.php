<?php
/**
 * Admin View: Report by Date (with date filters)
 *
 * @package WooCommerce\Admin\Reporting
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if( isset($_GET['range']) ) {
	$range = $_GET['range'];
}else{
	$range = '';
}
?>

<div id="poststuff" class="woocommerce-reports-wide">
	<div class="postbox">

	<?php if ( 'custom' === $current_range && isset( $_GET['start_date'], $_GET['end_date'] ) ) : ?>
		<h3 class="screen-reader-text">
			<?php
			/* translators: 1: start date 2: end date */
			printf(
				esc_html__( 'From %1$s to %2$s', 'woocommerce' ),
				esc_html( wc_clean( wp_unslash( $_GET['start_date'] ) ) ),
				esc_html( wc_clean( wp_unslash( $_GET['end_date'] ) ) )
			);
			?>
		</h3>
	<?php else : ?>
		<h3 class="screen-reader-text"><?php echo esc_html( $ranges[ $current_range ] ); ?></h3>
	<?php endif; ?>

		<div class="stats_range">
			<?php $this->get_export_button(); ?>
			<ul>
				<?php
				foreach ( $ranges as $range => $name ) {
					
					$possibly = ( $current_range == $range ) ? 'active' : '';
					if(isset($_GET['range']))
					if	(	$_GET['range'] == 'q1' && $name == 'Jan - Mar' ||
							$_GET['range'] == 'q2' && $name == 'Apr - Jun' ||
							$_GET['range'] == 'q3' && $name == 'Jul - Sept' ||
							$_GET['range'] == 'q4' && $name == 'Oct - Dec'
						)
					{
						$possibly = 'active';
					}
					echo '<li class="' . $possibly . '"><a href="' . esc_url( remove_query_arg( array( 'start_date', 'end_date' ), add_query_arg( 'range', $range ) ) ) . '">' . esc_html( $name ) . '</a></li>';
					//echo '<li class="' . $possibly . '"><a onClick="setUrl(\'range\', ' . $range . ')>' . $name . '</a></li>';
				}
				?>

				<!-- <label for="months">Month:</label> -->
				<li>
					<select class="monthSelect" name="months" id="months" onchange="setUrl('months', this.value)">>
					<option value="">Month</option>';
					<?php
						foreach ( $ranges_months as $range => $name ) {
							
							$possibly = ( $current_range == $range ) ? 'selected' : '';
							if(isset($_GET['range']))
							if	(	$_GET['range'] == 'Jan' && $name == 'Jan' ||
									$_GET['range'] == 'Feb' && $name == 'Feb' ||
									$_GET['range'] == 'Mar' && $name == 'Mar' ||
									$_GET['range'] == 'Apr' && $name == 'Apr' ||
									$_GET['range'] == 'May' && $name == 'May' ||
									$_GET['range'] == 'Jun' && $name == 'Jun' ||
									$_GET['range'] == 'Jul' && $name == 'Jul' ||
									$_GET['range'] == 'Aug' && $name == 'Aug' ||
									$_GET['range'] == 'Sep' && $name == 'Sep' ||
									$_GET['range'] == 'Oct' && $name == 'Oct' ||
									$_GET['range'] == 'Nov' && $name == 'Nov' ||
									$_GET['range'] == 'Dec' && $name == 'Dec'
								)
							{
								$possibly = 'selected';
							}
							//echo '<option ' . $possibly . ' value="' . esc_url( remove_query_arg( array( 'start_date', 'end_date' ), add_query_arg( 'range', $range ) ) ) . '">' . esc_html( $name ) . '</option>';
							echo '<option ' . $possibly . ' value="' . $name . '">' . esc_html( $name ) . '</option>';
						}
					?>
					</select>
				</li>
				<style>
					.monthSelect{
						padding: 10px;
						margin: 7px;
					}
				</style>


					<!-- <label for="years">Year:</label> -->
					<li>
					<select class="yearSelect" name="year" id="years" onchange="setUrl('years', this.value)">>
					<option value="">Year</option>';
					<?php
						foreach ( $ranges_years as $years => $name ) {
							
							$possibly = ( $current_range == $years ) ? 'selected' : '';
							if(isset($_GET['years']))
							if	(	$_GET['years'] == '2021' && $name == '2021' ||
									$_GET['years'] == '2022' && $name == '2022' ||
									$_GET['years'] == '2023' && $name == '2023'
								)
							{
								$possibly = 'selected';
							}
							//echo '<option ' . $possibly . ' value="' . esc_url( remove_query_arg( array( 'start_date', 'end_date' ), add_query_arg( 'years', $years ) ) ) . '">' . esc_html( $name ) . '</option>';
							echo '<option ' . $possibly . ' value="' . $years . '">' . esc_html( $name ) . '</option>';
						}
					?>
					</select>
				</li>
				<style>
					.yearSelect{
						padding: 10px;
						margin: 7px;
					}
				</style>
				<!-- End Year Select -->
				
				<li id="gs_expenses_li">
					<label for="gs_expenses">GS Expenses</label>
					<?php
						if( !isset($_GET['gs_expenses']) ) {
							//echo '<input onClick="setUrl('gs_expenses', this.value)" type="checkbox" id="gs_expenses" name="gs_expenses" value="' . esc_url( remove_query_arg( array( 'start_date', 'end_date' ), add_query_arg( array( 'range' => (isset($_GET['range']))?$_GET['range']:'', 'gs_expenses' => 'true' ) ) ) ) . '">';
							?><input onClick="setUrl('gs_expenses', this.checked)" type="checkbox" id="gs_expenses" name="gs_expenses"><?php
						 }else{
							//echo '<input checked onClick="setUrl('gs_expenses', this.value)" type="checkbox" id="gs_expenses" name="gs_expenses" value="' . esc_url( remove_query_arg( array( 'start_date', 'end_date', 'gs_expenses' ), add_query_arg( array( 'range' => (isset($_GET['range']))?$_GET['range']:'' ) ) ) ) . '">';
							?><input checked onClick="setUrl('gs_expenses', this.checked)" type="checkbox" id="gs_expenses" name="gs_expenses"><?php
						 }
						 //echo "<br />Check1: " . esc_url( remove_query_arg( array( 'start_date', 'end_date' ), add_query_arg( array( 'range' => $_GET['range'], 'gs_expenses' => 'true' ) ) ) ) . "<br />";
						 //echo "Check2: " . esc_url( remove_query_arg( array( 'start_date', 'end_date', 'gs_expenses' ), add_query_arg( array( 'range' => $_GET['range'] ) ) ) ) . "<br />";
					?>
				</li>
				<li id="eh_costs_li">
					<label for="eh_costs">EH Costs</label>
					<?php
						if( !isset($_GET['eh_costs']) ) {
							?><input onClick="setUrl('eh_costs', this.checked)" type="checkbox" id="eh_costs" name="eh_costs"><?php
						 }else{
							?><input checked onClick="setUrl('eh_costs', this.checked)" type="checkbox" id="eh_costs" name="eh_costs"><?php
						 }
						 //echo "<br />Check1: " . esc_url( remove_query_arg( array( 'start_date', 'end_date' ), add_query_arg( array( 'range' => $_GET['range'], 'gs_expenses' => 'true' ) ) ) ) . "<br />";
						 //echo "Check2: " . esc_url( remove_query_arg( array( 'start_date', 'end_date', 'gs_expenses' ), add_query_arg( array( 'range' => $_GET['range'] ) ) ) ) . "<br />";
					?>
				</li>
				<li id="submit_li">
					<!-- <label for="submit_input">Submit</label> -->
					<input name="submit_input" id="submit_input" onClick="letsGo(); return false;" type="submit">

				<style>
					#gs_expenses_li, #eh_costs_li, #submit_li{
						margin: 7px;
					}
				</style>
				<script>
					var urlHolder = new URL(window.location.href);
					function setUrl(paramToSet, paramValue){
						//document.urlHolder = newUrl;
						//location = this.value;

// If your expected result is "http://foo.bar/?x=1&y=2&x=42"
//url.searchParams.append('x', 42);

// If your expected result is "http://foo.bar/?x=42&y=2"
//url.searchParams.set('x', 42);

						urlHolder.searchParams.set(paramToSet, paramValue);
						if(paramToSet == 'months'){
							urlHolder.searchParams.set('range', paramValue);
						}
						urlHolder.searchParams.delete('start_date');
						urlHolder.searchParams.delete('end_date');
						//urlHolder.searchParams.delete('range');

						console.log('urlHolder: ' + urlHolder);
						// var eh_costs = document.getElementById("eh_costs");
						// if(eh_costs){
						//  	console.log('eh_costs: ' + document.getElementById('eh_costs').value);
						// }
					}
					function letsGo(){
						if(urlHolder != ''){
							console.log(urlHolder.toString());
							location = urlHolder.toString();
						}
					}
				</script>
				
				<!-- <li class="custom <?php echo ( 'custom' === $current_range ) ? 'active' : ''; ?>">
					<?php
						esc_html_e( 'Custom:', 'woocommerce' );
					?>
					<form method="GET">
						<div>
							<?php
							// Maintain query string.
							foreach ( $_GET as $key => $value ) {
								if ( is_array( $value ) ) {
									foreach ( $value as $v ) {
										echo '<input type="hidden" name="' . esc_attr( sanitize_text_field( $key ) ) . '[]" value="' . esc_attr( sanitize_text_field( $v ) ) . '" />';
									}
								} else {
									echo '<input type="hidden" name="' . esc_attr( sanitize_text_field( $key ) ) . '" value="' . esc_attr( sanitize_text_field( $value ) ) . '" />';
								}
							}
							?>
							<input type="hidden" name="range" value="custom" />
							<input type="text" size="11" placeholder="yyyy-mm-dd" value="<?php echo ( ! empty( $_GET['start_date'] ) ) ? esc_attr( wp_unslash( $_GET['start_date'] ) ) : ''; ?>" name="start_date" class="range_datepicker from" autocomplete="off" /><?php //@codingStandardsIgnoreLine ?>
							<span>&ndash;</span>
							<input type="text" size="11" placeholder="yyyy-mm-dd" value="<?php echo ( ! empty( $_GET['end_date'] ) ) ? esc_attr( wp_unslash( $_GET['end_date'] ) ) : ''; ?>" name="end_date" class="range_datepicker to" autocomplete="off" /><?php //@codingStandardsIgnoreLine ?>
                            
                            
                            
                            <button type="submit" class="button" value="<?php esc_attr_e( 'Go', 'woocommerce' ); ?>"><?php esc_html_e( 'Go', 'woocommerce' ); ?></button>
							<?php wp_nonce_field( 'custom_range', 'wc_reports_nonce', false ); ?>
						</div>
					</form>
				</li> -->
			</ul>
		</div>
		<?php if ( empty( $hide_sidebar ) ) : ?>
			<div class="inside chart-with-sidebar">
				<div class="chart-sidebar">
					<?php if ( $legends = $this->get_chart_legend() ) : ?>
						<ul class="chart-legend">
							<?php foreach ( $legends as $legend ) : ?>
								<?php // @codingStandardsIgnoreStart ?>
								<li style="border-color: <?php echo $legend['color']; ?>" <?php if ( isset( $legend['highlight_series'] ) ) echo 'class="highlight_series ' . ( isset( $legend['placeholder'] ) ? 'tips' : '' ) . '" data-series="' . esc_attr( $legend['highlight_series'] ) . '"'; ?> data-tip="<?php echo isset( $legend['placeholder'] ) ? $legend['placeholder'] : ''; ?>">
									<?php echo $legend['title']; ?>
								</li>
								<?php // @codingStandardsIgnoreEnd ?>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
					<ul class="chart-widgets">
						<?php foreach ( $this->get_chart_widgets() as $widget ) : ?>
							<li class="chart-widget">
								<?php if ( $widget['title'] ) : ?>
									<h4><?php echo esc_html( $widget['title'] ); ?></h4>
								<?php endif; ?>
								<?php call_user_func( $widget['callback'] ); ?>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
				<div class="main">
					<?php $this->get_main_chart(); ?>
				</div>
			</div>
		<?php else : ?>
			<div class="inside">
				<?php $this->get_main_chart(); ?>
			</div>
		<?php endif; ?>
	</div>
</div>
