<?php

function profitability_to_cost_callback() {

    $report = new WC_Report_Everhour_Profitability();
    $report->output_report();
}

class WC_Report_Everhour_Profitability extends WC_Admin_Report {

    /**
     * Output the report.
     */
    public function output_report() {

      $ranges = array(
        
      );
  
      $current_range = ! empty( $_GET['range'] ) ? sanitize_text_field( $_GET['range'] ) : 'month';

      //$company_filter = ! empty( $_GET['company'] ) ? sanitize_text_field( $_GET['company'] ) : '';

      //$category_filter = ! empty( $_GET['category'] ) ? sanitize_text_field( $_GET['category'] ) : '';
  
      if ( ! in_array( $current_range, array( 'custom' ) ) ) {
        $current_range = 'month';
      }
  
      $this->check_current_range_nonce( $current_range );
      $this->calculate_current_range( $current_range );
  
      $hide_sidebar = true;
  
      require_once( get_template_directory() . '/woocommerce/html-report-by-date.php');
    }
    

    /**
     * Get the main chart.
     */
    public function get_main_chart() {
        global $wpdb;

        $default_current = new DateTime();
        $start_date_default = $default_current->format('Y') . "-" . $default_current->format('m') . "-01";
        $end_date_default = $default_current->format('Y') . "-" . $default_current->format('m') . "-" . cal_days_in_month(CAL_GREGORIAN, $default_current->format('m'), $default_current->format('y'));
        //echo "from: " . $start_date_default . ", to: " . $end_date_default . "<br />";

        $start_date = ! empty( $_GET['start_date'] ) ? sanitize_text_field( $_GET['start_date'] ) : $start_date_default;
        $end_date = ! empty( $_GET['end_date'] ) ? sanitize_text_field( $_GET['end_date'] ) : $end_date_default;
        echo "<div style='display:none' id='date_loaded_info'>updated from: " . $start_date . " to: " . $end_date . "<br />";

        /* Cached via daily Transient. Clearable */
        $EH_Clients = new Everhour_Clients();
        $EH_Clients->set_api_key(wp_cache_get( 'api_key'));
        $EH_Clients->get_clients();
        //$all_clients = $EH_Clients->get_clients();
        //print_r($all_clients); // Is EH list of Clients, Not Woo's
        // echo "<pre>";
        // print_r($EH_Clients->get_clients());
        // echo "</pre>";
        
        /* Not cached because parameters (dates) change */
        $EH_Reports = new Everhour_Reports();
        $EH_Reports->set_api_key(wp_cache_get( 'api_key'));
        $EH_Reports->get_report_all_date_range($start_date, $end_date);
        
        /* Not cached because parameters (dates) change */
        $EH_Time_Records = new Everhour_Time_Records();
        $EH_Time_Records->set_api_key(wp_cache_get( 'api_key'));


        /*
        *   Replace API call with Cached db data as in workspaces.php report
        */
        //$EH_Time_Records->call_to_retrieve_time_records($start_date, $end_date);

        $arr = explode("-", $start_date);
        //print_r($arr);
        $start_year = intval($arr[0]);
        echo "start year: " . $start_year . "<br />";
        
        $arr2 = explode("-", $end_date);
        $end_year = $arr2[0];
        echo "end year: " . $end_year . "<br />";

        if( $start_year != $end_year ){
            echo "Sorry, multi-year ranges are not supported at this time<br />";
            exit;
        }
        
        $month_from = intval($arr[1]);
        $month_to = intval($arr2[1]);
        echo "month from: " . $arr[1] . "<br />";
        echo "month to: " . $arr2[1] . "<br />";
        
        echo "we'll add the data from:<br />";
        $x = $month_from;
        while($x <= $month_to) {
            if( $x <= 9 ){ $x = "0" . $x; }
            echo $start_year  . "-" . $x . "-01 to " . $start_year . "-" . $x . "-14<br />";
            $data_call = $EH_Time_Records->retrieve_from_db( $start_year . "-" . $x . "-01", $start_year . "-" . $x . "-14");
            switch($x){
                case 1:case 3:case 5:case 7:case 8:case 10:case 12: $month_end_date = 31; break;
                case 2: $month_end_date = 28; break;
                default: $month_end_date = 30;
            }
            echo $start_year  . "-" . $x . "-15 to " . $start_year . "-" . $x . "-" . $month_end_date . "<br />";
            $EH_Time_Records->retrieve_from_db( $start_year . "-" . $x . "-15", $start_year . "-" . $x . "-" . $month_end_date);
            $x++;
        }
        echo "</div><div id='display_date_loaded_info'>&#9432;</div>";
        ?>
        <script>
            jQuery(document).ready(function($){
                $("#display_date_loaded_info").click(function(){
                    $("#date_loaded_info").slideToggle();
                });
            });
        </script>
        <?php        
        /*
        *   End Replace
        */


        $time_records = $EH_Time_Records->get_time_records();
        // echo "<pre>";
        // print_r($time_records);
        // echo "</pre>";
    

        /* Cached via daily Transient. */
        $EH_Users = new Everhour_Users();
        $EH_Users->set_api_key(wp_cache_get( 'api_key' ));

        /* When Expenses were WP Posts post-type 'expense' */
        //$Expenses = new Expenses();
        //$all_expenses = $Expenses->get_all_expenses();

        /* Expenses now come from Google Sheet */
        $Expenses = new Google_Sheet();
        $all_expenses = $Expenses->get_all_expenses();
        $expenses_companies_encountered = array(); // empty, will fill as we go
        

        // echo "<pre>";
        // print_r($all_expenses);
        // echo "</pre>";

        $all_companies_in_woo = $Expenses->get_all_companies_in_woo();
        //echo "Number of Companies in Woo: " . count($all_companies_in_woo) . "<br >";
        //$Expenses->output_expenses();

        /* Output Object initialization */
        $display_items = new Output_Line_Items();

        if( isset($_GET['company']) ){
            $display_items->set_company_dropdown(sanitize_text_field( $_GET['company'] ));
        }else{
            $display_items->set_company_dropdown('xyz');
        }

        if( isset($_GET['category']) ){
            //echo "catgory get: " . sanitize_text_field( $_GET['category']) . "<br />";
            $tempService = sanitize_text_field( $_GET['category']);
            switch($tempService){
                // case 'Search Engine Optimization': $tempService = "Search Engine Optimization"; break;
                // case 'Email': $tempService = "Email Marketing"; break;
                // case 'Email Marketing': $tempService = "Email Marketing"; break;
                // case 'Digital Marketing (Other)': $tempService = "Digital Marketing"; break;
                // case 'Marketing Strategy': $tempService = "Marketing Strategy"; break;
                // case 'Website Hosting & Maintenance': $tempService = "Hosting & Maintenance"; break;
                // case 'Hosting & Maintenance': $tempService = "Hosting & Maintenance"; break;
                // case 'Web Hosting and Maintenance': $tempService = "Hosting & Maintenance"; break;
                // case 'Uncategorized': $tempService = "Uncategorized"; break;
                // case 'Web Design': $tempService = "Web Design"; break;
                // case 'Acct Mgmt': $tempService = "Acct Mgmt"; break;
                // default: $tempService = 'Product Categories';
            }   
            $display_items->set_category_dropdown($tempService);
        }else{
            $display_items->set_category_dropdown('');
        }


        $orders = $this->get_order_report_data( array(
            'data'         => array(
                'ID'             => array(
                    'type'     => 'post_data',
                    'function' => '',
                    'name'     => 'id',
                    'distinct' => true,
                ),
                'post_date'           => array(
                    'type'     => 'post_data',
                    'function' => '',
                    //'name'  =>  'date',
                    'name'     => 'post_date',
                ),
                '_billing_company'   =>  array(
                    'type'  =>  'meta',
                    'function'  =>  '',
                    'name'  =>  'company',
                ),
                '_order_total' => array(
                    'type'     => 'meta',
                    'function' => 'SUM',
                    //'name'      => 'order_total'
                    'name'     => 'total_sales',
                ),
                '_order_shipping'     => array(
                    'type'     => 'meta',
                    'function' => 'SUM',
                    'name'     => 'total_shipping',
                ),
                '_order_tax'          => array(
                    'type'     => 'meta',
                    'function' => 'SUM',
                    'name'     => 'total_tax',
                ),
                '_order_shipping_tax' => array(
                    'type'     => 'meta',
                    'function' => 'SUM',
                    'name'     => 'total_shipping_tax',
                ),
                '_qty'      => array(
                    'type'            => 'order_item_meta',
                    'order_item_type' => 'line_item',
                    'function'        => 'SUM',
                    'name'            => 'order_item_count',
                ),
            ),
            'group_by'      => 'id',
            //'group_by'      => 'YEAR(posts.post_date), MONTH(posts.post_date), DAY(posts.post_date)',
            //'group_by'    => $this->group_by_query,
            'order_by'      => 'post_date ASC',
            'query_type'    => 'get_results',
            'nocache'       =>  true,
            'filter_range' => true,
            //'order_types'  => wc_get_order_types( 'sales-reports' ),
            'order_types'  => wc_get_order_types( 'order-count' ),
            'order_status' => array( 'completed', 'processing', 'on-hold', 'refunded' ),
            'where' => array(
                array(
                    'key'      => 'order_items.order_item_type',
                    'value'    => 'line_item',
                    'operator' => '=',
                ),
                array(
                    'key'      => 'post_date',
                    'value'    => date('Y-m-d', strtotime($start_date)),
                    'operator' => '>'
                ),
                array(
                    'key'      => 'post_date',
                    'value'    => date('Y-m-d', strtotime($end_date)),
                    'operator' => '<'
                ),
            ),
        ));

        $empty_expense = array(
            "single"        =>  0,
            "amount"        =>  0,
        );
        $empty_order = (object) array(
            "company"       =>  '',

        );


        


        //echo "Orders Found: " . count($orders) . "<br />";
        /*
        *   Look through Orders
        */
        foreach($orders as $order){
            // if($order->id == 'Multiple'){
            //     print_r($order);
            //     continue;
            // };

            $woo_order = wc_get_order( $order->id );
            
            // later we can check for companies in expenses that don't have invoices in this range
            array_push($expenses_companies_encountered, $order->company);
            $expenses_company_categories_encountered = array(); // empty, will fill as we go on each order
           
            // echo "<pre>";
            // print_r($woo_order);
            // echo "</pre>";
            // echo $woo_order->id . ", " . $woo_order->total . ", " . $woo_order->discount_total . "<br />";
            // echo "<br /><br />";
            //echo "Test: " . $woo_order->get_date_paid() . "<br />";
            //if($woo_order){
                $orderDateTime = new DateTime($woo_order->get_date_paid());
                $orderDateTime = $orderDateTime->format('Y-m');
            //}else{
            //    continue;
            //}

            // This sets the current Client/Project,
            // note: needs to happen even if we don't display a dropdown!
            ?>
            <!-- <td>
                <?php
                    echo $EH_Clients->display_clients_dropdown($order->company, true);
                ?>
            </td> -->

            <?php
            // Get all EH time records for this time range for this Client and build an array by task labels
            $everhour_project_ids = $EH_Clients->get_project_ids_array();
            
            $array_of_eh_labels = $EH_Time_Records->filter_time_records_by_project_ids($everhour_project_ids, $orderDateTime);
    
            // Loop through each woo order line item
            $services_once = array();
            
            foreach ( $woo_order->get_items() as  $item_key => $item_values ) {
                // echo "<pre>";
                // print_r($item_values);
                // echo "</pre>";
                // echo "<br />";

                $line_item_entered = false;

                $Expenses->reset_temp_expenses_object();

                $item_data = $item_values->get_data();
                // echo "<pre>";
                // print_r($item_data);
                // echo "</pre><br /><br />";

                $product_cats = wc_get_product_category_list( $item_data['product_id'] );
                $product_cats_array = explode(",", $product_cats);

                /*
                *   for each Category that line item has (should be 1),
                *   - find an EH Label match
                *   - calculate expenses
                */
                $label = '';
                //$single_cat = $item_data['name'];
                //echo $label . "<br />";
                
                $object_built;

                //$line_item_qualifies = false;

                $Expenses->reset_temp_expenses_object();

                // For each of the Product Categories in Woo
                foreach($product_cats_array as $single_cat){
                    //echo $single_cat; // careful, are links!
                    $cost = 0;
                    $time = 0;
                    
                    $single_cat = strip_tags(html_entity_decode($single_cat));
                    
                    // We don't want duplicate Service/Category EH time records lookups
                    if(!in_array($single_cat, $services_once)){

                        /*
                        *   Everhour:
                        *   time records costs for this Category
                        *   Array({label:value,cost:value,time:value})
                        */
                        $costs_for_this_line_item = $EH_Time_Records->find_service_costs($single_cat); 
                        // echo "Company: " . $order->company . "<br />";
                        // echo "Service: " . $single_cat . "<br/>";
                        // print_r($costs_for_this_line_item);
                        // echo "<br /><br />";

                        $label = $costs_for_this_line_item['label'];
                        $cost += $costs_for_this_line_item['cost'];
                        $time += $costs_for_this_line_item['time'];
                        //echo "time records size: " . count($EH_Time_Records->get_array_of_eh_labels()) . "<br />";
                        //echo "label: " . $label . ", cat: " . $single_cat . "<br />";
                        
                        /*
                        *   Expenses:
                        *   Company Match AND Category Match
                        *   OR Category ONLY Match // <!-- I don't think it does this last one...
                        */                        
                        $expenses_found = $Expenses->find_expenses_company_match_AND_category_match($order, $single_cat, $start_date, $end_date);
                        
                        if($expenses_found == false){
                            //echo $label . ", " . $single_cat . ", " . $cost . "<br />";
                            $display_items->add_a_line_item($label, $single_cat, $order, $item_data, $cost, $time, $empty_expense);
                            $line_item_entered = true;
                            array_push($expenses_company_categories_encountered, $single_cat);
                        }

                        array_push($services_once, $single_cat);
                       
                    }else{
                        //echo $single_cat . "<br />";
                        // NO. We could still have a second line item with the same category! NO.
                        /* NO, this needs to be NOT included, double adds items. We full on don't want non category matching items! */
                        //$display_items->add_a_line_item('No Match', $single_cat, $order, $item_data, $cost, $time, $empty_expense);
                        //$Expenses->find_expenses_company_match_OR_category_match($order, $single_cat, $start_date, $end_date);
                    }
                }

                $object_built = $Expenses->get_expenses_object_built();
                //print_r($object_built);
                if($line_item_entered != true){ // It didn't match Company AND Category
                    // echo "<pre>";
                    // print_r($order);
                    // echo "</pre>";
                    array_push($expenses_company_categories_encountered, $single_cat);
                    $display_items->add_a_line_item($label, $single_cat, $order, $item_data, $cost, $time, $object_built);
                }
            }
             
            // echo "<pre>";
            // print_r($services_once);
            // echo "</pre>";
            unset($services_once);


            
            /*
            *   Expenses:
            *   Company Match, Category All
            */
            $Expenses->reset_temp_expenses_object();
            $Expenses->find_expenses_company_match_category_all($order->company, 'All', $start_date, $end_date);
            $expenses_company_match_category_all = $Expenses->get_expenses_object_built();
            //print_r($expenses_company_match_category_all);
            if($expenses_company_match_category_all['amount'] != 0 ){
            //if($expenses_company_match_category_all['single']['amount'] != 0 || $expenses_company_match_category_all['all']['amount'] != 0){
                //foreach($expenses_company_match_category_all as $single){
                    echo "company match, category all<br />"; // never fires
                    $display_items->add_a_line_item('', '', $order, null, 0, 0, $expenses_company_match_category_all);
                //}
            }


            /*
            *   Expenses:
            *   Company Match, Expense Category didn't match any/is additional to Invoice Line Items
            */            
            $remaining_expenses_additional_line_items = $Expenses->find_remaining_expenses_additional_line_items($order->company, $expenses_company_categories_encountered, $start_date, $end_date);
            //echo "array count: " . count($remaining_expenses_additional_line_items) . "<br />";
            if( count($remaining_expenses_additional_line_items) > 0 ){
                // echo "<pre>";
                // print_r($remaining_expenses_additional_line_items);
                // echo "</pre>";
                foreach($remaining_expenses_additional_line_items as $expense_remaining){
                    $empty_expense3 = array(  
                        "amount"        =>  $expense_remaining['amount'],
                        "description"   =>  $expense_remaining['description'],
                    );
                    $empty_order3 = (object) array(
                        "company"       =>  $expense_remaining['company'],
                    );
        
                    //$display_items->add_a_line_item('', $expense_remaining['category'], $order, null, 0, 0, $expenses_company_match_category_all);
                    $display_items->add_a_line_item('', '-' . $expense_remaining['category'] . '-', $empty_order3, null, 0, 0, $empty_expense3);
                }
            }
            


            /*
            *   Everhour:
            *   Display items in the EH Labels Array that didn't match any Woo Order Line Items
            *   still takes $order so as to reference the Company
            */
            $costs_for_non_line_items = $EH_Time_Records->find_remaining_costs();
            //print_r($costs_for_non_line_items);
            foreach($costs_for_non_line_items as $key => $expense_remaining){
                //print_r($expense_remaining);
                $display_items->add_a_line_item($expense_remaining['label'], '', $order, null, $expense_remaining['cost'], $expense_remaining['time'], $empty_expense);
            }
            
        }
        /*
        *   End of Order -> Line Items, Costs, Expenses loop
        */



        /*
        *   Expenses:
        *   attributed to Companies that didn't have a Woo Invoice at all to match
        */
        $remaining_expenses_with_companies = $Expenses->find_remaining_expenses_with_companies($expenses_companies_encountered, $start_date, $end_date);
        // echo "<pre>";
        // print_r($remaining_expenses_with_companies);
        // echo "</pre>";
        
        // [description] => Mailchimp*
        // [amount] => 35.99
        // [billing_date] => 1/1/2021
        // [tier] => Client Facing
        // [sub-category] => Clients
        // [company] => Truth x Vision
        // [category] => Email Marketing
        foreach($remaining_expenses_with_companies as $expense_remaining){
            //print_r($expense_remaining);
            // $empty_order_holder = (object) array(
            //     "company"       =>  $expense_remaining['company'],
            // );
            $empty_expense2 = array(  
                "amount"        =>  $expense_remaining['amount'],
                "description"   =>  $expense_remaining['description'],
            );
            $empty_order2 = (object) array(
                "company"       =>  $expense_remaining['company'],
            );

            //$display_items->add_a_line_item('', $expense_remaining['category'], $order, null, 0, 0, $expenses_company_match_category_all);
            $display_items->add_a_line_item('', '-' . $expense_remaining['category'] . '-', $empty_order2, null, 0, 0, $empty_expense2);
        }
       


        /*
        *   Everhour:
        *   There are MANY more costs than those that match Woo Company->Everhour Project IDs
        *   Those that DID match were iteratively removed along the way from the EH time records Object
        */
        //echo "<pre>";
        //print_r($EH_Clients->get_clients_object());
        //print_r($EH_Time_Records->get_time_records());
        //echo "</pre>";
        $time_records_that_did_not_match = $EH_Time_Records->get_time_records();
        $additional_project_costs = array();

        //$companies_with_cost_but_not_invoiced = $EH_Time_Records->get_companies_that_have_not_been_unset();
        if ( is_array($time_records_that_did_not_match) || is_object($time_records_that_did_not_match) ){
            foreach( $time_records_that_did_not_match as $key => $single_cost ){
                // echo "<pre>";
                // print_r($single_cost);
                // echo "</pre>";
                
                if($single_cost->task->projects[0]){
                    $client_name = $EH_Clients->project_to_client_lookup($single_cost->task->projects[0]);
                }
                if($client_name){}else{
                    $client_name = 'Misc';
                }
                
                if( isset($single_cost->task->labels[0]) && $single_cost->task->labels[0] != '' && $single_cost->task->labels[0] != null ){
                    $temp_label = $single_cost->task->labels[0];
                }else{
                    $temp_label = '';
                }
                $temp_cost = $single_cost->cost/100;
                $temp_time = $single_cost->time/60/60;

                $empty_order = (object) array(
                    "company"       =>  $client_name,
                );
                $display_items->add_a_line_item( $temp_label, '', $empty_order, null, $temp_cost, $temp_time, $empty_expense);
                //$single_cost->task->iteration
            }
        }
        /*
        *   End of Additional Everhour Costs not associated with a Company, Project, Expense
        */



        /*
        *   Expenses:
        *   Still need to display Expenses->Company All -> Category NOT in Woo
        */
        //$Expenses->reset_temp_expenses_object();
        $Expenses->find_expenses_company_all_category_no_match($start_date, $end_date);
        $expenses_company_all_category_no_match = $Expenses->get_expenses_company_all_category_no_match();
        foreach($expenses_company_all_category_no_match as $expense_all_no_match){
            // echo "<pre>";
            // print_r($expense_all_no_match);
            // echo "</pre><br /><br />";
            $reconfigure = (object) $expense_all_no_match;
            $reconfigure->id = $expense_all_no_match['post'];
            $reconfigure->category = 'Company All - Category not in Woo';
            $reconfigure->total = $expense_all_no_match['amount'];
            $reconfigure->amount = $expense_all_no_match;
            //echo $reconfigure->total . "<br />";
            $reconfigure->billing_date = $expense_all_no_match['billing_date'];
            $expense_all_no_match['total'] = 0;
            $empty_expense['amount'] = $expense_all_no_match['amount'];
            $empty_expense['description'] = $expense_all_no_match['description'];
            //$display_items->add_a_line_item('All - Specific - Whole', $expense_all_no_match['repeats'] . ' - No Woo Category Match', $reconfigure, $expense_all_no_match, 0, 0, $empty_expense);
            $display_items->add_a_line_item('All', '-' . $expense_all_no_match['category'] . '-', $reconfigure, $expense_all_no_match, 0, 0, $empty_expense);
        }


        /*
        *   Expenses:
        *   Company All Category All
        *   We still need to display Expenses that don't match Company NOR Category
        */
        //print_r($Expenses->get_expenses_all());
        //Array ( [0] => Array ( [category] => All [amount] => 35 [company] => 18651 [company-name] => All [duration] => 12 [billing_date] => 2021-01-30 00:00:00 [repeats] => Monthly ) 
        foreach($Expenses->get_expenses_company_all_category_all($start_date, $end_date) as $expense_all_single){
            $reconfigure = (object) $expense_all_single;
            // echo "<pre>";
            // print_r($expense_all_single);
            // echo "</pre>";
            $reconfigure->id = $expense_all_single['post'];
            $reconfigure->category = 'All';
            $reconfigure->total = $expense_all_single['amount'];
            //$reconfigure->description = $expense_all_single['description'];
            $reconfigure->amount = $expense_all_single;
            $reconfigure->billing_date = $expense_all_single['billing_date'];
            $expense_all_single['total'] = 0;
            $empty_expense['amount'] = $expense_all_single['amount'];
            $empty_expense['description'] = $expense_all_single['description'];
            //$display_items->add_a_line_item('All', $expense_all_single['repeats'], $reconfigure, $expense_all_single, 0, 0, $empty_expense);
            $display_items->add_a_line_item('All', '-' . $empty_expense['description'] . '-', $reconfigure, $expense_all_single, 0, 0, $empty_expense);
        }
        /*
        *   End All/All Expenses
        */


        /*
        *   Expenses:
        *   We still need to display Company N/A & Category N/A Expenses
        */
        $Expenses->filter_expenses_company_na_category_na($start_date, $end_date);
        foreach($Expenses->get_expenses_company_na_category_na_filtered() as $expense_na_single){
            // print_r($expense_na_single);
            // echo "<br /><br />";
            $reconfigure = (object) $expense_na_single;

            $reconfigure->id = $expense_na_single['post'];
            //$reconfigure->category = 'N/A';
            $reconfigure->total = $expense_na_single['amount'];
            $reconfigure->amount = $expense_na_single;
            //echo $reconfigure->total . "<br />";
            $reconfigure->billing_date = $expense_na_single['billing_date'];
            $expense_na_single['total'] = 0;
            $empty_expense['amount'] = $expense_na_single['amount'];
            $empty_expense['description'] = $expense_na_single['description'];

            // echo "<br />-------<br/ >";
            // echo 'N/A - Whole: ' . $expense_na_single['repeats'] . ' - N/A:<br /><br />';
            // echo "reconfigure<br />";
            // print_r($reconfigure);
            // echo "<br />na_single<br />:";
            // print_r($expense_na_single);
            // echo "<br/ >";
            // print_r($empty_expense);

            //$display_items->add_a_line_item('N/A', $expense_na_single['repeats'], $reconfigure, $expense_na_single, 0, 0, $empty_expense);
            $display_items->add_a_line_item('N/A', '-' . $expense_na_single['sub-category'] . '-', $reconfigure, $expense_na_single, 0, 0, $empty_expense);
        }
        /*
        *   End NA/NA Expenses
        */

        
        //$divisors = $Expenses->get_divisors();
        //$display_items->set_divisors($divisors);
        $temp_company = '';
        if(isset( $_GET['company'] )){
            $temp_company = sanitize_text_field( $_GET['company'] );
        }
        
        $display_items->output( $temp_company );
        echo "<br />Companies represented in this time window: " . $display_items->get_companies_represented();
        echo "<br />* denotes Expense undivided, belongs to this Company in full";
        // echo "divisors:<pre>";
        // print_r($divisors);
        // echo "</pre>";
    }
}

