<?php
/**
 * Empty cart page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-empty.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 7.0.1
 */

?>

<style>
	.woocommerce-notices-wrapper .woocommerce-info{
		color: black;
	}
</style>
<?php
defined( 'ABSPATH' ) || exit;

/*
 * @hooked wc_empty_cart_message - 10
 */
if ( WC()->cart->get_cart_contents_count() == 0 ) {
        echo "Your Cart is Empty. Please return to your <a href='https://my.flyrise.io/dashboard'>Dashboard</a>, or contact your Flyrise representative. Thank you";
}
do_action( 'woocommerce_cart_is_empty' );

if ( wc_get_page_id( 'shop' ) > 0 ) : ?>
	<p class="return-to-shop">
		<a class="button wc-backward<?php echo esc_attr( wc_wp_theme_get_element_class_name( 'button' ) ? ' ' . wc_wp_theme_get_element_class_name( 'button' ) : '' ); ?>" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
			<?php
				/**
				 * Filter "Return To Shop" text.
				 *
				 * @since 4.6.0
				 * @param string $default_text Default text.
				 */
				//echo esc_html( apply_filters( 'woocommerce_return_to_shop_text', __( 'Return to shop', 'woocommerce' ) ) );
				echo "Reloading to log in. Please refresh if the page does not reload.";
			?>
		</a>
	</p>
<?php endif; ?>

<script>
/*
*	Single Order Checkout Needs an initial submission to load adequately
*	so we hide things, saying logging in, and submit,
*	and then on subsequent click it should succeed to charge for the Order
*/
//jQuery(function() {
window.onload = function() {
	console.log('loaded /cart');
	
	jQuery('.woocommerce-notices-wrapper').text('logging in')
	jQuery('#order_review').css('display','none')
	
	if(!window.location.hash) {
    		console.log('first load');
        
		/*
		*	instead of this 'logging in' reload approach,
		*	we're just going to redirect to /payment/checkout
		*	for a unified all items checkout
		*	instead of the quick single item checkout
		*/
    	setTimeout(function(){
    		//window.location = '/cart#reloaded';
    		window.location.reload();
    	},2000);
		
		//wp_redirect('/payment/checkout'); // not defined
    	//window.location = '/cart#reloaded';
    	window.location.reload();
		//window.location = window.location + '#loaded';
        //window.location.reload();
		//jQuery('#place_order').click();
	
	}else{
    	console.log('second load');
		jQuery('.woocommerce-notices-wrapper').text('');
		jQuery('#order_review').css('display','block')
    	}
}//);
</script>
