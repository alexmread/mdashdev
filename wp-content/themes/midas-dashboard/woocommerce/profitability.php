<?php
//ini_set('memory_limit','512M');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL);

function profitability_to_cost_callback() {

    $report = new WC_Report_Everhour_Profitability();
    $report->output_report();
}

class WC_Report_Everhour_Profitability extends WC_Admin_Report {

    /**
     * Output the report.
     */
    public function output_report() {

      $ranges = array(
        
      );
  
      $current_range = ! empty( $_GET['range'] ) ? sanitize_text_field( $_GET['range'] ) : 'month';

      //$company_filter = ! empty( $_GET['company'] ) ? sanitize_text_field( strtolower($_GET['company']) ) : '';

      //$category_filter = ! empty( $_GET['category'] ) ? sanitize_text_field( strtolower($_GET['category']) ) : '';
    
      if ( ! in_array( $current_range, array( 'custom' ) ) ) {
        $current_range = 'month';
      }
  
      $this->check_current_range_nonce( $current_range );
      $this->calculate_current_range( $current_range );
  
      $hide_sidebar = true;
  
      require_once( get_template_directory() . '/woocommerce/html-report-by-date.php');
    }
    

    /**
     * Get the main chart.
     */
    public function get_main_chart() {
        global $wpdb;

        $company_counter;

        /*
        *   LOAD CLASSES
        */
        /* Cached via daily Transient. Clearable */
        $EH_Clients = new Everhour_Clients();
        $EH_Clients->set_api_key(wp_cache_get( 'api_key'));
        $EH_Clients->get_clients();
        $client_lookup = $EH_Clients->get_clients_object();
        // echo "<br />Clients : <br />";
        // echo "<pre>";
        // print_r($client_lookup);
        // echo "</pre>";
        /* Example:
            [0] => stdClass Object
            (
                [projects] => Array
                    (
                    )

                [id] => 931096
                [name] => Midas
                [createdAt] => 2019-09-24 22:02:28
                [lineItemMask] => %MEMBER% :: %PROJECT% :: for %PERIOD%
                [paymentDueDays] => 0
                [reference] => 
                [businessDetails] => 
                [invoicePublicNotes] => 
                [excludedLabels] => Array
                    (
                    )

                [status] => archived
                [enableResourcePlanner] => 
                [favorite] => 
            )
        }
        
        /* Not cached because parameters (dates) change */
        $EH_Time_Records = new Everhour_Time_Records();
        $EH_Time_Records->set_api_key(wp_cache_get( 'api_key'));

        /* NEW Initialize Company Collection */
        $company_collection = new Company_Collection();
        // echo "<pre>";
        // print_r($company_collection); // initially empty
        // echo "</pre>";

        /* NEW keep track of categories per Invoices */
        $category_counter = new Category_Counter();

        /* Expenses now come from Google Sheet */
        $Expenses = new Google_Sheet();
        $all_expenses = $Expenses->get_all_expenses_new();
        // echo "<pre>";
        // print_r($all_expenses);
        // echo "</pre>";
        // exit;

        // let's lookup the company and category and fix a few
        // *** this becomes moot after we switch to EH Company ID ***
        $company_category_lookup = new Company_Category_Lookup();

        //$expenses_companies_encountered = array(); // empty, will fill as we go

        /* Output initialization and set dropdowns */
        $display_items = new Output_Line_Items();


        if( isset($_GET['company']) ){
            //echo "company filter set: " . $_GET['company'];
            $display_items->set_company_dropdown(sanitize_text_field( $_GET['company'] ));
        }else{
            $display_items->set_company_dropdown('xyz');
        }

        if( isset($_GET['category']) ){
            $display_items->set_category_dropdown(sanitize_text_field( strtolower($_GET['category'])));
        }else{
            $display_items->set_category_dropdown('');
        }

        /*
        *   READ DATE RANGE REQUESTED
        *   GET CORRESONDING EH COSTS/MONTHS FROM DB
        */
        $default_current = new DateTime();
        $start_date_default = $default_current->format('Y') . "-" . $default_current->format('m') . "-01";
        $end_date_default = $default_current->format('Y') . "-" . $default_current->format('m') . "-" . cal_days_in_month(CAL_GREGORIAN, $default_current->format('m'), $default_current->format('y'));
        //echo "from: " . $start_date_default . ", to: " . $end_date_default . "<br />";

        $start_date = ! empty( $_GET['start_date'] ) ? sanitize_text_field( $_GET['start_date'] ) : $start_date_default;
        $end_date = ! empty( $_GET['end_date'] ) ? sanitize_text_field( $_GET['end_date'] ) : $end_date_default;
        echo "<div style='display:none' id='date_loaded_info'>updated from: " . $start_date . " to: " . $end_date . "<br />";

        $arr = explode("-", $start_date);
        //print_r($arr);
        $start_year = intval($arr[0]);
        $month_from = intval($arr[1]);
        echo "start month: " . $month_from . "<br />";
        echo "start year: " . $start_year . "<br />";
        
        $arr2 = explode("-", $end_date);
        $end_year = $arr2[0];
        $month_to = intval($arr2[1]);
        echo "end month: " . $month_to . "<br />";
        echo "end year: " . $end_year . "<br />";

        function conglomerate_years($month_from, $month_to, $start_year, $end_year, $EH_Time_Records){
            echo "we'll add the data from:<br />";
            $x = $month_from;
            while($x <= $month_to) {
                if( $x <= 9 ){ $x = "0" . $x; } // just format months less than 10 to, example, 09
                echo $start_year  . "-" . $x . "-01 to " . $start_year . "-" . $x . "-14<br />";
                $data_call = $EH_Time_Records->retrieve_from_db( $start_year . "-" . $x . "-01", $start_year . "-" . $x . "-14");
                switch($x){
                    case 1:case 3:case 5:case 7:case 8:case 10:case 12: $month_end_date = 31; break;
                    case 2: $month_end_date = 28; break;
                    default: $month_end_date = 30;
                }
                echo $start_year  . "-" . $x . "-15 to " . $start_year . "-" . $x . "-" . $month_end_date . "<br />";
                $EH_Time_Records->retrieve_from_db( $start_year . "-" . $x . "-15", $start_year . "-" . $x . "-" . $month_end_date);
                $x++;
            }
        }

        if( $start_year != $end_year ){
            $month_from_inc = $month_from;
            $start_year_inc = $start_year;
            $end_year_inc = $start_year;
            $month_to_altered = 12;
            echo "Multi-year range selected<br />";
            while( $start_year_inc <= $end_year ){
                if ( $start_year_inc == $end_year ) {
                    $month_to_altered = $month_to;
                    $end_year_inc = $end_year;
                }
                //while($month_from_inc <= $month_to_altered){
                    conglomerate_years($month_from_inc, $month_to_altered, $start_year_inc, $end_year_inc, $EH_Time_Records);
                //    $month_from_inc++;
                //}
                $month_from_inc = 1;
                $start_year_inc++;
                $end_year_inc++;
            }
        }else{
            conglomerate_years($month_from, $month_to, $start_year, $end_year, $EH_Time_Records);
        }
        
        
        //$month_from = intval($arr[1]);
        //$month_to = intval($arr2[1]);
        //echo "month from: " . $arr[1] . "<br />";
        //echo "month to: " . $arr2[1] . "<br />";
        
        
        echo "</div><div id='display_date_loaded_info'>&#9432;</div>";
        ?>
        <script>
            jQuery(document).ready(function($){
                $("#display_date_loaded_info").click(function(){
                    $("#date_loaded_info").slideToggle();
                });
            });
        </script>
        <?php        


        /*
        *   TURN ORDERS INTO COMPANY OBJECTS/CARDS
        */
        $orders = $this->get_order_report_data( array(
            'data' => array(
                'ID'           => array(
                    'type'     => 'post_data',
                    'function' => '',
                    'name'     => 'id',
                    //'distinct' => false,
                    'distinct' => true, // as in divisions "workspace" query
                ),
                'post_date'           => array(
                    'type'     => 'post_data',
                    'function' => '',
                    //'name'  =>  'date',
                    'name'     => 'post_date',
                ),
                '_billing_company'   =>  array(
                    'type'  =>  'meta',
                    'function'  =>  '',
                    'name'  =>  'company',
                ),
                '_order_total' => array(
                    'type'     => 'meta',
                    'function' => 'SUM',
                    'name'      => 'order_total'
                    //'name'     => 'total_sales',
                ),
                '_product_id' => array(
                    'type' => 'order_item_meta',
                    'order_item_type' => 'line_item',
                    'function' => '',
                    'name' => 'product_id'
                ),
        
                '_qty' => array(
                    'type' => 'order_item_meta',
                    'order_item_type' => 'line_item',
                    'function' => 'SUM',
                    'name' => 'quantity'
                ),
                '_line_subtotal' => array(
                    'type' => 'order_item_meta',
                    'order_item_type' => 'line_item',
                    'function' => 'SUM',
                    'name' => 'gross'
                ),
                '_line_total' => array(
                    'type' => 'order_item_meta',
                    'order_item_type' => 'line_item',
                    'function' => 'SUM',
                    'name' => 'gross_after_discount'
                )
            ),
            'query_type' => 'get_results',
            'group_by' => 'id',
            'where_meta' => '',
            'order_by' => 'quantity DESC',
            'order_types' => wc_get_order_types('order_count'),
            'nocache'       =>  true,
            'filter_range' => TRUE,
            //'order_status' => array( 'completed', 'processing' ), // took out 'refunded', 'on-hold'
            'order_status' => array( 'completed', 'processing', 'refunded' ), // as in divisions "workspace" query
            'date_query' => array(
                // array(
                //     'key'      => 'order_items.order_item_type',
                //     'value'    => 'line_item',
                //     'operator' => '=',
                // ),
                // array(
                //     'key'      => 'post_date',
                //     'value'    => date('Y-m-d', strtotime($start_date)),
                //     'operator' => '>='
                // ),
                // array(
                //     'key'      => 'post_date',
                //     'value'    => date('Y-m-d', strtotime($end_date)),
                //     'operator' => '<='
                //     'inclusive' => true,
                // ),
                array(
                    'after'     => date('Y-m-d', strtotime($start_date)),
                    'before'    => date('Y-m-d', strtotime($end_date)),
                    'inclusive' => true,
                ),
            ),
        ));
        // $orders = $this->get_order_report_data( array(
        //     'data'         => array(
        //         'ID'             => array(
        //             'type'     => 'post_data',
        //             'function' => '',
        //             'name'     => 'id',
        //             'distinct' => false,
        //         ),
        //         'post_date'           => array(
        //             'type'     => 'post_data',
        //             'function' => '',
        //             //'name'  =>  'date',
        //             'name'     => 'post_date',
        //         ),
        //         '_billing_company'   =>  array(
        //             'type'  =>  'meta',
        //             'function'  =>  '',
        //             'name'  =>  'company',
        //         ),
        //         '_order_total' => array(
        //             'type'     => 'meta',
        //             'function' => 'SUM',
        //             'name'      => 'order_total'
        //             //'name'     => 'total_sales',
        //         ),
        //         '_order_shipping'     => array(
        //             'type'     => 'meta',
        //             'function' => 'SUM',
        //             'name'     => 'total_shipping',
        //         ),
        //         '_order_tax'          => array(
        //             'type'     => 'meta',
        //             'function' => 'SUM',
        //             'name'     => 'total_tax',
        //         ),
        //         '_order_shipping_tax' => array(
        //             'type'     => 'meta',
        //             'function' => 'SUM',
        //             'name'     => 'total_shipping_tax',
        //         ),
        //         '_qty'      => array(
        //             'type'            => 'order_item_meta',
        //             'order_item_type' => 'line_item',
        //             'function'        => 'SUM',
        //             'name'            => 'order_item_count',
        //         ),
        //     ),
        //     'group_by'      => 'id',
        //     //'group_by'    => 'YEAR(posts.post_date), MONTH(posts.post_date), DAY(posts.post_date)',
        //     //'group_by'    => $this->group_by_query,
        //     'order_by'      => 'post_date ASC',
        //     'query_type'    => 'get_results',
        //     'nocache'       =>  true,
        //     'filter_range'  =>  true,
        //     //'order_types'   =>  wc_get_order_types( 'sales-reports' ),
        //     'order_types' =>  wc_get_order_types( 'order-count' ),
        //     'order_status'  =>  array( 'completed', 'processing', 'on-hold', 'refunded' ),
        //     'where' => array(
        //         array(
        //             'key'      => 'order_items.order_item_type',
        //             'value'    => 'line_item',
        //             'operator' => '=',
        //         ),
        //         array(
        //             'key'      => 'post_date',
        //             'value'    => date('Y-m-d', strtotime($start_date)),
        //             'operator' => '>'
        //         ),
        //         array(
        //             'key'      => 'post_date',
        //             'value'    => date('Y-m-d', strtotime($end_date)),
        //             'operator' => '<'
        //         ),
        //     ),
        // ));

        //echo "Orders Found: " . count($orders) . "<br />";


        // Catch All Company for aggregating Products Only View
        $productsOnlyObj = $company_collection->check_company('Products', 123456789, true);

        foreach($orders as $order){

            $woo_order = wc_get_order( $order->id );

            $customer_id = $woo_order->get_customer_id(); // Or $order->get_user_id();
            //$user = $order->get_user($customer_id);
            $eh_id = get_user_meta( $customer_id, 'everhour_client_id', true );
            //echo "Customer ID: " . $customer_id . ", Everhour ID: " . $eh_id . "<br />"; // works!


            // Lookup/homogenize a few company names to make them all agree
            // *** Not needed after conversion to EH ID ***
            $order->company = $company_category_lookup->company( strtolower($order->company) );
            
            if(gettype($order->company)!='string'){
                echo "Found an Object<br />";
            }
            //echo "Order Company: " . $order->company . "<br />";
            //$companyObj = $company_collection->check_company($order->company, true);
            $companyObj = $company_collection->check_company($order->company, $eh_id, true);
            // echo "companyObj: <pre>";
            // print_r($companyObj);
            // echo "</pre>";


            $orderDateTime = new DateTime($woo_order->get_date_paid());
            $orderDateTime = $orderDateTime->format('Y-m');
            
            foreach ( $woo_order->get_items() as  $item_key => $item_values ) {

                $item_data = $item_values->get_data();

                $product_cats = wc_get_product_category_list( $item_data['product_id'] );
                $product_cats_array = explode(",", $product_cats);

                // For each of the Product Categories in Woo
                foreach($product_cats_array as $single_cat){
                    //echo $single_cat; // careful, are links!
                    
                    $single_cat = strip_tags(html_entity_decode($single_cat));

                    //if($isset($order->category)){
                    $order->category = $company_category_lookup->category( strtolower($single_cat) );
                    //}

                    $order_info = new stdClass();
                    $order_info->order_id = $item_data['order_id'];
                    $order_info->category = $single_cat;
                    $order_info->product_id = $item_data['product_id'];
                    $order_info->revenue = $item_data['total'];
                    $order_info->cost = 0;
                    $order_info->time = 0;
                    $order_info->occurrences = 1;
                    $order_info->company = $order->company;
                    $order_info->eh = $eh_id;
                    //$order_info->expense = $expenses;
                    
                    $companyObj->add_line_item($order_info, $category_counter);

                    /* add this to the aggregate as well */
                    $order_info->company = "Products";
                    $order_info->eh_id = 123456789;
                    $productsOnlyObj->add_line_item($order_info, $category_counter, true);

                }
            }         
        }
        //echo $company_collection->company_counter . " Companies Invoiced<br />";  


        /*
        *   GO THROUGH EH COSTS OBJECTS
        *   UPDATE COMPANY OBJECTS/CARDS
        */
        
        
        $time_records = $EH_Time_Records->get_time_records(); 
        // echo "<pre>";
        // print_r($time_records);
        // echo "</pre>";
        // exit;   
        foreach( $time_records as $x => $record ){
            $client_matched = false;
            //echo "Record: " . $x . ", Project: " . $record->task->projects[0] . "<br />";

            $record_info = new stdClass();
            $record_info->order_id = null;
            //echo $record->task->labels[0] . "<br />";
            if(sizeOf($record->task->labels) > 0){
                $record_info->category = $record->task->labels[0];

                if(isset($record_info->category)){
                    $record_info->category = $company_category_lookup->category( strtolower($record_info->category) );
                }
            }else{
                $record_info->category = "No Label";
            }
            $record_info->product_id = null;
            $record_info->revenue = 0;
            $record_info->cost = $record->cost/100;
            $record_info->time = $record->time/60/60; // is in seconds!? so we'll do hours
            $record_info->occurrences = null;


            foreach ( $client_lookup as $client ){

                /*
                *   Exclude Flyrise or Ridgecap Marketing filter Start
                */
                if (
                    isset($_GET['exclude_flyrise']) &&
                    $_GET['exclude_flyrise']=='on' &&
                    //$client->name == "Flyrise"
                    $client->id == 931098
                )
                {
                    $client_matched = true;
                    continue;
                }
                if (
                    isset($_GET['exclude_ridgecapmarketing']) &&
                    $_GET['exclude_ridgecapmarketing']=='on' &&
                    //$client->name == "Ridgecap Marketing"
                    $client->id == 6836522
                )
                {
                    $client_matched = true;
                    continue;
                }
                /*
                *   Exclude Flyrise or Ridgecap Marketing filter End
                */


                $eh_id = $client->id;

                // if(is_object($client->name)){
                //     echo "<pre>";
                //     print_r($client->name);
                //     echo "</pre>";
                // }else{
                //     echo $client->name;
                // }
                foreach ( $client->projects as $project ){
                    if ( $project == $record->task->projects[0] ){
                        //echo "Client match: " . strtolower($client->name) . "<br/ >";
                        $client_matched = true;

                        // Lookup/homogenize a few company names to make them all agree
                        // shouldn't be needed after switch to EH id
                        $client->name = $company_category_lookup->company( strtolower($client->name) );

                        //$companyObj = $company_collection->check_company($client->name, true);
                        $companyObj = $company_collection->check_company($client->name, $eh_id, true);

                        // if line item already exists, update it, otherwise create it
                        $companyObj->add_line_item($record_info, $category_counter);


                        /* add this to the aggregate as well */
                        $record_info->company = "Products";
                        $record_info->eh_id = 123456789;
                        $productsOnlyObj->add_line_item($record_info, $category_counter, true);


                        break 2;
                    }
                }
            }

            if($client_matched == false){
                //echo "EH Project ID did not match any Client Project IDs" . $record->task->name . "<br />";
                //$companyObj = $company_collection->check_company($record->task->name);
                // Let's make the additional Company Objects created when the EH Cost doesn't match a Company = 'No Company' now and consolidate them as such.
                $companyObj = $company_collection->check_company('No Company', null, true);
                
                //print_r($companyObj);
                // echo "Check the Labels: ";
                // print_r($record->task->labels);
                //$record_info->category = (!empty($record->task->labels))?$record->task->labels[0]:'No Label';
                $companyObj->add_line_item($record_info, $category_counter);
                //echo "Added a line item: " . $record_info->category . "<br />";


                /* add this to the aggregate as well */
                $record_info->company = "Products";
                $record_info->eh_id = 123456789;
                $productsOnlyObj->add_line_item($record_info, $category_counter, true);
            }
        }
        

        /*
        *   GO THROUGH GOOGLE EXPENSES
        *   UPDATE COMPANY OBJECTS/CARDS
        */  
        $log_expense_errors = array();
        // echo "<pre>";
        // print_r($category_counter->category_collection);
        // echo "</pre>";
        foreach ( $all_expenses as $single_expense ){
            
            if (
                isset($_GET['exclude_flyrise']) &&
                $_GET['exclude_flyrise']=='on' &&
                $single_expense['company'] == "flyrise"
                //$company_eh_id == 931098
            )
            {
                $client_matched = true;
                //echo "<pre>";print_r($single_expense);echo "</pre><br />";
                continue;
            }
            if (
                isset($_GET['exclude_ridgecapmarketing']) &&
                $_GET['exclude_ridgecapmarketing']=='on' &&
                //$client->name == "Ridgecap Marketing"
                $single_expense['company'] == "Ridgecap Marketing"
                //$company_eh_id == 6836522
            )
            {
                $client_matched = true;
                continue;
            }

            $expenseLoggedOnce = false;

            //echo "Co: " . $single_expense['company'] . "<br />";
            //echo "Cat: " . $single_expense['category'] . "<br />";
            

            // echo "<pre>";
            // print_r($single_expense);
            // echo "</pre>";

            $expense_stamp = strtotime($single_expense['billing_date']);
            $start_date_stamp = strtotime($start_date);
            $end_date_stamp = strtotime($end_date);
            // echo "start date stamp: " . $start_date_stamp . "<br />";
            // echo "Billing date stamp: " . $expense_stamp . "<br />";
            // echo "end date stamp: " . $end_date_stamp . "<br />";
            if ( $expense_stamp >= $start_date_stamp && $expense_stamp <= $end_date_stamp ){
                //echo "Date included<br />";
                
            }else{
                //echo "Date out of selected range<br />";
                continue; // bail, exclude this expense based on date range
            }
            

            // Lookup/homogenize a few company names to make them all agree
            // shouldn't be needed after switch to EH id
            //$single_expense['company'] = $company_category_lookup->company( strtolower($single_expense['company']) );


            //$single_expense['category'] = $company_category_lookup->category( strtolower($single_expense['category']) );

            $single_expense['category'] = strtolower($single_expense['category']);

            if(
                $single_expense['company'] == '' ||
                $single_expense['company'] == false
            ){
                $single_expense['company'] = null;
            }


            if(
                $single_expense['category'] == '' ||
                $single_expense['category'] == false
            ){
                $single_expense['category'] = null;
            }
            
            $expense_info = new stdClass();
            $expense_info->expense = 0;
            $expense_info->expense_description = null;


            //echo "Block 0: " . $single_expense['company'] . ", " . $single_expense['category'] . "<br/ >";
            
            // Company && Category are ALL or N/A
            if(
                //(
                    $single_expense['company'] == 'all' &&
                    $single_expense['category'] == 'all'
                //)
                // ||
                // (
                //     $single_expense['company'] == 'n/a' &&
                //     $single_expense['category'] == 'n/a'
                // )
            ){
                //echo "Block 1: <br/ >";
                // Make an All Company or N/A Company Card and line items become the expense's description
                $expense_info->expense = $single_expense['amount'];
                //$expense_info->category = $single_expense['description'];
                $expense_info->expense_description = $single_expense['description'] . ": $" . $single_expense['amount'];

                //echo "Single expense company: " . $single_expense['company'] . "<br />";
                //echo "All expense company: " . $single_expense['eh_id'] . "<br />";

                $expense_info->category = strtolower($single_expense['sub-category']);
                $expense_info->eh_id = $single_expense['eh_id'];

                $company = $company_collection->check_company($single_expense['company'], $single_expense['eh_id'], true);
                $company->add_line_item($expense_info, $category_counter, true);
                
                $expenseLoggedOnce = true;


                /* add this to the aggregate as well */
                $expense_info->company = "Products";
                $expense_info->eh_id = 123456789;
                $productsOnlyObj->add_line_item($expense_info, $category_counter, true);
            }

            
            //print_r($category_counter->get_category_collection());
            //echo $category_counter->category_collection['search engine optimization']['count'];
            //echo "</pre>";
            //exit;//[$category_key];

            // Company is All & Category is specified
            if (
                $single_expense['company'] == 'all' &&

                $single_expense['category'] != 'all' &&
                $single_expense['category'] != 'n/a' &&
                $single_expense['category'] != null &&
                $expenseLoggedOnce == false
            ){
                //echo "company: " . $single_expense['company'] . ", category: " . $single_expense['category'] . ", description: " . $single_expense['description'] . ", amount: " . $single_expense['amount'] . ", eh_id: " . $single_expense['eh_id'] . "<br />";
                //echo "Block 2: <br/ >";
                foreach ( $company_collection->companies as $company_eh_id => $company_object ){
                    //echo "company_eh_id: " . $company_eh_id . ", expense_eh_id: " . $single_expense['eh_id'] . "<br />";

                    if( $company_eh_id != '0 companies' && $company_eh_id != 'all' ) {
                    //if( $company_object->eh_id != '0 companies' ) {
                        
                        //echo "company_eh_id: " . $company_eh_id . ", single expense eh_id: " . $single_expense['eh_id'] . "<br />";
                        $company_match_and_category_match_and_has_revenue = false;
                        
                        //if ( $company_object->eh_id == $single_expense['eh_id'] ){
                        //if ( $company_eh_id == $single_expense['eh_id'] ){
                            foreach ( $company_object->line_items as $category_key => $line_itemsObj ){
                                //echo "line_item_category_key: " . $category_key . ", single_expense_category: " . $single_expense['category'] . "<br />";
                                
                                // Apply to all Cards with specified Category
                                if ( $category_key == $single_expense['category'] ){
                                    //echo "line item category match: " . $category_key . ", company: " . $company_object->name . ", revenue: " . $line_itemsObj->revenue . "<br />";
                                    
                                    // This stops Expenses from updating line items that had no Woo line item, those were created by EH!
                                    if ( $line_itemsObj->revenue != 0 ) {
                                        $company_match_and_category_match_and_has_revenue = true;
                                        //echo "Revenue check: " . $line_itemsObj->revenue . "<br />";
                                        $expense_info->expense = $single_expense['amount']/$category_counter->category_collection[$single_expense['category']]['count'];
                                        $expense_info->category = $single_expense['category'];
                                        $expense_info->expense_description = $single_expense['description'] . ": $" . $single_expense['amount'] . "/" . $category_counter->category_collection[$single_expense['category']]['count'] . " = $" . number_format($single_expense['amount']/$category_counter->category_collection[$single_expense['category']]['count'], 2);
                                        $expense_info->eh_id = $single_expense['eh_id'];
                                        
                                        //echo "Company key: " . $company_eh_id . "<br />";

                                    

                                        $company = $company_collection->check_company($company_object->name, $company_eh_id, true); // this is the key! grab the company we want to add it to!
                                        $company->add_line_item($expense_info, $category_counter, true);


                                        /* add this to the aggregate as well */
                                        $expense_info->company = "Products";
                                        $expense_info->eh_id = 123456789;
                                        $productsOnlyObj->add_line_item($expense_info, $category_counter, true);
                                    }else{
                                        // no revenue, so this was created by EH!
                                        if($company_match_and_category_match_and_has_revenue == false ){
                                            //echo "Expenses had a 'new' line item " . $line_itemsObj->category . ", " . $line_itemsObj->revenue . "<br />";
                                        }
                                    }
                                    //return;
                                }else{
                                    
                                }
                            }
                        //}else{
                            // this is a company in Expenses that hasn't been created yet, excluding 'all'
                            // really shouldn't ever happen, and thus currently wouldn't get added/created
                            // if( $single_expense['company'] != 'all' ) {
                            //     echo "Expenses found a Company to create: " . $single_expense['company'] . "<br />";
                            // }
                        //}
                    }else{
                        
                    }
                }
                $expenseLoggedOnce == true;
            }
            
            // Company & Category are specified
            if (
                $single_expense['company'] != 'all' &&
                $single_expense['company'] != 'n/a' &&
                $single_expense['company'] != null &&

                $single_expense['category'] != 'all' &&
                $single_expense['category'] != 'n/a' &&
                $single_expense['category'] != null &&
                $expenseLoggedOnce == false
            ){
                //echo "Block 3: <br/ >";
                // foreach ( $company_collection->companies as $company_key => $company_object ){
                //     if( $company_key != 'all' ) {
                        //if ( isset( $company_collection->companies[$company_key] ) ) {
                        $expense_info->expense = $single_expense['amount'];
                        $expense_info->category = $single_expense['category'];                        
                        $expense_info->expense_description = $single_expense['description'] . ": $" . $single_expense['amount'];
                        $expense_info->eh_id = $single_expense['eh_id'];
                        
                        $company = $company_collection->check_company($single_expense['company'], $single_expense['eh_id'], true);

                        $company->add_line_item($expense_info, $category_counter, true);


                        /* add this to the aggregate as well */
                        $expense_info->company = "Products";
                        $expense_info->eh_id = 123456789;
                        $productsOnlyObj->add_line_item($expense_info, $category_counter, true);

                //     }
                // }
            }
        }


        echo $company_collection->company_counter . " Companies Invoiced<br />";  
        /*
        *   DISPLAY OUTPUT TABLE
        *   We used to pass each line item to this Class
        *   Now we just pass the Company Collection in full
        */

        //ksort($company_collection->companies); // this no longer works because it's an array of everhour ids now, not company names
        //echo "Company Collection is an: " . gettype($company_collection->companies) . "<br />";
        //echo "<pre>"; print_r($company_collection->companies); echo "</pre>";
        usort($company_collection->companies, function($a, $b){ return strcmp($a->name, $b->name); });
//         function cmp($a, $b) {
//             return strcmp($a->name, $b->name);
// }

// usort($your_data, "cmp");

        $totals_only = false;
        $products_only = false;
        if ( isset($_GET['totals_only']) && $_GET['totals_only']=='on' ){ $totals_only = true; }
        if ( isset($_GET['products_only']) && $_GET['products_only']=='on' ){ $products_only = true; }

        $display_items->submit_collection($company_collection, $category_counter->get_category_collection(), $totals_only, $products_only );
        //$display_items->add_a_line_item($label, $single_cat, $order, $item_data, $cost, $time, $empty_expense);
        
        $display_items->output($company_collection, $totals_only, $products_only);


        /*
        *   VERIFICATION PREVIEWS
        */
        //$company_collection->output_companies();
        //echo "Test accessing: " . $company_collection->companies['seadon llc']->line_items['search engine optimization']->revenue . "<br />";
        //$category_counter->output_category_collection();
    }

    
}


