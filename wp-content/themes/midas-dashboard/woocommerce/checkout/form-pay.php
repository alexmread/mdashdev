<style>
	#order_review {
		display: none;
	}
</style>

<?php
/**
 * Pay for order form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-pay.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 7.8.0
 */

defined( 'ABSPATH' ) || exit;

	global $current_user;
	$user = wp_get_current_user();
	$brand_switch = get_user_meta($user->ID, 'brand', true);
	//echo "brand switch: " . $brand_switch . "<br />";
	if($brand_switch != null && $brand_switch != false && $brand_switch != 'undefined' && $brand_switch != ''){
		$brand_switch_var = $brand_switch;
	}else{
		$brand_switch_var = false;
	}
/* Order, looking for ridgecap:
	if ( isset($_GET['brand']) && $_GET['brand'] == 'ridgecapmarketing' ){
		... alter ajax-vuejs-dynamic-info action

		add_action( 'woocommerce_thankyou', 'bbloomer_redirectcustom');
      
	    function bbloomer_redirectcustom( $order_id ){
	        $order = wc_get_order( $order_id );
	        $url = 'http://my.ridgecapmarketing.com/pages/settings/orders?brand=ridgecapmarketing';
	        // and/or does it go /thankyou.php?brand=ridgecapmarketing... I think it actually doesn't ...
	        //echo "***************************************bbloomer_redirectcustom ridgecapmarketing in form-pay FIRING!";
	        if ( ! $order->has_status( 'failed' ) ) {
	            wp_safe_redirect( $url );
	            exit;
	        }
	    }
	}
*/

$totals = $order->get_order_item_totals(); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited
?>
<script>
/*
*	Single Order Checkout Needs an initial submission to load adequately
*	so we hide things, saying logging in, and submit,
*	and then on subsequent click it should succeed to charge for the Order
*/
$(function() {
//window.onload = function() {
	console.log('loaded checkout/form-pay');
	
	jQuery('.woocommerce-notices-wrapper').text('logging in')
	jQuery('#order_review').css('display','none')
	
	if(!window.location.hash) {
    	console.log('first load');
        
		/*
		*	instead of this 'logging in' reload approach,
		*	we're just going to redirect to /payment/checkout
		*	for a unified all items checkout
		*	instead of the quick single item checkout
		*/
		//wp_redirect('/payment/checkout'); // not defined
    	window.location = 'https://launch.flyrise.io/payment/checkout';
		//window.location = window.location + '#loaded';
        //window.location.reload();
		//jQuery('#place_order').click();
    }else{
    	console.log('second load');
		jQuery('.woocommerce-notices-wrapper').text('');
		jQuery('#order_review').css('display','block')
    }
});
</script>
<div class="checkout-header">
	<?php
		//if ( isset($_GET['brand']) && $_GET['brand'] == 'ridgecapmarketing' ){
		if ( $brand_switch_var == 'ridgecapmarketing' ){
			?><a href="https://my.ridgecapmarketing.com/dashboard"><img src="https://my.ridgecapmarketing.com/ridgecap-icon-40.png" class="checkout-logo"></a><?php
		}else{
			?><a href="https://my.flyrise.io/dashboard"><img src="/wp-content/themes/midas-dashboard/assets/images/flyrise-icon-gradient.svg" class="checkout-logo"></a><?php
		}
	?>
	<h1 class="card-preheader large">Checkout</h1>
</div>
<hr>

<form id="order_review" method="post" class="mb-0">

	<h3 id="order_review_heading" class="card-preheader mb-3">Your Order</h3>

	<table class="shop_table mb-5">
		<thead>
			<tr>
				<th class="product-name"><?php esc_html_e( 'Product', 'woocommerce' ); ?></th>
				<th class="product-quantity"><?php esc_html_e( 'Qty', 'woocommerce' ); ?></th>
				<th class="product-total"><?php esc_html_e( 'Totals', 'woocommerce' ); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php if ( count( $order->get_items() ) > 0 ) : ?>
				<?php foreach ( $order->get_items() as $item_id => $item ) : ?>
					<?php
					if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
						continue;
					}
					?>
					<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'order_item', $item, $order ) ); ?>">
						<td class="product-name">
							<?php
								echo wp_kses_post( apply_filters( 'woocommerce_order_item_name', $item->get_name(), $item, false ) );
								//echo apply_filters( 'woocommerce_order_item_name', esc_html( $item->get_name() ), $item, false ); // @codingStandardsIgnoreLine

								do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order, false );

								wc_display_item_meta( $item );

								do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order, false );
							?>
						</td>
						<td class="product-quantity"><?php echo apply_filters( 'woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf( '&times;&nbsp;%s', esc_html( $item->get_quantity() ) ) . '</strong>', $item ); ?></td><?php // @codingStandardsIgnoreLine ?>
						<td class="product-subtotal"><?php echo $order->get_formatted_line_subtotal( $item ); ?></td><?php // @codingStandardsIgnoreLine ?>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
		<tfoot>
			<?php if ( $totals ) : ?>
				<?php foreach ( $totals as $total ) : ?>
					<tr>
						<th scope="row" colspan="2"><?php echo $total['label']; ?></th><?php // @codingStandardsIgnoreLine ?>
						<td class="product-total"><?php echo $total['value']; ?></td><?php // @codingStandardsIgnoreLine ?>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tfoot>
	</table>

	<h3 class="card-preheader mb-3">Payment Method</h3>
	<div id="payment">
		<?php if ( $order->needs_payment() ) : ?>
			<ul class="wc_payment_methods payment_methods methods mb-5">
				<?php
				if ( ! empty( $available_gateways ) ) {
					foreach ( $available_gateways as $gateway ) {
						wc_get_template( 'checkout/payment-method.php', array( 'gateway' => $gateway ) );
					}
				} else {
					echo '<li class="woocommerce-notice woocommerce-notice--info woocommerce-info">' . apply_filters( 'woocommerce_no_available_payment_methods_message', esc_html__( 'Sorry, it seems that there are no available payment methods for your location. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce' ) ) . '</li>'; // @codingStandardsIgnoreLine
				}
				?>
			</ul>
			<script type="text/javascript">
				var $ = jQuery;

				$(function(){

					// on page load
					$('li.wc_payment_method > input[type="radio"]').each(function(){
						if ( $(this).prop('checked') == true ) {
							$(this).parents('li.wc_payment_method').addClass("selected");

							if ( $(this).parents('li.wc_payment_method').hasClass('payment_method_plaid') == true ) {
								$('#place_order').text("Connect and Pay");
								// $('#ach-btn').show();
							} else if ( $(this).parents('li.wc_payment_method').hasClass('payment_method_plaid') == false ) {
								$('#place_order').text("Pay for order");
								// $('#ach-btn').hide();
							}
						}
					});

					// on change
					$('li.wc_payment_method > input[type="radio"]').change(function(){
						$('li.wc_payment_method').removeClass("selected");
						if ( $(this).prop('checked') == true ) {
							$(this).parents('li.wc_payment_method').addClass("selected");

							if ( $(this).parents('li.wc_payment_method').hasClass('payment_method_plaid') == true ) {
								$('#place_order').text("Connect and Pay");
								// $('#ach-btn').show();
							} else if ( $(this).parents('li.wc_payment_method').hasClass('payment_method_plaid') == false ) {
								$('#place_order').text("Pay for order");
								// $('#ach-btn').hide();
							}
						}
					});

				});

			</script>
		<?php endif; ?>

		<h3 class="card-preheader mb-3">Terms &amp; Privacy</h3>
		<div class="form-row">
			<input type="hidden" name="woocommerce_pay" value="1" />

			<?php wc_get_template( 'checkout/terms.php' ); ?>

			<?php do_action( 'woocommerce_pay_order_before_submit' ); ?>

			<?php echo apply_filters( 'woocommerce_pay_order_button_html', '<button type="submit" class="button alt' . esc_attr( wc_wp_theme_get_element_class_name( 'button' ) ? ' ' . wc_wp_theme_get_element_class_name( 'button' ) : '' ) . '" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '">' . esc_html( $order_button_text ) . '</button>' ); // @codingStandardsIgnoreLine ?>

			<?php do_action( 'woocommerce_pay_order_after_submit' ); ?>

			<?php wp_nonce_field( 'woocommerce-pay', 'woocommerce-pay-nonce' ); ?>
		</div>
	</div>
</form>
