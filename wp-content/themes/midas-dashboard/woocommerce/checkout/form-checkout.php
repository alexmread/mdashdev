<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

	global $current_user;
	$user = get_currentuserinfo();
	$brand_switch = get_user_meta($user->ID, 'brand', true);
	//echo "brand switch: " . $brand_switch . "<br />";
	if($brand_switch != null && $brand_switch != false && $brand_switch != 'undefined' && $brand_switch != ''){
		$brand_switch_var = $brand_switch;
	}else{
		$brand_switch_var = false;
	}
?>
<!-- <bold>wp-content/themes/midas-dashboard/woocommerce/checkout/form-checkout.php</bold> -->

<div class="checkout-header">
	<?php
		//if ( isset($_GET['brand']) && $_GET['brand'] == 'ridgecapmarketing' ){
		if ( $brand_switch_var == 'ridgecapmarketing' ){
			?><a href="https://my.ridgecapmarketing.com/dashboard"><img src="/wp-content/themes/midas-dashboard/assets/images/ridgecap-icon-40.png" class="checkout-logo"></a><?php
		}else{
			?><a href="https://my.flyrise.io/dashboard"><img src="/wp-content/themes/midas-dashboard/assets/images/flyrise-icon-gradient.svg" class="checkout-logo"></a><?php
		}
	?>
	<h1 class="card-preheader large">Checkout</h1>
</div>
<hr>

<?php

// do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
	return;
}

?>

<form name="checkout" method="post" class="checkout woocommerce-checkout row" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<?php if ( $checkout->get_checkout_fields() ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col-md-6" id="customer_details">
			<div class="">
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
			</div>

			<div class="">
				<?php do_action( 'woocommerce_checkout_shipping' ); ?>
			</div>
		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

	<?php endif; ?>
	
	<?php do_action( 'woocommerce_checkout_before_order_review_heading' ); ?>
	
	
	
	<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>


	<div id="order_review" class="woocommerce-checkout-review-order col-md-6">
	<h3 id="order_review_heading" class="card-preheader mb-3"><?php esc_html_e( 'Your order', 'woocommerce' ); ?></h3>
		<?php do_action( 'woocommerce_checkout_order_review' ); ?>
	</div>

	<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
