<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

	global $current_user;
	$user = get_currentuserinfo();
	$brand_switch = get_user_meta($user->ID, 'brand', true);
	//echo "brand switch: " . $brand_switch . "<br />";
	if($brand_switch != null && $brand_switch != false && $brand_switch != 'undefined' && $brand_switch != ''){
		$brand_switch_var = $brand_switch;
	}else{
		$brand_switch_var = false;
	}
?>

<div class="checkout-header">
	<?php
		if ( $brand_switch_var == 'ridgecapmarketing' ){
			?><a href="https://my.ridgecapmarketing.com/dashboard"><img src="/wp-content/themes/midas-dashboard/assets/images/ridgecap-icon-40.png" class="checkout-logo"></a><?php
		}else{
			?><a href="https://my.flyrise.io/dashboard"><img src="/wp-content/themes/midas-dashboard/assets/images/flyrise-icon-gradient.svg" class="checkout-logo"></a><?php
	}
	?>
	<h1 class="card-preheader large">Order Received</h1>
</div>
<hr>

<div class="woocommerce-order">

	<?php
	if ( $order ) :

		do_action( 'woocommerce_before_thankyou', $order->get_id() );
		?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed alert text-white font-weight-bold alert-danger null"><?php esc_html_e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions alert text-white font-weight-bold alert-danger null">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php esc_html_e( 'Pay', 'woocommerce' ); ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php esc_html_e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>

		<?php else : ?>

			<div class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received alert text-white font-weight-bold alert-success null"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), $order ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></div>

			<div class="order-overview-wrapper card">
				<div class="card-body">
					<ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">

						<li class="woocommerce-order-overview__order order">
							<?php esc_html_e( 'Order number:', 'woocommerce' ); ?>
							<strong><?php echo $order->get_order_number(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
						</li>

						<li class="woocommerce-order-overview__date date">
							<?php esc_html_e( 'Date:', 'woocommerce' ); ?>
							<strong><?php echo wc_format_datetime( $order->get_date_created() ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
						</li>

						<?php if ( is_user_logged_in() && $order->get_user_id() === get_current_user_id() && $order->get_billing_email() ) : ?>
							<li class="woocommerce-order-overview__email email">
								<?php esc_html_e( 'Email:', 'woocommerce' ); ?>
								<strong><?php echo $order->get_billing_email(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
							</li>
						<?php endif; ?>

						<li class="woocommerce-order-overview__total total">
							<?php esc_html_e( 'Total:', 'woocommerce' ); ?>
							<strong><?php echo $order->get_formatted_order_total(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
						</li>

						<?php if ( $order->get_payment_method_title() ) : ?>
							<li class="woocommerce-order-overview__payment-method method">
								<?php esc_html_e( 'Payment method:', 'woocommerce' ); ?>
								<strong><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></strong>
							</li>
						<?php endif; ?>

					</ul>
				</div>
			</div>

		<?php endif; ?>

		<?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
		<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>

	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received alert text-white font-weight-bold alert-success null"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), null ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>

	<?php endif; ?>

	<a href="/dashboard" class="btn btn-dark">Return To Dashboard</a>
</div>

<script>
	var $ = jQuery;

	$(function(){
		$('body').addClass("woocommerce-order-received");

		$('table.order_details').addClass('el-table no-outside-column-padding');

		$('section.woocommerce-order-details').prepend('<div class="border-0 card-header"><h3 class="mb-0">Order Details</h3></div>');

		$('section.woocommerce-order-details').addClass("card").append('<div class="card-body"></div>').children('table').appendTo('section.woocommerce-order-details .card-body');

		$('section.woocommerce-customer-details').prepend('<div class="border-0 card-header"><h3 class="mb-0">Billing Address</h3></div>');

		$('section.woocommerce-customer-details').addClass("card").append('<div class="card-body"></div>').children('address').appendTo('section.woocommerce-customer-details .card-body');

	});
</script>



