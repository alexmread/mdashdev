<?php
/**
 * Customer completed order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-completed-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>
								<?php 
									/**
									 * Show user-defined additional content - this is set in each email's settings.
									 */
									if ( $additional_content ) {
										echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) );
									}
								?>
							</div>
						</td>
					</tr>
					<tr>
						<td align="left" valign="top" colspan="2">
							<h1 id="flyrise-header-headline"><?php echo $email_heading; ?></h1>
						</td>
					</tr>
					<tr>
						<td align="left" valign="top" colspan="2">
							<div id="flyrise-header-headline-after">
								<?php /* translators: %s: Customer first name */ ?>
								<p><?php printf( esc_html__( 'Hi %s,', 'woocommerce' ), esc_html( $order->get_billing_first_name() ) ); ?></p>
								<?php /* translators: %s: Site title */ ?>
								<p><?php esc_html_e( 'We have finished processing your order.', 'woocommerce' ); ?></p>
							</div>
						</td>
					</tr>
					
					<!-- TOP OVERLAP SPACER -->
					<!-- <tr>
						<td align="center" valign="bottom" colspan="2">
							<table border="0" cellpadding="0" cellspacing="0" width="100%" style="">
								<tr>
									<td align="center" valign="bottom" colspan="2">
										<table border="0" cellpadding="30" cellspacing="0" width="570" bgcolor="#ffffff" style="">
											<tr>
												<td align="center" valign="bottom" colspan="2">
													<div id="top-spacer-wrapper" style="">
														<div id="top-spacer" style="background-color: #ffffff;">&nbsp;</div>
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr> -->
					<!-- TOP OVERLAP SPACER -->

				</table>
			</div>
		</td>
	</tr>
</table>
<!-- END HEADER -->

<!-- START BODY -->
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
	<tr>
		<td align="center" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" height="100%" width="600" id="template_container">
				<tr>
					<td align="center" valign="top">
						<!-- Body -->
						<table border="0" cellpadding="0" cellspacing="0" width="570" id="template_body">
							<tr>
								<td valign="top" id="body_content">
									<!-- Content -->
									<table border="0" cellpadding="20" cellspacing="0" height="100%" width="100%">
										<tr>
											<td valign="top" bgcolor="#ffffff">
												<div id="body_content_inner">


<?php

/*
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
