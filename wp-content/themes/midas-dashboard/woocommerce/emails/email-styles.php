<?php
/**
 * Email Styles
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-styles.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 4.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Load colors.
$bg        = get_option( 'woocommerce_email_background_color' );
$body      = get_option( 'woocommerce_email_body_background_color' );
$base      = get_option( 'woocommerce_email_base_color' );
$base_text = wc_light_or_dark( $base, '#202020', '#ffffff' );
$text      = get_option( 'woocommerce_email_text_color' );

// Pick a contrasting color for links.
// $link_color = wc_hex_is_light( $base ) ? $base : $base_text;

if ( wc_hex_is_light( $body ) ) {
	// $link_color = wc_hex_is_light( $base ) ? $base_text : $base;
}

// Call the global WC_Email object
global $email; 

// Get an instance of the WC_Order object
$order = $email->object;
$email_customer_user_id = get_post_meta( $email->object->ID, '_customer_user', true );
$customer_brand = get_user_meta($email_customer_user_id, 'brand', true);

if ($email_customer_user_id && $customer_brand == "ridgecapmarketing") {
  $link_color = "#ef8354";
} else {
  $link_color = "#F84470";
}



$bg_darker_10    = wc_hex_darker( $bg, 10 );
$body_darker_10  = wc_hex_darker( $body, 10 );
$base_lighter_20 = wc_hex_lighter( $base, 20 );
$base_lighter_40 = wc_hex_lighter( $base, 40 );
$text_lighter_20 = wc_hex_lighter( $text, 20 );
$text_lighter_40 = wc_hex_lighter( $text, 40 );


// $bg_darker_10    = "#03e3fc";
// $body_darker_10  = "#fcba03";
// $base_lighter_20 = "#0000ff";
// $base_lighter_40 = "#ff0000";
// $text_lighter_20 = "#00ff00";
// $text_lighter_40 = "#ff00ff";

// !important; is a gmail hack to prevent styles being stripped if it doesn't like something.
// body{padding: 0;} ensures proper scale/positioning of the email in the iOS native email app.
?>

body {
	padding: 0;
}

#wrapper {
	background-color: <?php echo esc_attr( $bg ); ?>;
	margin: 0;
	padding: 0;
	-webkit-text-size-adjust: none !important;
	width: 100%;
}

#template_container {
	box-shadow: none !important;
	background-color: <?php echo esc_attr( $body ); ?>;
	border: 0px solid transparent;
	border-radius: 0px !important;
	margin-bottom: 60px
}

#template_header {
	background-color: <?php echo esc_attr( $base ); ?>;
	border-radius: 3px 3px 0 0 !important;
	color: <?php echo esc_attr( $base_text ); ?>;
	border-bottom: 0;
	font-weight: bold;
	line-height: 100%;
	vertical-align: middle;
	font-family: "Muli", "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
}

#template_header h1,
#template_header h1 a {
	color: <?php echo esc_attr( $base_text ); ?>;
}

#template_header_image img {
	margin-left: 0;
	margin-right: 0;
	max-width: 250px;
	margin-bottom: 30px;
}

#template_footer td {
	padding: 0;
	border-radius: 6px;
}

#template_footer #credit {
	border: 0;
	color: <?php echo esc_attr( $text_lighter_40 ); ?>;
	font-family: "Muli", "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 12px;
	line-height: 150%;
	text-align: center;
	padding: 24px 0;
}

#template_footer #credit p {
	margin: 0 0 16px;
}

#body_content {
	background-color: <?php echo esc_attr( $body ); ?>;
}

#body_content table td {
	padding: 0px 48px 0px;
}

#body_content table td td {
	padding: 12px;
}

#body_content table td th {
	padding: 12px;
}

#body_content td ul.wc-item-meta {
	font-size: small;
	margin: 1em 0 0;
	padding: 0;
	list-style: none;
}

#body_content td ul.wc-item-meta li {
	margin: 0.5em 0 0;
	padding: 0;
}

#body_content td ul.wc-item-meta li p {
	margin: 0;
}

#body_content p {
	margin: 0 0 16px;
}

#body_content_inner {
	color: <?php echo esc_attr( $text_lighter_20 ); ?>;
	font-family: "Muli", "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 14px;
	line-height: 150%;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

.td {
	color: <?php echo esc_attr( $text_lighter_20 ); ?>;
	border: 0px solid transparent !important;
	vertical-align: middle;
}

.address {
	padding: 0;
	color: <?php echo esc_attr( $text_lighter_20 ); ?>;
	border: 0px solid transparent !important;
}

.text {
	color: <?php echo esc_attr( $text ); ?>;
	font-family: "Muli", "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
}

.link {
	color: <?php echo esc_attr( $base ); ?>;
}

#header_wrapper {
	padding: 36px 48px;
	display: block;
}

h1 {
	color: <?php echo esc_attr( $base ); ?>;
	font-family: 'Source Serif Pro', serif;
	font-size: 30px;
	font-weight: 700;
	line-height: 150%;
	margin: 0;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

h2 {
	color: <?php echo esc_attr( $base ); ?>;
	display: block;
	font-family: 'Source Serif Pro', serif;
	font-size: 18px;
	font-weight: 700;
	line-height: 130%;
	margin: 0 0 18px;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

h3 {
	color: <?php echo esc_attr( $base ); ?>;
	display: block;
	font-family: 'Source Serif Pro', serif;
	font-size: 16px;
	font-weight: 700;
	line-height: 130%;
	margin: 16px 0 8px;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

a {
	color: <?php echo esc_attr( $link_color ); ?>;
	font-weight: normal;
	text-decoration: underline;
}

img {
	border: none;
	display: inline-block;
	font-size: 14px;
	font-weight: bold;
	height: auto;
	outline: none;
	text-decoration: none;
	text-transform: capitalize;
	vertical-align: middle;
	margin-<?php echo is_rtl() ? 'left' : 'right'; ?>: 10px;
	max-width: 100%;
	height: auto;
}

<!-- START FLYRISE CUSTOM STYLES -->
<!-- START FLYRISE CUSTOM STYLES -->
<!-- START FLYRISE CUSTOM STYLES -->

#top-gradient-bar {
  background: #f84470;
  background: -webkit-linear-gradient(135deg, #f84470 50%, #fb7545 85%);
  background: -webkit-linear-gradient(315deg, #f84470 50%, #fb7545 85%);
  background: linear-gradient(135deg, #f84470 50%, #fb7545 85%);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#f84470",endColorstr="#fb7545",GradientType=1);
  width: 100% !important;
}

#top-gradient-bar #top-spacer-wrapper {
  background: #f84470;
  background: -webkit-linear-gradient(135deg, #f84470 50%, #fb7545 85%);
  background: -webkit-linear-gradient(315deg, #f84470 50%, #fb7545 85%);
  background: linear-gradient(135deg, #f84470 50%, #fb7545 85%);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#f84470",endColorstr="#fb7545",GradientType=1);
  width: 100% !important;
}

#top-gradient-bar #top-spacer-wrapper #top-spacer {
  background: #f84470;
  background: -webkit-linear-gradient(135deg, #f84470 50%, #fb7545 85%);
  background: -webkit-linear-gradient(315deg, #f84470 50%, #fb7545 85%);
  background: linear-gradient(135deg, #f84470 50%, #fb7545 85%);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#f84470",endColorstr="#fb7545",GradientType=1);
  width: 100% !important;
}

#wrapper {
  background-color: #f8f8f5;
}

#wrapper #flyrise-header {
  background-color: #f8f8f5;
}

#wrapper #flyrise-header #flyrise-header-content-wrap {
  margin: 60px 0 0;
}

#wrapper #flyrise-header #flyrise-header-content-wrap #flyrise-header-content #flyrise-header-logo {
  margin-left: 48px;
}

#wrapper #flyrise-header #flyrise-header-content-wrap #flyrise-header-content #flyrise-header-logo > p {
  margin-bottom: 0;
}

#wrapper #flyrise-header #flyrise-header-content-wrap #flyrise-header-content #flyrise-header-intro {
  margin-right: 48px;
  font-family: "Muli", "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif !important;
  font-size: 14px !important;
  font-weight: 400 !important;
  line-height: 150% !important;
  color: #161436;
}

#wrapper #flyrise-header #flyrise-header-content-wrap #flyrise-header-content #flyrise-header-headline {
  font-family: 'Source Serif Pro', serif !important;
  font-size: 42px !important;
  font-weight: 700 !important;
  line-height: 150% !important;
  color: #161436 !important;
  margin: 60px 48px 15px !important;
}

#wrapper #flyrise-header #flyrise-header-content-wrap #flyrise-header-content #flyrise-header-headline-after {
  font-family: "Muli", "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif !important;
  font-size: 20px !important;
  font-weight: 400 !important;
  line-height: 150% !important;
  color: #161436;
  margin: 0 48px 80px !important;
}

#template_container #template_body {
  height: 100% !important;
}

#body_content #body_content_inner {
  padding-top: 40px !important;
}

#body_content #body_content_inner h1 {
  font-family: 'Source Serif Pro', serif !important;
  font-size: 42px !important;
  font-weight: 700 !important;
  line-height: 150% !important;
}

#body_content #body_content_inner h2 {
  font-family: 'Source Serif Pro', serif !important;
  font-size: 20px !important;
  font-weight: 700 !important;
  line-height: 130% !important;
}

#body_content #body_content_inner h2 a {
  font-family: 'Source Serif Pro', serif !important;
  font-size: 20px !important;
  font-weight: 700 !important;
  line-height: 130% !important;
  font-weight: inherit !important;
}

#body_content #body_content_inner h3 {
  font-family: 'Source Serif Pro', serif !important;
  font-size: 14px !important;
  font-weight: 700 !important;
  line-height: 130% !important;
}

#body_content #body_content_inner table, #body_content #body_content_inner th, #body_content #body_content_inner td {
  border: 0px solid transparent !important;
  border-collapse: collapse;
  font-family: "Muli", "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif !important;
  font-size: 14px !important;
  font-weight: 400 !important;
  line-height: 150% !important;
  color: #161436;
}

#body_content #body_content_inner table th {
  background-color: #f8f8f5;
  font-weight: 700;
}

#body_content #body_content_inner table#address {
  width: 100%;
}

#flyrise-footer {
  background-color: #f8f8f5;
}

#flyrise-footer #flyrise-footer-content-wrap {
  margin: 0 0 60px;
}

#flyrise-footer #flyrise-footer-content-wrap #flyrise-footer-content #flyrise-footer-logo {
  margin: 60px 0 60px;
}

#flyrise-footer #flyrise-footer-content-wrap #flyrise-footer-content #flyrise-footer-copyright {
  font-family: "Muli", "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif !important;
  font-size: 14px !important;
  font-weight: 400 !important;
  line-height: 150% !important;
}






<?php
  // Call the global WC_Email object
  global $email; 

  // Get an instance of the WC_Order object
  $order = $email->object;
  $email_customer_user_id = get_post_meta( $email->object->ID, '_customer_user', true );
  $customer_brand = get_user_meta($email_customer_user_id, 'brand', true);

  if ($email_customer_user_id && $customer_brand == "ridgecapmarketing") {
?>
  <!-- START RIDGECAP CUSTOM STYLES -->
  <!-- START RIDGECAP CUSTOM STYLES -->
  <!-- START RIDGECAP CUSTOM STYLES -->

  #top-gradient-bar {
    background-color: #ef8354 !important;
    background: #ef8354 !important;
  }

  #top-gradient-bar #top-spacer-wrapper {
    background-color: #ef8354 !important;
    background: #ef8354 !important;
  }

  #top-gradient-bar #top-spacer-wrapper #top-spacer {
    background-color: #ef8354 !important;
    background: #ef8354 !important;
  }

  #wrapper {
    background-color: #efeee6;
  }

  #wrapper #flyrise-header {
    background-color: #efeee6;
  }

  #wrapper #flyrise-header #flyrise-header-content-wrap {
    margin: 60px 0 0;
  }

  #wrapper #flyrise-header #flyrise-header-content-wrap #flyrise-header-content #flyrise-header-logo {
    margin-left: 48px;
  }

  #wrapper #flyrise-header #flyrise-header-content-wrap #flyrise-header-content #flyrise-header-logo > p {
    margin-bottom: 0;
  }

  #wrapper #flyrise-header #flyrise-header-content-wrap #flyrise-header-content #flyrise-header-intro {
    margin-right: 48px;
    font-family: "Montserrat", "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif !important;
    font-size: 14px !important;
    font-weight: 400 !important;
    line-height: 150% !important;
    color: #2d3142;
  }

  #wrapper #flyrise-header #flyrise-header-content-wrap #flyrise-header-content #flyrise-header-headline {
    font-family: 'Bebas Neue', sans-serif !important;
    font-size: 42px !important;
    font-weight: 700 !important;
    line-height: 150% !important;
    color: #2d3142 !important;
    margin: 60px 48px 15px !important;
  }

  #wrapper #flyrise-header #flyrise-header-content-wrap #flyrise-header-content #flyrise-header-headline-after {
    font-family: "Montserrat", "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif !important;
    font-size: 20px !important;
    font-weight: 400 !important;
    line-height: 150% !important;
    color: #2d3142;
    margin: 0 48px 80px !important;
  }

  #template_container #template_body {
    height: 100% !important;
  }

  #body_content #body_content_inner {
    padding-top: 40px !important;
  }

  #body_content #body_content_inner h1 {
    font-family: 'Bebas Neue', sans-serif !important;
    font-size: 42px !important;
    font-weight: 700 !important;
    line-height: 150% !important;
  }

  #body_content #body_content_inner h2 {
    font-family: 'Bebas Neue', sans-serif !important;
    font-size: 20px !important;
    font-weight: 700 !important;
    line-height: 130% !important;
  }

  #body_content #body_content_inner h2 a {
    font-family: 'Bebas Neue', sans-serif !important;
    font-size: 20px !important;
    font-weight: 700 !important;
    line-height: 130% !important;
    font-weight: inherit !important;
  }

  #body_content #body_content_inner h3 {
    font-family: 'Bebas Neue', sans-serif !important;
    font-size: 14px !important;
    font-weight: 700 !important;
    line-height: 130% !important;
  }

  #body_content #body_content_inner table, #body_content #body_content_inner th, #body_content #body_content_inner td {
    border: 0px solid transparent !important;
    border-collapse: collapse;
    font-family: "Montserrat", "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif !important;
    font-size: 14px !important;
    font-weight: 400 !important;
    line-height: 150% !important;
    color: #2d3142;
  }

  #body_content #body_content_inner table th {
    background-color: #efeee6;
    font-weight: 700;
  }

  #body_content #body_content_inner table#address {
    width: 100%;
  }

  #flyrise-footer {
    background-color: #efeee6 !important;
  }

  #flyrise-footer #flyrise-footer-content-wrap {
    margin: 0 0 60px;
  }

  #flyrise-footer #flyrise-footer-content-wrap #flyrise-footer-content #flyrise-footer-logo {
    margin: 60px 0 60px;
  }

  #flyrise-footer #flyrise-footer-content-wrap #flyrise-footer-content #flyrise-footer-copyright {
    font-family: "Montserrat", "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif !important;
    font-size: 14px !important;
    font-weight: 400 !important;
    line-height: 150% !important;
    color: #2d3142 !important;
  }

  

<?php } else {} ?>


<?php
