<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>
								<?php 
									/**
									 * Show user-defined additional content - this is set in each email's settings.
									 */
									if ( $additional_content ) {
										echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) );
									}
								?>
							</div>
						</td>
					</tr>
					<tr>
						<td align="left" valign="top" colspan="2">
							<h1 id="flyrise-header-headline"><?php echo $email_heading; ?></h1>
						</td>
					</tr>
					<tr>
						<td align="left" valign="top" colspan="2">
							<div id="flyrise-header-headline-after">
								<p>Just a heads up to confirm your account is up and running with us. If you have any questions, just reply to this email and we'll be happy to assist!</p>
							</div>
						</td>
					</tr>
					<!-- TOP OVERLAP SPACER -->
					<!-- <tr>
						<td align="center" valign="bottom" colspan="2">
							<table border="0" cellpadding="0" cellspacing="0" width="100%" style="">
								<tr>
									<td align="center" valign="bottom" colspan="2">
										<table border="0" cellpadding="30" cellspacing="0" width="570" bgcolor="#ffffff" style="">
											<tr>
												<td align="center" valign="bottom" colspan="2">
													<div id="top-spacer-wrapper" style="">
														<div id="top-spacer" style="background-color: #ffffff;">&nbsp;</div>
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr> -->
					<!-- TOP OVERLAP SPACER -->

				</table>
			</div>
		</td>
	</tr>
</table>
<!-- END HEADER -->

<!-- START BODY -->
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
	<tr>
		<td align="center" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" height="100%" width="600" id="template_container">
				<tr>
					<td align="center" valign="top">
						<!-- Body -->
						<table border="0" cellpadding="0" cellspacing="0" width="570" id="template_body">
							<tr>
								<td valign="top" id="body_content">
									<!-- Content -->
									<table border="0" cellpadding="20" cellspacing="0" height="100%" width="100%">
										<tr>
											<td valign="top" bgcolor="#ffffff">
												<div id="body_content_inner">

													<?php /* translators: %s: Customer username */ ?>
													<p><?php printf( esc_html__( 'Hi %s,', 'woocommerce' ), esc_html( $user_login ) ); ?></p>
													<?php /* translators: %1$s: Site title, %2$s: Username, %3$s: My account link */ ?>
													<p><?php printf( esc_html__( 'Thanks for creating an account on %1$s. Your username is %2$s. You can access your account area to view orders, change your password, and more at: %3$s', 'woocommerce' ), esc_html( $blogname ), '<strong>' . esc_html( $user_login ) . '</strong>', make_clickable( esc_url( wc_get_page_permalink( 'myaccount' ) ) ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
													<?php if ( 'yes' === get_option( 'woocommerce_registration_generate_password' ) && $password_generated ) : ?>
														<?php /* translators: %s: Auto generated password */ ?>
														<p><?php printf( esc_html__( 'Your password has been automatically generated: %s', 'woocommerce' ), '<strong>' . esc_html( $user_pass ) . '</strong>' ); ?></p>
													<?php endif; ?>

<?php

do_action( 'woocommerce_email_footer', $email );
