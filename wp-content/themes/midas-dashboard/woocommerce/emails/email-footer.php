<?php
/**
 * Email Footer
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-footer.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 7.4.0
 */

defined( 'ABSPATH' ) || exit;
?>
															</div>
														</td>
													</tr>
												</table>
												<!-- End Content -->
											</td>
										</tr>
									</table>
									<!-- End Body -->
								</td>
							</tr>
						</table>
					</td>
				</tr>

			</table>

			<!-- flyrise-footer -->
			<?php
			  // Call the global WC_Email object
			  global $email; 

			  // Get an instance of the WC_Order object
			  $order = $email->object;
			  $email_customer_user_id = get_post_meta( $email->object->ID, '_customer_user', true );
			  $customer_brand = get_user_meta($email_customer_user_id, 'brand', true);

			  if ($email_customer_user_id && $customer_brand == "ridgecapmarketing") {
			?>
				<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#efeee6" id="flyrise-footer">
			<?php } else { ?>
				<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#f8f8f5" id="flyrise-footer">
			<?php } ?>
			
				<tr>
					<td align="center" valign="top">

						<!-- flyrise-footer-content-wrap -->
						<div id="flyrise-footer-content-wrap">
							<table border="0" cellpadding="0" cellspacing="0" width="600" id="flyrise-footer-content">
								<tr>
									<td align="center" valign="top" colspan="2" >
										<div>
											<!-- BOTTOM OVERLAP SPACER -->
											<!-- <table border="0" cellpadding="30" cellspacing="0" width="570" bgcolor="#ffffff" style="">
												<tr>
													<td align="center" valign="top" colspan="2">
														<div id="top-spacer-wrapper" style="">
															<div id="top-spacer" style="background-color: #ffffff;">&nbsp;</div>
														</div>
													</td>
												</tr>
											</table> -->
											<!-- BOTTOM OVERLAP SPACER -->
										</div>
									</td>
								</tr>
								<tr>
									<td align="center" valign="top">

										<!-- flyrise-footer-logo -->
										<div id="flyrise-footer-logo">
											<?php
											  // Call the global WC_Email object
											  global $email; 

											  // Get an instance of the WC_Order object
											  $order = $email->object;
											  $email_customer_user_id = get_post_meta( $email->object->ID, '_customer_user', true );
											  $customer_brand = get_user_meta($email_customer_user_id, 'brand', true);

											  if ($email_customer_user_id && $customer_brand == "ridgecapmarketing") {
											?>
												<p style="margin-top:0;margin-bottom:0;"><img src="https://my.ridgecapmarketing.com/ridgecap-icon-40.png"/></p>
											<?php } else { ?>
												<p style="margin-top:0;margin-bottom:0;"><img src="https://launch.flyrise.io/wp-content/uploads/flyrise-icon-pink.png"/></p>
											<?php } ?>
										</div>
									</td>
								</tr>
								<tr>
									<td align="center" valign="top">
										<div id="flyrise-footer-copyright">

											<?php
											  // Call the global WC_Email object
											  global $email; 

											  // Get an instance of the WC_Order object
											  $order = $email->object;
											  $email_customer_user_id = get_post_meta( $email->object->ID, '_customer_user', true );
											  $customer_brand = get_user_meta($email_customer_user_id, 'brand', true);

											  if ($email_customer_user_id && $customer_brand == "ridgecapmarketing") {
											?>
												© 2022 Ridgecap Marketing
											<?php } else { ?>
												© 2022 Flyrise
											<?php } ?>


										</div>

										

									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
			</table>
		
		</table>
		</div>
		<!-- wrapper -->
	</body>
</html>
