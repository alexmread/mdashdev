<?php
/**
 * Admin new renewal order email
 *
 * @author  Brent Shepherd
 * @package WooCommerce_Subscriptions/Templates/Emails
 * @version 2.6.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>
								<?php 
									/**
									 * Show user-defined additional content - this is set in each email's settings.
									 */
									if ( $additional_content ) {
										echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) );
									}
								?>
							</div>
						</td>
					</tr>
					<tr>
						<td align="left" valign="top" colspan="2">
							<h1 id="flyrise-header-headline"><?php echo $email_heading; ?></h1>
						</td>
					</tr>
					<tr>
						<td align="left" valign="top" colspan="2">
							<div id="flyrise-header-headline-after">
								<p><?php do_action( 'woocommerce_email_custom_body', $order, $sent_to_admin, $plain_text, $email ); ?></p>
							</div>
						</td>
					</tr>
					<!-- TOP OVERLAP SPACER -->
					<!-- <tr>
						<td align="center" valign="bottom" colspan="2">
							<table border="0" cellpadding="0" cellspacing="0" width="100%" style="">
								<tr>
									<td align="center" valign="bottom" colspan="2">
										<table border="0" cellpadding="30" cellspacing="0" width="570" bgcolor="#ffffff" style="">
											<tr>
												<td align="center" valign="bottom" colspan="2">
													<div id="top-spacer-wrapper" style="">
														<div id="top-spacer" style="background-color: #ffffff;">&nbsp;</div>
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr> -->
					<!-- TOP OVERLAP SPACER -->

				</table>
			</div>
		</td>
	</tr>
</table>
<!-- END HEADER -->

<!-- START BODY -->
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
	<tr>
		<td align="center" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" height="100%" width="600" id="template_container">
				<tr>
					<td align="center" valign="top">
						<!-- Body -->
						<table border="0" cellpadding="0" cellspacing="0" width="570" id="template_body">
							<tr>
								<td valign="top" id="body_content">
									<!-- Content -->
									<table border="0" cellpadding="20" cellspacing="0" height="100%" width="100%">
										<tr>
											<td valign="top" bgcolor="#ffffff">
												<div id="body_content_inner">


<?php

/**
 * @hooked WC_Subscriptions_Email::order_details() Shows the order details table.
 * @since 2.1.0
 */
do_action( 'woocommerce_subscriptions_email_order_details', $order, $sent_to_admin, $plain_text, $email );

do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

do_action( 'woocommerce_email_footer', $email );
