<?php
/**
 * Email Header
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-header.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 4.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
		<title><?php echo get_bloginfo( 'name', 'display' ); ?></title>
		<!-- <link rel="stylesheet" href="/wp-content/themes/midas-dashboard/assets/sass/compiled/email.css?v=2.1" media="all"> -->
		<!-- <link rel="stylesheet" href="https://my.flyrise.io/wp-content/themes/midas-dashboard/assets/sass/compiled/email.css?v=2" media="all"> -->
	</head>
	<body <?php echo is_rtl() ? 'rightmargin' : 'leftmargin'; ?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
		<?php
		    // Call the global WC_Email object
		    global $email; 

		    // Get an instance of the WC_Order object
		    $order = $email->object;
		    $email_customer_user_id = get_post_meta( $email->object->ID, '_customer_user', true );
		    $customer_brand = get_user_meta($email_customer_user_id, 'brand', true);
		    
		    if ($email_customer_user_id) {
		    	// echo 'user_id= ' . $email_customer_user_id . '<br>';
		    	// echo 'brand= ' . $customer_brand;

		    } else {
		    	// echo "error: cannot get customer_user_id in email header";
		    }
		?>

		<div id="wrapper" dir="<?php echo is_rtl() ? 'rtl' : 'ltr'; ?>">
			
			<table id="top-gradient-bar" border="0" cellpadding="0" cellspacing="0" width="100%" style="">
				<tr>
					<td align="center" valign="top" colspan="2">
						<div id="top-spacer-wrapper" style="">
							<div id="top-spacer" style="">&nbsp;</div>
						</div>
					</td>
				</tr>
			</table>

			<?php
			  // Call the global WC_Email object
			  global $email; 

			  // Get an instance of the WC_Order object
			  $order = $email->object;
			  $email_customer_user_id = get_post_meta( $email->object->ID, '_customer_user', true );
			  $customer_brand = get_user_meta($email_customer_user_id, 'brand', true);

			  if ($email_customer_user_id && $customer_brand == "ridgecapmarketing") {
			?>
				<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#efeee6" id="wrapper-inner">
			<?php } else { ?>
				<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#f8f8f5" id="wrapper-inner">
			<?php } ?>

			
			
			<!-- flyrise-header -->
			<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F84470" id="flyrise-header" class="foobar">
				<tr>
					<td align="center" valign="top">

						<!-- flyrise-header-content-wrap -->
						<div id="flyrise-header-content-wrap">
							<table border="0" cellpadding="0" cellspacing="0" width="600" id="flyrise-header-content">
								<tr>
									<td align="left" valign="top">

										<!-- flyrise-header-logo -->
										<div id="flyrise-header-logo">
											<?php
											  // Call the global WC_Email object
											  global $email; 

											  // Get an instance of the WC_Order object
											  $order = $email->object;
											  $email_customer_user_id = get_post_meta( $email->object->ID, '_customer_user', true );
											  $customer_brand = get_user_meta($email_customer_user_id, 'brand', true);

											  if ($email_customer_user_id && $customer_brand == "ridgecapmarketing") {
											?>
												<p style="margin-top:0;margin-bottom:0;"><img src="https://my.ridgecapmarketing.com/ridgecap-icon-40.png"/></p>
											<?php } else { ?>
												<p style="margin-top:0;margin-bottom:0;"><img src="https://launch.flyrise.io/wp-content/uploads/flyrise-icon-pink.png"/></p>
											<?php } ?>
										</div>
									</td>
									<td align="right" valign="center">
										<div id="flyrise-header-intro">

																








