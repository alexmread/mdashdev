# Midas Dashboard

1. create local server host domain for mdashdev
2. clone this repo
3. cd to wp-content/themes/midas-dashboard/ and clone vue-core repo (is gitignored from parent)
4. wp-cli core (elsewhere most likely)
5. copy missing core wp files into mdashdev folder (are safetly gitignored)
6. export db from dev, import locally, adjust wp-options to http://mdashdev, create user
7. add user to local wp-config
8. activate midas-dashboard theme via http://mdashdev/wp-admin
9. in vue-core folder, 'npm install'
10. 'npm build'
11. visit http://mdashdev

Remember to add, commit, push changed within both mdashdev and/or wp-content/themes/midas-dashboard/vue-core

Deploy to Dev via a push in mdashdev, vue-core will not get promoted