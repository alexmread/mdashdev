<?php
add_action( 'wp_ajax_md_support_save','md_support_save' );
add_action( 'wp_ajax_nopriv_md_support_save','md_support_save' );


function md_support_save(){
    $usingUploader = 2;
        $user = get_current_user_id();
    $fileErrors = array(
      0 => "There is no error, the file uploaded with success",
      1 => "The uploaded file exceeds the upload_max_files in server settings",
      2 => "The uploaded file exceeds the MAX_FILE_SIZE from html form",
      3 => "The uploaded file uploaded only partially",
      4 => "No file was uploaded",
      6 => "Missing a temporary folder",
      7 => "Failed to write file to disk",
      8 => "A PHP extension stoped file to upload" );

    $posted_data =  isset( $_POST ) ? $_POST : array();
    $file_data = isset( $_FILES ) ? $_FILES : array();

    $data = array_merge( $posted_data, $file_data );
    
    
      $response = array();  
      if( $usingUploader == 1 ) {
          $uploaded_file = wp_handle_upload( $data['file'], array( 'test_form' => false ) );
  
          if( $uploaded_file && ! isset( $uploaded_file['error'] ) ) {
              $response['response'] = "SUCCESS";
              $response['filename'] = basename( $uploaded_file['url'] );
              $response['url'] = $uploaded_file['url'];
              $response['type'] = $uploaded_file['type'];
          } else {
              $response['response'] = "ERROR";
              $response['error'] = $uploaded_file['error'];
          }
      } elseif ( $usingUploader == 2) {
          $attachment_id = media_handle_upload( 'file', 0 );
          
          if ( is_wp_error( $attachment_id ) ) { 
              $response['response'] = "ERROR";
              $response['error'] = $fileErrors[ $data['file']['error'] ];
          } else {
              $fullsize_path = get_attached_file( $attachment_id );
              $pathinfo = pathinfo( $fullsize_path );
              $url = wp_get_attachment_url( $attachment_id );
              update_user_meta($user, '_flyrise_user_profile', $url);
              $response['response'] = "SUCCESS";
              $response['filename'] = $pathinfo['filename'];
              $response['url'] = $url;
              $type = $pathinfo['extension'];
              if( $type == "jpeg"
              || $type == "jpg"
              || $type == "png"
              || $type == "gif" ) {
                  $type = "image/" . $type;
              }
              $response['type'] = $type;
          }
      }
  
      echo json_encode( $response );
      die();
}

add_action( 'wp_ajax_company_support_save','company_support_save' );
add_action( 'wp_ajax_nopriv_company_support_save','company_support_save' );


function company_support_save(){
    $usingUploader = 2;
        $user = get_current_user_id();
      $fileErrors = array(
          0 => "There is no error, the file uploaded with success",
          1 => "The uploaded file exceeds the upload_max_files in server settings",
          2 => "The uploaded file exceeds the MAX_FILE_SIZE from html form",
          3 => "The uploaded file uploaded only partially",
          4 => "No file was uploaded",
          6 => "Missing a temporary folder",
          7 => "Failed to write file to disk",
          8 => "A PHP extension stoped file to upload" );
  
      $posted_data =  isset( $_POST ) ? $_POST : array();
      $file_data = isset( $_FILES ) ? $_FILES : array();

      $postId = $_POST['compid'];
  
      $data = array_merge( $posted_data, $file_data );
  
      $response = array();  
      if( $usingUploader == 1 ) {
          $uploaded_file = wp_handle_upload( $data['file'], array( 'test_form' => false ) );
  
          if( $uploaded_file && ! isset( $uploaded_file['error'] ) ) {
              $response['response'] = "SUCCESS";
              $response['filename'] = basename( $uploaded_file['url'] );
              $response['url'] = $uploaded_file['url'];
              $response['type'] = $uploaded_file['type'];
          } else {
              $response['response'] = "ERROR";
              $response['error'] = $uploaded_file['error'];
          }
      } elseif ( $usingUploader == 2) {
          $attachment_id = media_handle_upload( 'file', 0 );
          
          if ( is_wp_error( $attachment_id ) ) { 
              $response['response'] = "ERROR";
              $response['error'] = $fileErrors[ $data['file']['error'] ];
          } else {
              $fullsize_path = get_attached_file( $attachment_id );
              $pathinfo = pathinfo( $fullsize_path );
              $url = wp_get_attachment_url( $attachment_id );
              set_post_thumbnail( $postId, $attachment_id );
              $response['response'] = "SUCCESS";
              $response['filename'] = $pathinfo['filename'];
              $response['url'] = $url;
              $type = $pathinfo['extension'];
              if( $type == "jpeg"
              || $type == "jpg"
              || $type == "png"
              || $type == "gif" ) {
                  $type = "image/" . $type;
              }
              $response['type'] = $type;
          }
      }
  
      echo json_encode( $response );
      die();
}


function objectToArray($object) {
    if(!is_object($object) && !is_array($object))
        return $object;

    return array_map('objectToArray', (array) $object);
}

function get_all_company_users() {
    $user = wp_get_current_user();
    $loggedin = is_user_logged_in();
    $compid = get_user_meta($user->ID, 'assignedcompany', true);
    $compname = get_post_meta($compid, 'companyname', true);
    $compemp = [];
    $users =
        get_users(
         array(
          'meta_key' => 'assignedcompany',
          'meta_value' => $compid,
         )
       );
       $i = 0;
       foreach ($users as $emp) {
          //  $compemp[$emp->ID] = [];
            $compemp[$i]['name'] = $emp->first_name . ' ' . $emp->last_name;
            $compemp[$i]['email'] = $emp->user_email;
            $compemp[$i]['role'] = get_user_meta($emp->ID, 'comprole', true);
        $i++;
        }
       $array = json_decode(json_encode($compemp), true);
      file_put_contents('compemp.txt', print_r($array, true));
     // $array = json_encode($array); */
       return (array) $compemp;
}



add_action('wp_ajax_nopriv_flyrise_update_password', 'flyrise_update_password');
add_action('wp_ajax_flyrise_update_password', 'flyrise_update_password');
function flyrise_update_password() {
    if (is_user_logged_in() === true) {
        $user = wp_get_current_user();
        $currentpass = $_POST['curpass'];
        $newpass = $_POST['newpass'];
        if ($user && wp_check_password( $currentpass, $user->data->user_pass, $user->ID ) ) {
            wp_set_password( $newpass, $user->ID);
            echo 'Password Updated Successfully';
        }
        else {
            echo 'Current Password Incorrect';
        }
    }
    else {
        echo 'What do you think you are doing poking your nose around here!';
    }
    die();
}

add_action('wp_ajax_nopriv_flyrise_update_account', 'flyrise_update_account');
add_action('wp_ajax_flyrise_update_account', 'flyrise_update_account');
function flyrise_update_account() {
    $uid = get_current_user_id();
    update_user_meta( $uid, 'first_name', $_POST['first_name'] );
    update_user_meta( $uid, 'last_name', $_POST['last_name'] );
    update_user_meta( $uid, 'user_email', $_POST['email'] );
    update_user_meta( $uid, 'account_company', $_POST['company'] );

    //require('../../../../../wp-load.php');
    $to = 'alexf@flyrise.io';
    $headers = '';
    $subject = 'User ID: ' . $uid . ' has changed their meta user_email';
    $message = 'User ID: ' . $uid . ' has changed their meta user_email via their Dashboard Account Settings! Manually change it for their Client in ContentSnare! Consider also changing their other Profile Email fields: Email and Meta Billing Email!';
    wp_mail( $to, $subject, $message, $headers );
}

 add_action('wp_ajax_nopriv_flyrise_update_company', 'flyrise_update_company');
 add_action('wp_ajax_flyrise_update_company', 'flyrise_update_company');
 function flyrise_update_company() {
     $uid = get_current_user_id();
     $cid = get_user_meta($uid, 'assignedcompany', true);
    update_post_meta( $cid, 'compaddress1', $_POST['address1'] );
    update_post_meta( $cid, 'compaddress2', $_POST['address2'] );
    update_post_meta( $cid, 'ccity', $_POST['city'] );
    update_post_meta( $cid, 'companyname', $_POST['name'] );
    update_post_meta( $cid, 'compcity', $_POST['city'] );
    update_post_meta( $cid, 'comppostalcode', $_POST['zip'] );
    update_post_meta( $cid, 'compstate', $_POST['state'] );
    update_post_meta( $cid, 'cstate', $_POST['state'] );
    update_post_meta( $cid, 'czip', $_POST['zip'] );
    die();
 }

add_action('wp_ajax_nopriv_flyrise_update_address_for_orders', 'flyrise_update_address_for_orders');
add_action('wp_ajax_flyrise_update_address_for_orders', 'flyrise_update_address_for_orders');
function flyrise_update_address_for_orders() {
    $uid = get_current_user_id();
    update_user_meta( $uid, 'billing_first_name', $_POST['billing_first_name'] );
    update_user_meta( $uid, 'billing_last_name', $_POST['billing_last_name'] );
    update_user_meta( $uid, 'billing_company', $_POST['billing_company'] );
    update_user_meta( $uid, 'billing_address_1', $_POST['billing_address_1'] );
    update_user_meta( $uid, 'billing_address_2', $_POST['billing_address_2'] );
    update_user_meta( $uid, 'billing_city', $_POST['billing_city'] );
    update_user_meta( $uid, 'billing_state', $_POST['billing_state'] );
    update_user_meta( $uid, 'billing_postcode', $_POST['billing_postcode'] );
    update_user_meta( $uid, 'billing_country', $_POST['billing_country'] );
    update_user_meta( $uid, 'billing_email', $_POST['billing_email'] );
    update_user_meta( $uid, 'billing_phone', $_POST['billing_phone'] );

    //print_r(wcs_get_users_subscriptions($uid));
    $users_subscriptions = wcs_get_users_subscriptions($uid);
    $address_type = 'billing';
    $address = array(
		'first_name' => $_POST['billing_first_name'],
		'last_name'  => $_POST['billing_last_name'],
		'email'      => $_POST['billing_email'],
		'address_1'  => $_POST['billing_address_1'],
		'address_2'  => $_POST['billing_address_2'],
		'city'       => $_POST['billing_city'],
		'state'      => $_POST['billing_state'],
		'postcode'   => $_POST['billing_postcode'],
		'country'    => $_POST['billing_country'],
	);
    foreach ($users_subscriptions as $subscription) {
        //if ($subscription->has_status(array('active', 'on-hold', 'pending', 'pending payment'))) {
            $subscription->set_address($address, $address_type);

            //echo "Parent: " . $subscription->parent_id;// => 19389
            $parentID = $subscription->parent_id;
            $order = wc_get_order( $parentID );
            $order->set_address($address, $address_type);
        //}
    }
    
}

add_action('wp_ajax_nopriv_flyrise_update_address_for_shipping', 'flyrise_update_address_for_shipping');
add_action('wp_ajax_flyrise_update_address_for_shipping', 'flyrise_update_address_for_shipping');
function flyrise_update_address_for_shipping() {
    $uid = get_current_user_id();
    
    update_user_meta( $uid, 'shipping_company', $_POST['shipping_company'] );

    update_user_meta( $uid, 'shipping_first_name', $_POST['shipping_first_name'] );
    update_user_meta( $uid, 'shipping_last_name', $_POST['shipping_last_name'] );
      
    update_user_meta( $uid, 'shipping_address_1', $_POST['shipping_address_1'] );
    update_user_meta( $uid, 'shipping_address_2', $_POST['shipping_address_2'] );
    
    update_user_meta( $uid, 'shipping_city', $_POST['shipping_city'] );
    update_user_meta( $uid, 'shipping_state', $_POST['shipping_state'] );
    update_user_meta( $uid, 'shipping_country', $_POST['shipping_country'] );
    update_user_meta( $uid, 'shipping_postcode', $_POST['shipping_postcode'] );
      
    update_user_meta( $uid, 'shipping_email', $_POST['shipping_email'] );
    update_user_meta( $uid, 'shipping_phone', $_POST['shipping_phone'] );
}


add_action('wp_ajax_nopriv_flyrise_update_journey', 'flyrise_update_journey');
add_action('wp_ajax_flyrise_update_journey', 'flyrise_update_journey');
function flyrise_update_journey(){
    $current = wp_get_current_user();
    $uid = get_current_user_id();

    $journey = json_decode( get_user_meta($uid, 'journey', true) );
    $journey->step_3 = true;
    update_user_meta( $uid, 'journey', json_encode($journey) );
    $message['journey'] = json_encode($journey);

    echo json_encode($message);
}

add_action('wp_ajax_nopriv_flyrise_update_addresses', 'flyrise_update_addresses');
add_action('wp_ajax_flyrise_update_addresses', 'flyrise_update_addresses');
function flyrise_update_addresses() {

    if(is_user_logged_in()){
        $message['loggedin'] = "User logged in";
    }else{
        $message['loggedin'] = "User Not Logged In";
    }

    $current = wp_get_current_user();
    $uid = get_current_user_id();
    $message['id'] = $uid;

    wp_update_user([
        'ID' => $uid,
        'first_name' => $_POST['billing_first_name'],
        'last_name' => $_POST['billing_last_name'],
    ]);

    if( isset( $_POST['billing_company'] ) ){
        update_user_meta( $uid, 'billing_company', $_POST['billing_company'] );
    }

    update_user_meta( $uid, 'billing_first_name', $_POST['billing_first_name'] );
    update_user_meta( $uid, 'billing_last_name', $_POST['billing_last_name'] );
    
    update_user_meta( $uid, 'billing_address_1', $_POST['billing_address_1'] );
    update_user_meta( $uid, 'billing_address_2', $_POST['billing_address_2'] );

    update_user_meta( $uid, 'billing_city', $_POST['billing_city'] );
    update_user_meta( $uid, 'billing_state', $_POST['billing_state'] );
    update_user_meta( $uid, 'billing_postcode', $_POST['billing_postcode'] );
    update_user_meta( $uid, 'billing_country', $_POST['billing_country'] );

    update_user_meta( $uid, 'billing_email', $_POST['billing_email'] );
    update_user_meta( $uid, 'billing_phone', $_POST['billing_phone'] );

    if( isset( $_POST['shipping_company'] ) ){
        update_user_meta( $uid, 'shipping_company', $_POST['shipping_company'] );
    }else{
        if( isset( $_POST['billing_company'] ) ){
            update_user_meta( $uid, 'shipping_company', $_POST['billing_company'] );
        }
    }

    if( isset( $_POST['shipping_first_name'] ) ){
        update_user_meta( $uid, 'shipping_first_name', $_POST['shipping_first_name'] );
        update_user_meta( $uid, 'shipping_last_name', $_POST['shipping_first_name'] );
    }else{
        update_user_meta( $uid, 'shipping_first_name', $_POST['billing_first_name'] );
        update_user_meta( $uid, 'shipping_last_name', $_POST['billing_first_name'] );
    }
    update_user_meta( $uid, 'shipping_address_1', $_POST['shipping_address_1'] );
    update_user_meta( $uid, 'shipping_address_2', $_POST['shipping_address_2'] );

    update_user_meta( $uid, 'shipping_city', $_POST['shipping_city'] );
    update_user_meta( $uid, 'shipping_state', $_POST['shipping_state'] );
    update_user_meta( $uid, 'shipping_country', $_POST['shipping_country'] );
    update_user_meta( $uid, 'shipping_postcode', $_POST['shipping_postcode'] );
    
    if( isset( $_POST['shipping_email'] ) ){
        update_user_meta( $uid, 'shipping_email', $_POST['shipping_email'] );
    }else{
        if( isset( $_POST['billing_email'] ) ){
            update_user_meta( $uid, 'shipping_email', $_POST['billing_email'] );
        }else{
            update_user_meta( $uid, 'shipping_email', '' );
        }
    }

    if( isset( $_POST['shipping_phone'] ) ){
        update_user_meta( $uid, 'shipping_phone', $_POST['shipping_phone'] );
    }else{
        if( isset( $_POST['billing_phone'] ) ){
            update_user_meta( $uid, 'shipping_phone', $_POST['billing_phone'] );
        }else{
            update_user_meta( $uid, 'shipping_phone', '' );
        }
    }
    //update_user_meta( $uid, 'qwilr_proposal_link', $_POST['qwilr'] );

    //print_r(wcs_get_users_subscriptions($uid));
    $users_subscriptions = wcs_get_users_subscriptions($uid);
    
    $address_billing = array(
		'first_name' => $_POST['billing_first_name'],
		'last_name'  => $_POST['billing_last_name'],
		'email'      => $_POST['billing_email'],
		'address_1'  => $_POST['billing_address_1'],
		'address_2'  => $_POST['billing_address_2'],
		'city'       => $_POST['billing_city'],
		'state'      => $_POST['billing_state'],
		'postcode'   => $_POST['billing_postcode'],
		'country'    => $_POST['billing_country'],
        'billing_company' => $POST['billing_company']
	);
    foreach ($users_subscriptions as $subscription) {
        //if ($subscription->has_status(array('active', 'on-hold', 'pending', 'pending payment'))) {
            $subscription->set_address($address_billing, 'billing');

            //echo "Parent: " . $subscription->parent_id;// => 19389
            $parentID = $subscription->parent_id;
            $order = wc_get_order( $parentID );
            $order->set_address($address_billing, 'billing');
        //}
    }

    $address_shipping = array(
		'first_name' => $_POST['shipping_first_name'],
		'last_name'  => $_POST['shipping_last_name'],
		'email'      => $_POST['shipping_email'],
		'address_1'  => $_POST['shipping_address_1'],
		'address_2'  => $_POST['shipping_address_2'],
		'city'       => $_POST['shipping_city'],
		'state'      => $_POST['shipping_state'],
		'postcode'   => $_POST['shipping_postcode'],
		'country'    => $_POST['shipping_country'],
		'shipping_company' => $_POST['shipping_company'],
	);
    foreach ($users_subscriptions as $subscription) {
        //if ($subscription->has_status(array('active', 'on-hold', 'pending', 'pending payment'))) {
            $subscription->set_address($address_shipping, 'shipping');

            //echo "Parent: " . $subscription->parent_id;// => 19389
            $parentID = $subscription->parent_id;
            $order = wc_get_order( $parentID );
            $order->set_address($address_shipping, 'shipping');
        //}
    }

    $journey = json_decode( get_user_meta($uid, 'journey', true) );
    $journey->step_2 = true;
    update_user_meta( $uid, 'journey', json_encode($journey) );
    $message['journey'] = json_encode($journey);

    $message['result'] = 'saved';
    //$message['qwilr'] = get_post_meta($uid, 'qwilr_proposal_link', true );
    echo json_encode($message);
}