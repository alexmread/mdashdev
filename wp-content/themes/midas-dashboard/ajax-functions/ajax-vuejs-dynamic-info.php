<?php 

function html5blank_header_scripts() {
  
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
        
        wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
        wp_enqueue_script('conditionizr'); // Enqueue it!

        wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
        wp_enqueue_script('modernizr'); // Enqueue it!

        wp_register_script('html5blankscripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0'); // Custom scripts
        wp_enqueue_script('html5blankscripts'); // Enqueue it!  
    }
  
    add_action( 'woocommerce_thankyou', 'bbloomer_redirectcustom');
      
    function bbloomer_redirectcustom( $order_id ){
        $order = wc_get_order( $order_id );
        $url = '/pages/settings/orders';
        //echo "***************************************bbloomer_redirectcustom FIRING!";
        if ( ! $order->has_status( 'failed' ) ) {
            wp_safe_redirect( $url );
            exit;
        }
    }
    
    /*
    *   Dev returns: 50.38.50.50
    */
    $whitelist = array(
        '127.0.0.1',
        '::1'
    );
    
    if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
        // not valid
    }else{
        /*echo "<!-- " . $_SERVER['REMOTE_ADDR'] . "-->";*/
        // wp_register_script('vueappdev', 'http://localhost:3000/.nuxt/app.js', '', '', true); // Custom scripts
        // wp_enqueue_script('vueappdev'); // Enqueue it!
        // wp_register_script('vueappdev2', 'http://localhost:3000/_nuxt/runtime.js', '', '', true); // Custom scripts
        // wp_enqueue_script('vueappdev2'); // Enqueue it!
        // wp_register_script('vueappdev3', 'http://localhost:3000/_nuxt/commons.app.js', '', '', true); // Custom scripts
        // wp_enqueue_script('vueappdev3'); // Enqueue it!
        // wp_register_script('vueappdev4', 'http://localhost:3000/_nuxt/vendors.app.js', '', '', true); // Custom scripts
        // wp_enqueue_script('vueappdev4'); // Enqueue it!
        // wp_register_script('vueappdev5', 'http://localhost:3000/_nuxt/pages_dashboard.js', '', '', true); // Custom scripts
        // // wp_enqueue_script('vueappdev5'); // Enqueue it
        // wp_register_script('vueappdev6', 'http://localhost:3000/_nuxt/6.js', '', '', true); // Custom scripts
        // wp_enqueue_script('vueappdev6'); // Enqueue it
        // wp_register_script('vueappdev7', 'http://localhost:3000/_nuxt/pages_index.js', '', '', true); // Custom scripts
        // // wp_enqueue_script('vueappdev7'); // Enqueue it
    
        // wp_register_style('vueappdev', 'http://localhost:3000/_nuxt/app.css'); // Custom scripts
        // wp_enqueue_style('vueappdev'); // Enqueue it!
        // wp_register_style('vueappdev2', 'http://localhost:3000/_nuxt/vendors.app.css'); // Custom scripts
        // wp_enqueue_style('vueappdev2'); // Enqueue it!
    }
  

  global $post;
  global $woocommerce;
  $user = wp_get_current_user();
  //print_r($user);

  $loggedin = is_user_logged_in();

  $compid = get_user_meta($user->ID, 'assignedcompany', true);
  $args = array(
      'customer_id' => $user->ID
  );

  $saved_methods = wc_get_customer_saved_methods_list( get_current_user_id() );
  $has_methods   = (bool) $saved_methods;
  $types         = wc_get_account_payment_methods_types();
  $orders = wc_get_orders($args);
  
  wp_enqueue_script('vue_search_app', get_template_directory_uri() . '/test.js');
  //echo "<p>Shipping Email: " . get_user_meta($user->ID, 'shipping_email', true) . "</p>"; exit;
  wp_localize_script(
    'vue_search_app', // vue script handle defined in wp_register_script.
    'wpData', // javascript object that will made availabe to Vue.
    array( // wordpress data to be made available to the Vue app in 'wpData'
            'userinfo' =>  array(
                'email' => $user->user_email,
                'display_name' => $user->display_name,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                //'company' => get_user_meta($user->ID, 'account_company', true),
                //'company' => get_the_title($compid),
                'company' => get_user_meta ( $user->ID, 'billing_company', true),
                'billing_company' => get_user_meta($user->ID, 'billing_company', true),
                'billingFirstName' => get_user_meta($user->ID, 'billing_first_name', true),
                'billingLastName' => get_user_meta($user->ID, 'billing_last_name', true),
                'billingAddress1' => get_user_meta($user->ID, 'billing_address_1', true),
                'billingAddress2' => get_user_meta($user->ID, 'billing_address_2', true),
                'billingCountry' => get_user_meta($user->ID, 'billing_country', true),
                'billingCity' => get_user_meta($user->ID, 'billing_city', true),
                'billingState' => get_user_meta($user->ID, 'billing_state', true),
                'billingZip' => get_user_meta($user->ID, 'billing_postcode', true),
                'billingEmail' => get_user_meta($user->ID, 'billing_email', true),
                'billingPhone' => get_user_meta($user->ID, 'billing_phone', true),
                'billingDefaultAddress' => true,
                'shipping_company' => get_user_meta($user->ID, 'shipping_company', true),
                'shippingFirstName' => get_user_meta($user->ID, 'shipping_first_name', true),
                'shippingLastName' => get_user_meta($user->ID, 'shipping_last_name', true),
                'shippingAddress1' => get_user_meta($user->ID, 'shipping_address_1', true),
                'shippingAddress2' => get_user_meta($user->ID, 'shipping_address_2', true),
                'shippingCountry' => get_user_meta($user->ID, 'shipping_country', true),
                'shippingCity' => get_user_meta($user->ID, 'shipping_city', true),
                'shippingState' => get_user_meta($user->ID, 'shipping_state', true),
                'shippingZip' => get_user_meta($user->ID, 'shipping_postcode', true),
                'shippingEmail' => get_user_meta($user->ID, 'shipping_email', true),
                'shippingPhone' => get_user_meta($user->ID, 'shipping_phone', true),
                'shippingDefaultAddress' => true,
                'profileimg' => get_user_meta($user->ID, '_flyrise_user_profile', true),
                'qwilr' =>  get_user_meta($user->ID, 'qwilr_proposal_link', true),
            ),
            'loggedin' => $loggedin,
            'orders' => wc_get_customer_orders($user->ID),
            'subscriptions' => bbloomer_products_bought_by_curr_user(),
            'payment_methods' => $saved_methods,
            'payment_form' => get_form_payment_method(),
            'company_info' => array(
                'image' => get_the_post_thumbnail_url($compid, 'full'),
                'name' => get_post_meta($compid, 'companyname', true),
                'address1' => get_post_meta($compid, 'compaddress1', true),
                'address2' => get_post_meta($compid, 'compaddress2', true),
                'city' => get_post_meta($compid, 'compcity', true),
                'state' => get_post_meta($compid, 'compstate', true),
                'zip' => get_post_meta($compid, 'comppostalcode', true),
                'id' => $compid
            ),
            'renewals' =>  get_last_customer_renewal_order(),
            //'compusers' => get_all_company_users(),
            'resetpass' => do_shortcode('[reset_password]'),
            'order_invoices' => wc_get_customer_orders_invoices($user->ID),
            'print_woo_notices' => wc_print_notices(true),
            'qwilr' =>  get_user_meta($user->ID, 'qwilr_proposal_link', true),
            'journey' => get_user_meta($user->ID, 'journey', true)
        )
    );
  // enqueue the Vue app script with localized data.
  wp_enqueue_script( 'vue_search_app' );
      
}