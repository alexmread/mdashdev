<?php 
add_action('wp_ajax_nopriv_fr_get_order_details', 'fr_get_order_details');
add_action('wp_ajax_fr_get_order_details', 'fr_get_order_details');
function fr_get_order_details() {
    if ($_POST['order_id']) {
        $order = wc_get_order( $_POST['order_id'] );
        $wuser = $order->get_user();
        $vuser = get_current_user_id();
        if ($wuser->id == $vuser) {
            $url = wp_nonce_url( admin_url( 'admin-ajax.php?action=generate_wpo_wcpdf&template_type=invoice&order_ids=' . $_POST['order_id'] . '&my-account'), 'generate_wpo_wcpdf' );
            $order = $order->get_data();
            $order = (array) $order;
            $order['view'] = html_entity_decode($url);
            $order = json_encode($order);
            echo $order;
        }
    }
    exit();
}

add_action('wp_ajax_nopriv_fr_get_order_items', 'fr_get_order_items');
add_action('wp_ajax_fr_get_order_items', 'fr_get_order_items');
function fr_get_order_items() {
    global $woocomerce;
    if ($_POST['order_id']) {
        $order = wc_get_order( $_POST['order_id'] );
        $wuser = $order->get_user();
        $vuser = get_current_user_id();
        $prod_data = [];
        if ($wuser->id == $vuser) {
            $items = $order->get_items();
            $i= 0;
            foreach ($items as $item) {
                $product_name = $item->get_name();
                $product_id = $item->get_product_id();
                $product = wc_get_product( $product_id );
                $product_price = $order->get_line_total( $item, $inc_tax, $round );
                $prod_data[$i]['name'] = $product_name;
                $prod_data[$i]['price'] = $product_price;
                $i++; 
            }
             $order = json_encode($prod_data);
            echo $order;
        }
    }
    exit();
}



function get_form_payment_method() {
    ob_start();
    global $woocommerce;
    $available_gateways = WC()->payment_gateways->get_available_payment_gateways();

if ( $available_gateways ) : ?>
	<form id="add_payment_method" method="post">
		<div id="payment" class="woocommerce-Payment">
			<ul class="woocommerce-PaymentMethods payment_methods methods">
				<?php
				// Chosen Method.
				if ( count( $available_gateways ) ) {
					current( $available_gateways )->set_current();
				}

				foreach ( $available_gateways as $gateway ) {
					?>
					<li class="woocommerce-PaymentMethod woocommerce-PaymentMethod--<?php echo esc_attr( $gateway->id ); ?> payment_method_<?php echo esc_attr( $gateway->id ); ?>">
						<input id="payment_method_<?php echo esc_attr( $gateway->id ); ?>" type="radio" class="input-radio" name="payment_method" value="<?php echo esc_attr( $gateway->id ); ?>" <?php checked( $gateway->chosen, true ); ?> />
						<label for="payment_method_<?php echo esc_attr( $gateway->id ); ?>"><?php echo wp_kses_post( $gateway->get_title() ); ?> <?php echo wp_kses_post( $gateway->get_icon() ); ?></label>
						<?php
						if ( $gateway->has_fields() || $gateway->get_description() ) {
							echo '<div class="woocommerce-PaymentBox woocommerce-PaymentBox--' . esc_attr( $gateway->id ) . ' payment_box payment_method_' . esc_attr( $gateway->id ) . '" style="display: none;">';
							$gateway->payment_fields();
							echo '</div>';
						}
						?>
					</li>
					<?php
				}
				?>
			</ul>

			<div class="form-row">
				<?php wp_nonce_field( 'woocommerce-add-payment-method', 'woocommerce-add-payment-method-nonce' ); ?>
				<button type="submit" class="woocommerce-Button woocommerce-Button--alt button alt" id="place_order" value="<?php esc_attr_e( 'Add payment method', 'woocommerce' ); ?>"><?php esc_html_e( 'Add payment method', 'woocommerce' ); ?></button>
				<input type="hidden" name="woocommerce_add_payment_method" id="woocommerce_add_payment_method" value="1" />
			</div>
		</div>
	</form>
<?php else : ?>
	<p class="woocommerce-notice woocommerce-notice--info woocommerce-info"><?php esc_html_e( 'New payment methods can only be added during checkout. Please contact us if you require assistance.', 'woocommerce' ); ?></p>
<?php endif; 

$output = ob_get_clean();
return $output;
}

function get_last_customer_renewal_order() {
    $current_user_id = get_current_user_id();

        $subscriptions = wcs_get_users_subscriptions($current_user_id);
        $related_order_ID = array();
        $related_orders = array();

        $renewals = array();

        foreach($subscriptions as $subscription) {

         // The subscription ID
         $related_order_ID[] = $subscription->get_related_orders( 'ids', 'renewal' );

            foreach($related_order_ID as $related_orders) {

                foreach($related_orders as $related_order) {
                    $order = wc_get_order(  $related_order );
                    $order_data = $order->get_data();
                    
                    $order_parent_id = $order_data['parent_id'];
                    $order_status = $order_data['status'];

                    // Using a formated date ( with php date() function as method)
                    $order_date_created = $order_data['date_created']->date('Y-m-d');
                    $order_date_modified = $order_data['date_modified']->date('Y-m-d');


                    $order_discount_total = $order_data['discount_total'];
                    $order_discount_tax = $order_data['discount_tax'];
                
                    $order_total = $order_data['cart_tax'];
                    $order_total_tax = $order_data['total_tax'];

                    $total = $order->get_total();

                    $renewals[$related_order]['id'] = $related_order;
                    $renewals[$related_order]['date'] = $order_date_created;
                    $renewals[$related_order]['status'] = $order_status;
                    $renewals[$related_order]['total'] = $total;



                }
            }
        }
       
        return $renewals;
  
}


function subscriber_start_date($sid, $uid) {
    // NOTE: You don't have the $user_id - are you setting it?

    // Somehow you need to identify the subscription you want.
    $subscription_id = 'MY_SUBSCRIPTION_ID';
    $subscription = WC_Subscriptions_Manager::get_users_subscription( $user_id, $subscription_id );

    $start_date = (isset($subscription['start_date'])) ? $subscription['start_date'] : FALSE;

    var_dump($start_date);
}

add_shortcode("subscriber-start-date","subscriber_start_date");

function bbloomer_products_bought_by_curr_user() {
   
    // GET CURR USER
    $current_user = wp_get_current_user();
    if ( 0 == $current_user->ID ) return;
   
    // GET USER ORDERS (COMPLETED + PROCESSING)
    $customer_orders = get_posts( array(
        'numberposts' => -1,
        'meta_key'    => '_customer_user',
        'meta_value'  => $current_user->ID,
        'post_type'   => wc_get_order_types(),
        'post_status' => array_keys( wc_get_is_paid_statuses() ),
    ) );

    $subscriptions = wcs_get_subscriptions(
        array(
            'customer_id'            => get_current_user_id(),
            'subscription_status'    => 'wc-active',
            'subscriptions_per_page' => - 1
        )
);
    $activeids = [];
    foreach ($subscriptions as $key => $value) {
        $activeids[] = $key;
    }
    // LOOP THROUGH ORDERS AND GET PRODUCT IDS
    if ( ! $customer_orders ) return;
    $product_ids = array();
    foreach ( $customer_orders as $customer_order ) {
        $order = wc_get_order( $customer_order->ID );
        $items = $order->get_items();
        $i = 0;
        foreach ( $items as $item_id => $item_data) {
            $item = $item_data->get_product();
    
            //print_r($key);
            if (in_array($item_id, $activeids)) {
                $status = 'Active';
            }
            else {
                $status = 'Inactive';
            }
 
            if ( !is_bool($item) && $item->is_type( 'subscription' ) && $status == 'Active') {

                $id = $item->get_id();
                $subscription = new WC_Subscription( $customer_order->ID );
                $oid = $customer_order->ID;
                $name = $item->get_name();
                $recurring = get_post_meta($item->get_id(), '_subscription_period', true);
                $total = get_latest_sub_price($id, $current_user->ID);
                $start = $subscription->get_date( 'start' );
                $start = date("m-d-Y", strtotime($start));
                $payment_method = wc_get_payment_gateway_by_order($subscription);
                $npaydate = $subscription->get_date( 'next_payment' );
                $view = wp_nonce_url( admin_url( 'admin-ajax.php?action=generate_wpo_wcpdf&template_type=invoice&order_ids=' . $oid . '&my-account'), 'generate_wpo_wcpdf' );

                if ($payment_method) {
                    $method_title = $payment_method->title;
                }

                else {
                    $method_title = 'N/A';
                }

                $product_ids[$id] = [];
                $product_ids[$id]['id'] = $oid;
                $product_ids[$id]['name'] = $name;
                $product_ids[$id]['recurring'] = $recurring;
                $product_ids[$id]['status'] = $status;
                $product_ids[$id]['total'] = $total;  
                $product_ids[$id]['start'] = $start;
                $product_ids[$id]['payment_method'] = $method_title;
                $product_ids[$id]['npaydate'] = $npaydate;
                $product_ids[$id]['view'] = html_entity_decode($view);
                $product_ids[$id]['description'] = $item->get_short_description();
                $product_ids[$id]['img_url'] = get_the_post_thumbnail_url($id);
            }
            $i++;
        }
       
    }
    return (array) $product_ids;
    
   
}

/**
 * Get All orders IDs for a given product ID.
 *
 * @param  integer  $product_id (required)
 * @param  array    $order_status (optional) Default is 'wc-completed'
 *
 * @return array
 */
function get_orders_ids_by_product_id($product_id, $uid){
    global $wpdb;

    $results = $wpdb->get_col("
        SELECT order_items.order_id
        FROM {$wpdb->prefix}woocommerce_order_items as order_items
        LEFT JOIN {$wpdb->prefix}woocommerce_order_itemmeta as order_item_meta ON order_items.order_item_id = order_item_meta.order_item_id
        LEFT JOIN {$wpdb->posts} AS posts ON order_items.order_id = posts.ID
        LEFT JOIN {$wpdb->prefix}postmeta AS postmeta ON order_items.order_id = posts.ID
        WHERE posts.post_type = 'shop_order'
        AND order_items.order_item_type = 'line_item'
        AND order_item_meta.meta_key = '_product_id'
        AND order_item_meta.meta_value = '$product_id'
        AND postmeta.meta_key = '_customer_user'
        AND postmeta.meta_value = '$uid'
        ORDER BY order_items.order_id DESC
        LIMIT 1
    ");
    //print_r($results);
   if ($results) {
    return $results[0];
   }
   else {
       return false;
   }
    
}

function get_latest_sub_price($pid, $uid) {
    
    $price = 0;
    
    $oid = get_orders_ids_by_product_id($pid, $uid);

    $order = wc_get_order( $oid );
    $order_data = $order->get_data(); // The Order data
    $items = $order->get_items();
    foreach ( $items as $item ) {

        $product_id = $item['product_id'];
        if ($product_id == $pid) {
            ## Option: Including or excluding Taxes
        $inc_tax = true; 
   
        ## Option: Round at item level (or not)
        $round   = false; // Not rounded at item level ("true"  for rounding at item level)
   
        $item_cost_excl_disc = $order->get_item_subtotal( $item, $inc_tax, $round ); // Calculate item cost (not discounted) - useful for gateways.
   
        $item_cost_incl_disc = $order->get_item_total( $item, $inc_tax, $round ); // Calculate item cost (discounted) - useful for gateways.
   
        $item_tax_cost       = $order->get_item_tax( $item, $inc_tax, $round ); // Get item tax cost - useful for gateways.
   
        $item_Line_subtotal = $order->get_line_subtotal( $item, $inc_tax, $round ); // Get line subtotal - not discounted.
   
        $item_Line_total     = $order->get_line_total( $item, $inc_tax, $round ); // Get line total - discounted
   
        $item_Line_tax       = $order->get_line_tax( $item ); // Get line tax
   
        $form_line_subtotal  = $order->get_formatted_line_subtotal( $item, $tax_display = '' ); // Gets line subtotal - formatted for display.
        
        $price = $item_Line_total;
        }
   }


   return $price;

}


function wc_get_subs($id) {
    global $woocommerce;
    $subsd = array();
    $args = array('customer_id' => $id);
    $subs = wcs_get_subscriptions( $args );
    foreach ($subs as $subid) {
        $sub = wc_get_order( $subid );
        $sub_data = $sub->get_data();
        $subsd[] = (array) $sub_data;
     }
     return $subsd;
}

function wc_get_customer_orders($id) {
    global $woocommerce;
    // Get all customer orders
    $ordersd = array();
    $query = new WC_Order_Query( array(
        'limit' => 10,
        'orderby' => 'date',
        'order' => 'DESC',
        'return' => 'ids',
        'customer_id' => $id
    ) );
    $orders = $query->get_orders();
     foreach ($orders as $orderid) {
        $order = wc_get_order( $orderid );
        $order_data = $order->get_data();
        $ordersd[] = (array) $order_data;

     }
    return $ordersd;
}

function wc_get_customer_orders_invoices($id) {
    global $woocommerce;
    // Get all customer orders
    $ordersd = array();
    $query = new WC_Order_Query( array(
        'limit' => 10,
        'orderby' => 'date',
        'order' => 'DESC',
        'return' => 'ids',
        'customer_id' => $id
    ) );
    $orders = $query->get_orders();
     foreach ($orders as $orderid) {
        $order = wc_get_order( $orderid );
        $order_data = $order->get_data();
        $ordersd[$orderid] = do_shortcode('[wcpdf_download_invoice link_text="view invoice" order_id="'. $orderid .'"]');

        //$ordersd[$orderid] = admin_url( 'admin-ajax.php?action=generate_wpo_wcpdf&template_type=invoice&order_ids=' . $order->get_id() . '&order_key=' . $order->get_order_key() );

     }
    return $ordersd;
}

add_action('wp_ajax_get_sub_order_history', 'get_sub_order_history');
add_action('wp_ajax_nopriv_get_sub_order_history', 'get_sub_order_history');
function get_sub_order_history() {
    if ($_POST['sid']) {
        $rtarr = array();
        $subscription = new WC_Subscription( $_POST['sid'] );
        $related_orders_ids_array = $subscription->get_related_orders();
        foreach ( $related_orders_ids_array as $order_id ) {
            $order = new WC_Order( $order_id );
            $items = $order->get_items();
            foreach ( $items as $product ) {
                $rtarr[] = $product;
            }

        }
        return json_encode($related_orders_ids_array);
    }
    die();
}

add_action('wp_ajax_get_sub_order_items', 'get_sub_order_items');
add_action('wp_ajax_nopriv_get_sub_order_items', 'get_sub_order_items');
function get_sub_order_items() {
    if ($_POST['sid']) {
        $rtarr = array();
        $subscription = new WC_Subscription( $the_id );
        $related_orders_ids_array = $subscription->get_related_orders();
        foreach ( $related_orders_ids_array as $order_id ) {
            $order = new WC_Order( $order_id );
            $items = $order->get_items();
            foreach ( $items as $product ) {
                echo $product;
            }

        }
    }
    die();
}


add_action('wp_ajax_flyrise_get_customer_orders', 'flyrise_get_customer_orders');
function flyrise_get_customer_orders() {
    
    $user_id = get_current_user_id();
    
    $args = array(
        'customer_id' => $user_id
    );
    $orders = wc_get_orders($args);
    return $orders;
}
