<?php
/*
*   New HEADLESS Methods Duplicated from ajax-core-functions.php
*   but now REST API rather than wp-admin/adamin-ajax.php
*/

add_action( 'rest_api_init', 'register_api_hooks' );
function register_api_hooks() {
    register_rest_route(
        'standalone', '/login',
        array(
            'methods'  => 'POST',
            'callback' => 'login',
            'permission_callback' => '__return_true'
        )
    );
    register_rest_route(
        'standalone', '/set_password',
        array(
            'methods'   => 'POST',
            'callback'  => 'set_password',
            'permission_callback'   => '__return_true'
        )
    );
    register_rest_route(
        'standalone', '/update_password',
        array(
            'methods'   => 'POST',
            'callback'  => 'update_password',
            'permission_callback'   => '__return_true'
        )
    );
    register_rest_route(
        'standalone', '/states',
        array(
            'methods'   => 'POST',
            'callback'  =>  'states',
            'permission_callback'   =>  '__return_true'
        )
    );
    register_rest_route(
        'standalone', '/logout',
        array(
            'methods'   => 'POST',
            'callback'  =>  'logout',
            'permission_callback'   =>  '__return_true'
        )
    );
    register_rest_route(
        'standalone', '/profile_image',
        array(
            'methods'   =>  'POST',
            'callback'  =>  'profile_image',
            'permission_callback'   =>  '__return_true'
        )
    );
    register_rest_route(
        'standalone', '/update_account',
        array(
            'methods'   =>  'POST',
            'callback'  =>  'update_account',
            //'permission_callback'   =>  '__return_false'
            'permission_callback'   =>  '__return_true'
            //'permission_callback'   =>  'plugin_name_route_api'
        )
    );
    register_rest_route(
        'standalone', '/update_addresses',
        array(
            'methods'   =>  'POST',
            'callback'  =>  'update_addresses',
            'permission_callback'   =>  '__return_true'
        )
    );

    register_rest_route(
        'standalone', '/get_order_details',
        array(
            'methods'   =>  'POST',
            'callback'  =>  'get_order_details',
            'permission_callback'   =>  '__return_true'
        )
    );
    register_rest_route(
        'standalone', '/get_order_items',
        array(
            'methods'   =>  'POST',
            'callback'  =>  'get_order_items',
            'permission_callback'   =>  '__return_true'
        )
    );

    register_rest_route(
        'standalone', '/update_journey',
        array(
            'methods'   =>  'POST',
            'callback'  =>  'update_journey',
            'permission_callback'    => '__return_true'
        )
    );
}



/*
*   Method: login
*/
function login($request){
    global $woocomerce;

    $message['action'] = 'logging in';

    // $creds = array();
    // $creds['user_login'] = $request["username"];
    // $creds['user_password'] =  $request["password"];
    // $creds['remember'] = true;
    // $user = wp_signon( $creds, false );
    //$errors = array();

    $user = get_user_by('id', get_current_user_id());
    //$message['user'] = $user;

    if ( is_wp_error($user) ) {
        $message['errors'][] = $user instanceof WP_Error;

        //$myObj->user_login = "invalid";
        //$myObj->user_password = 'invalid';
        //$user = $myObj;
        //$message['result'] = $user->get_error_message();
        $message['loggedin'] = false;
        return $message;
    }else{
        $message['result'] = 'seems to have worked';
        $message['loggedin'] = true;

        $userID = $user->ID;
        //wp_set_current_user( $userID, $creds['user_login'] );
        wp_set_auth_cookie( $userID );
        //do_action( 'wp_login', $creds['user_login'], $user );

        //$message['nonce'] = wp_create_nonce( 'my-nonce' );

        //$user = wp_get_current_user();
        $loggedin = is_user_logged_in();
        $saved_methods = wc_get_customer_saved_methods_list( get_current_user_id() );

        $orders = wc_get_customer_orders($user->ID);
        $subscriptions = wcs_get_users_subscriptions( get_current_user_id() );


        //$cart_items = $woocommerce->cart->get_cart();
        //$cart_itmes = WC()->cart->get_cart_contents_count(); 
        //$cart_itmes = WC()->cart->get_cart_contents_count()
        $session_handler = new WC_Session_Handler();
        $session = $session_handler->get_session($user->ID);
        //if($session['cart']){
        //     $cart_items = unserialize($session['cart']);
        //     if( (gettype($cart_items) == 'array'||gettype($cart_items) == 'object') && count($cart_items) > 0 ) {
        //         $cart_items = 1;
        //     }else{
        //         $cart_items = 0;
        //     }
        //}else{
        //     $cart_items = 0;
        //}
        //$cart = WC()->cart;
        //if(count($session['cart']){
            $cart_items = Array(unserialize($session['cart']))[0];
            $newCart = array();
            $cart_total = 0;
        foreach($cart_items as $key => $single_item){
            $prodTemp = wc_get_product( $single_item['product_id'] );
            //$newCart[] = $prodTemp->get_title();
//            $newCart[] = get_the_title( $prodTemp->get_title() );
            $newCart[] = $single_item['line_total'];
            //$single_item->product_title = get_the_title( $single_item->product_id );
            $cart_items[$key]['product_title'] = $prodTemp->get_title();
            $cart_total += intval($single_item['line_total']);
        }
        $cart_items['total'] = intval($cart_total);
        //}else{
          //  $cart_items = Array(unserialize($session['cart']));
        //}
        // $cart_items = $session['cart'];
        

        /*
        *   We only want to show the orders that are either single orders
        *   Or they belong to anything but the First Subscription
        *   Because on checkout it creates a new/copy subscription, a new parent order, and pays for that
        *   The original subscription and parent Order never get used and maybe should be deleted
        */
        $subscriptions_collection = array();
        
        $order_to_unset = false;

        function my_sort($a,$b)
        {
            //if ($a==$b) return 0;
            return ($a<$b)?-1:1;
        }

        $post_deleted = null;
        if( count($subscriptions) > 1 ){
            
            uasort($subscriptions, "my_sort");

            $sub_to_unset = array_key_first($subscriptions);
            
            $subscriptions_collection['sub_to_unset'] = $sub_to_unset;
            $subscriptions_collection['sub_to_unset_type'] = gettype($sub_to_unset);

            foreach($orders as $key => $order){
                //$order_obj = new WC_Order( $id );
                
                $subscription_holder = wcs_get_subscriptions_for_order( $order['id'], array( 'order_type' => 'any' ) );

                if($subscription_holder){
                    foreach( $subscription_holder as $subscription_id => $subscription_obj ){
                        if($subscription_id){
                            
                            $subscriptions_collection[$subscription_id][] = array('key' => $key, 'id' => $order['id']);

                            $subscriptions_collection['key_to_unset_type'] = gettype($key);

                            if( $subscription_id == $sub_to_unset ){
                                $subscriptions_collection['key_to_unset'] = $key;
                                unset($orders[$key]);

                                /* let's delete the original order/subscription */
                                $post_deleted = $subscription_id;
                                wp_delete_post($subscription_id, true);

                                $orders = array_values($orders);
                            }
                        }
                    }
                }else{
                    $subscriptions_collection['singles'][] = array('key' => $key, 'id' => $order['id'] );
                }

                // $orders_collection[] = array(
                //     'id'            => $order['id'],
                //     'subscription'  => ,
                // );
            }
        }
        



        $message['wpData'] = array(
            
            'email' => $user->user_email,
            'display_name' => $user->display_name,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            //'company' => get_user_meta($user->ID, 'account_company', true),
            //'company' => get_the_title($compid),
            'company' => get_user_meta ( $user->ID, 'billing_company', true),
            'billing_company' => get_user_meta($user->ID, 'billing_company', true),
            'billingFirstName' => get_user_meta($user->ID, 'billing_first_name', true),
            'billingLastName' => get_user_meta($user->ID, 'billing_last_name', true),
            'billingAddress1' => get_user_meta($user->ID, 'billing_address_1', true),
            'billingAddress2' => get_user_meta($user->ID, 'billing_address_2', true),
            'billingCountry' => get_user_meta($user->ID, 'billing_country', true),
            'billingCity' => get_user_meta($user->ID, 'billing_city', true),
            'billingState' => get_user_meta($user->ID, 'billing_state', true),
            'billingZip' => get_user_meta($user->ID, 'billing_postcode', true),
            'billingEmail' => get_user_meta($user->ID, 'billing_email', true),
            'billingPhone' => get_user_meta($user->ID, 'billing_phone', true),
            'billingDefaultAddress' => true,
            'shipping_company' => get_user_meta($user->ID, 'shipping_company', true),
            'shippingFirstName' => get_user_meta($user->ID, 'shipping_first_name', true),
            'shippingLastName' => get_user_meta($user->ID, 'shipping_last_name', true),
            'shippingAddress1' => get_user_meta($user->ID, 'shipping_address_1', true),
            'shippingAddress2' => get_user_meta($user->ID, 'shipping_address_2', true),
            'shippingCountry' => get_user_meta($user->ID, 'shipping_country', true),
            'shippingCity' => get_user_meta($user->ID, 'shipping_city', true),
            'shippingState' => get_user_meta($user->ID, 'shipping_state', true),
            'shippingZip' => get_user_meta($user->ID, 'shipping_postcode', true),
            'shippingEmail' => get_user_meta($user->ID, 'shipping_email', true),
            'shippingPhone' => get_user_meta($user->ID, 'shipping_phone', true),
            'shippingDefaultAddress' => true,
            'profileimg' => get_user_meta($user->ID, '_flyrise_user_profile', true),
            'loggedin' => $loggedin,
            'orders' => $orders,
            //'cart'  =>  $woocommerce->cart->get_cart(), //breaks aka 500
            //'cart'  =>  WC()->cart, // doesn't break, but empty
            //'cart'  =>  WC()->cart->get_cart(), // breaks aka 500
            //'cart'  =>  unserialize($cart_items), // works, but returns an object{}
            //'cart'  =>  (array_values(unserialize($cart_items))[0]['quantity'] > 0)?array_values(unserialize($cart_items))[0]['quantity'] :0, // works locally only it seems
            
            'cart'  =>  $cart_items, // works locally only it seems
            
            //'cart'  =>  json_decode($cart_items), // null
            //'orders_to_show'    =>  $orders_to_show,
            //'subscriptions' => bbloomer_products_bought_by_curr_user(),
            'subscriptions_collection' => $subscriptions_collection,
            'payment_methods' => $saved_methods,
            'payment_form' => get_form_payment_method(),
            'renewals' =>  get_last_customer_renewal_order(),
            //'compusers' => get_all_company_users(),
            //'resetpass' => do_shortcode('[reset_password]'),
            'order_invoices' => wc_get_customer_orders_invoices($user->ID),
            //'print_woo_notices' => wc_print_notices(true),
            'subscriptions' =>  $subscriptions,
            'qwilr' =>  get_user_meta($user->ID, 'qwilr_proposal_link', true),
            'journey' => get_user_meta($user->ID, 'journey', true),
            'post_deleted' => $post_deleted,
        );
        return $message;
    }
    
    return $message;
    
}


/*
*   Method: set_password
*   For: Onboarding step_1
*/
function set_password($request) {
    //check_ajax_referer( 'ajax-login-nonce', 'security' );
    $message['action'] = 'setting password';

    
    $creds = array();
    $creds['user_login'] = $request["username"];
    $creds['user_password'] =  'test';
    $creds['remember'] = true;
    $user = wp_signon( $creds, false );

    if ( is_wp_error($user) ) {
        //$message['result'] = $user->get_error_message();
        $message['loggedin'] = false;
    }else{
        $message['result'] = 'seems to have worked';
        $message['loggedin'] = true;

        $userID = $user->ID;

        //$message['nonce'] = wp_create_nonce( 'my-nonce' );
        //$saved_methods = wc_get_customer_saved_methods_list( get_current_user_id() );
    }

    if ( ! empty( $user ) ) {
        
        $message['user'] = $user ;
        $message['id'] = $user->ID;

        
        // Define arguments that will be passed to the wp_update_user()
        //$request["password"]
        $userdata = array(
            'ID'        =>  $user->ID,
            'user_pass' =>  sanitize_text_field($request['password']) // Wordpress automatically applies the wp_hash_password() function to the user_pass field.
        ); 
        $user_id = wp_update_user($userdata);
    
        // wp_update_user() will return the user_id on success and an array of error messages on failure.
        //if($user_id == $current_user->ID){
        if($user_id == $user->ID){
            $message['password'] = "success";

            $journey = new stdClass();
            //$journey = (get_user_meta( $user->ID, 'journey', true ))?get_user_meta( $user->ID, 'journey', true ):$journey = new stdClass();
            $journey->step_1 = true;
            update_user_meta( $user_id, 'journey', json_encode($journey) );
            $message['journey'] = json_encode($journey);

        } else {
            $message['password'] = "failed";
            $message['error'] = $user_id;
        }  
    
    }else{
        $message['found_user_by_login'] = 'failed';
    }

    return $message;
    //echo json_encode($message);
    //exit;
}

/*
*   Method: update_password
*   For: pages/settings/account password change
*/
function update_password($request){
    $message['action'] = 'updating password';

    $user = get_user_by('id', get_current_user_id()); // should be auto logged in by the jwt token

    if ( is_wp_error($user) ) {
        $message['result'] = $user->get_error_message();
        $message['loggedin'] = false;
    }else{

        //$user = wp_get_current_user();
        $currentpass = $request['curpass'];
        $newpass = $request['newpass'];
        if ($user && wp_check_password( $currentpass, $user->data->user_pass, $user->ID ) ) {
            wp_set_password( $newpass, $user->ID);
            $message['result'] = 'Password Updated Successfully';
            $message['loggedin'] = true;
        }
        else {
            $message['result'] = 'Current Password Incorrect';
            $message['loggedin'] = false;
        }
    }
    return $message;
}


/*
*   Method: states
*/
function states(){
    global $woocommerce;

    $country = isset($_POST['country']) ? sanitize_text_field($_POST['country']) : '';
    $countries = new WC_Countries();
    $states = $countries->get_states($country);
    
    // print_r($states);
    return $states;
}


/*
*   Method: logout
*/
function logout(){

    $result = update_user_meta( get_current_user_id(), 'jwt_token', '');

    //wp_logout();
    do_action( 'wp_logout', get_current_user_id() ); // this doesn't work

    return $result;
}


/*
*   Method: profile_image
*/
function profile_image($request){
    //ini_set('file_uploads', 'On');
    ini_set('max_exection_time', 60);

    $message['action'] = 'update profile image';

    // $message['response']    = "ATTEMPTING";
    // $message['filename']    = "unset filename";
    // $message['url']         = "unset url";
    // $message['type']        = "unset type";

    $user = get_user_by( 'id', get_current_user_id() );

    if ( is_wp_error($user) ) {
        $message['result'] = $user->get_error_message();
        $message['loggedin'] = false;
    }else{
        $message['result'] = 'loggedin for image update';
        $message['loggedin'] = true;

        $userID = $user->ID;
        $uid = $userID;
    
        // $compid = get_user_meta($uid, '_flyrise_user_profile', true);
        // $profileimg = get_user_meta($uid, 'profileimg', true);
        // $message['flyrise_user_profile'] = $compid;
        // $message['profileimg'] = $profileimg;

        $usingUploader = 2;
        //$usingUploader = 1;
        //$user = get_current_user_id();
        $fileErrors = array(
            0 => "There is no error, the file uploaded with success",
            1 => "The uploaded file exceeds the upload_max_files in server settings",
            2 => "The uploaded file exceeds the MAX_FILE_SIZE from html form",
            3 => "The uploaded file uploaded only partially",
            4 => "No file was uploaded",
            6 => "Missing a temporary folder",
            7 => "Failed to write file to disk",
            8 => "A PHP extension stoped file to upload"
        );
      
        $posted_data =  isset( $_POST ) ? $_POST : array();
        $file_data = isset( $_FILES ) ? $_FILES : array();
        $upload = $_FILES['file'];
        $message['upload'] = $upload;

        $data = array_merge( $posted_data, $file_data );
        $message['data'] = $data;

        //$response = array();  
        if( $usingUploader == 1 ) {
            //$uploaded_file = wp_handle_upload( $data['file'], array( 'test_form' => false ) );

            // if( $uploaded_file && ! isset( $uploaded_file['error'] ) ) {
            //     $message['response'] = "SUCCESS";
            //     $message['filename'] = basename( $uploaded_file['url'] );
            //     $message['url'] = $uploaded_file['url'];
            //     $message['type'] = $uploaded_file['type'];
            // } else {
            //     $message['response'] = "ERROR";
            //     $message['error'] = $uploaded_file['error'];
            // }
            $message['usingUploader'] = 1;
        } elseif ( $usingUploader == 2) {
            //$attachment_id = media_handle_upload( 'file', 0 );
            //$attachment_id = media_handle_upload( $data['filename'], 0 );
            //$message['error'] = $fileErrors[ $data['file']['error'] ];
            //$message['error'] = $data['file']['error'];
            //$message['error'] = $attachment_id;

            $uploads = wp_upload_dir(); /*Get path of upload dir of wordpress*/
            if (is_writable($uploads['path']))  /*Check if upload dir is writable*/
            {
                $message['upload_path_writable'] = true;
                if ((!empty($upload['tmp_name'])))  /*Check if uploaded image is not empty*/
                {
                    $message['tmp_name_empty'] = false;
                    if ($upload['tmp_name'])   /*Check if image has been uploaded in temp directory*/
                    {
                        require ABSPATH . 'wp-admin/includes/image.php';
                        require ABSPATH . 'wp-admin/includes/file.php';
                        require ABSPATH . 'wp-admin/includes/media.php';

                        $message['tmp_name'] = true;
                        
                        $overrides = array('test_form' => false);
                        $attachment_id = wp_handle_upload($upload, $overrides);
                        
                        //$attachment_id = media_handle_upload( $upload, 0 );
                        $message['attachment_id'] = $attachment_id;

                        $fullsize_path = get_attached_file( $attachment_id );
                        //$pathinfo = pathinfo( $fullsize_path );
                        //$url = wp_get_attachment_url( $attachment_id['url'] );
                        $url = $attachment_id['url'];
                        update_user_meta($uid, '_flyrise_user_profile', $url);
                        update_user_meta($uid, 'profileimg', $url);
                        $message['response'] = "SUCCESS";
                        //$message['filename'] = $pathinfo['filename'];
                        //$message['pathinfo'] = $pathinfo;
                        $message['fullsize_path'] = $fullsize_path;
                        $message['url'] = $url;
                        //$type = $pathinfo['extension'];
                        $type = $attachment_id['type'];
                        if( $type == "jpeg"
                        || $type == "jpg"
                        || $type == "png"
                        || $type == "gif" ) {
                            $type = "image/" . $type;
                        }
                        $message['type'] = $type;

                        
                    }else{
                        $message['tmp_name'] = true;
                    }
                }else{
                    $message['tmp_name_empty'] = true;
                }
            }else{
                $message['upload_path_writable'] = false;
            }

            // if ( is_wp_error( $attachment_id ) ) { 
            //     $message['response'] = "ERROR";
            //     $message['error'] = $fileErrors[ $data['file']['error'] ];
            // } else {
            //     // $fullsize_path = get_attached_file( $attachment_id );
            //     // $pathinfo = pathinfo( $fullsize_path );
            //     // $url = wp_get_attachment_url( $attachment_id );
            //     // update_user_meta($user, '_flyrise_user_profile', $url);
            //     $message['response'] = "SUCCESS";
            //     $message['filename'] = $pathinfo['filename'];
            //     $message['url'] = $url;
            //     $type = $pathinfo['extension'];
            //     if( $type == "jpeg"
            //     || $type == "jpg"
            //     || $type == "png"
            //     || $type == "gif" ) {
            //         $type = "image/" . $type;
            //     }
            //     $message['type'] = $type;
            // }
            $message['usingUploader'] = 2;
        }
    }
  
    return $message;
    die();
}


/*
*   Method: update_account
*   pages/settings/account
*   User Settings / Account Settings:
*   first name, last name, email
*/
function update_account($request) {

    $message['action'] = 'update account';
    $message['first_name'] = $request['first_name'];
    $message['last_name'] = $request['last_name'];
    $message['headers'] = $request->get_headers(); // shows bearer token!
    //$message['auth_token'] = $headers['authorization'][0]; // doesn't work

    /*
    *   Old approach passing username password
    $creds = array();
    $creds['user_login'] = $request["username"];
    $creds['user_password'] =  'test';
    $creds['remember'] = true;
    $user = wp_signon( $creds, true );

    wp_set_current_user( $user->ID, $user->user_login );
    wp_set_auth_cookie( $user->ID );
    do_action( 'wp_login', $user->user_login, $user );
    */

    //$user = get_current_user(); // the JWT plugin logged them in!
    $message['current_user'] = get_current_user_id();

    if ( is_wp_error($user) ) {
        $message['result'] = $user->get_error_message();
        $message['loggedin'] = false;
    }else{
        $message['result'] = 'loggedin for address updates';
        $message['loggedin'] = true;

        $uid = get_current_user_id();
        $message['uid'] = get_current_user_id();

        //$uid = get_current_user_id();
        update_user_meta( $uid, 'first_name', $request['first_name'] );
        update_user_meta( $uid, 'last_name', $request['last_name'] );
        update_user_meta( $uid, 'user_email', $request['email'] );
        //update_user_meta( $uid, 'account_company', $request['company'] );
        //update_user_meta( $uid, 'company', $request['company'] );

        $to = 'alexf@flyrise.io';
        $headers = '';
        $subject = 'User ID: ' . $uid . ' has changed their meta user_email';
        $messageBody = 'User ID: ' . $uid . ' has changed their meta user_email via their Dashboard Account Settings! Manually change it for their Client in ContentSnare! Consider also changing their other Profile Email fields: Email and Meta Billing Email!';
        wp_mail( $to, $subject, $messageBody, $headers );
    }
    return $message;
}


/*
*   Method: update_addresses
*   pages/settings/account
*   My Addresses
*   Supports Billing Address change AND Shipping Address change AND Onboarding Step 2!
*/
function update_addresses($request) {

    //global $woocomerce;
    //global $wcs;

    // return $request;
    // exit;

    $message['action'] = 'update addresses';

    $user = get_user_by( 'id', get_current_user_id() );
    $uid = get_current_user_id();
    $message['uid'] = $uid;

    if ( is_wp_error($user) ) {
        $message['result'] = $user->get_error_message();
        $message['loggedin'] = false;
    }else{
        $message['result'] = 'loggedin for address updates';
        $message['loggedin'] = true;

        // $userID = $user->ID;
        // $uid = $userID;
    }

    if ( ! empty( $user ) && $uid != 0 ){

        if( isset( $request['first_name'] ) || isset( $request['last_name'] ) ){
            wp_update_user([
                //'ID' => $uid,
                'first_name' => $request['billing_first_name'],
                'last_name' => $request['billing_last_name'],
            ]);
        }

        // if( isset( $request['billing_company'] ) ){
        //     update_user_meta( $uid, 'company', $request['billing_company'] );
        // }
        // if( isset( $request['billing_company'] ) ){
        //     update_user_meta( $uid, 'acount_company', $request['billing_company'] );
        // }

        if( isset( $request['billing_company'] ) ){
            update_user_meta( $uid, 'billing_company', $request['billing_company'] );
            //update_user_meta( $uid, 'company', $request['billing_company'] );
            //update_user_meta( $uid, '_billing_company', $request['billing_company'] );
        }

        if( isset( $request['billing_first_name'] ) ){
            update_user_meta( $uid, 'billing_first_name', $request['billing_first_name'] );
        }

        if( isset( $request['billing_last_name'] ) ){
            update_user_meta( $uid, 'billing_last_name', $request['billing_last_name'] );
        }
        
        if( isset( $request['billing_address_1'] ) ){
            update_user_meta( $uid, 'billing_address_1', $request['billing_address_1'] );
        }
        
        if( isset( $request['billing_address_2'] ) ){
            update_user_meta( $uid, 'billing_address_2', $request['billing_address_2'] );
        }

        if( isset( $request['billing_city'] ) ){
            update_user_meta( $uid, 'billing_city', $request['billing_city'] );
        }
        
        if( isset( $request['billing_state'] ) ){
            update_user_meta( $uid, 'billing_state', $request['billing_state'] );
        }

        if( isset( $request['billing_postcode'] ) ){
            update_user_meta( $uid, 'billing_postcode', $request['billing_postcode'] );
        }

        if( isset( $request['billing_country'] ) ){
            update_user_meta( $uid, 'billing_country', $request['billing_country'] );
        }

        if( isset( $request['billing_email'] ) ){
            update_user_meta( $uid, 'billing_email', $request['billing_email'] );
        }

        if( isset( $request['billing_phone'] ) ){
            update_user_meta( $uid, 'billing_phone', $request['billing_phone'] );
        }

        if( isset( $request['shipping_company'] ) ){
            update_user_meta( $uid, 'shipping_company', $request['shipping_company'] );
            //update_user_meta( $uid, 'company', $request['shipping_company'] );
        }

        if( isset( $request['shipping_first_name'] ) ){
            update_user_meta( $uid, 'shipping_first_name', $request['shipping_first_name'] );
        }

        if( isset( $request['shipping_last_name'] ) ){
            update_user_meta( $uid, 'shipping_last_name', $request['shipping_last_name'] );
        }

        if( isset( $request['shipping_address_1'] ) ){
            update_user_meta( $uid, 'shipping_address_1', $request['shipping_address_1'] );
        }

        if( isset( $request['shipping_address_2'] ) ){
            update_user_meta( $uid, 'shipping_address_2', $request['shipping_address_2'] );
        }

        if( isset( $request['shipping_city'] ) ){
            update_user_meta( $uid, 'shipping_city', $request['shipping_city'] );
        }
        
        if( isset( $request['shipping_state'] ) ){
            update_user_meta( $uid, 'shipping_state', $request['shipping_state'] );
        }

        if( isset( $request['shipping_country'] ) ){
            update_user_meta( $uid, 'shipping_country', $request['shipping_country'] );
        }

        if( isset( $request['shipping_postcode'] ) ){
            update_user_meta( $uid, 'shipping_postcode', $request['shipping_postcode'] );
        }

        if( isset( $request['shipping_email'] ) ){
            update_user_meta( $uid, 'shipping_email', $request['shipping_email'] );
        }

        if( isset( $request['shipping_phone'] ) ){
            update_user_meta( $uid, 'shipping_phone', $request['shipping_phone'] );
        }

        //update_user_meta( $uid, 'qwilr_proposal_link', $request['qwilr'] );

        $users_subscriptions = wcs_get_users_subscriptions($uid);
        $message['subscriptions_anew'] = $users_subscriptions;


        
        $order_args = array(
            'customer_id' => $uid,
            'limit' => -1,
        );
        //$users_orders = wc_get_orders($order_args); // doesn't work
        $users_orders = wc_get_customer_orders($order_args);
        $message['users_orders'] = $users_orders;
        


        /*
        *   Also Doesn't Work, basically can't get Woocommerce Orders, just subscriptions
        function get_all_orders($user_id_sent) {
            $customer_orders = get_posts(apply_filters('woocommerce_my_account_my_orders_query', array(
                'numberposts' => -1,
                'meta_key' => '_customer_user',
                'meta_value' => $user_id_sent,
                'post_type' => wc_get_order_types('view-orders'),
                'post_status' => array_keys(wc_get_order_statuses())
                    )));
            return $customer_orders;
        }

        if (!class_exists('WooCommerce')){
            $message['included_woo_file'] = true;
            require ABSPATH . 'wp-content/plugins/woocommerce/woocommerce.php';
        
            $users_orders = get_all_orders($uid);
            $message['orders_anew'] = $users_orders;
        }else{
           $message['included_woo_file'] = false;
        }
        */
        




        $address_billing = array();
        if( isset($request['billing_first_name']) ){  $address_billing['first_name'] = $request['billing_first_name']; }
        if( isset($request['billing_last_name']) ){ $address_billing['last_name'] = $request['billing_last_name']; }
        if( isset($request['billing_email']) ){ $address_billing['email'] = $request['billing_email']; }
        if( isset($request['billing_address_1']) ){ $address_billing['address_1'] = $request['billing_address_1']; }
        if( isset($request['billing_address_2']) ){ $address_billing['address_2'] = $request['billing_address_2']; }
        if( isset($request['billing_city']) ){ $address_billing['city'] = $request['billing_city']; }
        if( isset($request['billing_state']) ){ $address_billing['state'] = $request['billing_state']; }
        if( isset($request['billing_postcode']) ){ $address_billing['postcode'] = $request['billing_postcode']; }
        if( isset($request['billing_country']) ){ $address_billing['country'] = $request['billing_country']; }
        //if( isset($request['billing_company']) ){ $address_billing['billing_company'] = $request['billing_company']; } // doesn't work

            
        foreach ($users_subscriptions as $subscription) {
            $message['subscriptions_updated_billing'][] = $subscription->id;
            //if ($subscription->has_status(array('active', 'on-hold', 'pending', 'pending payment'))) {
                $subscription->set_address($address_billing, 'billing');
                $subscription->set_billing_company($request['billing_company']);
                $subscription->set_billing_phone($request['billing_phone']);
                $subscription->save();

                //echo "Pare/nt: " . $subscription->parent_id;// => 19389
                $parentID = $subscription->parent_id;
                // $order = wc_get_order( $parentID );
                // $order->set_address($address_billing, 'billing');
                // $order->set_billing_company($request['billing_company']);
                // $order->save();

                $parentOrder = new WC_Order( $parentID );
                $parentOrder->set_address($address_billing, 'billing');
                $parentOrder->set_billing_company($request['billing_company']);
                $parentOrder->set_billing_phone($request['billing_phone']);
                $parentOrder->save();       

                $customer = new WC_Customer( $uid ); 
                $customer->set_billing_company($request['billing_company']);
                $customer->save();
            //}
        }

        
        foreach ($users_orders as $singleOrder) {
            $message['users_orders_updated_billing'][] = $singleOrder['id'];
            //if ($subscription->has_status(array('active', 'on-hold', 'pending', 'pending payment'))) {
                $order = new WC_Order( $singleOrder['id'] );
                $order->set_address($address_billing, 'billing');
                $order->set_billing_company($request['billing_company']);
                $order->set_billing_phone($request['billing_phone']);
                $order->save();
            //}
        }
       



        // $address_shipping = array(
        //     'first_name' => $request['shipping_first_name'],
        //     'last_name'  => $request['shipping_last_name'],
        //     'email'      => $request['shipping_email'],
        //     'address_1'  => $request['shipping_address_1'],
        //     'address_2'  => $request['shipping_address_2'],
        //     'city'       => $request['shipping_city'],
        //     'state'      => $request['shipping_state'],
        //     'postcode'   => $request['shipping_postcode'],
        //     'country'    => $request['shipping_country'],
        //     'shipping_company' => $request['shipping_company'],
        // );

        $address_shipping = array();
        if( isset($request['shipping_first_name']) ){ $address_shipping['first_name'] = $request['shipping_first_name']; }
        if( isset($request['shipping_last_name']) ){ $address_shipping['last_name'] = $request['shipping_last_name']; }
        if( isset($request['shipping_email']) ){ $address_shipping['email'] = $request['shipping_email']; }
        if( isset($request['shipping_address_1']) ){ $address_shipping['address_1'] = $request['shipping_address_1']; }
        if( isset($request['shipping_address_2']) ){ $address_shipping['address_2'] = $request['shipping_address_2']; }
        if( isset($request['shipping_city']) ){ $address_shipping['city'] = $request['shipping_city']; }
        if( isset($request['shipping_state']) ){ $address_shipping['state'] = $request['shipping_state']; }
        if( isset($request['shipping_postcode']) ){ $address_shipping['postcode'] = $request['shipping_postcode']; }
        if( isset($request['shipping_country']) ){ $address_shipping['country'] = $request['shipping_country']; }
        if( isset($request['shipping_company']) ){ $address_shipping['billing_company'] = $request['shipping_company']; }

        foreach ($users_subscriptions as $subscription) {
            $message['subscriptions_updated_shipping'][] = $subscription;
            //if ($subscription->has_status(array('active', 'on-hold', 'pending', 'pending payment'))) {
                $subscription->set_address($address_shipping, 'shipping');
                $subscription->set_shipping_company($request['shipping_company']);
                $subscription->set_shipping_phone($request['shipping_phone']);
                $subscription->save();

                //echo "Parent: " . $subscription->parent_id;// => 19389
                $parentID = $subscription->parent_id;
                //$order = wc_get_order( $parentID );
                //$order->set_address($address_shipping, 'shipping');

                $parentOrder = new WC_Order( $parentID );
                $parentOrder->set_address($address_shipping, 'shipping');
                $parentOrder->set_shipping_company($request['shipping_company']);
                $parentOrder->set_shipping_phone($request['shipping_phone']);
                $parentOrder->save();       

                $customer = new WC_Customer( $uid ); 
                $customer->set_shipping_company($request['shipping_company']);
                $customer->save();

            //}
        }


        foreach ($users_orders as $singleOrder) {
            $message['users_orders_updated_shipping'][] = $singleOrder['id'];
            //if ($subscription->has_status(array('active', 'on-hold', 'pending', 'pending payment'))) {
                $order = new WC_Order( $singleOrder['id'] );
                $order->set_address($address_shipping, 'shipping');
                $order->set_shipping_company($request['shipping_company']);
                $order->set_shipping_phone($request['shipping_phone']);
                $order->save();
            //}
        }
        

        $journey = json_decode( get_user_meta($uid, 'journey', true) );
        $journey->step_2 = true;
        update_user_meta( $uid, 'journey', json_encode($journey) );
        $message['journey'] = json_encode($journey);

        
        $message['result'] = 'saved';
    }
    //$message['qwilr'] = get_post_meta($uid, 'qwilr_proposal_link', true );
    return $message;
}


/*
*   Method: get_order_details
*/
function get_order_details($request) {
    $order = wc_get_order( $request['order_id'] );
    $wuser = $order->get_user();
    $vuser = get_current_user_id();
    //if ($wuser->id == $vuser) {

        $pdf_url = admin_url( 'admin-ajax.php?action=generate_wpo_wcpdf&template_type=invoice&order_ids=' . $order->get_id() . '&order_key=' . $order->get_order_key() );
        
        // $pdf_url = wp_nonce_url( add_query_arg( array(
        //     'action'        => 'generate_wpo_wcpdf',
        //     'document_type' => 'invoice',
        //     'order_ids'     => $order->get_id(),
        //     'my-account'    => true,
        //     'key'           => $order->order_key
        // ), admin_url( 'admin-ajax.php' ) ), 'generate_wpo_wcpdf' );

        //$url = wp_nonce_url( admin_url( 'admin-ajax.php?action=generate_wpo_wcpdf&template_type=invoice&order_ids=' . $request['order_id'] . '&my-account'), 'generate_wpo_wcpdf' );
        $url = admin_url( 'admin-ajax.php');

        //$url = '/wp-admin/admin.php?page=foo';
        //$nonce = wp_create_nonce( 'generate_wpo_wcpdf' ); // create nonce
        //$url = add_query_arg( [ '_wpnonce'=>$nonce ], $url );
        // $url = add_query_arg( [ 'action'=>'generate_wpo_wcpdf' ], $url );
        // $url = add_query_arg( [ 'template_type'=>'invoice' ], $url );
        // $url = add_query_arg( [ 'order_ids'=>$request['order_id'] ], $url );
        // //$url = add_query_arg( [ 'my-account' ], $url );
        // $url = add_query_arg( [ 'key'=>$order->order_key ], $url );

        $order = $order->get_data();

        $order = (array) $order;
        $order['view'] = html_entity_decode($pdf_url);
        //$order = json_encode($order);
        return $order;
        
    //}
}


/*
*   Method: get_order_items
*/
function get_order_items($request) {
    $inc_tax = true;
    $round = false;
    $i = 0;
    $order = wc_get_order( $request['order_id'] );
    $items = $order->get_items();
    foreach ($items as $item) {
        $product_name = $item->get_name();
        $product_id = $item->get_product_id();
        $product = wc_get_product( $product_id );
        $product_price = $order->get_line_total( $item, $inc_tax, $round );
        $prod_data[$i]['name'] = $product_name;
        $prod_data[$i]['price'] = $product_price;
        $i++;
    }
    //echo $prod_data; exit();
    if(is_array($prod_data)){
        return $prod_data;
    }else{
        return array();
    }
    exit();
}


/*
*   Method: update_journey
*/
function update_journey($request) {

    $message['action'] = 'update journey';
    // $message['username'] = $request['username'];
    // $message['password'] = $request['password'];
    
    // $creds = array();
    // $creds['user_login'] = $request["username"];
    // $creds['user_password'] =  $request['password'];
    // $creds['remember'] = true;
    // $user = wp_signon( $creds, false );

    $user = get_user_by( 'id', get_current_user_id() );
    $uid = get_current_user_id();

    if ( is_wp_error($user) ) {
        $message['result'] = $user->get_error_message();
        $message['loggedin'] = false;
    }else{
        $message['result'] = 'loggedin for journey updates';
        $message['loggedin'] = true;

        // $userID = $user->ID;
        // $uid = $userID;
    }

    $message['user_id_signon'] = $uid;

    if ( ! empty( $user ) && $uid != 0 ){

        // $current = wp_get_current_user();
        // $uid = get_current_user_id();
        //$message['user_id_user'] = $uid; // 0!

        $journey = json_decode( get_user_meta( $uid, 'journey', true ) );
        $journey->step_3 = true;
        update_user_meta( $uid, 'journey', json_encode($journey) );
        $message['journey'] = json_encode($journey);
    }

    return $message;
}


/*
*   Method: auto_login
*/
add_action('wp', 'auto_login', 1);
function auto_login($request) {

    //if( isset($_POST['username']) && isset($_POST['password']) ){
    if( isset($_POST['jwt_token']) ){
        //$creds['user_login'] = $_POST['username'];
        //$creds['user_password'] = $_POST['password']; 
        $args = array(
            'meta_key'  =>  'jwt_token',
            'meta_value'=>  $_POST['jwt_token']
        );
        $user_to_login = get_users ( $args )[0];
        // print_r($user_to_login);
        // echo $user_to_login->user_login . "<br />";
        // echo $user_to_login->ID;
        // exit;
        //$user = wp_signon( $creds, true );
        //print_r($user);
        //$userID = $user->ID;
        
        //wp_set_current_user( $userID, $creds['user_login'] );
        wp_set_current_user( $user_to_login->ID, $user_to_login->user_login );
        wp_set_auth_cookie( $user_to_login->ID );
        //do_action('wp_login', $user);
    }
}

/*
*   END New Methods Duplicated
*/
