<?php

/*
*   This took a lot of perfecting, and now works great if again needed,
*   for Server control of the Routing.
*   It is no longer needed though,
*   We're using Vue Component Lifecycle Methods and Mixins instead.
*   Which seems far superior as it's one less player,
*   one less set of controlling logic,
*   one less thing to manage, no infinite loops, etc
*/
//add_action('init', 'fw_check_user'); // this loads too late, page requested is already wp-login not login
function fw_check_user(){

    $page = $_SERVER['REQUEST_URI'];

    echo "Page requested: " . $page . "<br />";
    echo "Root match: " . (int)($_SERVER['REQUEST_URI'] == '/') . "<br />"; //0
    echo "Login match: " . (int)strpos($page, 'login') . "<br />";
    echo "Dashboard match: " . (int)strpos($page, 'dashboard') . "<br />";

    $user = wp_get_current_user();
    $loggedin = is_user_logged_in($user);
    
    // echo "Loggedin: ";
    // if( $loggedin == false ){ echo "false<br />"; }
    // if( $loggedin == true ) { echo "true<br />"; }
    
    
    if( $_SERVER['REQUEST_URI'] == '/' ){
        //echo "matched /<br />";
        if( $loggedin == false ) {
            //echo "loggedin: ";
            //echo (int)strpos($page, 'login') . "<br />";
            if ( strpos($page, 'login') !== true ){
                //echo "entered forward block<br />";
                wp_redirect('/login');
                exit;
            }
        }
        if($loggedin == true){
             wp_redirect('/dashboard');
             exit;
        }
    }    
    if( $loggedin == false ){
        if(
            strpos($page, 'pages/settings/account') !== false ||
            strpos($page, 'dashboard') !== false ||
            strpos($page, 'detail') !== false ||
            strpos($page, 'approved') !== false ||
            strpos($page, 'pages/settings/orders') !== false ||
            strpos($page, 'pages/settings/view-order') !== false ||
            strpos($page, 'pages/settings/payment-methods') !== false
        ){
            wp_redirect('/login');
            exit;
        }
    }
    
}



// Execute the action only if the user isn't logged in
if (!is_user_logged_in()) {
    add_action('init', 'ajax_login_init');
}
function ajax_login_init(){

    wp_register_script('ajax-login-script', get_template_directory_uri() . '/ajax-login-script.js', array('jquery') ); 
    wp_enqueue_script('ajax-login-script');

    wp_localize_script( 'ajax-login-script', 'ajax_login_object', array( 
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'redirecturl' => home_url(),
        'loadingmessage' => __('Sending user info, please wait...')
    ));

    // Enable the user with no privileges to run ajax_login() in AJAX
    add_action( 'wp_ajax_nopriv_ajaxlogin', 'ajax_login' );
}

add_action('wp_ajax_tajax_states', 'tajax_states');
add_action('wp_ajax_nopriv_tajax_states', 'tajax_states');
function tajax_states(){
    //$message['result'] = "function get_states called";
    global $woocommerce;
    $country = isset($_POST['country']) ? sanitize_text_field($_POST['country']) : '';
    $countries = new WC_Countries();
    $states = $countries->get_states($country);
    // print_r($states);
    echo json_encode($states);
    die();
}

add_action('wp_ajax_tajax_login', 'tajax_login');
add_action('wp_ajax_nopriv_tajax_login', 'tajax_login');
function tajax_login(){

    // First check the nonce, if it fails the function will break
    //check_ajax_referer( 'ajax-login-nonce', 'security' );

    // Nonce is checked, get the POST data and sign user on
    $info = array();
    $info['user_login'] = $_POST['username'];
    $user_login = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = true;
    //print_r($info);

    $user_signon = wp_signon( $info, false );
    //wp_set_current_user(5045, 'NewTestUserAlex');
	//wp_set_auth_cookie(5045);
    //setcookie('Onboarder',  $_POST['username'], time()+14400);
    
    if ( is_wp_error($user_signon) ){
        $error_string = $user_signon->get_error_message();
        //echo '<div id="message" class="error"><p>' . $error_string . '</p></div>';
        echo json_encode(array('loggedin'=>false, 'message'=>__('Wrong username or password.'), 'details' =>__($error_string) ));
    } else {
        $userID = $user_signon->ID;
        //wp_set_current_user( $userID, $user_login );
        wp_set_current_user( $userID, $info['user_login'] );
        wp_set_auth_cookie( $userID );
        echo json_encode( array('loggedin'=>true, 'message'=>__('Login successful, redirecting...'), 'user'=>$userID, 'password'=>$info['user_password']) );
    }
    //echo 'something';
    //echo json_encode( array('loggedin'=>true, 'message'=>__('Login successful, redirecting...'), 'user'=>$info['user_login'], 'password'=>$info['user_password']) );
    die();
}

add_action('wp_ajax_tajax_logout', 'tajax_logout');
add_action('wp_ajax_nopriv_tajax_logout', 'tajax_logout');
function tajax_logout(){

    $info = array();
    $info['remember'] = true;

    $user_signon = wp_logout();
    
    echo json_encode( array( 'loggedin'=>false, 'message'=>__('Logged Out') ) );
    //die();
}

function my_login_logo() {
    ?>
        <style type="text/css">
            #login h1 a, .login h1 a {
                background-image: url('/wp-content/uploads/flyrise-enclosed-icon.png');
            height:65px;
            width:320px;
            background-size: 320px 65px;
            background-repeat: no-repeat;
                padding-bottom: 30px;
            }
        </style>
    <?php
}
//add_action( 'login_enqueue_scripts', 'my_login_logo' );






//--------------------- Onboarding new User, get Company Name, set fresh Password --------------------

add_action('wp_ajax_tajax_login_auto', 'tajax_login_auto');
add_action('wp_ajax_nopriv_tajax_login_auto', 'tajax_login_auto');

function tajax_login_auto(){
    //global $wp;
    //check_ajax_referer( 'ajax-login-nonce', 'security' );
    //wp_destroy_all_sessions();
    //$user = wp_get_current_user();
    //print_r($user);
    //wp_logout();

    //$message = array ('action'=>'checking username');
    //echo "checking username<br />";
    //if(!is_user_logged_in()){
    $message['action'] = 'checking password';
    $message['error'] = '';

    $username = $_POST['username'];
    
    if( $user = get_user_by('login', $username) ){
        
        $message['company'] = get_user_meta ( $user->ID )['billing_company'][0];
        //$message['billing_address_1'] = get_user_meta ( $user->ID )['billing_address_1'][0];
        $user_meta = get_user_meta($user->ID);
        //$message['qwilr'] = get_user_meta ( $user->ID )['qwilr_proposal_link'][0];
        // foreach($user_meta as $meta_field){
        //     var_dump($meta_field);
        // }
        $message['qwilr'] = get_user_meta( $user->ID, 'qwilr_proposal_link', true );

        $pass = 'test';
        if ( $user && wp_check_password( $pass, $user->data->user_pass, $user->ID ) ) {
            $message['error'] = 'password unset so far';
            // if the username exists, and their password is 'test', allow frontend to continue
            
            $info = array();
            $info['user_login'] = $_POST['username'];
            $user_login = $_POST['username'];
            $info['user_password'] = 'test';
            $info['remember'] = true;

            $user_signon = wp_signon( $info, false );
            if ( is_wp_error($user_signon) ){
                $error_string = $user_signon->get_error_message();
                //echo '<div id="message" class="error"><p>' . $error_string . '</p></div>';
                $message['result'] = "Logged Out";
                $message['error'] = 'Wrong username or password';
                $message['details'] = __($error_string);
            } else {
                $userID = $user_signon->ID;
                //$user->data->billing_address_1 = $user->billing_address_1; // $user->meta_key

                wp_set_current_user( $userID, $user_login );
                wp_set_auth_cookie( $userID, true, false );
                $message['result'] = "Logged In";
                $message['error'] = 'Password Still Unset';
                $message['message'] = 'Login successful';
            }

            $message['user_meta'] = $user_meta;
            $message['user'] = wp_get_current_user();

            //$message['orders'] = wc_get_customer_orders($user->ID);
            //$message['user']['orders'] = wc_get_customer_orders($user->ID);

            $args = array(
                'customer_id' => $user->ID
            );
            //$message['user']['orders'] = wc_get_orders($args);

            $message['username'] = $user->user_login;
            $message['id'] = $user->ID;
            //$message['password'] = "Password has not been set yet.";
    
        } else {
            // if the pass has already been set ...
            $message['error'] = "This password has already been changed.";
        }
    }else{
        $message['username'] = 'No User with that Username';
    }

    // if(is_user_logged_in()){
    //     $message['result'] = "User logged in";
    // }else{
    //     $message['result'] = "Logged Out";
    // }

    echo json_encode($message);
    //echo "data: " . json_encode($message) . "\n\n";
    exit;
}


//--------------------- Set new password --------------------

add_action('wp_ajax_tajax_set_password', 'tajax_set_password' );
add_action('wp_ajax_nopriv_tajax_set_password', 'tajax_set_password' );

function tajax_set_password() {
    //check_ajax_referer( 'ajax-login-nonce', 'security' );
    $message['action'] = 'setting password';
    $message['username'] = $_POST['username'];
    $message['password'] = $_POST['password'];

    $pass = 'test';
    
    // to here tested to work.

    $user = get_user_by('login', $_POST['username']);

    if ( ! empty( $user ) ) {
        
        $message['user'] = $user ;
        $message['id'] = $user->ID;

        // to here tested to work.

        if ( wp_check_password( $pass, $user->data->user_pass, $user->ID ) ) {

            $message['password unchanged'] = 'true';
            
            // to here tested to work.

            //wp_destroy_all_sessions();
            //clean_user_cache($user->ID);
            wp_set_current_user( $user->ID );
            wp_set_auth_cookie( $user->ID , true, false);
            //update_user_caches($user);
            //wp_clear_auth_cookie();
            //do_action( 'wp_login', $user->user_login ); // this fatal 500!
            
            $message['wp_login'] = 'completed';
            //echo json_encode($message);
            //exit;

            // Define arguments that will be passed to the wp_update_user()
            $userdata = array(
                'ID'        =>  $user->ID,
                'user_pass' =>  sanitize_text_field($_POST['password']) // Wordpress automatically applies the wp_hash_password() function to the user_pass field.
            ); 
            $user_id = wp_update_user($userdata);
        
            // wp_update_user() will return the user_id on success and an array of error messages on failure.
            //if($user_id == $current_user->ID){
            if($user_id == $user->ID){
                $message['password'] = "success";

                $journey = new stdClass();
                //$journey = (get_user_meta( $user->ID, 'journey', true ))?get_user_meta( $user->ID, 'journey', true ):$journey = new stdClass();
                $journey->step_1 = true;
                update_user_meta( $user_id, 'journey', json_encode($journey) );
                $message['journey'] = json_encode($journey);

            } else {
                $message['password'] = "failed";
                $message['error'] = $user_id;
            }  
        } else {
            $message['password'] = "This User has already initialized their password";
        }
    
    }else{
        $message['found_user_by_login'] = 'failed';
    }

    if(is_user_logged_in()){
        $message['loggedin'] = "User logged in";
        $message['result'] = "success";
    }else{
        $message['logginin'] = "Logged Out";
        $message['result'] = "failed";
    }

    echo json_encode($message);
    exit;
}


//--------------------- Get User Details --------------------

add_action('wp_ajax_tajax_get_details', 'tajax_get_details' );
add_action('wp_ajax_nopriv_tajax_get_details', 'tajax_get_details' );

function tajax_get_details() {

    $message['action'] = 'getting details';

    $current_user = wp_get_current_user();

    $message['id'] = $current_user->ID;
    $message['billing_first_name'] = get_user_meta( $current_user->ID, 'first_name', true );
    $message['billing_last_name'] = get_user_meta( $current_user->ID, 'last_name', true);
    $message['billing_company'] = get_user_meta( $current_user->ID, 'billing_company', true );
    $message['billing_address_1'] = get_user_meta( $current_user->ID, 'billing_address_1', true );
    $message['billing_address_2'] = get_user_meta( $current_user->ID, 'billing_address_2', true );
    $message['billing_city'] = get_user_meta( $current_user->ID, 'billing_city', true );
    $message['billing_state'] = get_user_meta( $current_user->ID, 'billing_state', true );
    $message['billing_postcode'] = get_user_meta( $current_user->ID, 'billing_postcode', true );
    $message['billing_country'] = get_user_meta( $current_user->ID, 'billing_country', true );
    $message['billing_email'] = get_user_meta( $current_user->ID, 'billing_email', true );
    $message['billing_phone'] = get_user_meta( $current_user->ID, 'billing_phone', true );

    $message['shipping_address_1'] = get_user_meta( $current_user->ID, 'shipping_address_1', true );
    $message['shipping_address_2'] = get_user_meta( $current_user->ID, 'shipping_address_2', true );
    $message['shipping_city'] = get_user_meta( $current_user->ID, 'shipping_city', true );
    $message['shipping_state'] = get_user_meta( $current_user->ID, 'shipping_state', true );
    $message['shipping_postcode'] = get_user_meta( $current_user->ID, 'shipping_postcode', true );
    $message['shipping_country'] = get_user_meta( $current_user->ID, 'shipping_country', true );
    
    $message['qwilr'] = get_user_meta( $current_user->ID, 'qwilr_proposal_link', true );

    if(is_user_logged_in()){
        $message['result'] = "User logged in";
    }else{
        $message['result'] = "Logged Out";
    }

    echo json_encode($message);
    exit;
}

/*
*   THIS IS THE CRUCIAL LINE!
*/
// $userName = get_current_user(); // returns Alex
// //$userID = wp_get_current_user();
// //$current = wp_set_current_user( $userID->ID, $userName ); // Full User Object
// $current = wp_set_current_user( null, $userName );
// print_r($current);
//exit;
/* NOTE:
get_user_by('id', get_current_user_id());
function get_current_user_id() {
    $user = wp_get_current_user();
    return ( isset( $user->ID ) ? (int) $user->ID : 0 );
}
*/
/*
*   MAYBE PUT ELSEWHERE? DOESN'T LOG IN WITHOUT THIS!
*/