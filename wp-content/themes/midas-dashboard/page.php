<?php wp_head(); ?>

<main role="main">
	<?php

		/* Start the Loop */
		while ( have_posts() ) :
			the_post();

			the_content();

		endwhile; // End of the loop.
	?>

</main>

<?php get_footer(); ?>