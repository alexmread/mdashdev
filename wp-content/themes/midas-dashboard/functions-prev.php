<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

@ini_set( 'upload_max_size' , '1200M' );
@ini_set( 'post_max_size', '1200M');
@ini_set( 'max_execution_time', '3000' );

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here


/******************************* Include All Core AJAX and DynamicJS Files *********************************************/

include get_stylesheet_directory() . '/ajax-functions/ajax-core-functions.php';
include get_stylesheet_directory() . '/ajax-functions/ajax-user-functions.php';
include get_stylesheet_directory() . '/ajax-functions/ajax-woocommerce-functions.php';
include get_stylesheet_directory() . '/ajax-functions/ajax-vuejs-dynamic-info.php';


/******************************* Theme Functions Listed Below ***************************************/

/* LOAD CHILD SASS STYLESHEETS
================================================== */
function load_css_files() {
    wp_enqueue_style( 'mdashstyle', get_stylesheet_uri() );
    $nocachetime = time();
    wp_enqueue_style( 'muli-font', 'https://fonts.googleapis.com/css?family=Muli:400,500,600,700,800,900&display=swap', array(), '1.1', 'all');
    wp_enqueue_style( 'sss-font', 'https://fonts.googleapis.com/css2?family=Source+Serif+Pro:wght@700&display=swap', array(), '1.1', 'all');
    wp_enqueue_style( 'nucleo-icons-font', get_stylesheet_directory_uri() . '/assets/fonts/nucleo/css/nucleo.css', array(), '6.0.0.0', 'all');
 	//wp_enqueue_style( 'sass-style', get_stylesheet_directory_uri() . '/assets/sass/compiled/sass-style.css', array('vueappdev'), '1.1', 'all');
    wp_enqueue_style( 'faw-style', get_stylesheet_directory_uri() . '/assets/fonts/font-awesome/css/font-awesome.min.css', array(), '1.1', 'all');
}
add_action( 'wp_enqueue_scripts', 'load_css_files', 9999 );

function master_override_css() {
	echo '<link href="' . get_stylesheet_directory_uri() . '/assets/sass/compiled/sass-style.css?v='.filemtime( get_stylesheet_directory() . '/assets/sass/compiled/sass-style.css' ).'" rel="stylesheet"/>';
}
add_action('get_footer', 'master_override_css', 9999);

/******************************* WordPress Login Page Customization *********************************************/
function custom_wp_login_styles() {
    wp_enqueue_style( 'bootstrap-style', get_stylesheet_directory_uri() . '/vue-core/assets/sass/bootstrap/dist/css/bootstrap.css', array('login'), filemtime( get_stylesheet_directory() . '/vue-core/assets/sass/bootstrap/dist/css/bootstrap.css' ) );
    wp_enqueue_style( 'muli-font', 'https://fonts.googleapis.com/css?family=Muli:400,500,600,700,800,900&display=swap', array(), '1.1', 'all');
    wp_enqueue_style( 'sss-font', 'https://fonts.googleapis.com/css2?family=Source+Serif+Pro:wght@700&display=swap', array(), '1.1', 'all');
    wp_enqueue_style( 'sass-style', get_stylesheet_directory_uri() . '/assets/sass/compiled/sass-style.css', array('login'), filemtime( get_stylesheet_directory() . '/assets/sass/compiled/sass-style.css' ) );
}
add_action( 'login_enqueue_scripts', 'custom_wp_login_styles' );


function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Flyrise Customer Dashboard';
}
//add_filter( 'login_headertitle', 'my_login_logo_url_title' );
add_filter( 'login_headertext', 'my_login_logo_url_title' );











/*------------------------------------*\
	Theme Support
\*------------------------------------*/

add_filter ( 'woocommerce_account_menu_items', 'remove_my_account_links' );
function remove_my_account_links( $menu_links ){
    unset( $menu_links['edit-address'] ); // Addresses
    unset( $menu_links['dashboard'] ); // Remove Dashboard
    unset( $menu_links['payment-methods'] ); // Remove Payment Methods
    unset( $menu_links['orders'] ); // Remove Orders
    unset( $menu_links['downloads'] ); // Disable Downloads
    unset( $menu_links['edit-account'] ); // Remove Account details tab
    unset( $menu_links['customer-logout'] ); // Remove Logout link
    unset( $menu_links['subscriptions'] ); // Remove Logout link
 
    return $menu_links;
}

if (!isset($content_width)) {
    $content_width = 900;
}

if (function_exists('add_theme_support')) {
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// HTML5 Blank navigation
function html5blank_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}

function enqueue_my_scripts(){
    foreach( glob( get_template_directory(). '/vue-dist/_nuxt/*.js' ) as $file ) {
        // $file contains the name and extension of the file
        $filename = substr($file, strrpos($file, '/') + 1);
        wp_enqueue_script( $file, get_template_directory_uri().'/vue-dist/_nuxt/'. $filename, '', true);
    }
}
add_action('wp_enqueue_scripts', 'enqueue_my_scripts');

function enqueue_my_styles(){
    foreach( glob( get_template_directory(). '/vue-dist/_nuxt/*.css' ) as $file ) {
        $filename = substr($file, strrpos($file, '/') + 1);
        wp_enqueue_style( $file, get_template_directory_uri().'/vue-dist/_nuxt/'.$filename, true);
    }
}
add_action('wp_enqueue_scripts', 'enqueue_my_styles');

// Load HTML5 Blank scripts (header.php)










// Load HTML5 Blank conditional scripts
function html5blank_conditional_scripts()
{
    if (is_page('pagenamehere')) {
        wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
        wp_enqueue_script('scriptname'); // Enqueue it!
    }
}

// Load HTML5 Blank styles
function html5blank_styles()
{
  //  wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
 //   wp_enqueue_style('normalize'); // Enqueue it!

//    wp_register_style('html5blank', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
 //   wp_enqueue_style('html5blank'); // Enqueue it!
}

// Register HTML5 Blank Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
        'sidebar-menu' => __('Sidebar Menu', 'html5blank'), // Sidebar Navigation
        'extra-menu' => __('Extra Menu', 'html5blank') // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Widget Area 2', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('wp_enqueue_scripts', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_html5()
{
    register_taxonomy_for_object_type('category', 'html5-blank'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'html5-blank');
    register_post_type('html5-blank', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('HTML5 Blank Custom Post', 'html5blank'), // Rename these to suit
            'singular_name' => __('HTML5 Blank Custom Post', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New HTML5 Blank Custom Post', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit HTML5 Blank Custom Post', 'html5blank'),
            'new_item' => __('New HTML5 Blank Custom Post', 'html5blank'),
            'view' => __('View HTML5 Blank Custom Post', 'html5blank'),
            'view_item' => __('View HTML5 Blank Custom Post', 'html5blank'),
            'search_items' => __('Search HTML5 Blank Custom Post', 'html5blank'),
            'not_found' => __('No HTML5 Blank Custom Posts found', 'html5blank'),
            'not_found_in_trash' => __('No HTML5 Blank Custom Posts found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
}

/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    return '<h2>' . $content . '</h2>';
}

//add_action( 'show_user_profile', 'extra_user_profile_fields' );
//add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function extra_user_profile_fields( $object ) { 
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
    <h3><?php _e("Company Information", "blank"); ?></h3>

    <table class="form-table">
    <tr>
        <th><label for="companyname"><?php _e("Company Name"); ?></label></th>
        <td>
            <input type="text" name="companyname" id="companyname" value="<?php echo esc_attr( get_post_meta($object->ID, 'companyname', true ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter your Company Name."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="compaddress1"><?php _e("Address 1"); ?></label></th>
        <td>
            <input type="text" name="compaddress1" id="compaddress1" value="<?php echo esc_attr( get_post_meta($object->ID, 'compaddress1', true  ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("compaddress1"); ?></span>
        </td>
    </tr>
    <tr>
    <tr>
        <th><label for="compaddress2"><?php _e("Address 2"); ?></label></th>
        <td>
            <input type="text" name="compaddress2" id="compaddress2" value="<?php echo esc_attr( get_post_meta($object->ID, 'compaddress2', true ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("compaddress2"); ?></span>
        </td>
    </tr>
    <tr>
    <tr>
        <th><label for="compcity"><?php _e("City"); ?></label></th>
        <td>
            <input type="text" name="compcity" id="compcity" value="<?php echo esc_attr( get_post_meta($object->ID, 'compcity', true ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter your city."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="compstate"><?php _e("State"); ?></label></th>
        <td>
            <input type="text" name="compstate" id="compstate" value="<?php echo esc_attr( get_post_meta($object->ID, 'compstate', true ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter your city."); ?></span>
        </td>
    </tr>
    <tr>
    <th><label for="comppostalcode"><?php _e("Zip Code"); ?></label></th>
        <td>
            <input type="text" name="comppostalcode" id="comppostalcode" value="<?php echo esc_attr( get_post_meta($object->ID, 'comppostalcode', true ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter your Zip"); ?></span>
        </td>
    </tr>
    </table>
<?php 
}

function add_custom_meta_box()
{
    add_meta_box("company-meta-box", "Company Info", "extra_user_profile_fields", "companies", "normal", "high", null);
}

add_action("add_meta_boxes", "add_custom_meta_box");

add_action("save_post",  'save_extra_user_profile_fields', 10, 3);

function save_extra_user_profile_fields($post_id, $post, $update) {
    update_post_meta( $post_id, 'companyname', $_POST['companyname'] );
    update_post_meta( $post_id, 'compaddress1', $_POST['compaddress1'] );
    update_post_meta( $post_id, 'compaddress2', $_POST['compaddress2'] );
    update_post_meta( $post_id, 'compstate', $_POST['compstate'] );
    update_post_meta( $post_id, 'compcity', $_POST['compcity'] );
    update_post_meta( $post_id, 'comppostalcode', $_POST['comppostalcode'] );
}


add_action( 'show_user_profile', 'extra_user_profile_fields2' );
add_action( 'edit_user_profile', 'extra_user_profile_fields2' );

function extra_user_profile_fields2( $user ) { 
    $args = array( 
        'numberposts'		=> -1, // -1 is for all
        'post_type'		=> 'companies', // or 'post', 'page'
        'orderby' 		=> 'title', // or 'date', 'rand'
        'order' 		=> 'ASC', // or 'DESC'
        //'category' 		=> $category_id,
        //'exclude'		=> get_the_ID()
        // ...
        // http://codex.wordpress.org/Template_Tags/get_posts#Usage
      );
      
      // Get the posts
      $myposts = get_posts($args);
      $current_company = get_the_author_meta( 'assignedcompany', $user->ID );
    ?>
    <h3><?php _e("Assigned Company", "blank"); ?></h3>

    <table class="form-table">
    <tr>
        <th><label for="assignedcompany"><?php _e("Assigned Company"); ?></label></th>
        <td>
            <select name="assignedcompany" id="assignedcompany" class="regular-text">
            <?php foreach ($myposts as $mypost) { 
                    if ($mypost->ID == $current_company) { ?>  
                <option value="<?php echo $mypost->ID;?>" selected><?php echo $mypost->post_title; ?> </option>
                <?php }  else { ?>
                <option value="<?php echo $mypost->ID;?>"><?php echo $mypost->post_title; ?> </option>
                <?php } } ?>
            </select>
            <span class="description"><?php _e("Please select your Company."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="comprole"><?php _e("Company Role"); ?></label></th>
        <td>
            <select name="comprole" id="comprole" class="regular-text">
            <option value="viewonly"> View Only </option>
            <option value="billing"> Billing </option>
            <option value="admin"> Admin </option>
            </select>
            <span class="description"><?php _e("Please select your Company Role."); ?></span>
        </td>
    </tr>
    </table>
<?php
}

add_action( 'personal_options_update', 'save_extra_user_profile_fields2' );
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields2' );

function save_extra_user_profile_fields2( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) ) { 
        return false; 
    }
    update_user_meta( $user_id, 'assignedcompany', $_POST['assignedcompany'] );
    update_user_meta( $user_id, 'comprole', $_POST['comprole'] );
}

add_filter('woocommerce_checkout_fields', 'addBootstrapToCheckoutFields' );
function addBootstrapToCheckoutFields($fields) {
    foreach ($fields as &$fieldset) {
        foreach ($fieldset as &$field) {
            // if you want to add the form-group class around the label and the input
            $field['class'][] = 'form-group'; 

            // add form-control to the actual input
            $field['input_class'][] = 'form-control';
        }
    }
    return $fields;
}

add_action('check_admin_referer', 'logout_without_confirm', 10, 2);
function logout_without_confirm($action, $result)
{
    /**
     * Allow logout without confirmation
     */
    if ($action == "log-out" && !isset($_GET['_wpnonce'])) {
        $redirect_to = isset($_REQUEST['redirect_to']) ? $_REQUEST['redirect_to'] : 'login';
        $location = str_replace('&amp;', '&', wp_logout_url($redirect_to));
        header("Location: $location");
        die;
    }
}




/* Add 'Order Cost Report' to Woocommerce Reports -> Orders tab */
    require_once( get_template_directory() . '/woocommerce/custom-reports.php');


/* Alter Woocommerce Subscription Periods Filter */
    
    function alter_subscription_filters( $title ) {
        $subscription_periods = array("month"=>"month", "day"=>"day", "week"=>"week", "year"=>"year");
        return $subscription_periods;
    }
    add_filter( 'woocommerce_subscription_periods', 'alter_subscription_filters' );



/* Custom Expense Taxonomies should be radio rather than check boxes */

/**
 * Custom Expense Taxonomies radio inputs instead of checkboxes for specified taxonomies.
 *
 * @param   array   $args
 * @return  array
 */
function wpse_139269_term_radio_checklist( $args ) {
    if ( ! empty( $args['taxonomy'] ) && ($args['taxonomy'] === 'tier_1' || $args['taxonomy'] === 'sub_category' || $args['taxonomy'] === 'repeats') /* <== Change to required taxonomy */ ) {
        if ( empty( $args['walker'] ) || is_a( $args['walker'], 'Walker' ) ) { // Don't override 3rd party walkers.
            if ( ! class_exists( 'WPSE_139269_Walker_Category_Radio_Checklist' ) ) {
                /**
                 * Custom walker for switching checkbox inputs to radio.
                 *
                 * @see Walker_Category_Checklist
                 */
                class WPSE_139269_Walker_Category_Radio_Checklist extends Walker_Category_Checklist {
                    function walk( $elements, $max_depth, ...$args ) {
                        $output = parent::walk( $elements, $max_depth, ...$args );
                        $output = str_replace(
                            array( 'type="checkbox"', "type='checkbox'" ),
                            array( 'type="radio"', "type='radio'" ),
                            $output
                        );

                        return $output;
                    }
                }
            }

            $args['walker'] = new WPSE_139269_Walker_Category_Radio_Checklist;
        }
    }

    return $args;
}

add_filter( 'wp_terms_checklist_args', 'wpse_139269_term_radio_checklist' );
?>