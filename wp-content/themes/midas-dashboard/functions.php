<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

@ini_set( 'upload_max_size' , '1200M' );
@ini_set( 'post_max_size', '1200M');
@ini_set( 'max_execution_time', '3000' );

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here


/******************************* Include All Core AJAX and DynamicJS Files *********************************************/

include get_stylesheet_directory() . '/ajax-functions/ajax-core-functions.php';
include get_stylesheet_directory() . '/ajax-functions/ajax-user-functions.php';
include get_stylesheet_directory() . '/ajax-functions/ajax-woocommerce-functions.php';
include get_stylesheet_directory() . '/ajax-functions/ajax-vuejs-dynamic-info.php';


/* !! New Headless versions */
include get_stylesheet_directory() . '/ajax-functions/ajax-headless-functions.php';


/******************************* Theme Functions Listed Below ***************************************/

/* LOAD CHILD SASS STYLESHEETS
================================================== */
function load_css_files() {
    wp_enqueue_style( 'mdashstyle', get_stylesheet_uri() );
    $nocachetime = time();
    wp_enqueue_style( 'muli-font', 'https://fonts.googleapis.com/css?family=Muli:400,500,600,700,800,900&display=swap', array(), '1.1', 'all');
    wp_enqueue_style( 'sss-font', 'https://fonts.googleapis.com/css2?family=Source+Serif+Pro:wght@700&display=swap', array(), '1.1', 'all');
    // wp_enqueue_style( 'nucleo-icons-font', get_stylesheet_directory_uri() . '/assets/fonts/nucleo/css/nucleo.css', array(), '6.0.0.0', 'all');
 	// wp_enqueue_style( 'sass-style', get_stylesheet_directory_uri() . '/assets/sass/compiled/sass-style.css', array('vueappdev'), '1.1', 'all');
    // wp_enqueue_style( 'faw-style', get_stylesheet_directory_uri() . '/assets/fonts/font-awesome/css/font-awesome.min.css', array(), '1.1', 'all');
}
add_action( 'wp_enqueue_scripts', 'load_css_files', 9999 );





function master_override_css() {
	// echo '<link rel="preconnect" href="https://fonts.googleapis.com">';
 //    echo '<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>';
 //    echo '<link href="https://fonts.googleapis.com/css2?family=Mulish:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Source+Serif+Pro:ital,wght@0,200;0,300;0,400;0,600;0,700;0,900;1,200;1,300;1,400;1,600;1,700;1,900&display=swap" rel="stylesheet">';
    
    echo '<link href="' . get_stylesheet_directory_uri() . '/assets/fonts/font-awesome/css/font-awesome.min.css?v='.filemtime( get_stylesheet_directory() . '/assets/fonts/font-awesome/css/font-awesome.min.css' ).'" rel="stylesheet"/>';
    echo '<link href="' . get_stylesheet_directory_uri() . '/assets/fonts/nucleo/css/nucleo.css?v='.filemtime( get_stylesheet_directory() . '/assets/fonts/nucleo/css/nucleo.css' ).'" rel="stylesheet"/>';
    echo '<link href="' . get_stylesheet_directory_uri() . '/assets/sass/compiled/sass-style.css?v='.filemtime( get_stylesheet_directory() . '/assets/sass/compiled/sass-style.css' ).'" rel="stylesheet"/>';
}
add_action('get_footer', 'master_override_css', 9999);



function ridgecapmarketing_enqueue_stylesheets(){
    global $current_user;
    //$user = get_currentuserinfo();
    $user = wp_get_current_user();
    $brand_switch = get_user_meta($user->ID, 'brand', true);
    //echo "brand switch: " . $brand_switch . "<br />";
    
    if( $brand_switch == 'ridgecapmarketing' ){
        wp_enqueue_style( 'ridgecapmarketing-style', get_stylesheet_directory_uri() . '/assets/sass/ridgecap/compiled/sass-style.css', array(), '2.0' );
    }else{
        //echo "this firing?";exit;
        wp_enqueue_style( 'sass-style', get_stylesheet_directory_uri() . '/assets/sass/compiled/sass-style.css', array('vueappdev'), '1.1', 'all');
        echo '<link href="' . get_stylesheet_directory_uri() . '/assets/sass/compiled/sass-style.css?v='.filemtime( get_stylesheet_directory() . '/assets/sass/compiled/sass-style.css' ).'" rel="stylesheet"/>';
    }
}
add_action( 'wp_enqueue_scripts', 'ridgecapmarketing_enqueue_stylesheets', 9999 );


function ridgecapmarketing_enqueue_stylesheets_login() {
    //echo "brand parameter: " . $_GET['brand']; exit;
    if( isset($_GET['brand']) && $_GET['brand'] == 'ridgecapmarketing' ){
        //echo "brand parameter: " . $_GET['brand']; exit;
        wp_enqueue_style( 'ridgecapmarketing-style', get_stylesheet_directory_uri() . '/assets/sass/ridgecap/compiled/sass-style.css', array(), '2.0' );
        ridgecap_login_logo();
    }else{
        //echo "brand parameter flyrise"; exit;
        wp_enqueue_style( 'sass-style', get_stylesheet_directory_uri() . '/assets/sass/compiled/sass-style.css', array('vueappdev'), '1.1', 'all');
        echo '<link href="' . get_stylesheet_directory_uri() . '/assets/sass/compiled/sass-style.css?v='.filemtime( get_stylesheet_directory() . '/assets/sass/compiled/sass-style.css' ).'" rel="stylesheet"/>';
    }
}
add_action( 'login_enqueue_scripts', 'ridgecapmarketing_enqueue_stylesheets_login', 9999 );


function ridgecap_login_logo() {
    ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(https://my.ridgecapmarketing.com/ridgecap-by-flyrise-logo-w40.png) !important;
        }
    </style>
    <?php
}



add_filter('password_change_email', 'replace_admin_email_in_notification_emails', 10, 3);
add_filter('email_change_email', 'replace_admin_email_in_notification_emails', 10, 3);

function replace_admin_email_in_notification_emails( $pass_change_email, $user, $user_data ) {

    //$brand_switch = get_user_meta($user_data->ID, 'brand', true);
    $brand_switch = get_user_meta($user_data['ID'], 'brand', true);
    
    if( $brand_switch == 'ridgecapmarketing' ){
        $pass_change_email['message'] = str_replace( '###ADMIN_EMAIL###', 'info@ridgecapmarketing.com', $pass_change_email['message'] );
    }else{
        if( $brand_switch == 'flyrise' ){
            $pass_change_email['message'] = str_replace( '###ADMIN_EMAIL###', 'info@flyrise.io', $pass_change_email['message'] );
        }else{
            $pass_change_email['message'] = str_replace( '###ADMIN_EMAIL###', 'info@flyrise.io', $pass_change_email['message'] );
        }
    }
    

  return $pass_change_email;
}

/*
*   Alter the Reset your password email message if it's ridgecapmarketing
*/
function my_retrieve_password_message( $message, $key, $user_login, $user_data ) {

    $brand_switch = get_user_meta($user_data->ID, 'brand', true);

    if( $brand_switch == 'ridgecapmarketing' ){
        $site_name = wp_specialchars_decode( 'Ridgecapmarketing', ENT_QUOTES );
        $message = __( 'Someone has requested a password reset for the following account: ' );
        $message .= $site_name . "\r\n\r\n";
        $message .= sprintf( __( 'Username: %s' ), $user_login ) . "\r\n\r\n";
        $message .= __( 'If this was a mistake, just ignore this email and nothing will happen.' ) . "\r\n\r\n";
        $message .= __( 'To reset your password, visit the following address:' ) . "\r\n\r\n";
        $message .= '<' . network_site_url( "wp-login.php?action=rp&brand=ridgecapmarketing&key=$key&login=" . rawurlencode( $user_login ), 'login' ) . ">\r\n";
        // $message .= '<http://yoursite.com/wp-login.php?action=rp&key=' . $key . '&login=' . rawurlencode( $user_login ) . ">\r\n";
    }else{
        if( $brand_switch == 'flyrise' ){
            $site_name = wp_specialchars_decode( 'Flyrise', ENT_QUOTES );
            $message = __( 'Someone has requested a password reset for the following account: ' );
            $message .= $site_name . "\r\n\r\n";
            $message .= sprintf( __( 'Username: %s' ), $user_login ) . "\r\n\r\n";
            $message .= __( 'If this was a mistake, just ignore this email and nothing will happen.' ) . "\r\n\r\n";
            $message .= __( 'To reset your password, visit the following address:' ) . "\r\n\r\n";
            $message .= '<' . network_site_url( "wp-login.php?action=rp&brand=flyrise&key=$key&login=" . rawurlencode( $user_login ), 'login' ) . ">\r\n";
        }else{
            $site_name = wp_specialchars_decode( 'Launch', ENT_QUOTES );
            $message = __( 'Someone has requested a password reset for the following account: ' );
            $message .= $site_name . "\r\n\r\n";
            $message .= sprintf( __( 'Username: %s' ), $user_login ) . "\r\n\r\n";
            $message .= __( 'If this was a mistake, just ignore this email and nothing will happen.' ) . "\r\n\r\n";
            $message .= __( 'To reset your password, visit the following address:' ) . "\r\n\r\n";
            $message .= '<' . network_site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' ) . ">\r\n";
        }
    }
    return $message;
}
add_filter( 'retrieve_password_message', 'my_retrieve_password_message', 10, 4 );

/*
*   Alter the Reset your password email title if it's ridgecapmarketing
*/
function my_retrieve_password_title( $title, $user_login, $user_data ){
    $brand_switch = get_user_meta($user_data->ID, 'brand', true);

    if( $brand_switch == 'ridgecapmarketing' ){
        $title = __( 'Password reset for Ridgecapmarketing' );
    }else{
        if($brand_switch == 'flyrise' ){
          $title = __( 'Password reset for Flyrise' );  
        }else{
            $title = __( 'Password reset for Launch' );  
        }
    }
    return $title;
}
add_filter( 'retrieve_password_title', 'my_retrieve_password_title', 10, 4 );


/*
*   Alter the Password Changed Confirmation email if it's ridgecapmarketing
*/
function my_change_password_mail_message( $pass_change_mail, $user, $user_data ) {    
    
    //$brand_switch = get_user_meta($user_data->ID, 'brand', true);
    $brand_switch = get_user_meta($user_data['ID'], 'brand', true);

    if( $brand_switch == 'ridgecapmarketing' ){

        $subject = 'Ridgecapmarketing Customer Dashboard Password Changed';

        $site_name = wp_specialchars_decode( 'Ridgecapmarketing', ENT_QUOTES );
        $message = __( 'This notice confirms that your password was changed on: ' );
        $message .= $site_name . "\r\n\r\n";
        //$message .= sprintf( __( 'Username: %s' ), $user ) . "\r\n\r\n";
        $message .= __( 'If you did not change your password, please contact the Site Administrator at info@ridgecapmarketing.com' ) . "\r\n\r\n";
        $message .= __( 'Regards,' ) . "\r\n\r\n";
        $message .= __( 'All at Ridgecapmarketing Customer Dashboard' ) . "\r\n\r\n";
    }else{
        if( $brand_switch == 'flyrise' ){

            $subject = "Flyrise Customer Dashboard Password Changed";

            $site_name = wp_specialchars_decode( 'Flyrise', ENT_QUOTES );
            $message = __( 'This notice confirms that your password was changed on: ' );
            $message .= $site_name . "\r\n\r\n";
            //$message .= sprintf( __( 'Username: %s' ), $user ) . "\r\n\r\n";
            $message .= __( 'If you did not change your password, please contact the Site Administrator at info@flyrise.io' ) . "\r\n\r\n";
            $message .= __( 'Regards,' ) . "\r\n\r\n";
            $message .= __( 'All at Flyrise Customer Dashboard' ) . "\r\n\r\n";
        }else{
            $subject = "Flyrise Launch Dashboard Password Changed";

            $site_name = wp_specialchars_decode( 'Launch', ENT_QUOTES );
            $message = __( 'This notice confirms that your password was changed on: ');
            $message .= $site_name . "\r\n\r\n";
            //$message .= sprintf( __( 'Username: %s' ), $user ) . "\r\n\r\n";
            $message .= __( 'If you did not change your password, please contact the Site Administrator at info@flyrise.io' ) . "\r\n\r\n";
            $message .= __( 'Regards,' ) . "\r\n\r\n";
            $message .= __( 'All at Flyrise Launch Dashboard' ) . "\r\n\r\n";
        }
    }

    $pass_change_mail[ 'message' ] = $message;
    $pass_change_mail[ 'subject' ] = $subject;

    return $pass_change_mail;
}
add_filter( 'password_change_email', 'my_change_password_mail_message', 10, 3 );


function mytheme_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

/*
*   Change the Woocommerce Sender/From Name per the Brand
*/
add_filter( 'woocommerce_email_from_name', function( $from_name, $wc_email ){

    $from_name = 'Team Flyrise';

    //echo "recipient: " . $wc_email->recipient . "<br />";
    $recipient = $wc_email->get_recipient();
    //echo "method 2: " . $recipient . "<br />";
    
    // echo "<pre>";
    // print_r($wc_email);
    // echo "</pre>";

    $user = get_user_by( 'email', $recipient );

    // echo "user: <br />";
    // echo "<pre>";
    // print_r($user);
    // echo "</pre>";

    $brand = get_user_meta( $user->id, 'brand', true ); 
    //echo $brand . "<br />";

    switch( $brand ) {
        case 'flyrise':
            //echo "brand flyrise<br />";
            $from_name = 'Team Flyrise';
        break;
        case 'ridgecapmarketing':
            //echo "brand ridgecapmarketing<br />";
            $from_name = 'Team Ridgecapmarketing';
        break;
    }
    
    //if( $wc_email->id == 'customer_processing_order' ) 
    //$from_name = 'Team Flyrise';

    return $from_name;
}, 10, 2 );

add_filter( 'woocommerce_email_from_address', function( $from_email, $wc_email ){
    //if( $wc_email->id == 'customer_processing_order' )
    $from_email = 'info@flyrise.io';

    $recipient = $wc_email->get_recipient();
    $user = get_user_by( 'email', $recipient );
    $brand = get_user_meta( $user->id, 'brand', true ); 
    switch( $brand ) {
        case 'flyrise':
            //echo "brand flyrise<br />";
            $from_email = 'info@flyrise.io';
        break;
        case 'ridgecapmarketing':
            //echo "brand ridgecapmarketing<br />";
            $from_email = 'info@ridgecapmarketing.com';
        break;
    }

    return $from_email;
}, 10, 2 );

//add_filter( 'woocommerce_email_headers', 'bbloomer_order_completed_email_add_cc_bcc', 9999, 3 );
 
// function bbloomer_order_completed_email_add_cc_bcc( $headers, $email_id, $order ) {
//     if ( 'customer_completed_order' == $email_id ) {
//         $headers .= "Cc: Name <afrascona@mmail.com>\r\n"; // delete if not needed
//         $headers .= "Bcc: Name <afrascona@gmail.com>\r\n"; // delete if not needed
//     }
//     return $headers;
// }



/*
    if ridgecap, after successful payment redirect to ridgecapmarketing dash!
*/  
function ridgecapmarketing_successful_checkout_redirect_custom( $order_id ){
    
    $order = wc_get_order($order_id);

    $user_id = $order->get_user_id();
    //echo "user id: " . $user_id . "<br />";

    $brand_switch = get_user_meta($user_id, 'brand', true);
    //echo "brand switch: " . $brand_switch . "<br />";

    
    /***
    *   add switch here for if it's local! 
    */


    if( $brand_switch == 'ridgecapmarketing' ){
        $url = 'https://my.ridgecapmarketing.com/pages/settings/orders';
    }else{
        $url = 'https://my.flyrise.io/pages/settings/orders';
    }
    //echo "redirect url: " . $url . "<br />";
    //exit;
    
    if ( ! $order->has_status( 'failed' ) ) {
        wp_redirect( $url );
        exit;
    }
}
add_action( 'woocommerce_thankyou', 'ridgecapmarketing_successful_checkout_redirect_custom');


/******************************* WordPress Login Page Customization *********************************************/
function custom_wp_login_styles() {
    wp_enqueue_style( 'bootstrap-style', get_stylesheet_directory_uri() . '/vue-core/assets/sass/bootstrap/dist/css/bootstrap.css', array('login'), get_stylesheet_directory() . '/vue-core/assets/sass/bootstrap/dist/css/bootstrap.css' );
    wp_enqueue_style( 'muli-font', 'https://fonts.googleapis.com/css?family=Muli:400,500,600,700,800,900&display=swap', array(), '1.1', 'all');
    wp_enqueue_style( 'sss-font', 'https://fonts.googleapis.com/css2?family=Source+Serif+Pro:wght@700&display=swap', array(), '1.1', 'all');
    wp_enqueue_style( 'sass-style', get_stylesheet_directory_uri() . '/assets/sass/compiled/sass-style.css', array('login'), filemtime( get_stylesheet_directory() . '/assets/sass/compiled/sass-style.css' ) );
}
add_action( 'login_enqueue_scripts', 'custom_wp_login_styles' );


function my_login_logo_url() {
    return home_url();
}
//add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Flyrise Customer Dashboard';
}
//add_filter( 'login_headertitle', 'my_login_logo_url_title' );
add_filter( 'login_headertext', 'my_login_logo_url_title' );











/*------------------------------------*\
	Theme Support
\*------------------------------------*/

add_filter ( 'woocommerce_account_menu_items', 'remove_my_account_links' );
function remove_my_account_links( $menu_links ){
    unset( $menu_links['edit-address'] ); // Addresses
    unset( $menu_links['dashboard'] ); // Remove Dashboard
    unset( $menu_links['payment-methods'] ); // Remove Payment Methods
    unset( $menu_links['orders'] ); // Remove Orders
    unset( $menu_links['downloads'] ); // Disable Downloads
    unset( $menu_links['edit-account'] ); // Remove Account details tab
    unset( $menu_links['customer-logout'] ); // Remove Logout link
    unset( $menu_links['subscriptions'] ); // Remove Logout link
 
    return $menu_links;
}

if (!isset($content_width)) {
    $content_width = 900;
}

if (function_exists('add_theme_support')) {
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// HTML5 Blank navigation
function html5blank_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}

function enqueue_my_scripts(){
    foreach( glob( get_template_directory(). '/vue-dist/_nuxt/*.js' ) as $file ) {
        // $file contains the name and extension of the file
        $filename = substr($file, strrpos($file, '/') + 1);
        wp_enqueue_script( $file, get_template_directory_uri().'/vue-dist/_nuxt/'. $filename, '', true);
    }
}
add_action('wp_enqueue_scripts', 'enqueue_my_scripts');

function enqueue_my_styles(){
    foreach( glob( get_template_directory(). '/vue-dist/_nuxt/css/*.css' ) as $file ) {
        $filename = substr($file, strrpos($file, '/') + 1);
        //echo get_template_directory_uri().'/vue-dist/_nuxt/css/'.$filename . "<br />";
        //result: mdashdev/wp-content/themes/midas-dashboard/vue-dist/_nuxt/css/07ab945.css
        wp_enqueue_style( $file, get_template_directory_uri().'/vue-dist/_nuxt/css/'.$filename, true);
    }
}
add_action('wp_enqueue_scripts', 'enqueue_my_styles');

// Load HTML5 Blank scripts (header.php)










// Load HTML5 Blank conditional scripts
function html5blank_conditional_scripts()
{
    if (is_page('pagenamehere')) {
        wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
        wp_enqueue_script('scriptname'); // Enqueue it!
    }
}

// Load HTML5 Blank styles
function html5blank_styles()
{
   wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
   wp_enqueue_style('normalize'); // Enqueue it!

   wp_register_style('html5blank', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
   wp_enqueue_style('html5blank'); // Enqueue it!
}

// Register HTML5 Blank Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
        'sidebar-menu' => __('Sidebar Menu', 'html5blank'), // Sidebar Navigation
        'extra-menu' => __('Extra Menu', 'html5blank') // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Widget Area 2', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('wp_enqueue_scripts', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_html5()
{
    register_taxonomy_for_object_type('category', 'html5-blank'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'html5-blank');
    register_post_type('html5-blank', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('HTML5 Blank Custom Post', 'html5blank'), // Rename these to suit
            'singular_name' => __('HTML5 Blank Custom Post', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New HTML5 Blank Custom Post', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit HTML5 Blank Custom Post', 'html5blank'),
            'new_item' => __('New HTML5 Blank Custom Post', 'html5blank'),
            'view' => __('View HTML5 Blank Custom Post', 'html5blank'),
            'view_item' => __('View HTML5 Blank Custom Post', 'html5blank'),
            'search_items' => __('Search HTML5 Blank Custom Post', 'html5blank'),
            'not_found' => __('No HTML5 Blank Custom Posts found', 'html5blank'),
            'not_found_in_trash' => __('No HTML5 Blank Custom Posts found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
}

/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    return '<h2>' . $content . '</h2>';
}



/*
add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );
function extra_user_profile_fields( $object ) { 
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
    <h3><?php _e("Company Information", "blank"); ?></h3>

    <table class="form-table">
    <tr>
        <th><label for="companyname"><?php _e("Company Name"); ?></label></th>
        <td>
            <input type="text" name="companyname" id="companyname" value="<?php echo esc_attr( get_post_meta($object->ID, 'companyname', true ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter your Company Name."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="compaddress1"><?php _e("Address 1"); ?></label></th>
        <td>
            <input type="text" name="compaddress1" id="compaddress1" value="<?php echo esc_attr( get_post_meta($object->ID, 'compaddress1', true  ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("compaddress1"); ?></span>
        </td>
    </tr>
    <tr>
    <tr>
        <th><label for="compaddress2"><?php _e("Address 2"); ?></label></th>
        <td>
            <input type="text" name="compaddress2" id="compaddress2" value="<?php echo esc_attr( get_post_meta($object->ID, 'compaddress2', true ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("compaddress2"); ?></span>
        </td>
    </tr>
    <tr>
    <tr>
        <th><label for="compcity"><?php _e("City"); ?></label></th>
        <td>
            <input type="text" name="compcity" id="compcity" value="<?php echo esc_attr( get_post_meta($object->ID, 'compcity', true ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter your city."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="compstate"><?php _e("State"); ?></label></th>
        <td>
            <input type="text" name="compstate" id="compstate" value="<?php echo esc_attr( get_post_meta($object->ID, 'compstate', true ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter your city."); ?></span>
        </td>
    </tr>
    <tr>
    <th><label for="comppostalcode"><?php _e("Zip Code"); ?></label></th>
        <td>
            <input type="text" name="comppostalcode" id="comppostalcode" value="<?php echo esc_attr( get_post_meta($object->ID, 'comppostalcode', true ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter your Zip"); ?></span>
        </td>
    </tr>
    </table>
<?php 
}
*/


// add_action("add_meta_boxes", "add_custom_meta_box");
// function add_custom_meta_box()
// {
//     add_meta_box("company-meta-box", "Company Info", "extra_user_profile_fields", "companies", "normal", "high", null);
// }



//add_action("save_post",  'save_extra_user_profile_fields', 10, 3);
//function save_extra_user_profile_fields($post_id, $post, $update) {
    //update_post_meta( $post_id, 'companyname', $_POST['companyname'] );
    //update_post_meta( $post_id, 'compaddress1', $_POST['compaddress1'] );
    //update_post_meta( $post_id, 'compaddress2', $_POST['compaddress2'] );
    //update_post_meta( $post_id, 'compstate', $_POST['compstate'] );
    //update_post_meta( $post_id, 'compcity', $_POST['compcity'] );
    //update_post_meta( $post_id, 'comppostalcode', $_POST['comppostalcode'] );
//}

// add_action("save_post",  'save_extra_user_profile_fields', 10, 3);
// function save_extra_user_profile_fields($post_id, $post, $update) {
//     error_log('wrote extra fields');
//     //update_post_meta( $post_id, 'qwilr_proposal_link', $_POST['acf-field_624c79d7add85'] );
//     //update_post_meta( $post_id, 'everhour_client_id', $_POST['acf-field_624c792ad1b98'] );
//     //update_post_meta( $post_id, 'brand', $_POST['brand'] );
// }




/*
*   Add Fields to User Post
*   Everhour Client ID
*   Qwilr Proposal Link
*   Journey (Onboarding Steps)
*   Custom User Image
*   Brand
*/
add_action( 'show_user_profile', 'crf_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'crf_show_extra_profile_fields' );

function crf_show_extra_profile_fields( $user ) {
	$qpl = get_user_meta( $user->ID, 'qwilr_proposal_link', true );
	$eci = get_user_meta( $user->ID, 'everhour_client_id', true );
    $csi = get_user_meta( $user->ID, 'contentsnare_client_id', true);
	$journey = get_user_meta( $user->ID, 'journey', true );
    $brand = get_user_meta( $user->ID, 'brand', true );
    $jwt_token = get_user_meta( $user->ID, 'jwt_token', true );
    //$companyname = get_user_meta( $user->ID, 'companyname', true);
    //$shipping_email = get_user_meta( $user->ID, 'shipping_email', true);
    $profileimg_current = get_user_meta( $user->ID, '_flyrise_user_profile', true);

	?>

    <h3><?php esc_html_e( 'Profile Image', 'profileimg' ); ?></h3>

    <table class="form-table">
        <tr>
            <th><label for="profileimg_current" style="vertical-align: top;">
                <img width="50px"
                   id="profileimg_current"
                   name="profileimg_current"
                   src="<?php echo esc_attr( $profileimg_current ); ?>"
                />
            </label></th>
            <td>
                <input type="file"
                    id="profileimg"
                    ref="file"
                    name="profileimg"
                    onchange="document.getElementById('profileimg_current').src = window.URL.createObjectURL(this.files[0])"
                />
            </td>
        </tr>
    </table>

	<h3><?php esc_html_e( 'Qwilr', 'crf' ); ?></h3>

	<table class="form-table">
		<tr>
			<th><label for="qwilr_proposal_link"><?php esc_html_e( 'Qwilr Proposal Link', 'crf' ); ?></label></th>
			<td>
				<input type="text"
			       id="qwilr_proposal_link"
			       name="qwilr_proposal_link"
			       value="<?php echo esc_attr( $qpl ); ?>"
			       class="regular-text"
				/>
			</td>
		</tr>
	</table>

    <h3><?php esc_html_e( 'Everhour Client ID', 'crf' ); ?></h3>
	<table class="form-table">
		<tr>
			<th><label for="everhour_client_id"><?php esc_html_e( 'Everhour Client ID', 'crf' ); ?></label></th>
			<td>
				<input type="text"
			       id="everhour_client_id"
			       name="everhour_client_id"
			       value="<?php echo esc_attr( $eci ); ?>"
			       class="regular-text"
				/>
			</td>
		</tr>
	</table>

    <h3><?php esc_html_e( 'ContentSnare Client ID', 'csi' ); ?></h3>
    <table class="form-table">
        <tr>
            <th><label for="contentsnare_client_id"><?php esc_html_e( 'ContentSnare Client ID', 'csi' ); ?></label></th>
            <td>
                <input type="text"
                   id="contentsnare_client_id"
                   name="contentsnare_client_id"
                   value="<?php echo esc_attr( $csi ); ?>"
                   class="regular-text"
                />
            </td>
        </tr>
    </table>

    <h3><?php esc_html_e( 'Journey', 'journey' ); ?></h3>
	<table class="form-table">
		<tr>
			<th><label for="journey"><?php esc_html_e( 'Journey', 'journey' ); ?></label></th>
			<td>
				<input type="text"
			       id="journey"
			       name="journey"
			       value="<?php echo esc_attr( $journey ); ?>"
			       class="regular-text"
				/>
			</td>
		</tr>
	</table>

    <h3><?php esc_html_e( 'Brand', 'brand' ); ?></h3>
    <table class="form-table">
        <tr>
            <th><label for="brand"><?php esc_html_e( 'Brand', 'brand' ); ?></label></th>
            <td>
                <input type="text"
                   id="brand"
                   name="brand"
                   value="<?php echo esc_attr( $brand ); ?>"
                   class="regular-text"
                />
            </td>
        </tr>
    </table>

    <!-- <h3><?php esc_html_e( 'Shipping Email', 'shipping_email' ); ?></h3>
    <table class="form-table">
        <tr>
            <th><label for="shipping_email"><?php esc_html_e( 'Shipping Email', 'shipping_email' ); ?></label></th>
            <td>
                <input type="text"
                   id="shipping_email"
                   name="shipping_email"
                   value="<?php echo esc_attr( $shipping_email ); ?>"
                   class="regular-text"
                />
            </td>
        </tr>
    </table> -->

    <!-- <h3></php esc_html_e( 'Company', 'companyname' ); ?></h3>
    <table class="form-table">
		<tr>
			<th><label for="companyname"><?php //esc_html_e( 'Company', 'companyname' ); ?></label></th>
			<td>
				<input type="text"
			       id="company"
			       name="company"
			       value="<?php //echo esc_attr( $companyname ); ?>"
			       class="regular-text"
				/>
			</td>
		</tr>
	</table> -->
    <!--
        Needed for the user-edit.php form to capture the image uploaded
    -->
    <script>
        jQuery(function() {
            jQuery('#your-profile').attr("enctype","multipart/form-data");
            console.log( "changed form encryption to multipart" );
        });
    </script>

	<?php
}


add_filter( 'woocommerce_customer_meta_fields', 'filter_add_customer_meta_fields', 10, 1 );
function filter_add_customer_meta_fields( $args ) {
    $args['shipping']['fields']['shipping_email'] = array(
        'label'       => __( 'Shipping Email', 'woocommerce' ),
        'description' => '',
    );
    return $args;
}


// add_action( 'user_profile_update_errors', 'crf_user_profile_update_errors', 10, 3 );
// function crf_user_profile_update_errors( $errors, $update, $user ) {
// 	if ( ! $update ) {
// 		return;
// 	}

// 	if ( empty( $_POST['year_of_birth'] ) ) {
// 		$errors->add( 'year_of_birth_error', __( '<strong>ERROR</strong>: Please enter your year of birth.', 'crf' ) );
// 	}

// 	if ( ! empty( $_POST['year_of_birth'] ) && intval( $_POST['year_of_birth'] ) < 1900 ) {
// 		$errors->add( 'year_of_birth_error', __( '<strong>ERROR</strong>: You must be born after 1900.', 'crf' ) );
// 	}
// }


add_action( 'personal_options_update', 'crf_update_profile_fields' );
add_action( 'edit_user_profile_update', 'crf_update_profile_fields' );

function crf_update_profile_fields( $user_id ) {
	// if ( ! current_user_can( 'edit_user', $user_id ) ) {
	// 	return false;
	// }

	if ( ! empty( $_POST['qwilr_proposal_link'] ) ) {
		update_user_meta( $user_id, 'qwilr_proposal_link', $_POST['qwilr_proposal_link'] );
    }
    if ( ! empty( $_POST['everhour_client_id'] ) ) {
		update_user_meta( $user_id, 'everhour_client_id', $_POST['everhour_client_id'] );
    }
    if ( ! empty( $_POST['contentsnare_client_id'] ) ) {
        update_user_meta( $user_id, 'contentsnare_client_id', $_POST['contentsnare_client_id'] );
    }
    //if ( ! empty( $_POST['journey'] ) ) {
		update_user_meta( $user_id, 'journey', $_POST['journey'] );
    //}
    if ( ! empty( $_POST['companyname'] ) ) {
        update_user_meta( $user_id, 'companyname', $_POST['companyname'] );
    }

    if ( ! empty( $_POST['brand'] ) ) {
        update_user_meta( $user_id, 'brand', $_POST['brand'] );
    }

    if ( ! empty( $_POST['shipping_email'] ) ) {
        update_user_meta( $user_id, 'shipping_email', $_POST['shipping_email'] );
    }

    $uid = $user_id;
        // echo "<pre>";
        // print_r($_FILES);
        // print_r($_POST);
        // echo "</pre>";
        // exit;
        // $userID = $new_client->ID;
    if(isset($_FILES['profileimg']) && $_FILES['profileimg']['size'] != 0){
      
        $posted_data =  isset( $_POST ) ? $_POST : array();
        $file_data = isset( $_FILES ) ? $_FILES : array();
        $upload = $_FILES['profileimg'];

        $data = array_merge( $posted_data, $file_data );

        $uploads = wp_upload_dir(); /*Get path of upload dir of wordpress*/
        if (is_writable($uploads['path']))  /*Check if upload dir is writable*/
        {
            if ((!empty($upload['tmp_name'])))  /*Check if uploaded image is not empty*/
            {
                if ($upload['tmp_name'])   /*Check if image has been uploaded in temp directory*/
                {
                    //require ABSPATH . 'wp-admin/includes/image.php';
                    //require ABSPATH . 'wp-admin/includes/file.php';
                    //require ABSPATH . 'wp-admin/includes/media.php';
                    
                    $overrides = array('test_form' => false);
                    $attachment_id = wp_handle_upload($upload, $overrides);

                    $fullsize_path = get_attached_file( $attachment_id );
                    $url = $attachment_id['url'];
                    update_user_meta($uid, '_flyrise_user_profile', $url);           
                    update_user_meta($uid, 'profileimg', $url);               
                }
            }
        }
    }else{
        // if (isset($_POST['brand'])){
        //     switch($_POST['brand']){
        //         case 'flyrise':
        //             $url = 'https://secure.gravatar.com/avatar/40b0a56c4c4f4c34fa808ebc62909043?s=96&d=wp_user_avatar&r=g';
        //             update_user_meta($uid, '_flyrise_user_profile', $url);
        //             update_user_meta($uid, 'profileimg', $url);   
        //         break;
        //         case 'ridgecapmarketing':
        //             $url = 'https://secure.gravatar.com/avatar/40b0a56c4c4f4c34fa808ebc62909043?s=96&d=wp_user_avatar&r=g';
        //             update_user_meta($uid, '_flyrise_user_profile', $url);
        //             update_user_meta($uid, 'profileimg', $url);   
        //         break;
        //         default:
        //             $url = 'https://secure.gravatar.com/avatar/40b0a56c4c4f4c34fa808ebc62909043?s=96&d=wp_user_avatar&r=g';
        //             update_user_meta($uid, '_flyrise_user_profile', $url);
        //             update_user_meta($uid, 'profileimg', $url);   
        //     }
        // }
    }

    // $headers = '';
    // $message = "User: " . $user_id . "has changed their Email! Manually change it for them in ContentSnare!";
    // wp_mail( 'alexf@flyriseio', 'A User has changed their email', $message, $headers );
}
/*
*   End Add Everhour Client ID & Qwilr Proposal Link Fields to User Profile
*/


//add_action( 'show_user_profile', 'extra_user_profile_fields2' );
//add_action( 'edit_user_profile', 'extra_user_profile_fields2' );

/*
function extra_user_profile_fields2( $user ) { 
    $args = array( 
        'numberposts'		=> -1, // -1 is for all
        'post_type'		=> 'companies', // or 'post', 'page'
        'orderby' 		=> 'title', // or 'date', 'rand'
        'order' 		=> 'ASC', // or 'DESC'
        //'category' 		=> $category_id,
        //'exclude'		=> get_the_ID()
        // ...
        // http://codex.wordpress.org/Template_Tags/get_posts#Usage
      );
      
      // Get the posts
      $myposts = get_posts($args);
      $current_company = get_the_author_meta( 'assignedcompany', $user->ID );
    ?>
    <h3><?php _e("Assigned Company", "blank"); ?></h3>

    <table class="form-table">
    <tr>
        <th><label for="assignedcompany"><?php _e("Assigned Company"); ?></label></th>
        <td>
            <select name="assignedcompany" id="assignedcompany" class="regular-text">
            <?php foreach ($myposts as $mypost) { 
                    if ($mypost->ID == $current_company) { ?>  
                <option value="<?php echo $mypost->ID;?>" selected><?php echo $mypost->post_title; ?> </option>
                <?php }  else { ?>
                <option value="<?php echo $mypost->ID;?>"><?php echo $mypost->post_title; ?> </option>
                <?php } } ?>
            </select>
            <span class="description"><?php _e("Please select your Company."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="comprole"><?php _e("Company Role"); ?></label></th>
        <td>
            <select name="comprole" id="comprole" class="regular-text">
            <option value="viewonly"> View Only </option>
            <option value="billing"> Billing </option>
            <option value="admin"> Admin </option>
            </select>
            <span class="description"><?php _e("Please select your Company Role."); ?></span>
        </td>
    </tr>
    </table>
<?php
}
*/

add_action( 'personal_options_update', 'save_extra_user_profile_fields2' );
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields2' );

function save_extra_user_profile_fields2( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) ) { 
        return false; 
    }
    //update_user_meta( $user_id, 'assignedcompany', $_POST['assignedcompany'] );
    //update_user_meta( $user_id, 'comprole', $_POST['comprole'] );
    update_user_meta( $user_id, 'brand', $_POST['brand'] );
}

add_filter('woocommerce_checkout_fields', 'addBootstrapToCheckoutFields' );
function addBootstrapToCheckoutFields($fields) {
    foreach ($fields as &$fieldset) {
        foreach ($fieldset as &$field) {
            // if you want to add the form-group class around the label and the input
            $field['class'][] = 'form-group'; 

            // add form-control to the actual input
            $field['input_class'][] = 'form-control';
        }
    }
    return $fields;
}

add_action('check_admin_referer', 'logout_without_confirm', 10, 2);
function logout_without_confirm($action, $result)
{
    /**
     * Allow logout without confirmation
     */
    if ($action == "log-out" && !isset($_GET['_wpnonce'])) {
        $redirect_to = isset($_REQUEST['redirect_to']) ? $_REQUEST['redirect_to'] : 'login';
        //$location = str_replace('&amp;', '&', wp_logout_url($redirect_to));
        $location = "/login";
        header("Location: $location");
        die;
    }
}




/* Add 'Order Cost Report' to Woocommerce Reports -> Orders tab */
    require_once( get_template_directory() . '/woocommerce/custom-reports.php');


/* Alter Woocommerce Subscription Periods Filter */
    
    function alter_subscription_filters( $title ) {
        $subscription_periods = array("month"=>"month", "day"=>"day", "week"=>"week", "year"=>"year");
        return $subscription_periods;
    }
    add_filter( 'woocommerce_subscription_periods', 'alter_subscription_filters' );



/* Custom Expense Taxonomies should be radio rather than check boxes */

/**
 * Custom Expense Taxonomies radio inputs instead of checkboxes for specified taxonomies.
 *
 * @param   array   $args
 * @return  array
 */
function wpse_139269_term_radio_checklist( $args ) {
    if ( ! empty( $args['taxonomy'] ) && ($args['taxonomy'] === 'tier_1' || $args['taxonomy'] === 'sub_category' || $args['taxonomy'] === 'repeats') /* <== Change to required taxonomy */ ) {
        if ( empty( $args['walker'] ) || is_a( $args['walker'], 'Walker' ) ) { // Don't override 3rd party walkers.
            if ( ! class_exists( 'WPSE_139269_Walker_Category_Radio_Checklist' ) ) {
                /**
                 * Custom walker for switching checkbox inputs to radio.
                 *
                 * @see Walker_Category_Checklist
                 */
                class WPSE_139269_Walker_Category_Radio_Checklist extends Walker_Category_Checklist {
                    function walk( $elements, $max_depth, ...$args ) {
                        $output = parent::walk( $elements, $max_depth, ...$args );
                        $output = str_replace(
                            array( 'type="checkbox"', "type='checkbox'" ),
                            array( 'type="radio"', "type='radio'" ),
                            $output
                        );

                        return $output;
                    }
                }
            }

            $args['walker'] = new WPSE_139269_Walker_Category_Radio_Checklist;
        }
    }

    return $args;
}

add_filter( 'wp_terms_checklist_args', 'wpse_139269_term_radio_checklist' );



/**
 * @snippet       Get Sales by State & Month @ WooCommerce Admin
 * @author        Original: Rodolfo Melogli, Modified: Alex Frascona
 * @testedwith    WooCommerce 6
 */
 
// -----------------------
// 1. Create extra tab under Reports / Orders
 
add_filter( 'woocommerce_admin_reports', 'bbloomer_admin_add_report_orders_tab' );
 
function bbloomer_admin_add_report_orders_tab( $reports ) { 
 
    $array = array(
        'sales_by_state' => array(
            'title' => 'Sales by state',
            'description' => '',
            'hide_title' => 1,
            'callback' => 'bbloomer_yearly_sales_by_state'
        )
    );
    
    if(isset($reports['orders']))
    $reports['orders']['reports'] = array_merge($reports['orders']['reports'],$array);
    
    return $reports; 
}
 
// -----------------------
// 2. Calculate sales by state
 
function bbloomer_yearly_sales_by_state() {
 
    $year = (isset($_GET['year'])) ? $_GET['year']: date("Y"); // change this if needed
    
    $total_sales = 0;

    $us_state_abbrevs_names = array();

    $empty_orders = array();
 
    $args = [
        'post_type' => 'shop_order',
        'posts_per_page' => '-1',
        'year' => $year,
        'post_status' => ['completed', 'processing']
    ];
    $my_query = new WP_Query($args);
    $orders = $my_query->posts;
 
    foreach ($orders as $order => $value) {
        
        $order_id = $value->ID;
        $order = wc_get_order($order_id);
        $order_data = $order->get_data();
        
        $order_paid = $order->get_date_paid();
        $date_month = (int)date_format(date_create($order_paid), 'm');

        if( $order_data['billing']['state'] == null ){
            $empty_orders[] = $order_id;
            $order_data['billing']['state'] = 'No Billing State!';
        }

        if (array_key_exists($order_data['billing']['state'], $us_state_abbrevs_names)){
            $us_state_abbrevs_names[$order_data['billing']['state']]['total'] += $order->get_total();

            if (array_key_exists($date_month, $us_state_abbrevs_names[$order_data['billing']['state']])){
                $us_state_abbrevs_names[$order_data['billing']['state']][$date_month] += $order->get_total();
            }else{
                $us_state_abbrevs_names[$order_data['billing']['state']][$date_month] = $order->get_total();
            }
        }else{
            $us_state_abbrevs_names[] = $order_data['billing']['state'];
            $us_state_abbrevs_names[$order_data['billing']['state']]['total'] = $order->get_total();
            $us_state_abbrevs_names[$order_data['billing']['state']][$date_month] = $order->get_total();
        }
        
        $total_sales += $order->get_total();
 
    }

    if(!empty($empty_orders)){
    echo "<p>Fix these Orders without a Billing State!: ";
    foreach($empty_orders as $order_id){
        echo "<a target='_blank' href='/wp-admin/post.php?post=" . $order_id . "&action=edit'>" . $order_id . "</a> ";
    }
    echo "</p>";
    }

    // echo "<pre>";
    // print_r($us_state_abbrevs_names); //exit;
    // echo "</pre>";
    ?>
        <style>
            table#sales_by_state{
                font-size: 12px;
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                border-spacing: 0;
                width: 100%;
                display: table;
                border: 1px solid #ccc;
            }
            #sales_by_state td{
                padding: 6px;
            }
            #sales_by_state th {
                background-color: #6ba65d63;
                border-bottom: 1px solid #ccc;
            }
            #sales_by_state tr:nth-child(even){
                background-color: #E7E9EB;
            }
            #sales_by_state tr:nth-child(odd){
                background-color: #FFFFFF;
            }
            #sales_by_state tr:hover{
				background-color: #eddd807a;
			}
            .sales_final_row{
                border-top: 1px solid #ccc;
            }
        </style>

    <?php
        $years = array(2017, 2018, 2019, 2020, 2021, 2022, 2023);
    ?>
    <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
        <?php
            foreach($years as $single_year){
                $current_year = '';
                if($year == $single_year){
                    $current_year = 'selected';
                }
                echo '<option ' . $current_year . ' value="/wp-admin/admin.php?page=wc-reports&tab=orders&report=sales_by_state&year=' . $single_year . '">' . $single_year . '</option>';
            }
            
        ?>
    </select><br /><br />

    <?php

    //echo "<h3>Sales by State for " . $year . "</h3>";
    
    ?><table id='sales_by_state'><tr><th></th><th>Jan</th> <th>Feb</th><th>Mar</th><th>Apr</th> <th>May</th> <th>Jun</th> <th>Jul</th> <th>Aug</th> <th>Sep</th> <th>Oct</th> <th>Nov</th> <th>Dec</th> <th>Total</th> </tr><?php

    foreach($us_state_abbrevs_names as $abbreviation => $full){
        if(gettype($full) == 'array'){
            foreach ( $full as $key => $value ){
                if ( $value == "$0.00" || $value == 0 ){ unset($full[$key]); }
                //if ( $value == 0 ){ unset($full[$key]); }
                //if ( $value == null ){ unset($full[$key]); }
            }
            
            //echo "<tr><td>$abbreviation</td><td>" . wc_price(${$abbreviation}) . "</td></tr>";
            ?>
            <tr>
                <td><?php echo $abbreviation; ?></td>
                <td><?php echo (isset($full['1']))? wc_price($full['1']): ''; ?></td>
                <td><?php echo (isset($full['2']))? wc_price($full['2']): ''; ?></td>
                <td><?php echo (isset($full['3']))? wc_price($full['3']): ''; ?></td>
                <td><?php echo (isset($full['4']))? wc_price($full['4']): ''; ?></td>
                <td><?php echo (isset($full['5']))? wc_price($full['5']): ''; ?></td>
                <td><?php echo (isset($full['6']))? wc_price($full['6']): ''; ?></td>
                <td><?php echo (isset($full['7']))? wc_price($full['7']): ''; ?></td>
                <td><?php echo (isset($full['8']))? wc_price($full['8']): ''; ?></td>
                <td><?php echo (isset($full['9']))? wc_price($full['9']): ''; ?></td>
                <td><?php echo (isset($full['10']))? wc_price($full['10']): ''; ?></td>
                <td><?php echo (isset($full['11']))? wc_price($full['11']): ''; ?></td>
                <td><?php echo (isset($full['12']))? wc_price($full['12']): ''; ?></td>
                <td><?php echo wc_price($full['total']); ?></td>
            
            </tr>
            <?php
        }
    }

    ?>
        <tr class='sales_final_row'>
            <td>Total Sales:</td>
            <td></td><td></td><td></td><td></td><td></td><td></td> <td></td><td></td><td></td><td></td><td></td><td></td>
            <td><?php echo wc_price($total_sales); ?></td>
        </tr>
    <?php
}


/*
*   This is HUGE
*   without the htaccess Rule, retirement thereof required by wpEngine
*   We have to route every request to the Nuxt/VueJS App index.php!
*   Or rather, the WP theme wp-content/themes/midas-dashboard/index.php,
*   which pulls in and echos the Nuxt index.html, either this
*   deployed: /wp-content/themes/midas-dashboard/vue-dist/' . $filepath . '/index.html
*   or this
*   local: http://localhost:3000' . $filepath
*   The Latter is what carries the dynamic path, such as /onboard, and avoids the WP router
*/

/*
*	After Headless, this is less crucial.
*	This WP is only for checkout and admin now
*/
// Load custom template for web requests going to "/account" or "/account/<..>/..."
function load_captaincore_template( $original_template ) {
    global $wp;

    //echo gettype($wp->request);
    $request = explode( '/', $wp->request );
    //echo !isset($request[2]) . "<br />";
    //var_dump($request); exit();
  
    /*
    *   For these on Reload! we need it to use Vue!:
pages/settings/add-new-payment-method/
    */
    if( isset($request[2]) ){
        //echo $request[2];
    }else{
        $request[2] = "";
    }

    switch( $request ){

        case ($request[0] == "pages" && $request[1] == "settings" && $request[2] == ''):
		wp_redirect( '/' );
		exit;
		break;

        case $request[0] == "":
        case $request[0] == "login":
        case $request[0] == "pages" && $request[1] == "settings" && $request[2] == "orders":
        case $request[0] == "pages" && $request[1] == "settings" && $request[2] == "view-order":
        case $request[0] == "pages" && $request[1] == "settings" && $request[2] == "account":
        case $request[0] == "onboard":
        case $request[0] == "detail":
        case $request[0] == "approved":
        case $request[0] == "dashboard":
            return get_template_directory() . '/index.php';
            exit;
            break;
	
	case $request[0] == "cart":
	case $request[0] == "shop":
	case $request[0] == "wp-login":
        case $request[0] == "wp-admin":
        case $request[0] == "wp-content":
        case $request[0] == "wp-includes":
        case $request[0] == "checkout":
        case $request[0] == "payment":
        case $request[0] == "order-pay":
        case $request[0] == "account":
        case $request[0] == "payment" && $request[1] == "checkout":
        case $request[0] == "payment" && $request[1] == "checkout" && $request[2] == "payments-checkout":
        case $request[0] == "pages" && $request[1] == "settings" && $request[2] == "add-new-payment-method":
        case $request[0] == "pages" && $request[1] == "settings" && $request[2] == "delete-payment-method":
        case $request[0] == "payment" && $request[1] == "checkout" && $request[2] == "order-received":
        case $request[0] == "pages" && $request[1] == "settings" && $request[2] == "payment-methods":
        case $request[0] == "pages" && $request[1] == "settings" && $request[2] == '':
            return $original_template;
            exit;
            break;
	
	//case $request[0] == "cart":
	//case $request[0] == "shop":
	//	echo "shop deactivated";
	//	exit;
	//	break;
    }
}
add_filter( 'template_include', 'load_captaincore_template' );

// Remove all default WP template redirects/lookups
remove_action( 'template_redirect', 'redirect_canonical' );




//add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

function custom_override_checkout_fields( $fields ) {
    //$fields['order']['brand']['placeholder'] = "Ridgecap Marketing";
    $fields['order']['brand']['placeholder'] = $_GET['brand'];
    
    return $fields;
}


/*
*   Save the issued jwt token to the User Profile upon creation
*   to use it to auto login on checkout and payment methods woo pages
*/
function mod_jwt_auth_token_before_dispatch($data, $user, $switch) {
    //do_action('wp_login', $user->data->username, $user);
    //update_post_meta( $user->data->ID, 'jwt_token', 'testeryo' );
    //update_post_meta( $user->ID, 'jwt_token', 'testeryo' );

    if ( ! empty( $user->roles ) && is_array( $user->roles ) && in_array( 'administrator', $user->roles ) )
    {
        if( $switch != null ){

            $switch_user = get_user_by('login', $switch);
            if ($switch_user) {
                //$details['name'] = $user->first_name . ' ' . $user->last_name;
                //$details['email'] = $user->user_email;
                update_user_meta( $user->ID, 'jwt_token', '');
                update_user_meta( $switch_user->ID, 'jwt_token', $data['token']);
                $data['user_id'] = $switch_user->ID;
            };
            
        }
    }else{
        update_user_meta( $user->ID, 'jwt_token', $data['token']);
        $data['user_id'] = $user->ID;
    }
    return $data;
 }
 add_filter('jwt_auth_token_before_dispatch', 'mod_jwt_auth_token_before_dispatch', 10, 3);


/*
*   When the User is an Admin, change the User to Any other User
*/
function mod_jwt_auth_token_before_sign($token, $user, $switch) {
    $userObj = new WP_User( $user->data->ID );
    // write_log($user->roles);
    // write_log(is_array( $user->roles ));
    // write_log(!empty( $user->roles ));
    // write_log(in_array( 'administrator', $user->roles ));

    if ( ! empty( $user->roles ) && is_array( $user->roles ) && in_array( 'administrator', $user->roles ) )
    {
        //write_log('is Admin!');
        if( $switch != null ){

            $switch_user = get_user_by('login', $switch);
            if ($switch_user) {
                $token['data']['user']['id'] = $switch_user->ID;
            };
            //$token['data']['user']['id'] = $switch;
            //update_user_meta( $user->ID, 'jwt_token', $token);
        }
    }
    return $token;
}
add_filter('jwt_auth_token_before_sign', 'mod_jwt_auth_token_before_sign', 10, 3);


if ( ! function_exists('write_log')) {
   function write_log ( $log )  {
      if ( is_array( $log ) || is_object( $log ) ) {
         error_log( print_r( $log, true ) );
      } else {
         error_log( $log );
      }
   }
}


 // EMAIL TEMPLATE FUNCTIONALITY TO ACCESS ORDER OBJECT IN HEADER AND FOOTER
 
 add_action( 'woocommerce_email_header', 'email_header_before', 1, 2 );
 function email_header_before( $email_heading, $email ){
     $GLOBALS['email'] = $email;
 }

 // PDF INVOICE TEMPLATE FUNCTIONALITY FOR BRAND SWITCHING
  add_action( 'custom_invoice_logo', 'generate_custom_invoice_logo', 10, 2 );
  function generate_custom_invoice_logo($template_type, $order) {
      $user_id = $order->get_user_id();
      if ( !empty($user_id) ) {
          $meta_key = 'brand';
          $order_customer_brand = get_user_meta( $user_id, $meta_key, true );
         
         if (!empty($order_customer_brand)){
             if ($order_customer_brand == "ridgecapmarketing"){
                 echo '<img src="https://my.ridgecapmarketing.com/ridgecap-icon-40.png"/>';
             } else {
                 echo '<img src="https://launch.flyrise.io/wp-content/uploads/flyrise-icon-pink.png"/>';
             }
         } else {
             echo '<img src="https://launch.flyrise.io/wp-content/uploads/flyrise-icon-pink.png"/>';
         }
     }
  }

  add_action( 'custom_invoice_brand', 'generate_custom_invoice_brand', 10, 2 );
  function generate_custom_invoice_brand($template_type, $order) {
      $user_id = $order->get_user_id();
      if ( !empty($user_id) ) {
          $meta_key = 'brand';
          $order_customer_brand = get_user_meta( $user_id, $meta_key, true );
         
         if (!empty($order_customer_brand)){
             if ($order_customer_brand == "ridgecapmarketing"){
                 echo 'Ridgecap Marketing';
             } else {
                 echo 'Flyrise';
             }
         } else {
             echo 'Flyrise';
         }
     }
  }


//add_filter('pre_get_users', 'ow_change_avatar', 100, 1);
//add_filter('get_avatar_data', 'ow_change_avatar', 100, 2);
//add_filter('manage_users_custom_column', 'ow_change_avatar', 10, 3);
function ow_change_avatar($user) {

    
        if( $author_pic = get_user_meta( $user->ID, '_flyrise_user_profile', true ) ){
            if($author_pic && is_array($author_pic) && $author_pic[0] != 'https://launch.flyrise.io/wp-content/uploads/user-avatar-placeholder.png'){
                $args['url'] = $author_pic[0];

                update_user_meta($uid, '_flyrise_user_profile', $author_pic[0]);
                update_user_meta($uid, 'profileimg', $author_pic[0]);
                //$value = $author_pic[0];
            }
            
            if($author_pic && !is_array($author_pic) && $author_pic != 'https://launch.flyrise.io/wp-content/uploads/user-avatar-placeholder.png'){
                $args['url'] = $author_pic;

                update_user_meta($uid, '_flyrise_user_profile', $author_pic);
                update_user_meta($uid, 'profileimg', $author_pic);
                //$value = $author_pic;
            }
        }else{
            $brand = get_user_meta($user->ID, 'brand', true);
            //echo "brand: " . $brand . "<br />";
            //if($brand){
                switch($brand){
                    case 'flyrise':
                        $url = 'https://launch.flyrise.io/wp-content/uploads/flyrise.png';

                        update_user_meta($uid, '_flyrise_user_profile', $url);
                        update_user_meta($uid, 'profileimg', $url);
                        //$value = $url;
                    break;
                    case 'ridgecapmarketing':
                        $url = 'https://launch.flyrise.io/wp-content/uploads/android-chrome-192x192-1-30x30.png';

                        update_user_meta($uid, '_flyrise_user_profile', $url);
                        update_user_meta($uid, 'profileimg', $url);
                        //$value = $url;
                    break;
                    // default:
                    //     $args['url'] = 'https://secure.gravatar.com/avatar/40b0a56c4c4f4c34fa808ebc62909043?s=96&d=wp_user_avatar&r=g';
                }
        }
    
    return $user;

    // if(is_object($user_data)){
    //     $user_id = $user_data->user_id;
    // } else{
    //     $user_id = $user_data;  
    // }

    // if($user_id){
    //     $author_pic = get_user_meta($user_id, '_flyrise_user_profile', true);

        
        //echo $author_pic . "<br />";
        // if($author_pic){
        //     if(is_array($author_pic)){
        //         $args['url'] = $author_pic[0];
        //     }else{
        //         $args['url'] = $author_pic;
        //     }
            
        // if (!$author_pic) {
        //     $brand = get_user_meta($user_id, 'brand', true);
            //echo "brand: " . $brand . "<br />";
            //if($brand){
                // switch($brand){
                //     case 'flyrise':
                //         $url = 'https://launch.flyrise.io/wp-content/uploads/flyrise.png';

                //         update_user_meta($uid, '_flyrise_user_profile', $url);
                //         update_user_meta($uid, 'profileimg', $url);
                //     break;
                //     case 'ridgecapmarketing':
                //         $url = 'https://launch.flyrise.io/wp-content/uploads/android-chrome-192x192-1-30x30.png';

                //         update_user_meta($uid, '_flyrise_user_profile', $url);
                //         update_user_meta($uid, 'profileimg', $url);
                //     break;
                //     // default:
                //     //     $args['url'] = 'https://secure.gravatar.com/avatar/40b0a56c4c4f4c34fa808ebc62909043?s=96&d=wp_user_avatar&r=g';
                // }
            //}
    //     }
    // } else {
    //     //$args['url'] = 'https://secure.gravatar.com/avatar/40b0a56c4c4f4c34fa808ebc62909043?s=96&d=wp_user_avatar&r=g';
    // }
    // return $user_data;
} 


//add_filter( 'woocommerce_enqueue_styles', '__return_false' );

    
remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );



/*
*   Exterding the guest cart, user cart should already persist
*/
// For Example set when the session is about to expire
add_filter( 'wc_session_expiring', 'woocommerce_cart_session_about_to_expire', 100, 1 ); 
function woocommerce_cart_session_about_to_expire( $session_time ) {

  // return 99 hours 1 hours before expiry time (100 hours). default is 48
  return 60 * 60 * 718; //24 hours x 30 days - a couple hours for the about to expire time

}

// extend session will expire.
add_filter( 'wc_session_expiration', 'woocommerce_cart_session_expires', 100, 1); 
function woocommerce_cart_session_expires( $session_time ) {

  // Return seconds of 100 hours.
  return 60 * 60 * 720; 

}
/*
*   end: Exterding the guest cart, user cart should already persist
*/


add_action( 'rest_api_init', function () {
    add_action( 'rest_pre_serve_request', function () {
        header( 'Access-Control-Allow-Headers: Authorization, Content-Type, X-WP-Wpml-Language', true );
        header("Access-Control-Allow-Origin: *");
    } );
}, 15 );

add_action( 'rest_api_init', function()
{
    header( "Access-Control-Allow-Origin: *" );
} );