<?php wp_head(); ?>

	<main role="main" style="height: calc(100vh - 100px); display: flex; flex-flow: row wrap; justify-content: center; align-content: center;">
		<div class="checkout-header" style="margin-bottom: 2rem; display: flex; width: 100%; justify-content: center; align-self: flex-start">
			<img src="/wp-content/themes/midas-dashboard/assets/images/flyrise-icon-gradient.svg" class="checkout-logo" style="height: 40px; width: 40px; margin-bottom: 0;">
		</div>
		<!-- <hr style="width: 100%; align-self: flex-start;"> -->
		<!-- section -->
		<section>

			<!-- article -->
			<article id="post-404" style="text-align: center;">

				
				<h1 class="card-preheader large" style="line-height: 1; color: #2F2D66 !important">Page Not Found</h1>
				<h6 class="mb-4">Error 404</h6>
				<a href="/dashboard" class="btn btn-sm">Return Home?</a>

			</article>
			<!-- /article -->

		</section>
		<!-- /section -->
	</main>



<?php get_footer(); ?>
